import pymysql
import pymysql.cursors
import re
import os
import shutil
import urlparse
import sys
reload(sys)
sys.setdefaultencoding( "utf-8" )
    
class Data(object):    

    def __init__(self):    
        self.connect = pymysql.connect(host='localhost',
                                             user='root',
                                             password='123456',
                                             db='wpblog_cn',
                                             charset='utf8',
                                             )
        									 
    def make_data(self):    
        if os.path.exists("sql.sql"):
            os.remove("sql.sql")
        if os.path.exists("images"):
            os.system("rm -rf images")
        os.mkdir("images")
        #    os.system("touch sql.sql")

        cursor = self.connect.cursor()
        sql = "select ID from wp_posts where post_content like '%<img%'"
        #sql = "select ID from wp_posts where post_content like '%<img%' limit 10"
        try:
           cursor.execute(sql)
           result = cursor.fetchall()
           #print result
           dic = {}
           for i in result:
           
              
                 ID = i[0]
                 sql2 = "select slug from wp_terms where term_id=(select term_taxonomy_id from wp_term_relationships where object_id=%d limit 1)" % ID
                 #try:
                 cursor.execute(sql2)
                 kind = cursor.fetchall()
                 print "ID= " + str(ID)
                 #print kind
                  
                 sql3 = "select post_content from wp_posts where ID=%d" % ID
                 cursor.execute(sql3)
                 result3 = cursor.fetchall()
                 result3 = re.findall(r"<img.*?src=\"(.*?)\"",result3[0][0])
                 print result3
        
                 for j in result3:
                     name = j.split('/')[-1]
                     new_url = '/images/' + str(kind[0][0]) + '/' + str(name)
                     #print new_url
                     if str(name) not in dic:
                         dic[str(name)] = new_url
                     else:
                         new_url = dic[str(name)]    
                     sql4 = "update wp_posts set post_content=replace(post_content,\'src=\"%s\"\',\'src=\"%s\"\') where ID=%d;update wp_posts set post_content=replace(post_content,\'src=\\'%s\\'\',\'src=\"%s\"\') where ID=%d;" % (j,new_url,ID,j,new_url,ID)
                     print sql4
                     try:
                         with open("sql.sql","a+") as f:
                             f.writelines(sql4)
                             f.writelines("\n")
                     except:
                         print "error"
                     #cmd = "echo \'" + str(sql4) + "\' >> sql.sql"
                     #os.system(cmd)
                     
                     tmp = urlparse.urlparse(j).path
                     print tmp
                     print "new url %s" % new_url
                     tmp3 = new_url.rfind('/')
                     tmp2 = new_url[1:tmp3]
                     print tmp2
                     
                     if not os.path.exists(tmp2):
                         os.mkdir(tmp2)
                     
                     #cmd2 = "\"cp " + "runoob_spider-master/file" + str(tmp) + " images/" + str(kind[0][0]) + "\""
                     #print cmd2
                     #    os.system(cmd2)
                     source = "runoob_spider-master/file" + str(tmp)
                     target = new_url[1:]
                     print target
                     try:
                         shutil.copy(source, target)
                     except IOError as e:
                         print("Unable to copy file. %s" % e)
                     except:
                         print("Unexpected error:", sys.exc_info())
                 
                 print "   " 
        except:
           pass
        f.close()
        cursor.close()
        self.connect.close()


    def test_data(self):
        cursor = self.connect.cursor()
        sql = "select ID from wp_posts where post_content like '%<img%'"
        dic = []
        try:
           cursor.execute(sql)
           result = cursor.fetchall()
           #print result
           
           for i in result:
                 ID = i[0]
                 #print "ID= " + str(ID)
                 sql2 = "select post_content from wp_posts where ID=%d" % ID
                 cursor.execute(sql2)
                 result2 = cursor.fetchall()
                 result2 = re.findall(r"<img.*?src=\"(.*?)\"",result2[0][0])
                 #print result2
                 for j in result2:
                     #print j[1:]
                     try:
                         a = os.path.exists(j[1:])
                         if a != 1:
                             dic.append(ID)
                     except:
                         dic.append(ID)
                     
        except:
            pass
        
        for k in dic:
            SQL = "select post_title from wp_posts where ID=%d" % k
            cursor.execute(SQL)
            Result = cursor.fetchall()
            p = "ID:" + str(k) + " title:" + str(Result[0][0])
            print p
            try:
                with open("error.txt","a+") as f:
                    f.writelines(p)
                    f.writelines("\n")
            except:
                print "error"
        f.close()
        cursor.close()
        self.connect.close()

    def make_class_data(self):    
        #if os.path.exists("change_image_class.sql"):
        #    os.remove("change_image_class.sql")
        if os.path.exists("change_tab_class.sql"):
            os.remove("change_tab_class.sql")

        cursor = self.connect.cursor()
        #sql = "select ID from wp_posts where post_content like '%<img%'"
        sql = "select ID from wp_posts where post_content like '%<table%'"
        #sql = "select ID from wp_posts where post_content like '%<img%' limit 10"
        try:
           cursor.execute(sql)
           result = cursor.fetchall()
           #print result
           dic = {}
           for i in result:
           
              
                 ID = i[0]
                  
                 sql2 = "select post_content from wp_posts where ID=%d" % ID
                 cursor.execute(sql2)
                 result2 = cursor.fetchall()
                 #result2 = re.findall(r"<table[^>]*/>",result2[0][0])
                 result2 = re.findall(r"<table[^>]*>",result2[0][0])
                 print ID
                 print result2
                 if result2:
                     
                     for j in result2:
                         #z=re.findall(r'class=\'.*?\'',j)
                         q=re.sub(r'<table  ','<table ',j);
                         q=re.sub(r'  class=".*?"','',q);
                         q=re.sub(r' class=".*?"','',q);
                         q=re.sub(r' >','>',q);
                         q=re.sub(r' >','>',q);
                         q=re.sub(r' >','>',q);
                         q=re.sub(r' >','>',q);
                         q=re.sub(r' >','>',q);
                         q=re.sub(r' >','>',q);
                         q=re.sub(r' >','>',q);
                         q=re.sub(r'>',' class="w3-table-all notranslate">',q);
                         print q
                               
                         sql3 = "update wp_posts set post_content=replace(post_content,'%s','%s') where ID=%d;" % (j,q,ID)
                         print sql3
                         try:
                             with open("change_tab_class.sql","a+") as f:
                                 f.writelines(sql3)
                                 f.writelines("\n")
                         except:
                             print "error"
                     
                          
                 print "   " 
        except:
           pass
        
        f.close()
        cursor.close()
        self.connect.close()
                 
    def make_div_data(self):    
        #if os.path.exists("change_image_class.sql"):
        #    os.remove("change_image_class.sql")
        if os.path.exists("change_div_class.sql"):
            os.remove("change_div_class.sql")

        cursor = self.connect.cursor()
        #sql = "select ID from wp_posts where post_content like '%<img%'"
        sql = "select ID from wp_posts where post_content like '%<table%'"
        #sql = "select ID from wp_posts where post_content like '%<img%' limit 10"
        try:
           cursor.execute(sql)
           result = cursor.fetchall()
           #print result
           dic = {}
           for i in result:
           
              
                 ID = i[0]
                  
                 sql2 = "select post_content from wp_posts where ID=%d" % ID
                 cursor.execute(sql2)
                 result2 = cursor.fetchall()
                 #result2 = re.findall(r"<table[^>]*/>",result2[0][0])
                 result2 = re.findall(r"<table.*?>[\s\S]*?<\/table>",result2[0][0])
                 
                 print ID
                 #print result2
                 
                 if result2:
                     
                     for j in result2:
                         if re.findall(r"<pre",j):
                             
                             q=re.sub(r'<table','<div class="w3-responsive"><table',j);
                             q=re.sub(r'</table>','</table></div>',q);
                             print q
                          
                             sql3 = "update wp_posts set post_content=replace(post_content,'%s','%s') where ID=%d;" % (pymysql.escape_string(j),pymysql.escape_string(q),ID)
                             print sql3
                             try:
                                 with open("change_div_class.sql","a+") as f:
                                     f.writelines(sql3)
                                     f.writelines("\n")
                             except:
                                 print "error"
                                        
                 print "   " 
        except:
           pass
        
        f.close()
        cursor.close()
        self.connect.close()

a=Data()
#a.test_data()
#a.make_data()
#a.make_class_data()
a.make_div_data()


