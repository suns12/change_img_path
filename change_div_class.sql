update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:25%">Property</th>
<th>Description</th>
</tr>
<tr>
<td>checkValidity()</td>
<td>如果 input 元素中的数据是合法的返回 true，否则返回 false。</td>
</tr>
<tr>
<td>setCustomValidity()</td>
<td><p>设置 input 元素的 validationMessage 属性，用于自定义错误提示信息的方法。</p><p>
使用 setCustomValidity 设置了自定义提示后，validity.customError 就会变成true，则 checkValidity 总是会返回false。如果要重新判断需要取消自定义提示，方式如下：</p>
<pre>
setCustomValidity('') 
setCustomValidity(null) 
setCustomValidity(undefined)
</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:25%">Property</th>
<th>Description</th>
</tr>
<tr>
<td>checkValidity()</td>
<td>如果 input 元素中的数据是合法的返回 true，否则返回 false。</td>
</tr>
<tr>
<td>setCustomValidity()</td>
<td><p>设置 input 元素的 validationMessage 属性，用于自定义错误提示信息的方法。</p><p>
使用 setCustomValidity 设置了自定义提示后，validity.customError 就会变成true，则 checkValidity 总是会返回false。如果要重新判断需要取消自定义提示，方式如下：</p>
<pre>
setCustomValidity('') 
setCustomValidity(null) 
setCustomValidity(undefined)
</pre>
</td>
</tr>
</tbody></table></div>') where ID=201;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="30%">描述</th>
	<th width="70%">代码</th>
</tr>
<tr>
	<td>debug：进行调试模式（表单不提交）。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate
({
&nbsp;&nbsp;&nbsp;&nbsp;debug:true
})
</pre>
	</td>
</tr>
<tr>
	<td>把调试设置为默认。</td>
    <td>
<pre style="white-space: pre-wrap;">
$.validator.setDefaults({
&nbsp;&nbsp;&nbsp;&nbsp;debug:true
})
</pre>
	</td>
</tr>
<tr>
	<td>submitHandler：通过验证后运行的函数，里面要加上表单提交的函数，否则表单不会提交。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
&nbsp;&nbsp;&nbsp;&nbsp;submitHandler:function(form) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(form).ajaxSubmit();
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>ignore：对某些元素不进行验证。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({
&nbsp;&nbsp;&nbsp;&nbsp;ignore:".ignore"
})
</pre>
	</td>
</tr>
<tr>
	<td>rules：自定义规则，key:value 的形式，key 是要验证的元素，value 可以是字符串或对象。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
&nbsp;&nbsp;&nbsp;&nbsp;rules:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name:"required",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;required:true,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:true
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>messages：自定义的提示信息，key:value 的形式，key 是要验证的元素，value 可以是字符串或函数。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
&nbsp;&nbsp;&nbsp;&nbsp;rules:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name:"required",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;required:true,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:true
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;messages:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name:"Name不能为空",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:{       
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;required:"E-mail不能为空",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:"E-mail地址不正确"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>

<tr>
	<td>groups：对一组元素的验证，用一个错误提示，用 errorPlacement 控制把出错信息放在哪里。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({
&nbsp;&nbsp;&nbsp;&nbsp;groups:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;username:"fname 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lname"
&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;errorPlacement:function(error,element) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (element.attr("name") == "fname" || element.attr("name") == "lname")   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error.insertAfter("#lastname");
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;else    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error.insertAfter(element);
&nbsp;&nbsp;&nbsp;&nbsp;},
   debug:true
})
</pre>
	</td>
</tr>
<tr>
	<td>OnSubmit：类型 Boolean，默认 true，指定是否提交时验证。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({  
&nbsp;&nbsp;&nbsp;&nbsp;onsubmit:false
})
</pre>
	</td>
</tr>
<tr>
	<td>onfocusout：类型 Boolean，默认 true，指定是否在获取焦点时验证。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({   
&nbsp;&nbsp;&nbsp;&nbsp;onfocusout:false
})
</pre>
	</td>
</tr>
<tr>
	<td>onkeyup：类型 Boolean，默认 true，指定是否在敲击键盘时验证。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   onkeyup:false
})
</pre>
	</td>
</tr>
<tr>
	<td>onclick：类型 Boolean，默认 true，指定是否在鼠标点击时验证（一般验证 checkbox、radiobox）。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   onclick:false
})
</pre>
	</td>
</tr>
<tr>
	<td>focusInvalid：类型 Boolean，默认 true。提交表单后，未通过验证的表单（第一个或提交之前获得焦点的未通过验证的表单）会获得焦点。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   focusInvalid:false
})
</pre>
	</td>
</tr>
<tr>
	<td>focusCleanup：类型 Boolean，默认 false。当未通过验证的元素获得焦点时，移除错误提示（避免和 focusInvalid 一起使用）。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   focusCleanup:true
})
</pre>
	</td>
</tr>
<tr>
	<td>errorClass：类型 String，默认 "error"。指定错误提示的 css 类名，可以自定义错误提示的样式。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({ 
&nbsp;&nbsp;&nbsp;&nbsp;errorClass:"invalid"
})
</pre>
	</td>
</tr>
<tr>
	<td>errorElement：类型 String，默认 "label"。指定使用什么标签标记错误。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate
   errorElement:"em"
})
</pre>
	</td>
</tr>
<tr>
	<td>wrapper：类型 String，指定使用什么标签再把上边的 errorELement 包起来。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   wrapper:"li"
})
</pre>
	</td>
</tr>
<tr>
	<td>errorLabelContainer：类型 Selector，把错误信息统一放在一个容器里面。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({   
&nbsp;&nbsp;&nbsp;&nbsp;errorLabelContainer:"#messageBox",
&nbsp;&nbsp;&nbsp;&nbsp;wrapper:"li",
&nbsp;&nbsp;&nbsp;&nbsp;submitHandler:function() { 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert("Submitted!") 
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>showErrors：跟一个函数，可以显示总共有多少个未通过验证的元素。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({  
&nbsp;&nbsp;&nbsp;&nbsp;showErrors:function(errorMap,errorList) {
        $("#summary").html("Your form contains " + this.numberOfInvalids() + " errors,see details below.");
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;this.defaultShowErrors();
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>errorPlacement：跟一个函数，可以自定义错误放到哪里。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({  
&nbsp;&nbsp;&nbsp;&nbsp;errorPlacement:function(error,element) {  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error.appendTo(element.parent("td").next("td"));
   },
   debug:true
})
</pre>
	</td>
</tr>
<tr>
	<td>success：要验证的元素通过验证后的动作，如果跟一个字符串，会当作一个 css 类，也可跟一个函数。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({        
&nbsp;&nbsp;&nbsp;&nbsp;success:"valid",
        submitHandler:function() { 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert("Submitted!") 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>highlight：可以给未通过验证的元素加效果、闪烁等。</td>
    <td></td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="30%">描述</th>
	<th width="70%">代码</th>
</tr>
<tr>
	<td>debug：进行调试模式（表单不提交）。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate
({
&nbsp;&nbsp;&nbsp;&nbsp;debug:true
})
</pre>
	</td>
</tr>
<tr>
	<td>把调试设置为默认。</td>
    <td>
<pre style="white-space: pre-wrap;">
$.validator.setDefaults({
&nbsp;&nbsp;&nbsp;&nbsp;debug:true
})
</pre>
	</td>
</tr>
<tr>
	<td>submitHandler：通过验证后运行的函数，里面要加上表单提交的函数，否则表单不会提交。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
&nbsp;&nbsp;&nbsp;&nbsp;submitHandler:function(form) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(form).ajaxSubmit();
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>ignore：对某些元素不进行验证。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({
&nbsp;&nbsp;&nbsp;&nbsp;ignore:".ignore"
})
</pre>
	</td>
</tr>
<tr>
	<td>rules：自定义规则，key:value 的形式，key 是要验证的元素，value 可以是字符串或对象。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
&nbsp;&nbsp;&nbsp;&nbsp;rules:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name:"required",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;required:true,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:true
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>messages：自定义的提示信息，key:value 的形式，key 是要验证的元素，value 可以是字符串或函数。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
&nbsp;&nbsp;&nbsp;&nbsp;rules:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name:"required",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;required:true,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:true
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;messages:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name:"Name不能为空",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:{       
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;required:"E-mail不能为空",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email:"E-mail地址不正确"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>

<tr>
	<td>groups：对一组元素的验证，用一个错误提示，用 errorPlacement 控制把出错信息放在哪里。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({
&nbsp;&nbsp;&nbsp;&nbsp;groups:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;username:"fname 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lname"
&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;errorPlacement:function(error,element) {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (element.attr("name") == "fname" || element.attr("name") == "lname")   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error.insertAfter("#lastname");
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;else    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error.insertAfter(element);
&nbsp;&nbsp;&nbsp;&nbsp;},
   debug:true
})
</pre>
	</td>
</tr>
<tr>
	<td>OnSubmit：类型 Boolean，默认 true，指定是否提交时验证。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({  
&nbsp;&nbsp;&nbsp;&nbsp;onsubmit:false
})
</pre>
	</td>
</tr>
<tr>
	<td>onfocusout：类型 Boolean，默认 true，指定是否在获取焦点时验证。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({   
&nbsp;&nbsp;&nbsp;&nbsp;onfocusout:false
})
</pre>
	</td>
</tr>
<tr>
	<td>onkeyup：类型 Boolean，默认 true，指定是否在敲击键盘时验证。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   onkeyup:false
})
</pre>
	</td>
</tr>
<tr>
	<td>onclick：类型 Boolean，默认 true，指定是否在鼠标点击时验证（一般验证 checkbox、radiobox）。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   onclick:false
})
</pre>
	</td>
</tr>
<tr>
	<td>focusInvalid：类型 Boolean，默认 true。提交表单后，未通过验证的表单（第一个或提交之前获得焦点的未通过验证的表单）会获得焦点。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   focusInvalid:false
})
</pre>
	</td>
</tr>
<tr>
	<td>focusCleanup：类型 Boolean，默认 false。当未通过验证的元素获得焦点时，移除错误提示（避免和 focusInvalid 一起使用）。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   focusCleanup:true
})
</pre>
	</td>
</tr>
<tr>
	<td>errorClass：类型 String，默认 "error"。指定错误提示的 css 类名，可以自定义错误提示的样式。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({ 
&nbsp;&nbsp;&nbsp;&nbsp;errorClass:"invalid"
})
</pre>
	</td>
</tr>
<tr>
	<td>errorElement：类型 String，默认 "label"。指定使用什么标签标记错误。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate
   errorElement:"em"
})
</pre>
	</td>
</tr>
<tr>
	<td>wrapper：类型 String，指定使用什么标签再把上边的 errorELement 包起来。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({
   wrapper:"li"
})
</pre>
	</td>
</tr>
<tr>
	<td>errorLabelContainer：类型 Selector，把错误信息统一放在一个容器里面。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({   
&nbsp;&nbsp;&nbsp;&nbsp;errorLabelContainer:"#messageBox",
&nbsp;&nbsp;&nbsp;&nbsp;wrapper:"li",
&nbsp;&nbsp;&nbsp;&nbsp;submitHandler:function() { 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert("Submitted!") 
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>showErrors：跟一个函数，可以显示总共有多少个未通过验证的元素。</td>
    <td>
<pre style="white-space: pre-wrap;">
$(".selector").validate({  
&nbsp;&nbsp;&nbsp;&nbsp;showErrors:function(errorMap,errorList) {
        $("#summary").html("Your form contains " + this.numberOfInvalids() + " errors,see details below.");
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;this.defaultShowErrors();
&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>errorPlacement：跟一个函数，可以自定义错误放到哪里。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({  
&nbsp;&nbsp;&nbsp;&nbsp;errorPlacement:function(error,element) {  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error.appendTo(element.parent("td").next("td"));
   },
   debug:true
})
</pre>
	</td>
</tr>
<tr>
	<td>success：要验证的元素通过验证后的动作，如果跟一个字符串，会当作一个 css 类，也可跟一个函数。</td>
    <td>
<pre style="white-space: pre-wrap;">
$("#myform").validate({        
&nbsp;&nbsp;&nbsp;&nbsp;success:"valid",
        submitHandler:function() { 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert("Submitted!") 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
})
</pre>
	</td>
</tr>
<tr>
	<td>highlight：可以给未通过验证的元素加效果、闪烁等。</td>
    <td></td>
</tr>
</table></div>') where ID=489;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tr>
<th width="20%">参数</th>
    <th width="80%">描述</th>
  </tr>
<tr>
<td><em>name</em></td>
    <td>必需。规定常量的名称。</td>
  </tr>
<tr>
<td><em>value</em></td>
    <td>必需。规定常量的值。PHP7 支持数组，实例如下：
<pre>
&lt;?php
// PHP7+ 支持
define('ANIMALS', [
    'dog',
    'cat',
    'bird'
]);

echo ANIMALS[1]; // 输出 "cat"
?&gt;
</pre>
</td>
  </tr>
<tr>
<td><em>case_insensitive</em></td>
    <td>可选。规定常量的名称是否对大小写敏感。可能值：<ul>
<li>TRUE - 大小写不敏感</li>
	<li>FALSE - 默认。大小写敏感</li>
</ul>
</td>
  </tr>
</table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tr>
<th width="20%">参数</th>
    <th width="80%">描述</th>
  </tr>
<tr>
<td><em>name</em></td>
    <td>必需。规定常量的名称。</td>
  </tr>
<tr>
<td><em>value</em></td>
    <td>必需。规定常量的值。PHP7 支持数组，实例如下：
<pre>
&lt;?php
// PHP7+ 支持
define('ANIMALS', [
    'dog',
    'cat',
    'bird'
]);

echo ANIMALS[1]; // 输出 "cat"
?&gt;
</pre>
</td>
  </tr>
<tr>
<td><em>case_insensitive</em></td>
    <td>可选。规定常量的名称是否对大小写敏感。可能值：<ul>
<li>TRUE - 大小写不敏感</li>
	<li>FALSE - 默认。大小写敏感</li>
</ul>
</td>
  </tr>
</table></div>') where ID=1033;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tr><td>

PDO::PARAM_BOOL (integer)</td><td>
表示布尔数据类型。</td></tr><tr><td>
PDO::PARAM_NULL (integer)</td><td>
表示 SQL 中的 NULL 数据类型。</td></tr><tr><td>
PDO::PARAM_INT (integer)</td><td>
表示 SQL 中的整型。</td></tr><tr><td>
PDO::PARAM_STR (integer)</td><td>
表示 SQL 中的 CHAR、VARCHAR 或其他字符串类型。</td></tr><tr><td>
PDO::PARAM_LOB (integer)</td><td>
表示 SQL 中大对象数据类型。</td></tr><tr><td>
PDO::PARAM_STMT (integer)</td><td>
表示一个记录集类型。当前尚未被任何驱动支持。</td></tr><tr><td>
PDO::PARAM_INPUT_OUTPUT (integer)</td><td>
指定参数为一个存储过程的 INOUT 参数。必须用一个明确的 PDO::PARAM_* 数据类型跟此值进行按位或。</td></tr><tr><td>
PDO::FETCH_LAZY (integer)</td><td>
指定获取方式，将结果集中的每一行作为一个对象返回，此对象的变量名对应着列名。PDO::FETCH_LAZY 创建用来访问的对象变量名。在 PDOStatement::fetchAll() 中无效。</td></tr><tr><td>
PDO::FETCH_ASSOC (integer)</td><td>
指定获取方式，将对应结果集中的每一行作为一个由列名索引的数组返回。如果结果集中包含多个名称相同的列，则PDO::FETCH_ASSOC每个列名只返回一个值。</td></tr><tr><td>
PDO::FETCH_NAMED (integer)</td><td>
指定获取方式，将对应结果集中的每一行作为一个由列名索引的数组返回。如果结果集中包含多个名称相同的列，则PDO::FETCH_ASSOC每个列名 返回一个包含值的数组。</td></tr><tr><td>
PDO::FETCH_NUM (integer)</td><td>
指定获取方式，将对应结果集中的每一行作为一个由列号索引的数组返回，从第 0 列开始。</td></tr><tr><td>
PDO::FETCH_BOTH (integer)</td><td>
指定获取方式，将对应结果集中的每一行作为一个由列号和列名索引的数组返回，从第 0 列开始。</td></tr><tr><td>
PDO::FETCH_OBJ (integer)</td><td>
指定获取方式，将结果集中的每一行作为一个属性名对应列名的对象返回。</td></tr><tr><td>
PDO::FETCH_BOUND (integer)</td><td>
指定获取方式，返回 TRUE 且将结果集中的列值分配给通过 PDOStatement::bindParam() 或 PDOStatement::bindColumn() 方法绑定的 PHP 变量。</td></tr><tr><td>
PDO::FETCH_COLUMN (integer)</td><td>
指定获取方式，从结果集中的下一行返回所需要的那一列。</td></tr><tr><td>
PDO::FETCH_CLASS (integer)</td><td>
指定获取方式，返回一个所请求类的新实例，映射列到类中对应的属性名。<br>
<p>注意: 如果所请求的类中不存在该属性，则调用 __set() 魔术方法</p></td></tr><tr><td>
PDO::FETCH_INTO (integer)</td><td>
指定获取方式，更新一个请求类的现有实例，映射列到类中对应的属性名。</td></tr><tr><td>
PDO::FETCH_FUNC (integer)</td><td>
允许在运行中完全用自定义的方式处理数据。（仅在 PDOStatement::fetchAll() 中有效）。</td></tr><tr><td>
PDO::FETCH_GROUP (integer)</td><td>
根据值分组返回。通常和 PDO::FETCH_COLUMN 或 PDO::FETCH_KEY_PAIR 一起使用。</td></tr><tr><td>
PDO::FETCH_UNIQUE (integer)</td><td>
只取唯一值。</td></tr><tr><td>
PDO::FETCH_KEY_PAIR (integer)</td><td>
获取一个有两列的结果集到一个数组，其中第一列为键名，第二列为值。自 PHP 5.2.3 起可用。</td></tr><tr><td>
PDO::FETCH_CLASSTYPE (integer)</td><td>
根据第一列的值确定类名。</td></tr><tr><td>
PDO::FETCH_SERIALIZE (integer)</td><td>
类似 PDO::FETCH_INTO ，但是以一个序列化的字符串表示对象。自 PHP 5.1.0 起可用。从 PHP 5.3.0 开始，如果设置此标志，则类的构造函数从不会被调用。</td></tr><tr><td>
PDO::FETCH_PROPS_LATE (integer)</td><td>
设置属性前调用构造函数。自 PHP 5.2.0 起可用。</td></tr><tr><td>
PDO::ATTR_AUTOCOMMIT (integer)</td><td>
如果此值为 FALSE ，PDO 将试图禁用自动提交以便数据库连接开始一个事务。</td></tr><tr><td>
PDO::ATTR_PREFETCH (integer)</td><td>
设置预取大小来为你的应用平衡速度和内存使用。并非所有的数据库/驱动组合都支持设置预取大小。较大的预取大小导致性能提高的同时也会占用更多的内存。</td></tr><tr><td>
PDO::ATTR_TIMEOUT (integer)</td><td>
设置连接数据库的超时秒数。</td></tr><tr><td>
PDO::ATTR_ERRMODE (integer)</td><td>
关于此属性的更多信息请参见 错误及错误处理 部分。</td></tr><tr><td>
PDO::ATTR_SERVER_VERSION (integer)</td><td>
此为只读属性；返回 PDO 所连接的数据库服务的版本信息。</td></tr><tr><td>
PDO::ATTR_CLIENT_VERSION (integer)</td><td>
此为只读属性；返回 PDO 驱动所用客户端库的版本信息。</td></tr><tr><td>
PDO::ATTR_SERVER_INFO (integer)</td><td>
此为只读属性。返回一些关于 PDO 所连接的数据库服务的元信息。</td></tr><tr><td>
PDO::ATTR_CONNECTION_STATUS (integer)</td><td>
</td></tr><tr><td>
PDO::ATTR_CASE (integer)</td><td>
用类似 PDO::CASE_* 的常量强制列名为指定的大小写。</td></tr><tr><td>
PDO::ATTR_CURSOR_NAME (integer)</td><td>
获取或设置使用游标的名称。当使用可滚动游标和定位更新时候非常有用。</td></tr><tr><td>
PDO::ATTR_CURSOR (integer)</td><td>
选择游标类型。 PDO 当前支持 PDO::CURSOR_FWDONLY 和 PDO::CURSOR_SCROLL。一般为 PDO::CURSOR_FWDONLY，除非确实需要一个可滚动游标。</td></tr><tr><td>
PDO::ATTR_DRIVER_NAME (string)</td><td>
返回驱动名称。
<p>
使用 PDO::ATTR_DRIVER_NAME 的例子：
</p>
<pre>
&lt;?php
if ($db-&gt;getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') {
  echo "Running on mysql; doing something mysql specific here\n";
}
?&gt;
</pre></td></tr><tr><td>
PDO::ATTR_ORACLE_NULLS (integer)</td><td>
在获取数据时将空字符串转换成 SQL 中的 NULL 。</td></tr><tr><td>
PDO::ATTR_PERSISTENT (integer)</td><td>
请求一个持久连接，而非创建一个新连接。关于此属性的更多信息请参见 连接与连接管理 。</td></tr><tr><td>
PDO::ATTR_STATEMENT_CLASS (integer)</td><td>
</td></tr><tr><td>
PDO::ATTR_FETCH_CATALOG_NAMES (integer)</td><td>
将包含的目录名添加到结果集中的每个列名前面。目录名和列名由一个小数点分开（.）。此属性在驱动层面支持，所以有些驱动可能不支持此属性。</td></tr><tr><td>
PDO::ATTR_FETCH_TABLE_NAMES (integer)</td><td>
将包含的表名添加到结果集中的每个列名前面。表名和列名由一个小数点分开（.）。此属性在驱动层面支持，所以有些驱动可能不支持此属性。</td></tr><tr><td>
PDO::ATTR_STRINGIFY_FETCHES (integer)</td><td>
</td></tr><tr><td>
PDO::ATTR_MAX_COLUMN_LEN (integer)</td><td>
</td></tr><tr><td>
PDO::ATTR_DEFAULT_FETCH_MODE (integer)</td><td>
自 PHP 5.2.0 起可用。</td></tr><tr><td>
PDO::ATTR_EMULATE_PREPARES (integer)</td><td>
自 PHP 5.1.3 起可用。</td></tr><tr><td>
PDO::ERRMODE_SILENT (integer)</td><td>
如果发生错误，则不显示错误或异常。希望开发人员显式地检查错误。此为默认模式。关于此属性的更多信息请参见 错误与错误处理 。</td></tr><tr><td>
PDO::ERRMODE_WARNING (integer)</td><td>
如果发生错误，则显示一个 PHP E_WARNING 消息。关于此属性的更多信息请参见 错误与错误处理。</td></tr><tr><td>
PDO::ERRMODE_EXCEPTION (integer)</td><td>
如果发生错误，则抛出一个 PDOException 异常。关于此属性的更多信息请参见 错误与错误处理。</td></tr><tr><td>
PDO::CASE_NATURAL (integer)</td><td>
保留数据库驱动返回的列名。</td></tr><tr><td>
PDO::CASE_LOWER (integer)</td><td>
强制列名小写。</td></tr><tr><td>
PDO::CASE_UPPER (integer)</td><td>
强制列名大写。</td></tr><tr><td>
PDO::NULL_NATURAL (integer)</td><td>
</td></tr><tr><td>
PDO::NULL_EMPTY_STRING (integer)</td><td>
</td></tr><tr><td>
PDO::NULL_TO_STRING (integer)</td><td>
</td></tr><tr><td>
PDO::FETCH_ORI_NEXT (integer)</td><td>
在结果集中获取下一行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_PRIOR (integer)</td><td>
在结果集中获取上一行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_FIRST (integer)</td><td>
在结果集中获取第一行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_LAST (integer)</td><td>
在结果集中获取最后一行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_ABS (integer)</td><td>
根据行号从结果集中获取需要的行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_REL (integer)</td><td>
根据当前游标位置的相对位置从结果集中获取需要的行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::CURSOR_FWDONLY (integer)</td><td>
创建一个只进游标的 PDOStatement 对象。此为默认的游标选项，因为此游标最快且是 PHP 中最常用的数据访问模式。</td></tr><tr><td>
PDO::CURSOR_SCROLL (integer)</td><td>
创建一个可滚动游标的 PDOStatement 对象。通过 PDO::FETCH_ORI_* 常量来控制结果集中获取的行。</td></tr><tr><td>
PDO::ERR_NONE (string)</td><td>
对应 SQLSTATE '00000'，表示 SQL 语句没有错误或警告地成功发出。当用 PDO::errorCode() 或 PDOStatement::errorCode() 来确定是否有错误发生时，此常量非常方便。在检查上述方法返回的错误状态代码时，会经常用到。</td></tr><tr><td>
PDO::PARAM_EVT_ALLOC (integer)</td><td>
分配事件</td></tr><tr><td>
PDO::PARAM_EVT_FREE (integer)</td><td>
解除分配事件</td></tr><tr><td>
PDO::PARAM_EVT_EXEC_PRE (integer)</td><td>
执行一条预处理语句之前触发事件。</td></tr><tr><td>
PDO::PARAM_EVT_EXEC_POST (integer)</td><td>
执行一条预处理语句之后触发事件。</td></tr><tr><td>
PDO::PARAM_EVT_FETCH_PRE (integer)</td><td>
从一个结果集中取出一条结果之前触发事件。</td></tr><tr><td>
PDO::PARAM_EVT_FETCH_POST (integer)</td><td>
从一个结果集中取出一条结果之后触发事件。</td></tr><tr><td>
PDO::PARAM_EVT_NORMALIZE (integer)</td><td>
在绑定参数注册允许驱动程序正常化变量名时触发事件。</td></tr></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tr><td>

PDO::PARAM_BOOL (integer)</td><td>
表示布尔数据类型。</td></tr><tr><td>
PDO::PARAM_NULL (integer)</td><td>
表示 SQL 中的 NULL 数据类型。</td></tr><tr><td>
PDO::PARAM_INT (integer)</td><td>
表示 SQL 中的整型。</td></tr><tr><td>
PDO::PARAM_STR (integer)</td><td>
表示 SQL 中的 CHAR、VARCHAR 或其他字符串类型。</td></tr><tr><td>
PDO::PARAM_LOB (integer)</td><td>
表示 SQL 中大对象数据类型。</td></tr><tr><td>
PDO::PARAM_STMT (integer)</td><td>
表示一个记录集类型。当前尚未被任何驱动支持。</td></tr><tr><td>
PDO::PARAM_INPUT_OUTPUT (integer)</td><td>
指定参数为一个存储过程的 INOUT 参数。必须用一个明确的 PDO::PARAM_* 数据类型跟此值进行按位或。</td></tr><tr><td>
PDO::FETCH_LAZY (integer)</td><td>
指定获取方式，将结果集中的每一行作为一个对象返回，此对象的变量名对应着列名。PDO::FETCH_LAZY 创建用来访问的对象变量名。在 PDOStatement::fetchAll() 中无效。</td></tr><tr><td>
PDO::FETCH_ASSOC (integer)</td><td>
指定获取方式，将对应结果集中的每一行作为一个由列名索引的数组返回。如果结果集中包含多个名称相同的列，则PDO::FETCH_ASSOC每个列名只返回一个值。</td></tr><tr><td>
PDO::FETCH_NAMED (integer)</td><td>
指定获取方式，将对应结果集中的每一行作为一个由列名索引的数组返回。如果结果集中包含多个名称相同的列，则PDO::FETCH_ASSOC每个列名 返回一个包含值的数组。</td></tr><tr><td>
PDO::FETCH_NUM (integer)</td><td>
指定获取方式，将对应结果集中的每一行作为一个由列号索引的数组返回，从第 0 列开始。</td></tr><tr><td>
PDO::FETCH_BOTH (integer)</td><td>
指定获取方式，将对应结果集中的每一行作为一个由列号和列名索引的数组返回，从第 0 列开始。</td></tr><tr><td>
PDO::FETCH_OBJ (integer)</td><td>
指定获取方式，将结果集中的每一行作为一个属性名对应列名的对象返回。</td></tr><tr><td>
PDO::FETCH_BOUND (integer)</td><td>
指定获取方式，返回 TRUE 且将结果集中的列值分配给通过 PDOStatement::bindParam() 或 PDOStatement::bindColumn() 方法绑定的 PHP 变量。</td></tr><tr><td>
PDO::FETCH_COLUMN (integer)</td><td>
指定获取方式，从结果集中的下一行返回所需要的那一列。</td></tr><tr><td>
PDO::FETCH_CLASS (integer)</td><td>
指定获取方式，返回一个所请求类的新实例，映射列到类中对应的属性名。<br>
<p>注意: 如果所请求的类中不存在该属性，则调用 __set() 魔术方法</p></td></tr><tr><td>
PDO::FETCH_INTO (integer)</td><td>
指定获取方式，更新一个请求类的现有实例，映射列到类中对应的属性名。</td></tr><tr><td>
PDO::FETCH_FUNC (integer)</td><td>
允许在运行中完全用自定义的方式处理数据。（仅在 PDOStatement::fetchAll() 中有效）。</td></tr><tr><td>
PDO::FETCH_GROUP (integer)</td><td>
根据值分组返回。通常和 PDO::FETCH_COLUMN 或 PDO::FETCH_KEY_PAIR 一起使用。</td></tr><tr><td>
PDO::FETCH_UNIQUE (integer)</td><td>
只取唯一值。</td></tr><tr><td>
PDO::FETCH_KEY_PAIR (integer)</td><td>
获取一个有两列的结果集到一个数组，其中第一列为键名，第二列为值。自 PHP 5.2.3 起可用。</td></tr><tr><td>
PDO::FETCH_CLASSTYPE (integer)</td><td>
根据第一列的值确定类名。</td></tr><tr><td>
PDO::FETCH_SERIALIZE (integer)</td><td>
类似 PDO::FETCH_INTO ，但是以一个序列化的字符串表示对象。自 PHP 5.1.0 起可用。从 PHP 5.3.0 开始，如果设置此标志，则类的构造函数从不会被调用。</td></tr><tr><td>
PDO::FETCH_PROPS_LATE (integer)</td><td>
设置属性前调用构造函数。自 PHP 5.2.0 起可用。</td></tr><tr><td>
PDO::ATTR_AUTOCOMMIT (integer)</td><td>
如果此值为 FALSE ，PDO 将试图禁用自动提交以便数据库连接开始一个事务。</td></tr><tr><td>
PDO::ATTR_PREFETCH (integer)</td><td>
设置预取大小来为你的应用平衡速度和内存使用。并非所有的数据库/驱动组合都支持设置预取大小。较大的预取大小导致性能提高的同时也会占用更多的内存。</td></tr><tr><td>
PDO::ATTR_TIMEOUT (integer)</td><td>
设置连接数据库的超时秒数。</td></tr><tr><td>
PDO::ATTR_ERRMODE (integer)</td><td>
关于此属性的更多信息请参见 错误及错误处理 部分。</td></tr><tr><td>
PDO::ATTR_SERVER_VERSION (integer)</td><td>
此为只读属性；返回 PDO 所连接的数据库服务的版本信息。</td></tr><tr><td>
PDO::ATTR_CLIENT_VERSION (integer)</td><td>
此为只读属性；返回 PDO 驱动所用客户端库的版本信息。</td></tr><tr><td>
PDO::ATTR_SERVER_INFO (integer)</td><td>
此为只读属性。返回一些关于 PDO 所连接的数据库服务的元信息。</td></tr><tr><td>
PDO::ATTR_CONNECTION_STATUS (integer)</td><td>
</td></tr><tr><td>
PDO::ATTR_CASE (integer)</td><td>
用类似 PDO::CASE_* 的常量强制列名为指定的大小写。</td></tr><tr><td>
PDO::ATTR_CURSOR_NAME (integer)</td><td>
获取或设置使用游标的名称。当使用可滚动游标和定位更新时候非常有用。</td></tr><tr><td>
PDO::ATTR_CURSOR (integer)</td><td>
选择游标类型。 PDO 当前支持 PDO::CURSOR_FWDONLY 和 PDO::CURSOR_SCROLL。一般为 PDO::CURSOR_FWDONLY，除非确实需要一个可滚动游标。</td></tr><tr><td>
PDO::ATTR_DRIVER_NAME (string)</td><td>
返回驱动名称。
<p>
使用 PDO::ATTR_DRIVER_NAME 的例子：
</p>
<pre>
&lt;?php
if ($db-&gt;getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') {
  echo "Running on mysql; doing something mysql specific here\n";
}
?&gt;
</pre></td></tr><tr><td>
PDO::ATTR_ORACLE_NULLS (integer)</td><td>
在获取数据时将空字符串转换成 SQL 中的 NULL 。</td></tr><tr><td>
PDO::ATTR_PERSISTENT (integer)</td><td>
请求一个持久连接，而非创建一个新连接。关于此属性的更多信息请参见 连接与连接管理 。</td></tr><tr><td>
PDO::ATTR_STATEMENT_CLASS (integer)</td><td>
</td></tr><tr><td>
PDO::ATTR_FETCH_CATALOG_NAMES (integer)</td><td>
将包含的目录名添加到结果集中的每个列名前面。目录名和列名由一个小数点分开（.）。此属性在驱动层面支持，所以有些驱动可能不支持此属性。</td></tr><tr><td>
PDO::ATTR_FETCH_TABLE_NAMES (integer)</td><td>
将包含的表名添加到结果集中的每个列名前面。表名和列名由一个小数点分开（.）。此属性在驱动层面支持，所以有些驱动可能不支持此属性。</td></tr><tr><td>
PDO::ATTR_STRINGIFY_FETCHES (integer)</td><td>
</td></tr><tr><td>
PDO::ATTR_MAX_COLUMN_LEN (integer)</td><td>
</td></tr><tr><td>
PDO::ATTR_DEFAULT_FETCH_MODE (integer)</td><td>
自 PHP 5.2.0 起可用。</td></tr><tr><td>
PDO::ATTR_EMULATE_PREPARES (integer)</td><td>
自 PHP 5.1.3 起可用。</td></tr><tr><td>
PDO::ERRMODE_SILENT (integer)</td><td>
如果发生错误，则不显示错误或异常。希望开发人员显式地检查错误。此为默认模式。关于此属性的更多信息请参见 错误与错误处理 。</td></tr><tr><td>
PDO::ERRMODE_WARNING (integer)</td><td>
如果发生错误，则显示一个 PHP E_WARNING 消息。关于此属性的更多信息请参见 错误与错误处理。</td></tr><tr><td>
PDO::ERRMODE_EXCEPTION (integer)</td><td>
如果发生错误，则抛出一个 PDOException 异常。关于此属性的更多信息请参见 错误与错误处理。</td></tr><tr><td>
PDO::CASE_NATURAL (integer)</td><td>
保留数据库驱动返回的列名。</td></tr><tr><td>
PDO::CASE_LOWER (integer)</td><td>
强制列名小写。</td></tr><tr><td>
PDO::CASE_UPPER (integer)</td><td>
强制列名大写。</td></tr><tr><td>
PDO::NULL_NATURAL (integer)</td><td>
</td></tr><tr><td>
PDO::NULL_EMPTY_STRING (integer)</td><td>
</td></tr><tr><td>
PDO::NULL_TO_STRING (integer)</td><td>
</td></tr><tr><td>
PDO::FETCH_ORI_NEXT (integer)</td><td>
在结果集中获取下一行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_PRIOR (integer)</td><td>
在结果集中获取上一行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_FIRST (integer)</td><td>
在结果集中获取第一行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_LAST (integer)</td><td>
在结果集中获取最后一行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_ABS (integer)</td><td>
根据行号从结果集中获取需要的行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::FETCH_ORI_REL (integer)</td><td>
根据当前游标位置的相对位置从结果集中获取需要的行。仅对可滚动游标有效。</td></tr><tr><td>
PDO::CURSOR_FWDONLY (integer)</td><td>
创建一个只进游标的 PDOStatement 对象。此为默认的游标选项，因为此游标最快且是 PHP 中最常用的数据访问模式。</td></tr><tr><td>
PDO::CURSOR_SCROLL (integer)</td><td>
创建一个可滚动游标的 PDOStatement 对象。通过 PDO::FETCH_ORI_* 常量来控制结果集中获取的行。</td></tr><tr><td>
PDO::ERR_NONE (string)</td><td>
对应 SQLSTATE '00000'，表示 SQL 语句没有错误或警告地成功发出。当用 PDO::errorCode() 或 PDOStatement::errorCode() 来确定是否有错误发生时，此常量非常方便。在检查上述方法返回的错误状态代码时，会经常用到。</td></tr><tr><td>
PDO::PARAM_EVT_ALLOC (integer)</td><td>
分配事件</td></tr><tr><td>
PDO::PARAM_EVT_FREE (integer)</td><td>
解除分配事件</td></tr><tr><td>
PDO::PARAM_EVT_EXEC_PRE (integer)</td><td>
执行一条预处理语句之前触发事件。</td></tr><tr><td>
PDO::PARAM_EVT_EXEC_POST (integer)</td><td>
执行一条预处理语句之后触发事件。</td></tr><tr><td>
PDO::PARAM_EVT_FETCH_PRE (integer)</td><td>
从一个结果集中取出一条结果之前触发事件。</td></tr><tr><td>
PDO::PARAM_EVT_FETCH_POST (integer)</td><td>
从一个结果集中取出一条结果之后触发事件。</td></tr><tr><td>
PDO::PARAM_EVT_NORMALIZE (integer)</td><td>
在绑定参数注册允许驱动程序正常化变量名时触发事件。</td></tr></table></div>') where ID=1298;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tr>
<th>属性</th>
<th>描述</th>
</tr>

<tr>
<td>Count </td>
<td>
<p>返回 fields 集合中的项目数。起始值为 0。</p>
<p>例子：</p>
<pre>countfields = rec.Fields.Count</pre>
</td>
</tr>

<tr>
<td>Item(named_item/number)</td>
<td>
<p>返回 fields 集合中的某个指定的项目。</p>
<p>例子：</p>
<pre>itemfields = rec.Fields.Item(1)
或者
itemfields = rec.Fields.Item(&amp;quot;Name&amp;quot;)
</pre>
</td>
</tr>
</table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tr>
<th>属性</th>
<th>描述</th>
</tr>

<tr>
<td>Count </td>
<td>
<p>返回 fields 集合中的项目数。起始值为 0。</p>
<p>例子：</p>
<pre>countfields = rec.Fields.Count</pre>
</td>
</tr>

<tr>
<td>Item(named_item/number)</td>
<td>
<p>返回 fields 集合中的某个指定的项目。</p>
<p>例子：</p>
<pre>itemfields = rec.Fields.Item(1)
或者
itemfields = rec.Fields.Item(&amp;quot;Name&amp;quot;)
</pre>
</td>
</tr>
</table></div>') where ID=1724;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tr>
<th>属性</th>
<th>描述</th>
</tr>

<tr>
<td>Count</td>
<td>
<p>返回 fields 集合中项目的数目。以 0 起始。</p>
<p>例子：</p>
<pre>countfields = rs.Fields.Count</pre>
</td>
</tr>

<tr>
<td>Item(named_item/number)</td>
<td>
<p>返回 fields 集合中的某个指定的项目。</p>
<p>例子：</p>
<pre>itemfields = rs.Fields.Item(1)
或者&nbsp;&nbsp;&nbsp;&nbsp;
itemfields = rs.Fields.Item(&amp;quot;Name&amp;quot;)
</pre>
</td>
</tr>
</table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tr>
<th>属性</th>
<th>描述</th>
</tr>

<tr>
<td>Count</td>
<td>
<p>返回 fields 集合中项目的数目。以 0 起始。</p>
<p>例子：</p>
<pre>countfields = rs.Fields.Count</pre>
</td>
</tr>

<tr>
<td>Item(named_item/number)</td>
<td>
<p>返回 fields 集合中的某个指定的项目。</p>
<p>例子：</p>
<pre>itemfields = rs.Fields.Item(1)
或者&nbsp;&nbsp;&nbsp;&nbsp;
itemfields = rs.Fields.Item(&amp;quot;Name&amp;quot;)
</pre>
</td>
</tr>
</table></div>') where ID=1725;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tr>
<th>属性</th>
<th>描述</th>
</tr>

<tr>
<td>Count</td>
<td>
<p>返回 properties 集合中项目的数目。以 0 起始。</p>
<p>例子：</p>
<pre>countprop = rs.Properties.Count</pre>
</td>
</tr>

<tr>
<td>Item(named_item/number)</td>
<td>
<p>返回 properties 集合中某个指定的项目。</p>
<p>例子：</p>
<pre>
itemprop = rs.Properties.Item(1)
或者
itemprop = rs.Properties.Item(&amp;quot;Name&amp;quot;)
</pre>
</td>
</tr>
</table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tr>
<th>属性</th>
<th>描述</th>
</tr>

<tr>
<td>Count</td>
<td>
<p>返回 properties 集合中项目的数目。以 0 起始。</p>
<p>例子：</p>
<pre>countprop = rs.Properties.Count</pre>
</td>
</tr>

<tr>
<td>Item(named_item/number)</td>
<td>
<p>返回 properties 集合中某个指定的项目。</p>
<p>例子：</p>
<pre>
itemprop = rs.Properties.Item(1)
或者
itemprop = rs.Properties.Item(&amp;quot;Name&amp;quot;)
</pre>
</td>
</tr>
</table></div>') where ID=1725;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tr>
<td>
HTML:
</td>
<td><pre>Hello           Tove</pre>
</td>
</tr>
<tr>
<td>
输出结果:
</td>
<td>Hello Tove</td>
</tr>
</table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tr>
<td>
HTML:
</td>
<td><pre>Hello           Tove</pre>
</td>
</tr>
<tr>
<td>
输出结果:
</td>
<td>Hello Tove</td>
</tr>
</table></div>') where ID=2134;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference"> <tbody><tr> <th style="width:20%">Filter</th> <th style="width:68%">描述</th> </tr> <tr> <td>none</td> <td>默认值，没有效果。</td> </tr> <tr> <td>blur(<em>px</em>)</td> <td>给图像设置高斯模糊。"radius"一值设定高斯函数的标准差，或者是屏幕上以多少像素融在一起， 所以值越大越模糊；<br><br>如果没有设定值，则默认是0；这个参数可设置css长度值，但不接受百分比值。</td> </tr> <tr> <td>brightness(<em>%</em>)</td> <td>给图片应用一种线性乘法，使其看起来更亮或更暗。如果值是0%，图像会全黑。值是100%，则图像无变化。其他的值对应线性乘数效果。值超过100%也是可以的，图像会比原来更亮。如果没有设定值，默认是1。</td> </tr> <tr> <td>contrast(<em>%</em>)</td> <td>调整图像的对比度。值是0%的话，图像会全黑。值是100%，图像不变。值可以超过100%，意味着会运用更低的对比。若没有设置值，默认是1。</td> </tr> <tr> <td>drop-shadow(<em>h-shadow v-shadow blur spread color</em>)</td> <td><p>给图像设置一个阴影效果。阴影是合成在图像下面，可以有模糊度的，可以以特定颜色画出的遮罩图的偏移版本。 函数接受&lt;shadow&gt;(在CSS3背景中定义)类型的值，除了"inset"关键字是不允许的。该函数与已有的box-shadow box-shadow属性很相似；不同之处在于，通过滤镜，一些浏览器为了更好的性能会提供硬件加速。<code style="font-style: normal;line-height: 1.5">&lt;shadow&gt;参数如下：</code></p><code style="font-style: normal;line-height: 1.5"> <dl> <dt><strong>&lt;offset-x&gt;</strong> <strong>&lt;offset-y&gt;</strong> <small>(必须)</small></dt> <dd>这是设置阴影偏移量的两个 &lt;length&gt;值. <strong>&lt;offset-x&gt;</strong>&nbsp;设定水平方向距离. 负值会使阴影出现在元素左边. <strong>&lt;offset-y&gt;</strong>设定垂直距离.负值会使阴影出现在元素上方。查看<strong>&lt;length&gt;</strong>可能的单位.<br> <strong><font face="Open Sans, sans-serif">如果两个值都是</font>0</strong>, 则阴影出现在元素正后面 (如果设置了<span style="line-height: 1.5">&nbsp;</span><code style="font-style: normal;line-height: 1.5">&lt;blur-radius&gt;<span style="line-height: 1.5">&nbsp;and/or&nbsp;</span><code style="font-style: normal;line-height: 1.5">&lt;spread-radius&gt;，<span style="line-height: 1.5">会有模糊效果</span><span style="line-height: 1.5">).</span></code></code></dd><code style="font-style: normal;line-height: 1.5"><code style="font-style: normal;line-height: 1.5"> <dt><strong>&lt;blur-radius&gt;</strong> <small>(可选)</small></dt> <dd>这是第三个code&gt;&lt;length&gt;值. 值越大，越模糊，则阴影会变得更大更淡.不允许负值 若未设定，默认是0&nbsp;(则阴影的边界很锐利).</dd> <dt><strong>&lt;spread-radius&gt;</strong> <small>(可选)</small></dt> <dd>这是第四个 &lt;length&gt;值. 正值会使阴影扩张和变大，负值会是阴影缩小.若未设定，默认是0&nbsp;(阴影会与元素一样大小).&nbsp;<br> 注意: Webkit, 以及一些其他浏览器 不支持第四个长度，如果加了也不会渲染。</dd> <dt>&nbsp;</dt> <dt><strong>&lt;color&gt;</strong> <small>(可选)</small></dt> <dd>查看 &lt;color&gt;该值可能的关键字和标记。若未设定，颜色值基于浏览器。在Gecko&nbsp;(Firefox), Presto (Opera)和Trident (Internet Explorer)中， 会应用color<strong>color</strong>属性的值。另外, 如果颜色值省略，WebKit中阴影是透明的。</dd> </code></code></dl><code style="font-style: normal;line-height: 1.5"><code style="font-style: normal;line-height: 1.5"> </code></code></code></td> </tr> <tr> <td>grayscale(<em>%</em>)</td> <td><p>将图像转换为灰度图像。值定义转换的比例。值为100%则完全转为灰度图像，值为0%图像无变化。值在0%到100%之间，则是效果的线性乘子。若未设置，值默认是0；</p></td> </tr> <tr> <td>hue-rotate(<em>deg</em>)</td> <td><p>给图像应用色相旋转。"angle"一值设定图像会被调整的色环角度值。值为0deg，则图像无变化。若值未设置，默认值是0deg。该值虽然没有最大值，超过360deg的值相当于又绕一圈。</p></td> </tr> <tr> <td>invert(<em>%</em>)</td> <td><p>反转输入图像。值定义转换的比例。100%的价值是完全反转。值为0%则图像无变化。值在0%和100%之间，则是效果的线性乘子。 若值未设置，值默认是0。</p></td> </tr> <tr> <td>opacity(<em>%</em>)</td> <td><p>转化图像的透明程度。值定义转换的比例。值为0%则是完全透明，值为100%则图像无变化。值在0%和100%之间，则是效果的线性乘子，也相当于图像样本乘以数量。 若值未设置，值默认是1。该函数与已有的opacity属性很相似，不同之处在于通过filter，一些浏览器为了提升性能会提供硬件加速。</p></td> </tr> <tr> <td>saturate(<em>%</em>)</td> <td><p>转换图像饱和度。值定义转换的比例。值为0%则是完全不饱和，值为100%则图像无变化。其他值，则是效果的线性乘子。超过100%的值是允许的，则有更高的饱和度。 若值未设置，值默认是1。</p></td> </tr> <tr> <td>sepia(<em>%</em>)</td> <td><p>将图像转换为深褐色。值定义转换的比例。值为100%则完全是深褐色的，值为0%图像无变化。值在0%到100%之间，则是效果的线性乘子。若未设置，值默认是0；</p></td> </tr> <tr> <td>url()</td> <td><p>URL函数接受一个XML文件，该文件设置了 一个SVG滤镜，且可以包含一个锚点来指定一个具体的滤镜元素。</p> <p>例如：</p> <pre>filter: url(svg-url#element-id)</pre></td> </tr> <tr> <td>initial</td> <td><p>设置属性为默认值，可参阅： <a href="css-initial.html">CSS initial 关键字</a></p></td> </tr> <tr> <td>inherit</td> <td>从父元素继承该属性，可参阅：<a href="css-inherit.html">CSS inherit 关键字</a></td> </tr> </tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference"> <tbody><tr> <th style="width:20%">Filter</th> <th style="width:68%">描述</th> </tr> <tr> <td>none</td> <td>默认值，没有效果。</td> </tr> <tr> <td>blur(<em>px</em>)</td> <td>给图像设置高斯模糊。"radius"一值设定高斯函数的标准差，或者是屏幕上以多少像素融在一起， 所以值越大越模糊；<br><br>如果没有设定值，则默认是0；这个参数可设置css长度值，但不接受百分比值。</td> </tr> <tr> <td>brightness(<em>%</em>)</td> <td>给图片应用一种线性乘法，使其看起来更亮或更暗。如果值是0%，图像会全黑。值是100%，则图像无变化。其他的值对应线性乘数效果。值超过100%也是可以的，图像会比原来更亮。如果没有设定值，默认是1。</td> </tr> <tr> <td>contrast(<em>%</em>)</td> <td>调整图像的对比度。值是0%的话，图像会全黑。值是100%，图像不变。值可以超过100%，意味着会运用更低的对比。若没有设置值，默认是1。</td> </tr> <tr> <td>drop-shadow(<em>h-shadow v-shadow blur spread color</em>)</td> <td><p>给图像设置一个阴影效果。阴影是合成在图像下面，可以有模糊度的，可以以特定颜色画出的遮罩图的偏移版本。 函数接受&lt;shadow&gt;(在CSS3背景中定义)类型的值，除了"inset"关键字是不允许的。该函数与已有的box-shadow box-shadow属性很相似；不同之处在于，通过滤镜，一些浏览器为了更好的性能会提供硬件加速。<code style="font-style: normal;line-height: 1.5">&lt;shadow&gt;参数如下：</code></p><code style="font-style: normal;line-height: 1.5"> <dl> <dt><strong>&lt;offset-x&gt;</strong> <strong>&lt;offset-y&gt;</strong> <small>(必须)</small></dt> <dd>这是设置阴影偏移量的两个 &lt;length&gt;值. <strong>&lt;offset-x&gt;</strong>&nbsp;设定水平方向距离. 负值会使阴影出现在元素左边. <strong>&lt;offset-y&gt;</strong>设定垂直距离.负值会使阴影出现在元素上方。查看<strong>&lt;length&gt;</strong>可能的单位.<br> <strong><font face="Open Sans, sans-serif">如果两个值都是</font>0</strong>, 则阴影出现在元素正后面 (如果设置了<span style="line-height: 1.5">&nbsp;</span><code style="font-style: normal;line-height: 1.5">&lt;blur-radius&gt;<span style="line-height: 1.5">&nbsp;and/or&nbsp;</span><code style="font-style: normal;line-height: 1.5">&lt;spread-radius&gt;，<span style="line-height: 1.5">会有模糊效果</span><span style="line-height: 1.5">).</span></code></code></dd><code style="font-style: normal;line-height: 1.5"><code style="font-style: normal;line-height: 1.5"> <dt><strong>&lt;blur-radius&gt;</strong> <small>(可选)</small></dt> <dd>这是第三个code&gt;&lt;length&gt;值. 值越大，越模糊，则阴影会变得更大更淡.不允许负值 若未设定，默认是0&nbsp;(则阴影的边界很锐利).</dd> <dt><strong>&lt;spread-radius&gt;</strong> <small>(可选)</small></dt> <dd>这是第四个 &lt;length&gt;值. 正值会使阴影扩张和变大，负值会是阴影缩小.若未设定，默认是0&nbsp;(阴影会与元素一样大小).&nbsp;<br> 注意: Webkit, 以及一些其他浏览器 不支持第四个长度，如果加了也不会渲染。</dd> <dt>&nbsp;</dt> <dt><strong>&lt;color&gt;</strong> <small>(可选)</small></dt> <dd>查看 &lt;color&gt;该值可能的关键字和标记。若未设定，颜色值基于浏览器。在Gecko&nbsp;(Firefox), Presto (Opera)和Trident (Internet Explorer)中， 会应用color<strong>color</strong>属性的值。另外, 如果颜色值省略，WebKit中阴影是透明的。</dd> </code></code></dl><code style="font-style: normal;line-height: 1.5"><code style="font-style: normal;line-height: 1.5"> </code></code></code></td> </tr> <tr> <td>grayscale(<em>%</em>)</td> <td><p>将图像转换为灰度图像。值定义转换的比例。值为100%则完全转为灰度图像，值为0%图像无变化。值在0%到100%之间，则是效果的线性乘子。若未设置，值默认是0；</p></td> </tr> <tr> <td>hue-rotate(<em>deg</em>)</td> <td><p>给图像应用色相旋转。"angle"一值设定图像会被调整的色环角度值。值为0deg，则图像无变化。若值未设置，默认值是0deg。该值虽然没有最大值，超过360deg的值相当于又绕一圈。</p></td> </tr> <tr> <td>invert(<em>%</em>)</td> <td><p>反转输入图像。值定义转换的比例。100%的价值是完全反转。值为0%则图像无变化。值在0%和100%之间，则是效果的线性乘子。 若值未设置，值默认是0。</p></td> </tr> <tr> <td>opacity(<em>%</em>)</td> <td><p>转化图像的透明程度。值定义转换的比例。值为0%则是完全透明，值为100%则图像无变化。值在0%和100%之间，则是效果的线性乘子，也相当于图像样本乘以数量。 若值未设置，值默认是1。该函数与已有的opacity属性很相似，不同之处在于通过filter，一些浏览器为了提升性能会提供硬件加速。</p></td> </tr> <tr> <td>saturate(<em>%</em>)</td> <td><p>转换图像饱和度。值定义转换的比例。值为0%则是完全不饱和，值为100%则图像无变化。其他值，则是效果的线性乘子。超过100%的值是允许的，则有更高的饱和度。 若值未设置，值默认是1。</p></td> </tr> <tr> <td>sepia(<em>%</em>)</td> <td><p>将图像转换为深褐色。值定义转换的比例。值为100%则完全是深褐色的，值为0%图像无变化。值在0%到100%之间，则是效果的线性乘子。若未设置，值默认是0；</p></td> </tr> <tr> <td>url()</td> <td><p>URL函数接受一个XML文件，该文件设置了 一个SVG滤镜，且可以包含一个锚点来指定一个具体的滤镜元素。</p> <p>例如：</p> <pre>filter: url(svg-url#element-id)</pre></td> </tr> <tr> <td>initial</td> <td><p>设置属性为默认值，可参阅： <a href="css-initial.html">CSS initial 关键字</a></p></td> </tr> <tr> <td>inherit</td> <td>从父元素继承该属性，可参阅：<a href="css-inherit.html">CSS inherit 关键字</a></td> </tr> </tbody></table></div>') where ID=3604;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
  <tbody><tr>
  <th style="width:20%">值</th>
    <th>描述</th>
  </tr>
  <tr>
    <td>application-name</td>
    <td>指定 web 应用名称</td>
  </tr>
  <tr>
    <td>author</td>
    <td><p>定义文档的作者。</p>
 <p>实例:</p>
  <pre>&lt;meta name="author" content="盘古学院"&gt;</pre></td>
  </tr>
  <tr>
    <td>description</td>
    <td><p>指定页面描述。搜索索引擎可以使用这个描述作为搜索结果。</p>
 <p>实例:</p>
 <pre>&lt;meta name="description" content="免费编程入门教程"&gt;</pre></td>
  </tr>
  <tr>
    <td>generator</td>
    <td><p>指定文档生成的工具名称。</p>
   <p>实例:</p>
  <pre>&lt;meta name="generator" content="FrontPage 4.0"&gt;</pre></td>
  </tr>
  <tr>
    <td>keywords</td>
    <td><p>指定网页关键字 - 用于告诉搜索引擎网页内容相关关键词。</p>
  <p>实例:</p>
 <pre>&lt;meta name="keywords" content="HTML, HTML DOM, JavaScript"&gt;</pre></td>
  </tr>
 </tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
  <tbody><tr>
  <th style="width:20%">值</th>
    <th>描述</th>
  </tr>
  <tr>
    <td>application-name</td>
    <td>指定 web 应用名称</td>
  </tr>
  <tr>
    <td>author</td>
    <td><p>定义文档的作者。</p>
 <p>实例:</p>
  <pre>&lt;meta name="author" content="盘古学院"&gt;</pre></td>
  </tr>
  <tr>
    <td>description</td>
    <td><p>指定页面描述。搜索索引擎可以使用这个描述作为搜索结果。</p>
 <p>实例:</p>
 <pre>&lt;meta name="description" content="免费编程入门教程"&gt;</pre></td>
  </tr>
  <tr>
    <td>generator</td>
    <td><p>指定文档生成的工具名称。</p>
   <p>实例:</p>
  <pre>&lt;meta name="generator" content="FrontPage 4.0"&gt;</pre></td>
  </tr>
  <tr>
    <td>keywords</td>
    <td><p>指定网页关键字 - 用于告诉搜索引擎网页内容相关关键词。</p>
  <p>实例:</p>
 <pre>&lt;meta name="keywords" content="HTML, HTML DOM, JavaScript"&gt;</pre></td>
  </tr>
 </tbody></table></div>') where ID=4183;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference"> <tbody><tr> <th style="width:20%">Filter</th> <th style="width:68%">描述</th> </tr> <tr> <td>none</td> <td>默认值，没有效果。</td> </tr> <tr> <td>blur(<em>px</em>)</td> <td>给图像设置高斯模糊。"radius"一值设定高斯函数的标准差，或者是屏幕上以多少像素融在一起， 所以值越大越模糊；<br><br>如果没有设定值，则默认是0；这个参数可设置css长度值，但不接受百分比值。</td> </tr> <tr> <td>brightness(<em>%</em>)</td> <td>给图片应用一种线性乘法，使其看起来更亮或更暗。如果值是0%，图像会全黑。值是100%，则图像无变化。其他的值对应线性乘数效果。值超过100%也是可以的，图像会比原来更亮。如果没有设定值，默认是1。</td> </tr> <tr> <td>contrast(<em>%</em>)</td> <td>调整图像的对比度。值是0%的话，图像会全黑。值是100%，图像不变。值可以超过100%，意味着会运用更低的对比。若没有设置值，默认是1。</td> </tr> <tr> <td>drop-shadow(<em>h-shadow v-shadow blur spread color</em>)</td> <td><p>给图像设置一个阴影效果。阴影是合成在图像下面，可以有模糊度的，可以以特定颜色画出的遮罩图的偏移版本。 函数接受&lt;shadow&gt;(在CSS3背景中定义)类型的值，除了"inset"关键字是不允许的。该函数与已有的box-shadow box-shadow属性很相似；不同之处在于，通过滤镜，一些浏览器为了更好的性能会提供硬件加速。<code style="font-style: normal; line-height: 1.5;">&lt;shadow&gt;参数如下：</strong></p> <dl> <dt><strong>&lt;offset-x&gt;</strong> <strong>&lt;offset-y&gt;</strong> <small>(必须)</small></dt> <dd>这是设置阴影偏移量的两个 &lt;length&gt;值. <strong>&lt;offset-x&gt;</strong>&nbsp;设定水平方向距离. 负值会使阴影出现在元素左边. <strong>&lt;offset-y&gt;</strong>设定垂直距离.负值会使阴影出现在元素上方。查看<strong>&lt;length&gt;</strong>可能的单位.<br> <strong><font face="Open Sans, sans-serif">如果两个值都是</font>0</strong>, 则阴影出现在元素正后面 (如果设置了<span style="line-height: 1.5;">&nbsp;</span><code style="font-style: normal; line-height: 1.5;">&lt;blur-radius&gt;</strong><span style="line-height: 1.5;">&nbsp;and/or&nbsp;</span><code style="font-style: normal; line-height: 1.5;">&lt;spread-radius&gt;，</strong><span style="line-height: 1.5;">会有模糊效果</span><span style="line-height: 1.5;">).</span></dd> <dt><strong>&lt;blur-radius&gt;</strong> <small>(可选)</small></dt> <dd>这是第三个code&gt;&lt;length&gt;值. 值越大，越模糊，则阴影会变得更大更淡.不允许负值 若未设定，默认是0&nbsp;(则阴影的边界很锐利).</dd> <dt><strong>&lt;spread-radius&gt;</strong> <small>(可选)</small></dt> <dd>这是第四个 &lt;length&gt;值. 正值会使阴影扩张和变大，负值会是阴影缩小.若未设定，默认是0&nbsp;(阴影会与元素一样大小).&nbsp;<br> 注意: Webkit, 以及一些其他浏览器 不支持第四个长度，如果加了也不会渲染。</dd> <dt>&nbsp;</dt> <dt><strong>&lt;color&gt;</strong> <small>(可选)</small></dt> <dd>查看 &lt;color&gt;该值可能的关键字和标记。若未设定，颜色值基于浏览器。在Gecko&nbsp;(Firefox), Presto (Opera)和Trident (Internet Explorer)中， 会应用color<strong>color</strong>属性的值。另外, 如果颜色值省略，WebKit中阴影是透明的。</dd> </dl> </td> </tr> <tr> <td>grayscale(<em>%</em>)</td> <td><p>将图像转换为灰度图像。值定义转换的比例。值为100%则完全转为灰度图像，值为0%图像无变化。值在0%到100%之间，则是效果的线性乘子。若未设置，值默认是0；</p></td> </tr> <tr> <td>hue-rotate(<em>deg</em>)</td> <td><p>给图像应用色相旋转。"angle"一值设定图像会被调整的色环角度值。值为0deg，则图像无变化。若值未设置，默认值是0deg。该值虽然没有最大值，超过360deg的值相当于又绕一圈。</p></td> </tr> <tr> <td>invert(<em>%</em>)</td> <td><p>反转输入图像。值定义转换的比例。100%的价值是完全反转。值为0%则图像无变化。值在0%和100%之间，则是效果的线性乘子。 若值未设置，值默认是0。</p></td> </tr> <tr> <td>opacity(<em>%</em>)</td> <td><p>转化图像的透明程度。值定义转换的比例。值为0%则是完全透明，值为100%则图像无变化。值在0%和100%之间，则是效果的线性乘子，也相当于图像样本乘以数量。 若值未设置，值默认是1。该函数与已有的opacity属性很相似，不同之处在于通过filter，一些浏览器为了提升性能会提供硬件加速。</p></td> </tr> <tr> <td>saturate(<em>%</em>)</td> <td><p>转换图像饱和度。值定义转换的比例。值为0%则是完全不饱和，值为100%则图像无变化。其他值，则是效果的线性乘子。超过100%的值是允许的，则有更高的饱和度。 若值未设置，值默认是1。</p></td> </tr> <tr> <td>sepia(<em>%</em>)</td> <td><p>将图像转换为深褐色。值定义转换的比例。值为100%则完全是深褐色的，值为0%图像无变化。值在0%到100%之间，则是效果的线性乘子。若未设置，值默认是0；</p></td> </tr> <tr> <td>url()</td> <td><p>URL函数接受一个XML文件，该文件设置了 一个SVG滤镜，且可以包含一个锚点来指定一个具体的滤镜元素。</p> <p>例如：</p> <pre>filter: url(svg-url#element-id)</pre></td> </tr> <tr> <td>initial</td> <td><p>设置属性为默认值，可参阅： <a href="css-initial.html">CSS initial 关键字</a></p></td> </tr> <tr> <td>inherit</td> <td>从父元素继承该属性，可参阅：<a href="css-inherit.html">CSS inherit 关键字</a></td> </tr> </tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference"> <tbody><tr> <th style="width:20%">Filter</th> <th style="width:68%">描述</th> </tr> <tr> <td>none</td> <td>默认值，没有效果。</td> </tr> <tr> <td>blur(<em>px</em>)</td> <td>给图像设置高斯模糊。"radius"一值设定高斯函数的标准差，或者是屏幕上以多少像素融在一起， 所以值越大越模糊；<br><br>如果没有设定值，则默认是0；这个参数可设置css长度值，但不接受百分比值。</td> </tr> <tr> <td>brightness(<em>%</em>)</td> <td>给图片应用一种线性乘法，使其看起来更亮或更暗。如果值是0%，图像会全黑。值是100%，则图像无变化。其他的值对应线性乘数效果。值超过100%也是可以的，图像会比原来更亮。如果没有设定值，默认是1。</td> </tr> <tr> <td>contrast(<em>%</em>)</td> <td>调整图像的对比度。值是0%的话，图像会全黑。值是100%，图像不变。值可以超过100%，意味着会运用更低的对比。若没有设置值，默认是1。</td> </tr> <tr> <td>drop-shadow(<em>h-shadow v-shadow blur spread color</em>)</td> <td><p>给图像设置一个阴影效果。阴影是合成在图像下面，可以有模糊度的，可以以特定颜色画出的遮罩图的偏移版本。 函数接受&lt;shadow&gt;(在CSS3背景中定义)类型的值，除了"inset"关键字是不允许的。该函数与已有的box-shadow box-shadow属性很相似；不同之处在于，通过滤镜，一些浏览器为了更好的性能会提供硬件加速。<code style="font-style: normal; line-height: 1.5;">&lt;shadow&gt;参数如下：</strong></p> <dl> <dt><strong>&lt;offset-x&gt;</strong> <strong>&lt;offset-y&gt;</strong> <small>(必须)</small></dt> <dd>这是设置阴影偏移量的两个 &lt;length&gt;值. <strong>&lt;offset-x&gt;</strong>&nbsp;设定水平方向距离. 负值会使阴影出现在元素左边. <strong>&lt;offset-y&gt;</strong>设定垂直距离.负值会使阴影出现在元素上方。查看<strong>&lt;length&gt;</strong>可能的单位.<br> <strong><font face="Open Sans, sans-serif">如果两个值都是</font>0</strong>, 则阴影出现在元素正后面 (如果设置了<span style="line-height: 1.5;">&nbsp;</span><code style="font-style: normal; line-height: 1.5;">&lt;blur-radius&gt;</strong><span style="line-height: 1.5;">&nbsp;and/or&nbsp;</span><code style="font-style: normal; line-height: 1.5;">&lt;spread-radius&gt;，</strong><span style="line-height: 1.5;">会有模糊效果</span><span style="line-height: 1.5;">).</span></dd> <dt><strong>&lt;blur-radius&gt;</strong> <small>(可选)</small></dt> <dd>这是第三个code&gt;&lt;length&gt;值. 值越大，越模糊，则阴影会变得更大更淡.不允许负值 若未设定，默认是0&nbsp;(则阴影的边界很锐利).</dd> <dt><strong>&lt;spread-radius&gt;</strong> <small>(可选)</small></dt> <dd>这是第四个 &lt;length&gt;值. 正值会使阴影扩张和变大，负值会是阴影缩小.若未设定，默认是0&nbsp;(阴影会与元素一样大小).&nbsp;<br> 注意: Webkit, 以及一些其他浏览器 不支持第四个长度，如果加了也不会渲染。</dd> <dt>&nbsp;</dt> <dt><strong>&lt;color&gt;</strong> <small>(可选)</small></dt> <dd>查看 &lt;color&gt;该值可能的关键字和标记。若未设定，颜色值基于浏览器。在Gecko&nbsp;(Firefox), Presto (Opera)和Trident (Internet Explorer)中， 会应用color<strong>color</strong>属性的值。另外, 如果颜色值省略，WebKit中阴影是透明的。</dd> </dl> </td> </tr> <tr> <td>grayscale(<em>%</em>)</td> <td><p>将图像转换为灰度图像。值定义转换的比例。值为100%则完全转为灰度图像，值为0%图像无变化。值在0%到100%之间，则是效果的线性乘子。若未设置，值默认是0；</p></td> </tr> <tr> <td>hue-rotate(<em>deg</em>)</td> <td><p>给图像应用色相旋转。"angle"一值设定图像会被调整的色环角度值。值为0deg，则图像无变化。若值未设置，默认值是0deg。该值虽然没有最大值，超过360deg的值相当于又绕一圈。</p></td> </tr> <tr> <td>invert(<em>%</em>)</td> <td><p>反转输入图像。值定义转换的比例。100%的价值是完全反转。值为0%则图像无变化。值在0%和100%之间，则是效果的线性乘子。 若值未设置，值默认是0。</p></td> </tr> <tr> <td>opacity(<em>%</em>)</td> <td><p>转化图像的透明程度。值定义转换的比例。值为0%则是完全透明，值为100%则图像无变化。值在0%和100%之间，则是效果的线性乘子，也相当于图像样本乘以数量。 若值未设置，值默认是1。该函数与已有的opacity属性很相似，不同之处在于通过filter，一些浏览器为了提升性能会提供硬件加速。</p></td> </tr> <tr> <td>saturate(<em>%</em>)</td> <td><p>转换图像饱和度。值定义转换的比例。值为0%则是完全不饱和，值为100%则图像无变化。其他值，则是效果的线性乘子。超过100%的值是允许的，则有更高的饱和度。 若值未设置，值默认是1。</p></td> </tr> <tr> <td>sepia(<em>%</em>)</td> <td><p>将图像转换为深褐色。值定义转换的比例。值为100%则完全是深褐色的，值为0%图像无变化。值在0%到100%之间，则是效果的线性乘子。若未设置，值默认是0；</p></td> </tr> <tr> <td>url()</td> <td><p>URL函数接受一个XML文件，该文件设置了 一个SVG滤镜，且可以包含一个锚点来指定一个具体的滤镜元素。</p> <p>例如：</p> <pre>filter: url(svg-url#element-id)</pre></td> </tr> <tr> <td>initial</td> <td><p>设置属性为默认值，可参阅： <a href="css-initial.html">CSS initial 关键字</a></p></td> </tr> <tr> <td>inherit</td> <td>从父元素继承该属性，可参阅：<a href="css-inherit.html">CSS inherit 关键字</a></td> </tr> </tbody></table></div>') where ID=4853;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
<th>CSS 代码</th>
<th>解释</th>
</tr>
<tr>
<td><pre>
.span12 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 940px;
}
</pre></td>
<td>如果该行有一个单一的列，列宽为 940px。</td>
</tr>
<tr>
<td><pre>
.span11 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 860px;
}
</pre></td>
<td>如果该行有一个由 11 列合并的列，列宽为 860px。</td>
</tr>
<tr>
<td><pre>
.span10 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 780px;
}
</pre></td>
<td>如果该行有一个由 10 列合并的列，列宽为 780px。</td>
</tr>
<tr>
<td><pre>
.span9 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 700px;
}
</pre></td>
<td>如果该行有一个由 9 列合并的列，列宽为 700px。</td>
</tr>
<tr>
<td><pre>
.span8 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 620px;
}
</pre></td>
<td>如果该行有一个由 8 列合并的列，列宽为 620px。</td>
</tr>
<tr>
<td><pre>
.span7 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 540px;
}
</pre></td>
<td>如果该行有一个由 7 列合并的列，列宽为 540px。</td>
</tr>
<tr>
<td><pre>
.span6 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 460px;
}
</pre></td>
<td>如果该行有一个由 6 列合并的列，列宽为 460px。</td>
</tr>
<tr>
<td><pre>
.span5 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 380px;
}
</pre></td>
<td>如果该行有一个由 5 列合并的列，列宽为 380px。</td>
</tr>
<tr>
<td><pre>
.span4 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 300px;
}
</pre></td>
<td>如果该行有一个由 4 列合并的列，列宽为 300px。</td>
</tr>
<tr>
<td><pre>
.span3 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 220px;
}
</pre></td>
<td>如果该行有一个由 3 列合并的列，列宽为 220px。</td>
</tr>
<tr>
<td><pre>
.span2 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 140px;
}
</pre></td>
<td>如果该行有一个由 2 列合并的列，列宽为 140px。</td>
</tr>
<tr>
<td><pre>
.span1 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 60px;
}
</pre></td>
<td>单个列宽为 60px。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
<th>CSS 代码</th>
<th>解释</th>
</tr>
<tr>
<td><pre>
.span12 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 940px;
}
</pre></td>
<td>如果该行有一个单一的列，列宽为 940px。</td>
</tr>
<tr>
<td><pre>
.span11 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 860px;
}
</pre></td>
<td>如果该行有一个由 11 列合并的列，列宽为 860px。</td>
</tr>
<tr>
<td><pre>
.span10 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 780px;
}
</pre></td>
<td>如果该行有一个由 10 列合并的列，列宽为 780px。</td>
</tr>
<tr>
<td><pre>
.span9 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 700px;
}
</pre></td>
<td>如果该行有一个由 9 列合并的列，列宽为 700px。</td>
</tr>
<tr>
<td><pre>
.span8 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 620px;
}
</pre></td>
<td>如果该行有一个由 8 列合并的列，列宽为 620px。</td>
</tr>
<tr>
<td><pre>
.span7 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 540px;
}
</pre></td>
<td>如果该行有一个由 7 列合并的列，列宽为 540px。</td>
</tr>
<tr>
<td><pre>
.span6 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 460px;
}
</pre></td>
<td>如果该行有一个由 6 列合并的列，列宽为 460px。</td>
</tr>
<tr>
<td><pre>
.span5 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 380px;
}
</pre></td>
<td>如果该行有一个由 5 列合并的列，列宽为 380px。</td>
</tr>
<tr>
<td><pre>
.span4 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 300px;
}
</pre></td>
<td>如果该行有一个由 4 列合并的列，列宽为 300px。</td>
</tr>
<tr>
<td><pre>
.span3 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 220px;
}
</pre></td>
<td>如果该行有一个由 3 列合并的列，列宽为 220px。</td>
</tr>
<tr>
<td><pre>
.span2 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 140px;
}
</pre></td>
<td>如果该行有一个由 2 列合并的列，列宽为 140px。</td>
</tr>
<tr>
<td><pre>
.span1 {
&nbsp;&nbsp;&nbsp;&nbsp;width: 60px;
}
</pre></td>
<td>单个列宽为 60px。</td>
</tr>
</table></div>') where ID=4995;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>show.bs.tab</td><td>
该事件可以在选项卡中触发，需要在选项卡显示之前。分别使用 <b>event.target</b> 和 <b>event.relatedTarget</b> 事件到目标激活选项卡和前一个选项卡。</td><td>
<pre>
$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
  e.target // 激活选项卡
  e.relatedTarget // 前一个选项卡
})
</pre>

</td></tr>
<tr><td>shown.bs.tab</td><td>该事件在选项卡显示前被触发。分别使用<b>event.target</b> 和 <b>event.relatedTarget</b>  事件到目标激活选项卡和前一个选项卡。</td><td>
<pre>
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  e.target // 激活选项卡
  e.relatedTarget // 前一个选项卡
})
</pre></td></tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>show.bs.tab</td><td>
该事件可以在选项卡中触发，需要在选项卡显示之前。分别使用 <b>event.target</b> 和 <b>event.relatedTarget</b> 事件到目标激活选项卡和前一个选项卡。</td><td>
<pre>
$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
  e.target // 激活选项卡
  e.relatedTarget // 前一个选项卡
})
</pre>

</td></tr>
<tr><td>shown.bs.tab</td><td>该事件在选项卡显示前被触发。分别使用<b>event.target</b> 和 <b>event.relatedTarget</b>  事件到目标激活选项卡和前一个选项卡。</td><td>
<pre>
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  e.target // 激活选项卡
  e.relatedTarget // 前一个选项卡
})
</pre></td></tr>
</tbody></table></div>') where ID=5027;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th style="30%">Class</th><th>描述</th><th>代码示例</th></tr>
<tr><td>.btn-group</td><td>该 class 用于形成基本的按钮组。在 <b>.btn-group</b> 中放置一系列带有 class <b>.btn</b> 的按钮。</td><td>
<pre class="prettyprint">
&lt;div class="btn-group"&gt;
  &lt;button type="button" class="btn btn-default"&gt;Button1&lt;/button&gt;
   &lt;button type="button" class="btn btn-default"&gt;Button2&lt;/button&gt;
&lt;/div&gt;
</pre></td></tr>
<tr><td>.btn-toolbar</td><td>该 class 有助于把几组 &lt;div class="btn-group"&gt; 结合到一个 &lt;div class="btn-toolbar"&gt; 中，一般获得更复杂的组件。</td><td>
<pre class="prettyprint">
&lt;div class="btn-toolbar" role="toolbar"&gt;
  &lt;div class="btn-group"&gt;...&lt;/div&gt;
  &lt;div class="btn-group"&gt;...&lt;/div&gt;
&lt;/div&gt;
</pre></td></tr>
<tr><td>.btn-group-lg, .btn-group-sm, .btn-group-xs</td><td>这些 class 可应用到整个按钮组的大小调整，而不需要对每个按钮进行大小调整。</td><td>
<pre class="prettyprint">
&lt;div class="btn-group btn-group-lg"&gt;...&lt;/div&gt;
&lt;div class="btn-group btn-group-sm"&gt;...&lt;/div&gt;
&lt;div class="btn-group btn-group-xs"&gt;...&lt;/div&gt;
</pre></td></tr>
<tr><td>.btn-group-vertical</td><td>该 class 让一组按钮垂直堆叠显示，而不是水平堆叠显示。</td>
<td><pre class="prettyprint">
&lt;div class="btn-group-vertical"&gt;
  ...
&lt;/div&gt;
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th style="30%">Class</th><th>描述</th><th>代码示例</th></tr>
<tr><td>.btn-group</td><td>该 class 用于形成基本的按钮组。在 <b>.btn-group</b> 中放置一系列带有 class <b>.btn</b> 的按钮。</td><td>
<pre class="prettyprint">
&lt;div class="btn-group"&gt;
  &lt;button type="button" class="btn btn-default"&gt;Button1&lt;/button&gt;
   &lt;button type="button" class="btn btn-default"&gt;Button2&lt;/button&gt;
&lt;/div&gt;
</pre></td></tr>
<tr><td>.btn-toolbar</td><td>该 class 有助于把几组 &lt;div class="btn-group"&gt; 结合到一个 &lt;div class="btn-toolbar"&gt; 中，一般获得更复杂的组件。</td><td>
<pre class="prettyprint">
&lt;div class="btn-toolbar" role="toolbar"&gt;
  &lt;div class="btn-group"&gt;...&lt;/div&gt;
  &lt;div class="btn-group"&gt;...&lt;/div&gt;
&lt;/div&gt;
</pre></td></tr>
<tr><td>.btn-group-lg, .btn-group-sm, .btn-group-xs</td><td>这些 class 可应用到整个按钮组的大小调整，而不需要对每个按钮进行大小调整。</td><td>
<pre class="prettyprint">
&lt;div class="btn-group btn-group-lg"&gt;...&lt;/div&gt;
&lt;div class="btn-group btn-group-sm"&gt;...&lt;/div&gt;
&lt;div class="btn-group btn-group-xs"&gt;...&lt;/div&gt;
</pre></td></tr>
<tr><td>.btn-group-vertical</td><td>该 class 让一组按钮垂直堆叠显示，而不是水平堆叠显示。</td>
<td><pre class="prettyprint">
&lt;div class="btn-group-vertical"&gt;
  ...
&lt;/div&gt;
</pre></td></tr>
</table></div>') where ID=5046;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>Class</th><th>描述</th><th>示例代码</th></tr>
<tr><td>.pagination</td><td>添加该 class 来在页面上显示分页。</td><td>
<pre class="prettyprint">
&lt;ul class="pagination"&gt;
  &lt;li&gt;&lt;a href="#"&gt;&amp;laquo;&lt;/a&gt;&lt;/li&gt;
  &lt;li&gt;&lt;a href="#"&gt;1&lt;/a&gt;&lt;/li&gt;
  .......
&lt;/ul&gt;
</pre></td></tr>
<tr><td>.disabled, .active</td><td>您可以自定义链接，通过使用 <b>.disabled</b> 来定义不可点击的链接，通过使用 <b>.active</b> 来指示当前的页面。</td><td>
<pre class="prettyprint" style="white-space: pre-wrap;">
&lt;ul class="pagination"&gt;
  &lt;li class="disabled"&gt;&lt;a href="#"&gt;&amp;laquo;&lt;/a&gt;&lt;/li&gt;
  &lt;li class="active"&gt;&lt;a href="#"&gt;1&lt;span class="sr-only"&gt;(current)&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
  .......
&lt;/ul&gt;
</pre></td></tr>
<tr><td>.pagination-lg, .pagination-sm</td><td>使用这些 class 来获取不同大小的项。</td><td>
<pre class="prettyprint">
&lt;ul class="pagination pagination-lg"&gt;...&lt;/ul&gt;
&lt;ul class="pagination"&gt;...&lt;/ul&gt;
&lt;ul class="pagination pagination-sm"&gt;...&lt;/ul&gt;
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>Class</th><th>描述</th><th>示例代码</th></tr>
<tr><td>.pagination</td><td>添加该 class 来在页面上显示分页。</td><td>
<pre class="prettyprint">
&lt;ul class="pagination"&gt;
  &lt;li&gt;&lt;a href="#"&gt;&amp;laquo;&lt;/a&gt;&lt;/li&gt;
  &lt;li&gt;&lt;a href="#"&gt;1&lt;/a&gt;&lt;/li&gt;
  .......
&lt;/ul&gt;
</pre></td></tr>
<tr><td>.disabled, .active</td><td>您可以自定义链接，通过使用 <b>.disabled</b> 来定义不可点击的链接，通过使用 <b>.active</b> 来指示当前的页面。</td><td>
<pre class="prettyprint" style="white-space: pre-wrap;">
&lt;ul class="pagination"&gt;
  &lt;li class="disabled"&gt;&lt;a href="#"&gt;&amp;laquo;&lt;/a&gt;&lt;/li&gt;
  &lt;li class="active"&gt;&lt;a href="#"&gt;1&lt;span class="sr-only"&gt;(current)&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
  .......
&lt;/ul&gt;
</pre></td></tr>
<tr><td>.pagination-lg, .pagination-sm</td><td>使用这些 class 来获取不同大小的项。</td><td>
<pre class="prettyprint">
&lt;ul class="pagination pagination-lg"&gt;...&lt;/ul&gt;
&lt;ul class="pagination"&gt;...&lt;/ul&gt;
&lt;ul class="pagination pagination-sm"&gt;...&lt;/ul&gt;
</pre></td></tr>
</table></div>') where ID=5052;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>Class</th><th>描述</th><th>示例代码</th></tr>
<tr><td>.pager</td><td>添加该 class 来获得翻页链接。</td><td>
<pre class="prettyprint">
&lt;ul class="pager"&gt;
  &lt;li&gt;&lt;a href="#"&gt;Previous&lt;/a&gt;&lt;/li&gt;
  &lt;li&gt;&lt;a href="#"&gt;Next&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</pre></td></tr>
<tr><td>.previous, .next</td><td>使用 class <b>.previous</b> 把链接向左对齐，使用 <b>.next</b> 把链接向右对齐。</td><td>
<pre class="prettyprint">
&lt;ul class="pager"&gt;
  &lt;li class="previous"&gt;&lt;a href="#"&gt;&amp;larr; Older&lt;/a&gt;&lt;/li&gt;
  &lt;li class="next"&gt;&lt;a href="#"&gt;Newer &amp;rarr;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</pre></td></tr>
<tr><td>.disabled</td><td>添加该 class 来设置对应按钮禁止使用。</td><td>
<pre class="prettyprint">
&lt;ul class="pager"&gt;
  &lt;li class="previous disabled"&gt;&lt;a href="#"&gt;&amp;larr; Older&lt;/a&gt;&lt;/li&gt;
  &lt;li class="next"&gt;&lt;a href="#"&gt;Newer &amp;rarr;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>Class</th><th>描述</th><th>示例代码</th></tr>
<tr><td>.pager</td><td>添加该 class 来获得翻页链接。</td><td>
<pre class="prettyprint">
&lt;ul class="pager"&gt;
  &lt;li&gt;&lt;a href="#"&gt;Previous&lt;/a&gt;&lt;/li&gt;
  &lt;li&gt;&lt;a href="#"&gt;Next&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</pre></td></tr>
<tr><td>.previous, .next</td><td>使用 class <b>.previous</b> 把链接向左对齐，使用 <b>.next</b> 把链接向右对齐。</td><td>
<pre class="prettyprint">
&lt;ul class="pager"&gt;
  &lt;li class="previous"&gt;&lt;a href="#"&gt;&amp;larr; Older&lt;/a&gt;&lt;/li&gt;
  &lt;li class="next"&gt;&lt;a href="#"&gt;Newer &amp;rarr;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</pre></td></tr>
<tr><td>.disabled</td><td>添加该 class 来设置对应按钮禁止使用。</td><td>
<pre class="prettyprint">
&lt;ul class="pager"&gt;
  &lt;li class="previous disabled"&gt;&lt;a href="#"&gt;&amp;larr; Older&lt;/a&gt;&lt;/li&gt;
  &lt;li class="next"&gt;&lt;a href="#"&gt;Newer &amp;rarr;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</pre></td></tr>
</table></div>') where ID=5052;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>选项名称</th><th>类型/默认值</th><th>Data 属性名称</th><th>描述</th></tr>
<tr><td>backdrop</td><td>boolean 或 string 'static'<br> <i>默认值：true</i></td><td>data-backdrop</td><td>指定一个静态的背景，当用户点击模态框外部时不会关闭模态框。
</td></tr>
<tr><td>keyboard</td><td>boolean<br> <i>默认值：true</i></td><td>data-keyboard</td><td>当按下 escape 键时关闭模态框，设置为 false 时则按键无效。
</td></tr>
<tr><td>show</td><td>boolean<br> <i>默认值：true</i></td><td>data-show</td><td>当初始化时显示模态框。</td></tr>
<tr><td>remote</td><td>path<br> <i>默认值：false</i></td><td>data-remote</td><td>使用 jQuery <i>.load</i> 方法，为模态框的主体注入内容。如果添加了一个带有有效 URL 的 href，则会加载其中的内容。如下面的实例所示：
<pre class="prettyprint" style="white-space: pre-wrap;">
&lt;a data-toggle="modal" href="remote.html" data-target="#modal" rel="noopener noreferrer"&gt;请点击我&lt;/a&gt;
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>选项名称</th><th>类型/默认值</th><th>Data 属性名称</th><th>描述</th></tr>
<tr><td>backdrop</td><td>boolean 或 string 'static'<br> <i>默认值：true</i></td><td>data-backdrop</td><td>指定一个静态的背景，当用户点击模态框外部时不会关闭模态框。
</td></tr>
<tr><td>keyboard</td><td>boolean<br> <i>默认值：true</i></td><td>data-keyboard</td><td>当按下 escape 键时关闭模态框，设置为 false 时则按键无效。
</td></tr>
<tr><td>show</td><td>boolean<br> <i>默认值：true</i></td><td>data-show</td><td>当初始化时显示模态框。</td></tr>
<tr><td>remote</td><td>path<br> <i>默认值：false</i></td><td>data-remote</td><td>使用 jQuery <i>.load</i> 方法，为模态框的主体注入内容。如果添加了一个带有有效 URL 的 href，则会加载其中的内容。如下面的实例所示：
<pre class="prettyprint" style="white-space: pre-wrap;">
&lt;a data-toggle="modal" href="remote.html" data-target="#modal" rel="noopener noreferrer"&gt;请点击我&lt;/a&gt;
</pre></td></tr>
</table></div>') where ID=5066;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td><b>Options:</b> .modal(options)</td><td>把内容作为模态框激活。接受一个可选的选项对象。</td><td>
<pre class="prettyprint">
$('#identifier').modal({
keyboard: false
})
</pre></td></tr>
<tr><td><b>Toggle:</b> .modal('toggle')</td><td>手动切换模态框。</td><td>
<pre class="prettyprint">
$('#identifier').modal('toggle')
</pre></td></tr>
<tr><td><b>Show:</b> .modal('show')</td><td>手动打开模态框。</td><td>
<pre class="prettyprint">
$('#identifier').modal('show')
</pre></td></tr>
<tr><td><b>Hide:</b> .modal('hide')</td><td>手动隐藏模态框。</td><td>
<pre class="prettyprint">
$('#identifier').modal('hide')
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td><b>Options:</b> .modal(options)</td><td>把内容作为模态框激活。接受一个可选的选项对象。</td><td>
<pre class="prettyprint">
$('#identifier').modal({
keyboard: false
})
</pre></td></tr>
<tr><td><b>Toggle:</b> .modal('toggle')</td><td>手动切换模态框。</td><td>
<pre class="prettyprint">
$('#identifier').modal('toggle')
</pre></td></tr>
<tr><td><b>Show:</b> .modal('show')</td><td>手动打开模态框。</td><td>
<pre class="prettyprint">
$('#identifier').modal('show')
</pre></td></tr>
<tr><td><b>Hide:</b> .modal('hide')</td><td>手动隐藏模态框。</td><td>
<pre class="prettyprint">
$('#identifier').modal('hide')
</pre></td></tr>
</table></div>') where ID=5066;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>show.bs.modal</td><td>在调用 show 方法后触发。</td><td>
<pre class="prettyprint">
$('#identifier').on('show.bs.modal', function () {
  // 执行一些动作...
})
</pre></td></tr>
<tr><td>shown.bs.modal</td><td>当模态框对用户可见时触发（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#identifier').on('shown.bs.modal', function () {
  // 执行一些动作...
})
</pre></td></tr>
<tr><td>hide.bs.modal</td><td>当调用 hide 实例方法时触发。</td><td>
<pre class="prettyprint">
$('#identifier').on('hide.bs.modal', function () {
  // 执行一些动作...
})
</pre></td></tr>
<tr><td>hidden.bs.modal</td><td>当模态框完全对用户隐藏时触发。</td><td>
<pre class="prettyprint">
$('#identifier').on('hidden.bs.modal', function () {
  // 执行一些动作...
})
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>show.bs.modal</td><td>在调用 show 方法后触发。</td><td>
<pre class="prettyprint">
$('#identifier').on('show.bs.modal', function () {
  // 执行一些动作...
})
</pre></td></tr>
<tr><td>shown.bs.modal</td><td>当模态框对用户可见时触发（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#identifier').on('shown.bs.modal', function () {
  // 执行一些动作...
})
</pre></td></tr>
<tr><td>hide.bs.modal</td><td>当调用 hide 实例方法时触发。</td><td>
<pre class="prettyprint">
$('#identifier').on('hide.bs.modal', function () {
  // 执行一些动作...
})
</pre></td></tr>
<tr><td>hidden.bs.modal</td><td>当模态框完全对用户隐藏时触发。</td><td>
<pre class="prettyprint">
$('#identifier').on('hidden.bs.modal', function () {
  // 执行一些动作...
})
</pre></td></tr>
</table></div>') where ID=5066;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
	<tr><th>事件</th><th>描述</th><th>实例</th></tr>
	<tr><td>activate.bs.scrollspy</td><td>每当一个新项目被滚动监听激活时，触发该事件。</td><td>
<pre class="prettyprint">
$('#myScrollspy').on('activate.bs.scrollspy', function () {
  // 执行一些动作...
})
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
	<tr><th>事件</th><th>描述</th><th>实例</th></tr>
	<tr><td>activate.bs.scrollspy</td><td>每当一个新项目被滚动监听激活时，触发该事件。</td><td>
<pre class="prettyprint">
$('#myScrollspy').on('activate.bs.scrollspy', function () {
  // 执行一些动作...
})
</pre></td></tr>
</table></div>') where ID=5068;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
	<tr><th>事件</th><th>描述</th><th>实例</th></tr>
	<tr><td>show.bs.tab</td><td>该事件在标签页显示时触发，但是必须在新标签页被显示之前。分别使用 <b>event.target</b> 和 <b>event.relatedTarget</b> 来定位到激活的标签页和前一个激活的标签页。</td><td>
<pre class="prettyprint">
$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
  e.target // 激活的标签页
  e.relatedTarget // 前一个激活的标签页
})
</pre></td></tr>
	<tr><td>shown.bs.tab</td><td>该事件在标签页显示时触发，但是必须在某个标签页已经显示之后。分别使用 <b>event.target</b> 和 <b>event.relatedTarget</b> 来定位到激活的标签页和前一个激活的标签页。</td><td>
<pre class="prettyprint">
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  e.target // 激活的标签页
  e.relatedTarget // 前一个激活的标签页
})
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
	<tr><th>事件</th><th>描述</th><th>实例</th></tr>
	<tr><td>show.bs.tab</td><td>该事件在标签页显示时触发，但是必须在新标签页被显示之前。分别使用 <b>event.target</b> 和 <b>event.relatedTarget</b> 来定位到激活的标签页和前一个激活的标签页。</td><td>
<pre class="prettyprint">
$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
  e.target // 激活的标签页
  e.relatedTarget // 前一个激活的标签页
})
</pre></td></tr>
	<tr><td>shown.bs.tab</td><td>该事件在标签页显示时触发，但是必须在某个标签页已经显示之后。分别使用 <b>event.target</b> 和 <b>event.relatedTarget</b> 来定位到激活的标签页和前一个激活的标签页。</td><td>
<pre class="prettyprint">
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  e.target // 激活的标签页
  e.relatedTarget // 前一个激活的标签页
})
</pre></td></tr>
</table></div>') where ID=5069;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
	<tr><th>选项名称</th><th>类型/默认值</th><th>Data 属性名称</th><th>描述</th></tr>
	<tr><td>animation</td><td>boolean<br> <i>默认值：true</i></td><td>data-animation</td><td>提示工具使用 CSS 渐变滤镜效果。</td></tr>
	<tr><td>html</td><td>boolean<br> <i>默认值：false</i></td><td>data-html</td><td>向提示工具插入 HTML。如果为 false，jQuery 的 text 方法将被用于向 dom 插入内容。如果您担心 XSS 攻击，请使用 text。</td></tr>
	<tr><td>placement</td><td>string|function<br> <i>默认值：top</i></td><td>data-placement</td><td>规定如何定位提示工具（即 top|bottom|left|right|auto）。<br>当指定为 <i>auto</i> 时，会动态调整提示工具。例如，如果 placement 是 "auto left"，提示工具将会尽可能显示在左边，在情况不允许的情况下它才会显示在右边。</td></tr>
	<tr><td>selector</td><td>string<br> <i>默认值：false</i></td><td>data-selector</td><td>如果提供了一个选择器，提示工具对象将被委派到指定的目标。</td></tr>
	<tr><td>title</td><td>string | function<br> <i>默认值：''</i></td><td>data-title</td><td>如果未指定 <i>title</i> 属性，则 title 选项是默认的 title 值。</td></tr>
	<tr><td>trigger</td><td>string<br> <i>默认值：'hover focus'</i></td><td>data-trigger</td><td>定义如何触发提示工具：<b> click| hover | focus | manual</b>。您可以传递多个触发器，每个触发器之间用空格分隔。</td></tr>
	
	<tr><td>delay</td><td>number | object<br> <i>默认值：0</i></td><td>data-delay</td><td>延迟显示和隐藏提示工具的毫秒数 - 对 manual 手动触发类型不适用。如果提供的是一个数字，那么延迟将会应用于显示和隐藏。如果提供的是对象，结构如下所示：
<pre class="prettyprint">
delay:
{ show: 500, hide: 100 }</pre></td></tr>
	<tr><td>container</td><td>string | false<br> <i>默认值：false</i></td><td>data-container</td><td>向指定元素追加提示工具。<br>实例： container: 'body'</td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
	<tr><th>选项名称</th><th>类型/默认值</th><th>Data 属性名称</th><th>描述</th></tr>
	<tr><td>animation</td><td>boolean<br> <i>默认值：true</i></td><td>data-animation</td><td>提示工具使用 CSS 渐变滤镜效果。</td></tr>
	<tr><td>html</td><td>boolean<br> <i>默认值：false</i></td><td>data-html</td><td>向提示工具插入 HTML。如果为 false，jQuery 的 text 方法将被用于向 dom 插入内容。如果您担心 XSS 攻击，请使用 text。</td></tr>
	<tr><td>placement</td><td>string|function<br> <i>默认值：top</i></td><td>data-placement</td><td>规定如何定位提示工具（即 top|bottom|left|right|auto）。<br>当指定为 <i>auto</i> 时，会动态调整提示工具。例如，如果 placement 是 "auto left"，提示工具将会尽可能显示在左边，在情况不允许的情况下它才会显示在右边。</td></tr>
	<tr><td>selector</td><td>string<br> <i>默认值：false</i></td><td>data-selector</td><td>如果提供了一个选择器，提示工具对象将被委派到指定的目标。</td></tr>
	<tr><td>title</td><td>string | function<br> <i>默认值：''</i></td><td>data-title</td><td>如果未指定 <i>title</i> 属性，则 title 选项是默认的 title 值。</td></tr>
	<tr><td>trigger</td><td>string<br> <i>默认值：'hover focus'</i></td><td>data-trigger</td><td>定义如何触发提示工具：<b> click| hover | focus | manual</b>。您可以传递多个触发器，每个触发器之间用空格分隔。</td></tr>
	
	<tr><td>delay</td><td>number | object<br> <i>默认值：0</i></td><td>data-delay</td><td>延迟显示和隐藏提示工具的毫秒数 - 对 manual 手动触发类型不适用。如果提供的是一个数字，那么延迟将会应用于显示和隐藏。如果提供的是对象，结构如下所示：
<pre class="prettyprint">
delay:
{ show: 500, hide: 100 }</pre></td></tr>
	<tr><td>container</td><td>string | false<br> <i>默认值：false</i></td><td>data-container</td><td>向指定元素追加提示工具。<br>实例： container: 'body'</td></tr>
</table></div>') where ID=5070;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
	<tr><th>方法</th><th>描述</th><th>实例</th></tr>
	<tr><td><b>Options:</b> .tooltip(options)</td><td>向元素集合附加提示工具句柄。</td><td>
<pre class="prettyprint">
$().tooltip(options)</pre>
		</td></tr>
	<tr><td><b>Toggle:</b> .tooltip('toggle')</td><td>切换显示/隐藏元素的提示工具。</td><td>
<pre class="prettyprint">
$('#element').tooltip('toggle')
</pre></td></tr>
	<tr><td><b>Show:</b> .tooltip('show')</td><td>显示元素的提示工具。</td><td>
<pre class="prettyprint">
$('#element').tooltip('show')
</pre></td></tr>
	<tr><td><b>Hide:</b> .tooltip('hide')</td><td>隐藏元素的提示工具。</td><td>
<pre class="prettyprint">
$('#element').tooltip('hide')
</pre></td></tr>
	<tr><td><b>Destroy:</b> .tooltip('destroy')</td><td>隐藏并销毁元素的提示工具。</td><td>
<pre class="prettyprint">
$('#element').tooltip('destroy')
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
	<tr><th>方法</th><th>描述</th><th>实例</th></tr>
	<tr><td><b>Options:</b> .tooltip(options)</td><td>向元素集合附加提示工具句柄。</td><td>
<pre class="prettyprint">
$().tooltip(options)</pre>
		</td></tr>
	<tr><td><b>Toggle:</b> .tooltip('toggle')</td><td>切换显示/隐藏元素的提示工具。</td><td>
<pre class="prettyprint">
$('#element').tooltip('toggle')
</pre></td></tr>
	<tr><td><b>Show:</b> .tooltip('show')</td><td>显示元素的提示工具。</td><td>
<pre class="prettyprint">
$('#element').tooltip('show')
</pre></td></tr>
	<tr><td><b>Hide:</b> .tooltip('hide')</td><td>隐藏元素的提示工具。</td><td>
<pre class="prettyprint">
$('#element').tooltip('hide')
</pre></td></tr>
	<tr><td><b>Destroy:</b> .tooltip('destroy')</td><td>隐藏并销毁元素的提示工具。</td><td>
<pre class="prettyprint">
$('#element').tooltip('destroy')
</pre></td></tr>
</table></div>') where ID=5070;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
	<tr><th>事件</th><th>描述</th><th>实例</th></tr>
	<tr><td>show.bs.tooltip</td><td>当调用 show 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#myTooltip').on('show.bs.tooltip', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>shown.bs.tooltip</td><td>当提示工具对用户可见时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#myTooltip').on('shown.bs.tooltip', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>hide.bs.tooltip</td><td>当调用 hide 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#myTooltip').on('hide.bs.tooltip', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>hidden.bs.tooltip</td><td>当提示工具对用户隐藏时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#myTooltip').on('hidden.bs.tooltip', function () {
  // 执行一些动作...
})
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
	<tr><th>事件</th><th>描述</th><th>实例</th></tr>
	<tr><td>show.bs.tooltip</td><td>当调用 show 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#myTooltip').on('show.bs.tooltip', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>shown.bs.tooltip</td><td>当提示工具对用户可见时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#myTooltip').on('shown.bs.tooltip', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>hide.bs.tooltip</td><td>当调用 hide 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#myTooltip').on('hide.bs.tooltip', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>hidden.bs.tooltip</td><td>当提示工具对用户隐藏时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#myTooltip').on('hidden.bs.tooltip', function () {
  // 执行一些动作...
})
</pre></td></tr>
</table></div>') where ID=5070;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
	<tr><th>选项名称</th><th>类型/默认值</th><th>Data 属性名称</th><th>描述</th></tr>
	<tr><td>animation</td><td>boolean<br> <i>默认值：true</i></td><td>data-animation</td><td>向弹出框应用 CSS 褪色过渡效果。</td></tr>
	<tr><td>html</td><td>boolean<br> <i>默认值：false</i></td><td>data-html</td><td>向弹出框插入 HTML。如果为 false，jQuery 的 text 方法将被用于向 dom 插入内容。如果您担心 XSS 攻击，请使用 text。</td></tr>
	<tr><td>placement</td><td>string|function<br> <i>默认值：top</i></td><td>data-placement</td><td>规定如何定位弹出框（即 top|bottom|left|right|auto）。<br>当指定为 <i>auto</i> 时，会动态调整弹出框。例如，如果 placement 是 "auto left"，弹出框将会尽可能显示在左边，在情况不允许的情况下它才会显示在右边。</td></tr>
	<tr><td>selector</td><td>string<br> <i>默认值：false</i></td><td>data-selector</td><td>如果提供了一个选择器，弹出框对象将被委派到指定的目标。</td></tr>
	<tr><td>title</td><td>string | function<br> <i>默认值：''</i></td><td>data-title</td><td>如果未指定 <i>title</i> 属性，则 title 选项是默认的 title 值。</td></tr>
	<tr><td>trigger</td><td>string<br> <i>默认值：'hover focus'</i></td><td>data-trigger</td><td>定义如何触发弹出框：<b> click| hover | focus | manual</b>。您可以传递多个触发器，每个触发器之间用空格分隔。</td></tr>
	<tr><td>delay</td><td>number | object<br> <i>默认值：0</i></td><td>data-delay</td><td>延迟显示和隐藏弹出框的毫秒数 - 对 manual 手动触发类型不适用。如果提供的是一个数字，那么延迟将会应用于显示和隐藏。如果提供的是对象，结构如下所示：
<pre class="prettyprint">
delay:
{ show: 500, hide: 100 }</pre></td></tr>
	<tr><td>container</td><td>string | false<br> <i>默认值：false</i></td><td>data-container</td><td>向指定元素追加弹出框。<br>实例： container: 'body'</td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
	<tr><th>选项名称</th><th>类型/默认值</th><th>Data 属性名称</th><th>描述</th></tr>
	<tr><td>animation</td><td>boolean<br> <i>默认值：true</i></td><td>data-animation</td><td>向弹出框应用 CSS 褪色过渡效果。</td></tr>
	<tr><td>html</td><td>boolean<br> <i>默认值：false</i></td><td>data-html</td><td>向弹出框插入 HTML。如果为 false，jQuery 的 text 方法将被用于向 dom 插入内容。如果您担心 XSS 攻击，请使用 text。</td></tr>
	<tr><td>placement</td><td>string|function<br> <i>默认值：top</i></td><td>data-placement</td><td>规定如何定位弹出框（即 top|bottom|left|right|auto）。<br>当指定为 <i>auto</i> 时，会动态调整弹出框。例如，如果 placement 是 "auto left"，弹出框将会尽可能显示在左边，在情况不允许的情况下它才会显示在右边。</td></tr>
	<tr><td>selector</td><td>string<br> <i>默认值：false</i></td><td>data-selector</td><td>如果提供了一个选择器，弹出框对象将被委派到指定的目标。</td></tr>
	<tr><td>title</td><td>string | function<br> <i>默认值：''</i></td><td>data-title</td><td>如果未指定 <i>title</i> 属性，则 title 选项是默认的 title 值。</td></tr>
	<tr><td>trigger</td><td>string<br> <i>默认值：'hover focus'</i></td><td>data-trigger</td><td>定义如何触发弹出框：<b> click| hover | focus | manual</b>。您可以传递多个触发器，每个触发器之间用空格分隔。</td></tr>
	<tr><td>delay</td><td>number | object<br> <i>默认值：0</i></td><td>data-delay</td><td>延迟显示和隐藏弹出框的毫秒数 - 对 manual 手动触发类型不适用。如果提供的是一个数字，那么延迟将会应用于显示和隐藏。如果提供的是对象，结构如下所示：
<pre class="prettyprint">
delay:
{ show: 500, hide: 100 }</pre></td></tr>
	<tr><td>container</td><td>string | false<br> <i>默认值：false</i></td><td>data-container</td><td>向指定元素追加弹出框。<br>实例： container: 'body'</td></tr>
</table></div>') where ID=5071;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
	<tr><th>方法</th><th>描述</th><th>实例</th></tr>
	<tr><td><b>Options:</b> .popover(options)</td><td>向元素集合附加弹出框句柄。</td><td>
<pre class="prettyprint">
$().popover(options)</pre>
		</td></tr>
	<tr><td><b>Toggle:</b> .popover('toggle')</td><td>切换显示/隐藏元素的弹出框。</td><td>
<pre class="prettyprint">
$('#element').popover('toggle')
</pre></td></tr>
	<tr><td><b>Show:</b> .popover('show')</td><td>显示元素的弹出框。</td><td>
<pre class="prettyprint">
$('#element').popover('show')
</pre></td></tr>
	<tr><td><b>Hide:</b> .popover('hide')</td><td>隐藏元素的弹出框。</td><td>
<pre class="prettyprint">
$('#element').popover('hide')
</pre></td></tr>
	<tr><td><b>Destroy:</b> .popover('destroy')</td><td>隐藏并销毁元素的弹出框。</td><td>
<pre class="prettyprint">
$('#element').popover('destroy')
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
	<tr><th>方法</th><th>描述</th><th>实例</th></tr>
	<tr><td><b>Options:</b> .popover(options)</td><td>向元素集合附加弹出框句柄。</td><td>
<pre class="prettyprint">
$().popover(options)</pre>
		</td></tr>
	<tr><td><b>Toggle:</b> .popover('toggle')</td><td>切换显示/隐藏元素的弹出框。</td><td>
<pre class="prettyprint">
$('#element').popover('toggle')
</pre></td></tr>
	<tr><td><b>Show:</b> .popover('show')</td><td>显示元素的弹出框。</td><td>
<pre class="prettyprint">
$('#element').popover('show')
</pre></td></tr>
	<tr><td><b>Hide:</b> .popover('hide')</td><td>隐藏元素的弹出框。</td><td>
<pre class="prettyprint">
$('#element').popover('hide')
</pre></td></tr>
	<tr><td><b>Destroy:</b> .popover('destroy')</td><td>隐藏并销毁元素的弹出框。</td><td>
<pre class="prettyprint">
$('#element').popover('destroy')
</pre></td></tr>
</table></div>') where ID=5071;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
	<tr><th>事件</th><th>描述</th><th>实例</th></tr>
	<tr><td>show.bs.popover</td><td>当调用 show 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#mypopover').on('show.bs.popover', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>shown.bs.popover</td><td>当弹出框对用户可见时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#mypopover').on('shown.bs.popover', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>hide.bs.popover</td><td>当调用 hide 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#mypopover').on('hide.bs.popover', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>hidden.bs.popover</td><td>当工具提示对用户隐藏时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#mypopover').on('hidden.bs.popover', function () {
  // 执行一些动作...
})
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
	<tr><th>事件</th><th>描述</th><th>实例</th></tr>
	<tr><td>show.bs.popover</td><td>当调用 show 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#mypopover').on('show.bs.popover', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>shown.bs.popover</td><td>当弹出框对用户可见时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#mypopover').on('shown.bs.popover', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>hide.bs.popover</td><td>当调用 hide 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#mypopover').on('hide.bs.popover', function () {
  // 执行一些动作...
})
</pre></td></tr>
	<tr><td>hidden.bs.popover</td><td>当工具提示对用户隐藏时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#mypopover').on('hidden.bs.popover', function () {
  // 执行一些动作...
})
</pre></td></tr>
</table></div>') where ID=5071;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td>.alert()</td><td>该方法让所有的警告框都带有关闭功能。</td><td>
<pre class="prettyprint">
$('#identifier').alert();
</pre></td></tr>
<tr><td><b>关闭方法</b> .alert('close')</td><td>关闭所有的警告框。</td><td>
<pre class="prettyprint">
$('#identifier').alert('close');
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td>.alert()</td><td>该方法让所有的警告框都带有关闭功能。</td><td>
<pre class="prettyprint">
$('#identifier').alert();
</pre></td></tr>
<tr><td><b>关闭方法</b> .alert('close')</td><td>关闭所有的警告框。</td><td>
<pre class="prettyprint">
$('#identifier').alert('close');
</pre></td></tr>
</table></div>') where ID=5072;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>close.bs.alert</td><td>当调用 <i>close</i> 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#myalert').bind('close.bs.alert', function () {
  // 执行一些动作...
})
</pre></td></tr>
<tr><td>closed.bs.alert</td><td>当警告框被关闭时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#myalert').bind('closed.bs.alert', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>close.bs.alert</td><td>当调用 <i>close</i> 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#myalert').bind('close.bs.alert', function () {
  // 执行一些动作...
})
</pre></td></tr>
<tr><td>closed.bs.alert</td><td>当警告框被关闭时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#myalert').bind('closed.bs.alert', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
</table></div>') where ID=5072;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td>button('toggle')</td><td>切换按压状态。赋予按钮被激活的外观。您可以使用 <b>data-toggle</b> 属性启用按钮的自动切换。</td>
<td>
<pre class="prettyprint">
$().button('toggle')</pre></td></tr>
<tr><td>.button('loading')</td><td>当加载时，按钮是禁用的，且文本变为 button 元素的 <b>data-loading-text</b> 属性的值。</td>
<td>
<pre class="prettyprint">
$().button('loading')</pre></td></tr>
<tr><td>.button('reset')</td><td>重置按钮状态，文本内容恢复为最初的内容。当您想要把按钮返回为原始的状态时，该方法非常有用。</td>
<td>
<pre class="prettyprint">
$().button('reset')</pre></td></tr>
<tr><td>.button(string)</td><td>该方法中的字符串是指由用户声明的任何字符串。使用该方法，重置按钮状态，并添加新的内容。</td>
<td>
<pre class="prettyprint">
$().button(string)</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td>button('toggle')</td><td>切换按压状态。赋予按钮被激活的外观。您可以使用 <b>data-toggle</b> 属性启用按钮的自动切换。</td>
<td>
<pre class="prettyprint">
$().button('toggle')</pre></td></tr>
<tr><td>.button('loading')</td><td>当加载时，按钮是禁用的，且文本变为 button 元素的 <b>data-loading-text</b> 属性的值。</td>
<td>
<pre class="prettyprint">
$().button('loading')</pre></td></tr>
<tr><td>.button('reset')</td><td>重置按钮状态，文本内容恢复为最初的内容。当您想要把按钮返回为原始的状态时，该方法非常有用。</td>
<td>
<pre class="prettyprint">
$().button('reset')</pre></td></tr>
<tr><td>.button(string)</td><td>该方法中的字符串是指由用户声明的任何字符串。使用该方法，重置按钮状态，并添加新的内容。</td>
<td>
<pre class="prettyprint">
$().button(string)</pre></td></tr>
</table></div>') where ID=5073;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td><b>Options:</b> .collapse(options)</td><td>激活内容为可折叠元素。接受一个可选的 options 对象。</td><td>
<pre class="prettyprint">
$('#identifier').collapse({
&nbsp;&nbsp;&nbsp;&nbsp;toggle: false
})
</pre></td></tr>
<tr><td><b>Toggle:</b> .collapse('toggle')</td><td>切换显示/隐藏可折叠元素。</td><td>
<pre class="prettyprint">$('#identifier').collapse('toggle')
</pre></td></tr>
<tr><td><b>Show:</b> .collapse('show')</td><td>显示可折叠元素。</td><td>
<pre class="prettyprint">$('#identifier').collapse('show')
</pre></td></tr>
<tr><td><b>Hide:</b> .collapse('hide')</td><td>隐藏可折叠元素。</td><td>
<pre class="prettyprint">$('#identifier').collapse('hide')
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td><b>Options:</b> .collapse(options)</td><td>激活内容为可折叠元素。接受一个可选的 options 对象。</td><td>
<pre class="prettyprint">
$('#identifier').collapse({
&nbsp;&nbsp;&nbsp;&nbsp;toggle: false
})
</pre></td></tr>
<tr><td><b>Toggle:</b> .collapse('toggle')</td><td>切换显示/隐藏可折叠元素。</td><td>
<pre class="prettyprint">$('#identifier').collapse('toggle')
</pre></td></tr>
<tr><td><b>Show:</b> .collapse('show')</td><td>显示可折叠元素。</td><td>
<pre class="prettyprint">$('#identifier').collapse('show')
</pre></td></tr>
<tr><td><b>Hide:</b> .collapse('hide')</td><td>隐藏可折叠元素。</td><td>
<pre class="prettyprint">$('#identifier').collapse('hide')
</pre></td></tr>
</table></div>') where ID=5074;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>show.bs.collapse</td><td>在调用 show 方法后触发该事件。</td><td>
<pre class="prettyprint">
$('#identifier').on('show.bs.collapse', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
<tr><td>shown.bs.collapse</td><td>当折叠元素对用户可见时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#identifier').on('shown.bs.collapse', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
<tr><td>hide.bs.collapse</td><td>当调用 hide 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#identifier').on('hide.bs.collapse', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
<tr><td>hidden.bs.collapse</td><td>当折叠元素对用户隐藏时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#identifier').on('hidden.bs.collapse', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>show.bs.collapse</td><td>在调用 show 方法后触发该事件。</td><td>
<pre class="prettyprint">
$('#identifier').on('show.bs.collapse', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
<tr><td>shown.bs.collapse</td><td>当折叠元素对用户可见时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#identifier').on('shown.bs.collapse', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
<tr><td>hide.bs.collapse</td><td>当调用 hide 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#identifier').on('hide.bs.collapse', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
<tr><td>hidden.bs.collapse</td><td>当折叠元素对用户隐藏时触发该事件（将等待 CSS 过渡效果完成）。</td><td>
<pre class="prettyprint">
$('#identifier').on('hidden.bs.collapse', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
</table></div>') where ID=5074;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td>.carousel(options)</td><td>初始化轮播为可选的 options 对象，并开始循环项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel({
&nbsp;&nbsp;&nbsp;&nbsp;interval: 2000
})
</pre></td></tr>
<tr><td>.carousel('cycle')</td><td>从左到右循环轮播项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel('cycle')
</pre></td></tr>
<tr><td>.carousel('pause')</td><td>停止轮播循环项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel('pause')</pre></td></tr>
<tr><td>.carousel(number)</td><td>循环轮播到某个特定的帧（从 0 开始计数，与数组类似）。</td><td>
<pre class="prettyprint">
$('#identifier').carousel(number)</pre></td></tr>
<tr><td>.carousel('prev')</td><td>循环轮播到上一个项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel('prev')</pre></td></tr>
<tr><td>.carousel('next')</td><td>循环轮播到下一个项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel('next')</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>方法</th><th>描述</th><th>实例</th></tr>
<tr><td>.carousel(options)</td><td>初始化轮播为可选的 options 对象，并开始循环项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel({
&nbsp;&nbsp;&nbsp;&nbsp;interval: 2000
})
</pre></td></tr>
<tr><td>.carousel('cycle')</td><td>从左到右循环轮播项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel('cycle')
</pre></td></tr>
<tr><td>.carousel('pause')</td><td>停止轮播循环项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel('pause')</pre></td></tr>
<tr><td>.carousel(number)</td><td>循环轮播到某个特定的帧（从 0 开始计数，与数组类似）。</td><td>
<pre class="prettyprint">
$('#identifier').carousel(number)</pre></td></tr>
<tr><td>.carousel('prev')</td><td>循环轮播到上一个项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel('prev')</pre></td></tr>
<tr><td>.carousel('next')</td><td>循环轮播到下一个项目。</td><td>
<pre class="prettyprint">
$('#identifier').carousel('next')</pre></td></tr>
</table></div>') where ID=5075;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>slide.bs.carousel</td><td>当调用 slide 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#identifier').on('slide.bs.carousel', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
<tr><td>slid.bs.carousel</td><td>当轮播完成幻灯片过渡效果时触发该事件。</td><td>
<pre class="prettyprint">
$('#identifier').on('slid.bs.carousel', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th>事件</th><th>描述</th><th>实例</th></tr>
<tr><td>slide.bs.carousel</td><td>当调用 slide 实例方法时立即触发该事件。</td><td>
<pre class="prettyprint">
$('#identifier').on('slide.bs.carousel', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
<tr><td>slid.bs.carousel</td><td>当轮播完成幻灯片过渡效果时触发该事件。</td><td>
<pre class="prettyprint">
$('#identifier').on('slid.bs.carousel', function () {
&nbsp;&nbsp;&nbsp;&nbsp;// 执行一些动作...
})
</pre></td></tr>
</table></div>') where ID=5075;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th>运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>+</td><td>加 - 两个对象相加</td><td> a + b 输出结果 30</td>
</tr>
<tr>
<td>-</td><td>减 - 得到负数或是一个数减去另一个数</td><td> a - b 输出结果 -10</td>
</tr>
<tr>
<td>*</td><td>乘 - 两个数相乘或是返回一个被重复若干次的字符串</td><td> a * b 输出结果 200</td>
</tr>
<tr>
<td>/</td><td>除 - x除以y</td><td> b / a 输出结果 2</td>
</tr>
<tr>
<td>%</td><td>取模 - 返回除法的余数</td><td> b % a 输出结果 0</td>
</tr>
<tr>
<td>**</td><td>幂 - 返回x的y次幂</td><td> a**b 为10的20次方， 输出结果 100000000000000000000</td>
</tr>
<tr>
<td>//</td><td>取整除 - 返回商的整数部分（<strong>向下取整</strong>）</td><td> 
<pre>&gt;&gt;&gt; 9//2
4
&gt;&gt;&gt; -9//2
-5</pre></td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th>运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>+</td><td>加 - 两个对象相加</td><td> a + b 输出结果 30</td>
</tr>
<tr>
<td>-</td><td>减 - 得到负数或是一个数减去另一个数</td><td> a - b 输出结果 -10</td>
</tr>
<tr>
<td>*</td><td>乘 - 两个数相乘或是返回一个被重复若干次的字符串</td><td> a * b 输出结果 200</td>
</tr>
<tr>
<td>/</td><td>除 - x除以y</td><td> b / a 输出结果 2</td>
</tr>
<tr>
<td>%</td><td>取模 - 返回除法的余数</td><td> b % a 输出结果 0</td>
</tr>
<tr>
<td>**</td><td>幂 - 返回x的y次幂</td><td> a**b 为10的20次方， 输出结果 100000000000000000000</td>
</tr>
<tr>
<td>//</td><td>取整除 - 返回商的整数部分（<strong>向下取整</strong>）</td><td> 
<pre>&gt;&gt;&gt; 9//2
4
&gt;&gt;&gt; -9//2
-5</pre></td>
</tr>
</tbody></table></div>') where ID=5131;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">函数及描述</th></tr>
<tr><td>1</td><td><b>calendar.calendar(year,w=2,l=1,c=6)</b><br>返回一个多行字符串格式的year年年历，3个月一行，间隔距离为c。 每日宽度间隔为w字符。每行长度为21* W+18+2* C。l是每星期行数。</td></tr>
<tr><td>2</td><td><b>calendar.firstweekday( )</b><br>返回当前每周起始日期的设置。默认情况下，首次载入 calendar 模块时返回 0，即星期一。</td></tr>
<tr><td>3</td><td><b>calendar.isleap(year)</b><br><p>是闰年返回 True，否则为 False。</p>

<pre>&gt;&gt;&gt; import calendar
&gt;&gt;&gt; print(calendar.isleap(2000))
True
&gt;&gt;&gt; print(calendar.isleap(1900))
False</pre></td></tr>
<tr><td>4</td><td><b>calendar.leapdays(y1,y2)</b><br>返回在Y1，Y2两年之间的闰年总数。</td></tr>
<tr><td>5</td><td><b>calendar.month(year,month,w=2,l=1)</b><br>返回一个多行字符串格式的year年month月日历，两行标题，一周一行。每日宽度间隔为w字符。每行的长度为7* w+6。l是每星期的行数。</td></tr>
<tr><td>6</td><td><b>calendar.monthcalendar(year,month)</b><br>返回一个整数的单层嵌套列表。每个子列表装载代表一个星期的整数。Year年month月外的日期都设为0;范围内的日子都由该月第几日表示，从1开始。</td></tr>
<tr><td>7</td><td><b>calendar.monthrange(year,month)</b><br>返回两个整数。第一个是该月的星期几的日期码，第二个是该月的日期码。日从0（星期一）到6（星期日）;月从1到12。</td></tr>
<tr><td>8</td><td><b>calendar.prcal(year,w=2,l=1,c=6)</b><br>相当于 <strong>print calendar.calendar(year,w=2,l=1,c=6)</strong>。</td></tr>
<tr><td>9</td><td><b>calendar.prmonth(year,month,w=2,l=1)</b><br>相当于 <strong>print calendar.month(year,month,w=2,l=1)</strong> 。</td></tr>
<tr><td>10</td><td><b>calendar.setfirstweekday(weekday)</b><br>设置每周的起始日期码。0（星期一）到6（星期日）。</td></tr>
<tr><td>11</td><td><b>calendar.timegm(tupletime)</b><br>和time.gmtime相反：接受一个时间元组形式，返回该时刻的时间戳（1970纪元后经过的浮点秒数）。</td></tr>
<tr><td>12</td><td><b>calendar.weekday(year,month,day)</b><br>返回给定日期的日期码。0（星期一）到6（星期日）。月份为 1（一月） 到 12（12月）。</td></tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">函数及描述</th></tr>
<tr><td>1</td><td><b>calendar.calendar(year,w=2,l=1,c=6)</b><br>返回一个多行字符串格式的year年年历，3个月一行，间隔距离为c。 每日宽度间隔为w字符。每行长度为21* W+18+2* C。l是每星期行数。</td></tr>
<tr><td>2</td><td><b>calendar.firstweekday( )</b><br>返回当前每周起始日期的设置。默认情况下，首次载入 calendar 模块时返回 0，即星期一。</td></tr>
<tr><td>3</td><td><b>calendar.isleap(year)</b><br><p>是闰年返回 True，否则为 False。</p>

<pre>&gt;&gt;&gt; import calendar
&gt;&gt;&gt; print(calendar.isleap(2000))
True
&gt;&gt;&gt; print(calendar.isleap(1900))
False</pre></td></tr>
<tr><td>4</td><td><b>calendar.leapdays(y1,y2)</b><br>返回在Y1，Y2两年之间的闰年总数。</td></tr>
<tr><td>5</td><td><b>calendar.month(year,month,w=2,l=1)</b><br>返回一个多行字符串格式的year年month月日历，两行标题，一周一行。每日宽度间隔为w字符。每行的长度为7* w+6。l是每星期的行数。</td></tr>
<tr><td>6</td><td><b>calendar.monthcalendar(year,month)</b><br>返回一个整数的单层嵌套列表。每个子列表装载代表一个星期的整数。Year年month月外的日期都设为0;范围内的日子都由该月第几日表示，从1开始。</td></tr>
<tr><td>7</td><td><b>calendar.monthrange(year,month)</b><br>返回两个整数。第一个是该月的星期几的日期码，第二个是该月的日期码。日从0（星期一）到6（星期日）;月从1到12。</td></tr>
<tr><td>8</td><td><b>calendar.prcal(year,w=2,l=1,c=6)</b><br>相当于 <strong>print calendar.calendar(year,w=2,l=1,c=6)</strong>。</td></tr>
<tr><td>9</td><td><b>calendar.prmonth(year,month,w=2,l=1)</b><br>相当于 <strong>print calendar.month(year,month,w=2,l=1)</strong> 。</td></tr>
<tr><td>10</td><td><b>calendar.setfirstweekday(weekday)</b><br>设置每周的起始日期码。0（星期一）到6（星期日）。</td></tr>
<tr><td>11</td><td><b>calendar.timegm(tupletime)</b><br>和time.gmtime相反：接受一个时间元组形式，返回该时刻的时间戳（1970纪元后经过的浮点秒数）。</td></tr>
<tr><td>12</td><td><b>calendar.weekday(year,month,day)</b><br>返回给定日期的日期码。0（星期一）到6（星期日）。月份为 1（一月） 到 12（12月）。</td></tr>
</tbody></table></div>') where ID=5175;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr><th width="10%">数字</th><th width="30%">格式</th><th width="30%">输出
</th><th width="30%">描述</th></tr>
<tr><td> 3.1415926 </td>
    <td> {:.2f} </td>
    <td> 3.14 </td>
    <td> 保留小数点后两位 </td>
</tr>
<tr><td> 3.1415926 </td>
    <td> {:+.2f} </td>
    <td> +3.14 </td>
    <td> 带符号保留小数点后两位 </td>
</tr>
<tr><td> -1 </td>
    <td> {:+.2f} </td>
    <td> -1.00 </td>
    <td> 带符号保留小数点后两位 </td>
</tr>
<tr><td> 2.71828 </td>
    <td> {:.0f} </td>
    <td> 3 </td>
    <td> 不带小数 </td>
</tr>
<tr><td> 5 </td>
    <td> {:0&gt;2d} </td>
    <td> 05 </td>
    <td> 数字补零 (填充左边, 宽度为2) </td>
</tr>
<tr><td> 5 </td>
    <td> {:x&lt;4d} </td>
    <td> 5xxx </td>
    <td> 数字补x (填充右边, 宽度为4) </td>
</tr>
<tr><td> 10 </td>
    <td> {:x&lt;4d} </td>
    <td> 10xx </td>
    <td> 数字补x (填充右边, 宽度为4) </td>
</tr>
<tr><td> 1000000 </td>
    <td> {:,} </td>
    <td> 1,000,000 </td>
    <td> 以逗号分隔的数字格式 </td>
</tr>
<tr><td> 0.25 </td>
    <td> {:.2%} </td>
    <td> 25.00% </td>
    <td> 百分比格式 </td>
</tr>
<tr><td> 1000000000 </td>
    <td> {:.2e} </td>
    <td> 1.00e+09 </td>
    <td> 指数记法 </td>
</tr>
<tr><td> 13 </td>
    <td> {:&gt;10d} </td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;13</td>
    <td> 右对齐 (默认, 宽度为10) </td>
</tr>
<tr><td> 13 </td>
    <td> {:&lt;10d} </td>
    <td> 13 </td>
    <td> 左对齐 (宽度为10)</td>
</tr>
<tr><td> 13 </td>
    <td> {:^10d} </td>
    <td> &nbsp;&nbsp;&nbsp;&nbsp;13 </td>
    <td> 中间对齐 (宽度为10) </td>
</tr>
<tr><td> 11 </td>
    <td><pre>'{:b}'.format(11)
'{:d}'.format(11)
'{:o}'.format(11)
'{:x}'.format(11)
'{:#x}'.format(11)
'{:#X}'.format(11)</pre></td>
    <td><pre>1011
11
13
b
0xb
0XB
</pre></td>
    <td> 进制</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr><th width="10%">数字</th><th width="30%">格式</th><th width="30%">输出
</th><th width="30%">描述</th></tr>
<tr><td> 3.1415926 </td>
    <td> {:.2f} </td>
    <td> 3.14 </td>
    <td> 保留小数点后两位 </td>
</tr>
<tr><td> 3.1415926 </td>
    <td> {:+.2f} </td>
    <td> +3.14 </td>
    <td> 带符号保留小数点后两位 </td>
</tr>
<tr><td> -1 </td>
    <td> {:+.2f} </td>
    <td> -1.00 </td>
    <td> 带符号保留小数点后两位 </td>
</tr>
<tr><td> 2.71828 </td>
    <td> {:.0f} </td>
    <td> 3 </td>
    <td> 不带小数 </td>
</tr>
<tr><td> 5 </td>
    <td> {:0&gt;2d} </td>
    <td> 05 </td>
    <td> 数字补零 (填充左边, 宽度为2) </td>
</tr>
<tr><td> 5 </td>
    <td> {:x&lt;4d} </td>
    <td> 5xxx </td>
    <td> 数字补x (填充右边, 宽度为4) </td>
</tr>
<tr><td> 10 </td>
    <td> {:x&lt;4d} </td>
    <td> 10xx </td>
    <td> 数字补x (填充右边, 宽度为4) </td>
</tr>
<tr><td> 1000000 </td>
    <td> {:,} </td>
    <td> 1,000,000 </td>
    <td> 以逗号分隔的数字格式 </td>
</tr>
<tr><td> 0.25 </td>
    <td> {:.2%} </td>
    <td> 25.00% </td>
    <td> 百分比格式 </td>
</tr>
<tr><td> 1000000000 </td>
    <td> {:.2e} </td>
    <td> 1.00e+09 </td>
    <td> 指数记法 </td>
</tr>
<tr><td> 13 </td>
    <td> {:&gt;10d} </td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;13</td>
    <td> 右对齐 (默认, 宽度为10) </td>
</tr>
<tr><td> 13 </td>
    <td> {:&lt;10d} </td>
    <td> 13 </td>
    <td> 左对齐 (宽度为10)</td>
</tr>
<tr><td> 13 </td>
    <td> {:^10d} </td>
    <td> &nbsp;&nbsp;&nbsp;&nbsp;13 </td>
    <td> 中间对齐 (宽度为10) </td>
</tr>
<tr><td> 11 </td>
    <td><pre>'{:b}'.format(11)
'{:d}'.format(11)
'{:o}'.format(11)
'{:x}'.format(11)
'{:#x}'.format(11)
'{:#X}'.format(11)</pre></td>
    <td><pre>1011
11
13
b
0xb
0XB
</pre></td>
    <td> 进制</td>
</tr>
</tbody></table></div>') where ID=5459;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center;width:10%">序号</th>
<th style="text-align:center;">方法 &amp; 描述</th>
</tr>
<tr>
<td class="ts">1</td>
<td><p><b>delete ( first, last=None )</b></p>
<p>删除文本框里直接位置值</p>
<pre>
text.delete(10)      # 删除索引值为10的值
text.delete(10, 20)  # 删除索引值从10到20之前的值
text.delete(0, END)  # 删除所有值
</pre>

</td>
</tr>
<tr>
<td class="ts">2</td>
<td><p><b>get()</b></p>
<p>获取文件框的值</p></td>
</tr>
<tr>
<td class="ts">3</td>
<td><p><b>icursor ( index )</b></p>
<p>将光标移动到指定索引位置，只有当文框获取焦点后成立</p></td>
</tr>
<tr>
<td class="ts">4</td>
<td><p><b>index ( index )</b></p>
<p>返回指定的索引值</p></td>
</tr>
<tr>
<td class="ts">5</td>
<td><p><b>insert ( index, s )</b></p>
<p>向文本框中插入值，index：插入位置，s：插入值</p></td>
</tr>
<tr>
<td class="ts">6</td>
<td><p><b>select_adjust ( index )</b></p>
<p>选中指定索引和光标所在位置之前的值 </p></td>
</tr>
<tr>
<td class="ts">7</td>
<td><p><b>select_clear()</b></p>
<p>清空文本框</p></td>
</tr>
<tr>
<td class="ts">8</td>
<td><p><b>select_from ( index )</b></p>
<p>设置光标的位置，通过索引值 index 来设置</p></td>
</tr>
<tr>
<td class="ts">9</td>
<td><p><b>select_present()</b></p>
<p>如果有选中，返回 true，否则返回 false。</p></td>
</tr>
<tr>
<td class="ts">10</td>
<td><p><b>select_range ( start, end )</b></p>
<p>选中指定索引位置的值，start(包含) 为开始位置，end(不包含) 为结束位置start必须比end小

</p></td>
</tr>
<tr>
<td class="ts">11</td>
<td><p><b>select_to ( index )</b></p>
<p>选中指定索引与光标之间的值</p></td>
</tr>
<tr>
<td class="ts">12</td>
<td><p><b>xview ( index )</b></p>
<p>该方法在文本框链接到水平滚动条上很有用。</p></td>
</tr>
<tr>
<td class="ts">13</td>
<td><p><b>xview_scroll ( number, what )</b></p>
<p>用于水平滚动文本框。 what 参数可以是 UNITS, 按字符宽度滚动，或者可以是 PAGES, 按文本框组件块滚动。  number 参数，正数为由左到右滚动，负数为由右到左滚动。</p></td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center;width:10%">序号</th>
<th style="text-align:center;">方法 &amp; 描述</th>
</tr>
<tr>
<td class="ts">1</td>
<td><p><b>delete ( first, last=None )</b></p>
<p>删除文本框里直接位置值</p>
<pre>
text.delete(10)      # 删除索引值为10的值
text.delete(10, 20)  # 删除索引值从10到20之前的值
text.delete(0, END)  # 删除所有值
</pre>

</td>
</tr>
<tr>
<td class="ts">2</td>
<td><p><b>get()</b></p>
<p>获取文件框的值</p></td>
</tr>
<tr>
<td class="ts">3</td>
<td><p><b>icursor ( index )</b></p>
<p>将光标移动到指定索引位置，只有当文框获取焦点后成立</p></td>
</tr>
<tr>
<td class="ts">4</td>
<td><p><b>index ( index )</b></p>
<p>返回指定的索引值</p></td>
</tr>
<tr>
<td class="ts">5</td>
<td><p><b>insert ( index, s )</b></p>
<p>向文本框中插入值，index：插入位置，s：插入值</p></td>
</tr>
<tr>
<td class="ts">6</td>
<td><p><b>select_adjust ( index )</b></p>
<p>选中指定索引和光标所在位置之前的值 </p></td>
</tr>
<tr>
<td class="ts">7</td>
<td><p><b>select_clear()</b></p>
<p>清空文本框</p></td>
</tr>
<tr>
<td class="ts">8</td>
<td><p><b>select_from ( index )</b></p>
<p>设置光标的位置，通过索引值 index 来设置</p></td>
</tr>
<tr>
<td class="ts">9</td>
<td><p><b>select_present()</b></p>
<p>如果有选中，返回 true，否则返回 false。</p></td>
</tr>
<tr>
<td class="ts">10</td>
<td><p><b>select_range ( start, end )</b></p>
<p>选中指定索引位置的值，start(包含) 为开始位置，end(不包含) 为结束位置start必须比end小

</p></td>
</tr>
<tr>
<td class="ts">11</td>
<td><p><b>select_to ( index )</b></p>
<p>选中指定索引与光标之间的值</p></td>
</tr>
<tr>
<td class="ts">12</td>
<td><p><b>xview ( index )</b></p>
<p>该方法在文本框链接到水平滚动条上很有用。</p></td>
</tr>
<tr>
<td class="ts">13</td>
<td><p><b>xview_scroll ( number, what )</b></p>
<p>用于水平滚动文本框。 what 参数可以是 UNITS, 按字符宽度滚动，或者可以是 PAGES, 按文本框组件块滚动。  number 参数，正数为由左到右滚动，负数为由右到左滚动。</p></td>
</tr>
</tbody></table></div>') where ID=5532;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>proxy</td>
    <td>string,function</td>
    <td>拖动时要使用的代理元素，设置为 'clone' 时，克隆元素将被用作代理。如果指定一个函数，它必须返回一个 jQuery 对象。<br>
	下面的实例演示了如何创建简单的代理对象。
<pre style="color:#006600">
$('.dragitem').draggable({
&nbsp;&nbsp;&nbsp;&nbsp;proxy: function(source){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var p = $('&lt;div style="border:1px solid #ccc;width:80px"&gt;&lt;/div&gt;');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p.html($(source).html()).appendTo('body');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return p;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>revert</td>
    <td>boolean</td>
    <td>如果设置为 true，拖动结束后元素将返回它的开始位置。</td>
	<td>false</td>
</tr>
<tr>
	<td>cursor</td>
    <td>string</td>
    <td>拖动时的 css 光标（cursor）。</td>
	<td>move</td>
</tr>
<tr>
	<td>deltaX</td>
    <td>number</td>
    <td>拖动的元素相对于当前光标的 X 轴位置。</td>
	<td>null</td>
</tr>
<tr>
	<td>deltaY</td>
    <td>number</td>
    <td>拖动的元素相对于当前光标的 Y 轴位置。</td>
	<td>null</td>
</tr>
<tr>
	<td>handle</td>
    <td>selector</td>
    <td>启动可拖动（draggable）的处理（handle）。</td>
	<td>null</td>
</tr>
<tr>
	<td>disabled</td>
    <td>boolean</td>
    <td>如果设置为 true，则停止可拖动（draggable）。</td>
	<td>false</td>
</tr>
<tr>
	<td>edge</td>
    <td>number</td>
    <td>能够在其中开始可拖动（draggable）的拖动宽度。</td>
	<td>0</td>
</tr>
<tr>
	<td>axis</td>
    <td>string</td>
    <td>定义拖动元素可在其上移动的轴，可用的值是 'v' 或 'h'，当设为 null，将会沿着 'v' 和 'h' 的方向移动。</td>
	<td>null</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>proxy</td>
    <td>string,function</td>
    <td>拖动时要使用的代理元素，设置为 'clone' 时，克隆元素将被用作代理。如果指定一个函数，它必须返回一个 jQuery 对象。<br>
	下面的实例演示了如何创建简单的代理对象。
<pre style="color:#006600">
$('.dragitem').draggable({
&nbsp;&nbsp;&nbsp;&nbsp;proxy: function(source){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var p = $('&lt;div style="border:1px solid #ccc;width:80px"&gt;&lt;/div&gt;');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p.html($(source).html()).appendTo('body');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return p;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>revert</td>
    <td>boolean</td>
    <td>如果设置为 true，拖动结束后元素将返回它的开始位置。</td>
	<td>false</td>
</tr>
<tr>
	<td>cursor</td>
    <td>string</td>
    <td>拖动时的 css 光标（cursor）。</td>
	<td>move</td>
</tr>
<tr>
	<td>deltaX</td>
    <td>number</td>
    <td>拖动的元素相对于当前光标的 X 轴位置。</td>
	<td>null</td>
</tr>
<tr>
	<td>deltaY</td>
    <td>number</td>
    <td>拖动的元素相对于当前光标的 Y 轴位置。</td>
	<td>null</td>
</tr>
<tr>
	<td>handle</td>
    <td>selector</td>
    <td>启动可拖动（draggable）的处理（handle）。</td>
	<td>null</td>
</tr>
<tr>
	<td>disabled</td>
    <td>boolean</td>
    <td>如果设置为 true，则停止可拖动（draggable）。</td>
	<td>false</td>
</tr>
<tr>
	<td>edge</td>
    <td>number</td>
    <td>能够在其中开始可拖动（draggable）的拖动宽度。</td>
	<td>0</td>
</tr>
<tr>
	<td>axis</td>
    <td>string</td>
    <td>定义拖动元素可在其上移动的轴，可用的值是 'v' 或 'h'，当设为 null，将会沿着 'v' 和 'h' 的方向移动。</td>
	<td>null</td>
</tr>
</table></div>') where ID=5653;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>total</td>
    <td>number</td>
    <td>记录总数，应该在创建分页（pagination）时设置。</td>
	<td>1</td>
</tr>
<tr>
	<td>pageSize</td>
    <td>number</td>
    <td>页面尺寸。（注：每页显示的最大记录数）</td>
	<td>10</td>
</tr>
<tr>
	<td>pageNumber</td>
    <td>number</td>
    <td>创建分页（pagination）时显示的页码。</td>
	<td>1</td>
</tr>
<tr>
	<td>pageList</td>
    <td>array</td>
    <td>用户能改变页面尺寸。pageList 属性定义了能改成多大的尺寸。<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination({
&nbsp;&nbsp;&nbsp;&nbsp;pageList: [10,20,50,100]
});
</pre>
	</td>
	<td>[10,20,30,50]</td>
</tr>
<tr>
	<td>loading</td>
    <td>boolean</td>
    <td>定义数据是否正在加载。</td>
	<td>false</td>
</tr>
<tr>
	<td>buttons</td>
    <td>array,selector</td>
    <td>定义自定义按钮，可能的值：<br>
		1、数组，每个按钮包含两个属性：<br>
		iconCls：CSS class，它将显示一个背景图片<br>
		handler：当按钮被点击时的处理函数<br>
		2、选择器，指示按钮。<br>
		<br>
		按钮可通过标记声明：
<pre style="color:#006600">
&lt;div class="easyui-pagination" style="border:1px solid #ccc" data-options="
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;total: 114,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buttons: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-add',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('add')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},'-',{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-save',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('save')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]"&gt;
&lt;/div&gt;
</pre>
		按钮也可以使用 javascript 创建：
<pre style="color:#006600">
$('#pp').pagination({
&nbsp;&nbsp;&nbsp;&nbsp;total: 114,
&nbsp;&nbsp;&nbsp;&nbsp;buttons: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-add',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('add')}
&nbsp;&nbsp;&nbsp;&nbsp;},'-',{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-save',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('save')}
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>layout</td>
    <td>array</td>
    <td>分页布局定义。该属性自版本 1.3.5 起可用。<br>
		布局项目包括一个或多个下列值：<br>
		1、list：页面尺寸列表。<br>
		2、sep：页面按钮分割。<br>
		3、first：第一个按钮。<br>
		4、prev：前一个按钮。<br>
		5、next：后一个按钮。<br>
		6、last：最后一个按钮。<br>
		7、efresh：刷新按钮。<br>
		8、manual：允许输入域页码的手动页码输入框。<br>
		9、links：页码链接。<br>
		<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination({
&nbsp;&nbsp;&nbsp;&nbsp;layout:['first','links','last']
});
</pre>
	</td>
	<td> </td>
</tr>
<tr>
	<td>links</td>
    <td>number</td>
    <td>链接数量，只有当 'links' 项包含在 'layout' 中时才是有效的。该属性自版本 1.3.5 起可用。</td>
	<td>10</td>
</tr>
<tr>
	<td>showPageList</td>
    <td>boolean</td>
    <td>定义是否显示页面列表。</td>
	<td>true</td>
</tr>
<tr>
	<td>showRefresh</td>
    <td>boolean</td>
    <td>定义是否显示刷新按钮。</td>
	<td>true</td>
</tr>
<tr>
	<td>beforePageText</td>
    <td>string</td>
    <td>在 input 组件之前显示 label。</td>
	<td>Page</td>
</tr>
<tr>
	<td>afterPageText</td>
    <td>string</td>
    <td>在 input 组件之后显示 label。</td>
	<td>of {pages}</td>
</tr>
<tr>
	<td>displayMsg</td>
    <td>string</td>
    <td>Display a page information.</td>
	<td>显示 {from} to {to} of {total} 页面信息。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>total</td>
    <td>number</td>
    <td>记录总数，应该在创建分页（pagination）时设置。</td>
	<td>1</td>
</tr>
<tr>
	<td>pageSize</td>
    <td>number</td>
    <td>页面尺寸。（注：每页显示的最大记录数）</td>
	<td>10</td>
</tr>
<tr>
	<td>pageNumber</td>
    <td>number</td>
    <td>创建分页（pagination）时显示的页码。</td>
	<td>1</td>
</tr>
<tr>
	<td>pageList</td>
    <td>array</td>
    <td>用户能改变页面尺寸。pageList 属性定义了能改成多大的尺寸。<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination({
&nbsp;&nbsp;&nbsp;&nbsp;pageList: [10,20,50,100]
});
</pre>
	</td>
	<td>[10,20,30,50]</td>
</tr>
<tr>
	<td>loading</td>
    <td>boolean</td>
    <td>定义数据是否正在加载。</td>
	<td>false</td>
</tr>
<tr>
	<td>buttons</td>
    <td>array,selector</td>
    <td>定义自定义按钮，可能的值：<br>
		1、数组，每个按钮包含两个属性：<br>
		iconCls：CSS class，它将显示一个背景图片<br>
		handler：当按钮被点击时的处理函数<br>
		2、选择器，指示按钮。<br>
		<br>
		按钮可通过标记声明：
<pre style="color:#006600">
&lt;div class="easyui-pagination" style="border:1px solid #ccc" data-options="
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;total: 114,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buttons: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-add',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('add')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},'-',{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-save',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('save')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]"&gt;
&lt;/div&gt;
</pre>
		按钮也可以使用 javascript 创建：
<pre style="color:#006600">
$('#pp').pagination({
&nbsp;&nbsp;&nbsp;&nbsp;total: 114,
&nbsp;&nbsp;&nbsp;&nbsp;buttons: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-add',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('add')}
&nbsp;&nbsp;&nbsp;&nbsp;},'-',{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-save',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('save')}
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>layout</td>
    <td>array</td>
    <td>分页布局定义。该属性自版本 1.3.5 起可用。<br>
		布局项目包括一个或多个下列值：<br>
		1、list：页面尺寸列表。<br>
		2、sep：页面按钮分割。<br>
		3、first：第一个按钮。<br>
		4、prev：前一个按钮。<br>
		5、next：后一个按钮。<br>
		6、last：最后一个按钮。<br>
		7、efresh：刷新按钮。<br>
		8、manual：允许输入域页码的手动页码输入框。<br>
		9、links：页码链接。<br>
		<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination({
&nbsp;&nbsp;&nbsp;&nbsp;layout:['first','links','last']
});
</pre>
	</td>
	<td> </td>
</tr>
<tr>
	<td>links</td>
    <td>number</td>
    <td>链接数量，只有当 'links' 项包含在 'layout' 中时才是有效的。该属性自版本 1.3.5 起可用。</td>
	<td>10</td>
</tr>
<tr>
	<td>showPageList</td>
    <td>boolean</td>
    <td>定义是否显示页面列表。</td>
	<td>true</td>
</tr>
<tr>
	<td>showRefresh</td>
    <td>boolean</td>
    <td>定义是否显示刷新按钮。</td>
	<td>true</td>
</tr>
<tr>
	<td>beforePageText</td>
    <td>string</td>
    <td>在 input 组件之前显示 label。</td>
	<td>Page</td>
</tr>
<tr>
	<td>afterPageText</td>
    <td>string</td>
    <td>在 input 组件之后显示 label。</td>
	<td>of {pages}</td>
</tr>
<tr>
	<td>displayMsg</td>
    <td>string</td>
    <td>Display a page information.</td>
	<td>显示 {from} to {to} of {total} 页面信息。</td>
</tr>
</table></div>') where ID=5656;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onSelectPage</td>
    <td>pageNumber, pageSize</td>
    <td>当用户选择新的页面时触发。回调函数包含两个参数：<br>
		pageNumber：新的页码<br>
		pageSize：新的页面尺寸<br>
		<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination({
&nbsp;&nbsp;&nbsp;&nbsp;onSelectPage:function(pageNumber, pageSize){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(this).pagination('loading');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('pageNumber:'+pageNumber+',pageSize:'+pageSize);
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(this).pagination('loaded');
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>onBeforeRefresh</td>
    <td>pageNumber, pageSize</td>
    <td>刷新按钮点击之前触发，返回 false 就取消刷新动作。</td>
</tr>
<tr>
	<td>onRefresh</td>
    <td>pageNumber, pageSize</td>
    <td>刷新之后触发。</td>
</tr>
<tr>
	<td>onChangePageSize</td>
    <td>pageSize</td>
    <td>当用户改变页面尺寸时触发。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onSelectPage</td>
    <td>pageNumber, pageSize</td>
    <td>当用户选择新的页面时触发。回调函数包含两个参数：<br>
		pageNumber：新的页码<br>
		pageSize：新的页面尺寸<br>
		<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination({
&nbsp;&nbsp;&nbsp;&nbsp;onSelectPage:function(pageNumber, pageSize){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(this).pagination('loading');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('pageNumber:'+pageNumber+',pageSize:'+pageSize);
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(this).pagination('loaded');
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>onBeforeRefresh</td>
    <td>pageNumber, pageSize</td>
    <td>刷新按钮点击之前触发，返回 false 就取消刷新动作。</td>
</tr>
<tr>
	<td>onRefresh</td>
    <td>pageNumber, pageSize</td>
    <td>刷新之后触发。</td>
</tr>
<tr>
	<td>onChangePageSize</td>
    <td>pageSize</td>
    <td>当用户改变页面尺寸时触发。</td>
</tr>
</table></div>') where ID=5656;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>loading</td>
    <td>none</td>
    <td>把分页（pagination）变成正在加载（loading）状态。</td>
</tr>
<tr>
	<td>loaded</td>
    <td>none</td>
    <td>把分页（pagination）变成加载完成（loaded）状态。</td>
</tr>
<tr>
	<td>refresh</td>
    <td>options</td>
    <td>刷新并显示分页信息。该方法自版本 1.3 起可用。<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination('refresh');&nbsp;&nbsp;&nbsp;&nbsp;// 刷新分页栏信息
$('#pp').pagination('refresh',{&nbsp;&nbsp;&nbsp;&nbsp;// 改变选项，并刷新分页栏信息
&nbsp;&nbsp;&nbsp;&nbsp;total: 114,
&nbsp;&nbsp;&nbsp;&nbsp;pageNumber: 6
});
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>page</td>
    <td>选择一个新页面。页面索引从 1 开始。该方法从版本 1.3 起可用。<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination('select');&nbsp;&nbsp;&nbsp;&nbsp;// 刷新当前页面
$('#pp').pagination('select', 2);&nbsp;&nbsp;&nbsp;&nbsp;// 选择第二页
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>loading</td>
    <td>none</td>
    <td>把分页（pagination）变成正在加载（loading）状态。</td>
</tr>
<tr>
	<td>loaded</td>
    <td>none</td>
    <td>把分页（pagination）变成加载完成（loaded）状态。</td>
</tr>
<tr>
	<td>refresh</td>
    <td>options</td>
    <td>刷新并显示分页信息。该方法自版本 1.3 起可用。<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination('refresh');&nbsp;&nbsp;&nbsp;&nbsp;// 刷新分页栏信息
$('#pp').pagination('refresh',{&nbsp;&nbsp;&nbsp;&nbsp;// 改变选项，并刷新分页栏信息
&nbsp;&nbsp;&nbsp;&nbsp;total: 114,
&nbsp;&nbsp;&nbsp;&nbsp;pageNumber: 6
});
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>page</td>
    <td>选择一个新页面。页面索引从 1 开始。该方法从版本 1.3 起可用。<br>
		代码实例：
<pre style="color:#006600">
$('#pp').pagination('select');&nbsp;&nbsp;&nbsp;&nbsp;// 刷新当前页面
$('#pp').pagination('select', 2);&nbsp;&nbsp;&nbsp;&nbsp;// 选择第二页
</pre>
	</td>
</tr>
</table></div>') where ID=5656;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>组件的宽度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>组件的高度。该属性自版本 1.3.2 起可用。</td>
	<td>22</td>
</tr>
<tr>
	<td>prompt</td>
    <td>string</td>
    <td>显示在输入框里的提示信息。</td>
	<td>''</td>
</tr>
<tr>
	<td>value</td>
    <td>string</td>
    <td>输入的值。</td>
	<td>''</td>
</tr>
<tr>
	<td>menu</td>
    <td>selector</td>
    <td>搜索类型的菜单。每个菜单项可以有下列的属性：<br>
		name：搜索类型名称。<br>
		selected：当前选择的搜索类型名称。<br>
		<br>
		下面的实例演示了如何定义一个选中的搜索类型名称。
<pre style="color:#006600">
&lt;input class="easyui-searchbox" style="width:300px" data-options="menu:'#mm'" /&gt;
&lt;div id="mm" style="width:150px"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'item1'"&gt;Search Item1&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'item2',selected:true"&gt;Search Item2&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'item3'"&gt;Search Item3&lt;/div&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>searcher</td>
    <td>function(value,name)</td>
    <td>当用户按下搜索按钮或者按下 ENTER 键时，searcher 函数将被调用。</td>
	<td>null</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>组件的宽度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>组件的高度。该属性自版本 1.3.2 起可用。</td>
	<td>22</td>
</tr>
<tr>
	<td>prompt</td>
    <td>string</td>
    <td>显示在输入框里的提示信息。</td>
	<td>''</td>
</tr>
<tr>
	<td>value</td>
    <td>string</td>
    <td>输入的值。</td>
	<td>''</td>
</tr>
<tr>
	<td>menu</td>
    <td>selector</td>
    <td>搜索类型的菜单。每个菜单项可以有下列的属性：<br>
		name：搜索类型名称。<br>
		selected：当前选择的搜索类型名称。<br>
		<br>
		下面的实例演示了如何定义一个选中的搜索类型名称。
<pre style="color:#006600">
&lt;input class="easyui-searchbox" style="width:300px" data-options="menu:'#mm'" /&gt;
&lt;div id="mm" style="width:150px"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'item1'"&gt;Search Item1&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'item2',selected:true"&gt;Search Item2&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'item3'"&gt;Search Item3&lt;/div&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>searcher</td>
    <td>function(value,name)</td>
    <td>当用户按下搜索按钮或者按下 ENTER 键时，searcher 函数将被调用。</td>
	<td>null</td>
</tr>
</table></div>') where ID=5657;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>menu</td>
    <td>none</td>
    <td>返回搜索类型的菜单对象。<br>
	下面的实例演示如何改变菜单项图标。
<pre style="color:#006600">
var m = $('#ss').searchbox('menu');  // get the menu object
var item = m.menu('findItem', 'Sports News');  // find the menu item
// change the menu item icon
m.menu('setIcon', {
&nbsp;&nbsp;&nbsp;&nbsp;target: item.target,
&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-save'
});
// select the searching type name
$('#ss').searchbox('selectName', 'sports');
</pre>
	</td>
</tr>
<tr>
	<td>textbox</td>
    <td>none</td>
    <td>返回文本框对象。</td>
</tr>
<tr>
	<td>getValue</td>
    <td>none</td>
    <td>返回当前的搜索值。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置新的搜索值。</td>
</tr>
<tr>
	<td>getName</td>
    <td>none</td>
    <td>返回当前的搜索类型名称。</td>
</tr>
<tr>
	<td>selectName</td>
    <td>name</td>
    <td>选择当前的搜索类型名称。<br>
		代码实例：
<pre style="color:#006600">
$('#ss').searchbox('selectName', 'sports');
</pre>
	</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁该组件。</td>
</tr>
<tr>
	<td>resize</td>
    <td>width</td>
    <td>重设组件的宽度。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>menu</td>
    <td>none</td>
    <td>返回搜索类型的菜单对象。<br>
	下面的实例演示如何改变菜单项图标。
<pre style="color:#006600">
var m = $('#ss').searchbox('menu');  // get the menu object
var item = m.menu('findItem', 'Sports News');  // find the menu item
// change the menu item icon
m.menu('setIcon', {
&nbsp;&nbsp;&nbsp;&nbsp;target: item.target,
&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-save'
});
// select the searching type name
$('#ss').searchbox('selectName', 'sports');
</pre>
	</td>
</tr>
<tr>
	<td>textbox</td>
    <td>none</td>
    <td>返回文本框对象。</td>
</tr>
<tr>
	<td>getValue</td>
    <td>none</td>
    <td>返回当前的搜索值。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置新的搜索值。</td>
</tr>
<tr>
	<td>getName</td>
    <td>none</td>
    <td>返回当前的搜索类型名称。</td>
</tr>
<tr>
	<td>selectName</td>
    <td>name</td>
    <td>选择当前的搜索类型名称。<br>
		代码实例：
<pre style="color:#006600">
$('#ss').searchbox('selectName', 'sports');
</pre>
	</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁该组件。</td>
</tr>
<tr>
	<td>resize</td>
    <td>width</td>
    <td>重设组件的宽度。</td>
</tr>
</table></div>') where ID=5657;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onChange</td>
    <td>newValue,oldValue</td>
    <td>当值改变时触发。<br>
	代码实例：
<pre style="color:#006600">
$('#p').progressbar({
&nbsp;&nbsp;&nbsp;&nbsp;onChange: function(value){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(value)
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onChange</td>
    <td>newValue,oldValue</td>
    <td>当值改变时触发。<br>
	代码实例：
<pre style="color:#006600">
$('#p').progressbar({
&nbsp;&nbsp;&nbsp;&nbsp;onChange: function(value){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(value)
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
</table></div>') where ID=5658;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>resize</td>
    <td>width</td>
    <td>调整组件尺寸。<br>
	代码实例：
<pre style="color:#006600">
$('#p').progressbar('resize');  // 调整进度条为初始宽度
$('#p').progressbar('resize', 350);  // 调整进度条为一个新的宽度
</pre>
	</td>
</tr>
<tr>
	<td>getValue</td>
    <td>none</td>
    <td>返回当前的进度值。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置一个新的进度值。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>resize</td>
    <td>width</td>
    <td>调整组件尺寸。<br>
	代码实例：
<pre style="color:#006600">
$('#p').progressbar('resize');  // 调整进度条为初始宽度
$('#p').progressbar('resize', 350);  // 调整进度条为一个新的宽度
</pre>
	</td>
</tr>
<tr>
	<td>getValue</td>
    <td>none</td>
    <td>返回当前的进度值。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置一个新的进度值。</td>
</tr>
</table></div>') where ID=5658;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>id</td>
    <td>string</td>
    <td>面板（panel）的 id 属性。</td>
	<td>null</td>
</tr>
<tr>
	<td>title</td>
    <td>string</td>
    <td>显示在面板（panel）头部的标题文字。</td>
	<td>null</td>
</tr>
<tr>
	<td>iconCls</td>
    <td>string</td>
    <td>在面板（panel）里显示一个 16x16 图标的 CSS class。</td>
	<td>null</td>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>设置面板（panel）的宽度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>设置面板（panel）的高度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>left</td>
    <td>number</td>
    <td>设置面板（panel）的左边位置。</td>
	<td>null</td>
</tr>
<tr>
	<td>top</td>
    <td>number</td>
    <td>设置面板（panel）的顶部位置。</td>
	<td>null</td>
</tr>
<tr>
	<td>cls</td>
    <td>string</td>
    <td>给面板（panel）添加一个 CSS class。</td>
	<td>null</td>
</tr>
<tr>
	<td>headerCls</td>
    <td>string</td>
    <td>给面板（panel）头部添加一个 CSS class。</td>
	<td>null</td>
</tr>
<tr>
	<td>bodyCls</td>
    <td>string</td>
    <td>给面板（panel）主体添加一个 CSS class。</td>
	<td>null</td>
</tr>
<tr>
	<td>style</td>
    <td>object</td>
    <td>给面板（panel）添加自定义格式的样式。<br>
	改变面板（panel）边框宽度的代码实例：
<pre style="color:#006600">
&lt;div class="easyui-panel" style="width:200px;height:100px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="style:{borderWidth:2}"&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>{}</td>
</tr>
<tr>
	<td>fit</td>
    <td>boolean</td>
    <td>当设置为 true 时，面板（panel）的尺寸就适应它的父容器。下面的实例演示了自动调整尺寸到它的父容器的最大内部尺寸的面板（panel）。
<pre style="color:#006600">
&lt;div style="width:200px;height:100px;padding:5px"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div class="easyui-panel" style="width:200px;height:100px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="fit:true,border:false"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Embedded Panel
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>false</td>
</tr>
<tr>
	<td>border</td>
    <td>boolean</td>
    <td>定义了是否显示面板（panel）的边框。</td>
	<td>true</td>
</tr>
<tr>
	<td>doSize</td>
    <td>boolean</td>
    <td>如果设置为 true，创建时面板（panel）就调整尺寸并做成布局。</td>
	<td>true</td>
</tr>
<tr>
	<td>noheader</td>
    <td>boolean</td>
    <td>如果设置为 true，面板（panel）的头部将不会被创建。</td>
	<td>false</td>
</tr>
<tr>
	<td>content</td>
    <td>string</td>
    <td>面板（panel）主体内容。</td>
	<td>null</td>
</tr>
<tr>
	<td>collapsible</td>
    <td>boolean</td>
    <td>定义是否显示折叠按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>minimizable</td>
    <td>boolean</td>
    <td>定义是否显示最小化按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>maximizable</td>
    <td>boolean</td>
    <td>定义是否显示最大化按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>closable</td>
    <td>boolean</td>
    <td>定义是否显示关闭按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>tools</td>
    <td>array,selector</td>
    <td>自定义工具组，可能的值：<br>
		1、数组，每个元素包含 iconCls 和 handler 两个属性。<br>
		2、选择器，指示工具组。<br>
		<br>
		面板（panel）工具组可通过已存在 &lt;div&gt; 标签声明：
<pre style="color:#006600">
&lt;div class="easyui-panel" style="width:300px;height:200px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title="My Panel" data-options="iconCls:'icon-ok',tools:'#tt'"&gt;
&lt;/div&gt;
&lt;div id="tt"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="icon-add" onclick="javascript:alert('add')"&gt;&lt;/a&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="icon-edit" onclick="javascript:alert('edit')"&gt;&lt;/a&gt;
&lt;/div&gt;
</pre>
		面板（panel）工具组可通过数组定义：
<pre style="color:#006600">
&lt;div class="easyui-panel" style="width:300px;height:200px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title="My Panel" data-options="iconCls:'icon-ok',tools:[
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-add',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('add')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-edit',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('edit')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]"&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>[]</td>
</tr>
<tr>
	<td>collapsed</td>
    <td>boolean</td>
    <td>定义初始化面板（panel）是不是折叠的。</td>
	<td>false</td>
</tr>
<tr>
	<td>minimized</td>
    <td>boolean</td>
    <td>定义初始化面板（panel）是不是最小化的。</td>
	<td>false</td>
</tr>
<tr>
	<td>maximized</td>
    <td>boolean</td>
    <td>定义初始化面板（panel）是不是最大化的。</td>
	<td>false</td>
</tr>
<tr>
	<td>closed</td>
    <td>boolean</td>
    <td>定义初始化面板（panel）是不是关闭的。</td>
	<td>false</td>
</tr>
<tr>
	<td>href</td>
    <td>string</td>
    <td>一个 URL，用它加载远程数据并且显示在面板（panel）里。请注意，除非面板（panel）打开，否则内容不会被加载。这对创建一个惰性加载的面板（panel）很有用：
<pre style="color:#006600">
&lt;div id="pp" class="easyui-panel" style="width:300px;height:200px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="href='get_content.php',closed:true"&gt;
&lt;/div&gt;
&lt;a href="#" onclick="javascript:$('#pp').panel('open')"&gt;Open&lt;/a&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>cache</td>
    <td>boolean</td>
    <td>设置为 true 就缓存从 href 加载的面板（panel）内容。</td>
	<td>true</td>
</tr>
<tr>
	<td>loadingMessage</td>
    <td>string</td>
    <td>当加载远程数据时在面板（panel）里显示一条信息。</td>
	<td>Loading…</td>
</tr>
<tr>
	<td>extractor</td>
    <td>function</td>
    <td>定义如何从 ajax 响应中提取内容，返回提取的数据。
<pre style="color:#006600">
extractor: function(data){
&nbsp;&nbsp;&nbsp;&nbsp;var pattern = /&lt;body[^&gt;]*&gt;((.|[\n\r])*)&lt;\/body&gt;/im;
&nbsp;&nbsp;&nbsp;&nbsp;var matches = pattern.exec(data);
&nbsp;&nbsp;&nbsp;&nbsp;if (matches){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return matches[1];&nbsp;&nbsp;&nbsp;&nbsp;// only extract body content
&nbsp;&nbsp;&nbsp;&nbsp;} else {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return data;
&nbsp;&nbsp;&nbsp;&nbsp;}
}
</pre>
	</td>
	<td> </td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>id</td>
    <td>string</td>
    <td>面板（panel）的 id 属性。</td>
	<td>null</td>
</tr>
<tr>
	<td>title</td>
    <td>string</td>
    <td>显示在面板（panel）头部的标题文字。</td>
	<td>null</td>
</tr>
<tr>
	<td>iconCls</td>
    <td>string</td>
    <td>在面板（panel）里显示一个 16x16 图标的 CSS class。</td>
	<td>null</td>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>设置面板（panel）的宽度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>设置面板（panel）的高度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>left</td>
    <td>number</td>
    <td>设置面板（panel）的左边位置。</td>
	<td>null</td>
</tr>
<tr>
	<td>top</td>
    <td>number</td>
    <td>设置面板（panel）的顶部位置。</td>
	<td>null</td>
</tr>
<tr>
	<td>cls</td>
    <td>string</td>
    <td>给面板（panel）添加一个 CSS class。</td>
	<td>null</td>
</tr>
<tr>
	<td>headerCls</td>
    <td>string</td>
    <td>给面板（panel）头部添加一个 CSS class。</td>
	<td>null</td>
</tr>
<tr>
	<td>bodyCls</td>
    <td>string</td>
    <td>给面板（panel）主体添加一个 CSS class。</td>
	<td>null</td>
</tr>
<tr>
	<td>style</td>
    <td>object</td>
    <td>给面板（panel）添加自定义格式的样式。<br>
	改变面板（panel）边框宽度的代码实例：
<pre style="color:#006600">
&lt;div class="easyui-panel" style="width:200px;height:100px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="style:{borderWidth:2}"&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>{}</td>
</tr>
<tr>
	<td>fit</td>
    <td>boolean</td>
    <td>当设置为 true 时，面板（panel）的尺寸就适应它的父容器。下面的实例演示了自动调整尺寸到它的父容器的最大内部尺寸的面板（panel）。
<pre style="color:#006600">
&lt;div style="width:200px;height:100px;padding:5px"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div class="easyui-panel" style="width:200px;height:100px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="fit:true,border:false"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Embedded Panel
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>false</td>
</tr>
<tr>
	<td>border</td>
    <td>boolean</td>
    <td>定义了是否显示面板（panel）的边框。</td>
	<td>true</td>
</tr>
<tr>
	<td>doSize</td>
    <td>boolean</td>
    <td>如果设置为 true，创建时面板（panel）就调整尺寸并做成布局。</td>
	<td>true</td>
</tr>
<tr>
	<td>noheader</td>
    <td>boolean</td>
    <td>如果设置为 true，面板（panel）的头部将不会被创建。</td>
	<td>false</td>
</tr>
<tr>
	<td>content</td>
    <td>string</td>
    <td>面板（panel）主体内容。</td>
	<td>null</td>
</tr>
<tr>
	<td>collapsible</td>
    <td>boolean</td>
    <td>定义是否显示折叠按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>minimizable</td>
    <td>boolean</td>
    <td>定义是否显示最小化按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>maximizable</td>
    <td>boolean</td>
    <td>定义是否显示最大化按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>closable</td>
    <td>boolean</td>
    <td>定义是否显示关闭按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>tools</td>
    <td>array,selector</td>
    <td>自定义工具组，可能的值：<br>
		1、数组，每个元素包含 iconCls 和 handler 两个属性。<br>
		2、选择器，指示工具组。<br>
		<br>
		面板（panel）工具组可通过已存在 &lt;div&gt; 标签声明：
<pre style="color:#006600">
&lt;div class="easyui-panel" style="width:300px;height:200px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title="My Panel" data-options="iconCls:'icon-ok',tools:'#tt'"&gt;
&lt;/div&gt;
&lt;div id="tt"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="icon-add" onclick="javascript:alert('add')"&gt;&lt;/a&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="icon-edit" onclick="javascript:alert('edit')"&gt;&lt;/a&gt;
&lt;/div&gt;
</pre>
		面板（panel）工具组可通过数组定义：
<pre style="color:#006600">
&lt;div class="easyui-panel" style="width:300px;height:200px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title="My Panel" data-options="iconCls:'icon-ok',tools:[
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-add',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('add')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-edit',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('edit')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]"&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>[]</td>
</tr>
<tr>
	<td>collapsed</td>
    <td>boolean</td>
    <td>定义初始化面板（panel）是不是折叠的。</td>
	<td>false</td>
</tr>
<tr>
	<td>minimized</td>
    <td>boolean</td>
    <td>定义初始化面板（panel）是不是最小化的。</td>
	<td>false</td>
</tr>
<tr>
	<td>maximized</td>
    <td>boolean</td>
    <td>定义初始化面板（panel）是不是最大化的。</td>
	<td>false</td>
</tr>
<tr>
	<td>closed</td>
    <td>boolean</td>
    <td>定义初始化面板（panel）是不是关闭的。</td>
	<td>false</td>
</tr>
<tr>
	<td>href</td>
    <td>string</td>
    <td>一个 URL，用它加载远程数据并且显示在面板（panel）里。请注意，除非面板（panel）打开，否则内容不会被加载。这对创建一个惰性加载的面板（panel）很有用：
<pre style="color:#006600">
&lt;div id="pp" class="easyui-panel" style="width:300px;height:200px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="href='get_content.php',closed:true"&gt;
&lt;/div&gt;
&lt;a href="#" onclick="javascript:$('#pp').panel('open')"&gt;Open&lt;/a&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>cache</td>
    <td>boolean</td>
    <td>设置为 true 就缓存从 href 加载的面板（panel）内容。</td>
	<td>true</td>
</tr>
<tr>
	<td>loadingMessage</td>
    <td>string</td>
    <td>当加载远程数据时在面板（panel）里显示一条信息。</td>
	<td>Loading…</td>
</tr>
<tr>
	<td>extractor</td>
    <td>function</td>
    <td>定义如何从 ajax 响应中提取内容，返回提取的数据。
<pre style="color:#006600">
extractor: function(data){
&nbsp;&nbsp;&nbsp;&nbsp;var pattern = /&lt;body[^&gt;]*&gt;((.|[\n\r])*)&lt;\/body&gt;/im;
&nbsp;&nbsp;&nbsp;&nbsp;var matches = pattern.exec(data);
&nbsp;&nbsp;&nbsp;&nbsp;if (matches){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return matches[1];&nbsp;&nbsp;&nbsp;&nbsp;// only extract body content
&nbsp;&nbsp;&nbsp;&nbsp;} else {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return data;
&nbsp;&nbsp;&nbsp;&nbsp;}
}
</pre>
	</td>
	<td> </td>
</tr>
</table></div>') where ID=5660;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onLoad</td>
    <td>none</td>
    <td>当远程数据被加载时触发。</td>
</tr>
<tr>
	<td>onBeforeOpen</td>
    <td>none</td>
    <td>面板（panel）打开前触发，返回 false 就停止打开。</td>
</tr>
<tr>
	<td>onOpen</td>
    <td>none</td>
    <td>面板（panel）打开后触发。</td>
</tr>
<tr>
	<td>onBeforeClose</td>
    <td>none</td>
    <td>面板（panel）关闭前触发，返回 false 就取消关闭。下面声明的面板（panel）不会关闭。
<pre style="color:#006600">
&lt;div class="easyui-panel" style="width:300px;height:200px;"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title="My Panel" data-options="onBeforeClose:function(){return false}"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;The panel cannot be closed.
&lt;/div&gt;
</pre>
	</td>
</tr>
<tr>
	<td>onClose</td>
    <td>none</td>
    <td>面板（panel）关闭后触发。</td>
</tr>
<tr>
	<td>onBeforeDestroy</td>
    <td>none</td>
    <td>面板（panel）销毁前触发，返回false就取消销毁。</td>
</tr>
<tr>
	<td>onDestroy</td>
    <td>none</td>
    <td>面板（panel）销毁后触发。</td>
</tr>
<tr>
	<td>onBeforeCollapse</td>
    <td>none</td>
    <td>面板（panel）折叠前触发，返回false就停止折叠。</td>
</tr>
<tr>
	<td>onCollapse</td>
    <td>none</td>
    <td>面板（panel）折叠后触发。</td>
</tr>
<tr>
	<td>onBeforeExpand</td>
    <td>none</td>
    <td>面板（panel）展开前触发，返回false就停止展开。</td>
</tr>
<tr>
	<td>onExpand</td>
    <td>none</td>
    <td>面板（panel）展开后触发。</td>
</tr>
<tr>
	<td>onResize</td>
    <td>width, height</td>
    <td>面板（panel）调整尺寸后触发。<br>
		width：新的外部宽度<br>
		height：新的外部高度
	</td>
</tr>
<tr>
	<td>onMove</td>
    <td>left,top</td>
    <td>面板（panel）移动后触发。<br>
		left：新的左边位置<br>
		top：新的顶部位置
	</td>
</tr>
<tr>
	<td>onMaximize</td>
    <td>none</td>
    <td>窗口最大化后触发。</td>
</tr>
<tr>
	<td>onRestore</td>
    <td>none</td>
    <td>窗口还原为它的原始尺寸后触发。</td>
</tr>
<tr>
	<td>onMinimize</td>
    <td>none</td>
    <td>窗口最小化后触发。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onLoad</td>
    <td>none</td>
    <td>当远程数据被加载时触发。</td>
</tr>
<tr>
	<td>onBeforeOpen</td>
    <td>none</td>
    <td>面板（panel）打开前触发，返回 false 就停止打开。</td>
</tr>
<tr>
	<td>onOpen</td>
    <td>none</td>
    <td>面板（panel）打开后触发。</td>
</tr>
<tr>
	<td>onBeforeClose</td>
    <td>none</td>
    <td>面板（panel）关闭前触发，返回 false 就取消关闭。下面声明的面板（panel）不会关闭。
<pre style="color:#006600">
&lt;div class="easyui-panel" style="width:300px;height:200px;"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title="My Panel" data-options="onBeforeClose:function(){return false}"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;The panel cannot be closed.
&lt;/div&gt;
</pre>
	</td>
</tr>
<tr>
	<td>onClose</td>
    <td>none</td>
    <td>面板（panel）关闭后触发。</td>
</tr>
<tr>
	<td>onBeforeDestroy</td>
    <td>none</td>
    <td>面板（panel）销毁前触发，返回false就取消销毁。</td>
</tr>
<tr>
	<td>onDestroy</td>
    <td>none</td>
    <td>面板（panel）销毁后触发。</td>
</tr>
<tr>
	<td>onBeforeCollapse</td>
    <td>none</td>
    <td>面板（panel）折叠前触发，返回false就停止折叠。</td>
</tr>
<tr>
	<td>onCollapse</td>
    <td>none</td>
    <td>面板（panel）折叠后触发。</td>
</tr>
<tr>
	<td>onBeforeExpand</td>
    <td>none</td>
    <td>面板（panel）展开前触发，返回false就停止展开。</td>
</tr>
<tr>
	<td>onExpand</td>
    <td>none</td>
    <td>面板（panel）展开后触发。</td>
</tr>
<tr>
	<td>onResize</td>
    <td>width, height</td>
    <td>面板（panel）调整尺寸后触发。<br>
		width：新的外部宽度<br>
		height：新的外部高度
	</td>
</tr>
<tr>
	<td>onMove</td>
    <td>left,top</td>
    <td>面板（panel）移动后触发。<br>
		left：新的左边位置<br>
		top：新的顶部位置
	</td>
</tr>
<tr>
	<td>onMaximize</td>
    <td>none</td>
    <td>窗口最大化后触发。</td>
</tr>
<tr>
	<td>onRestore</td>
    <td>none</td>
    <td>窗口还原为它的原始尺寸后触发。</td>
</tr>
<tr>
	<td>onMinimize</td>
    <td>none</td>
    <td>窗口最小化后触发。</td>
</tr>
</table></div>') where ID=5660;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）属性（property）。</td>
</tr>
<tr>
	<td>panel</td>
    <td>none</td>
    <td>返回外部面板（panel）对象。</td>
</tr>
<tr>
	<td>header</td>
    <td>none</td>
    <td>返回面板（panel）头部对象。</td>
</tr>
<tr>
	<td>body</td>
    <td>none</td>
    <td>返回面板（panel）主体对象。</td>
</tr>
<tr>
	<td>setTitle</td>
    <td>title</td>
    <td>设置头部的标题文本。</td>
</tr>
<tr>
	<td>open</td>
    <td>forceOpen</td>
    <td>当 forceOpen 参数设置为 true 时，就绕过 onBeforeOpen 回调函数打开面板（panel）。</td>
</tr>
<tr>
	<td>close</td>
    <td>forceClose</td>
    <td>当 forceClose 参数设置为 true 时，就绕过 onBeforeClose 回调函数关闭面板（panel）。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>forceDestroy</td>
    <td>当 forceDestroy 参数设置为 true 时，就绕过 onBeforeDestroy 回调函数销毁面板（panel）。</td>
</tr>
<tr>
	<td>refresh</td>
    <td>href</td>
    <td>刷新面板（panel）加载远程数据。如果分配了 'href' 参数，将重写旧的 'href' 属性。<br>
	代码实例：
<pre style="color:#006600">
// open a panel and then refresh its contents.
$('#pp').panel('open').panel('refresh');
// refresh contents with a new URL.
$('#pp').panel('open').panel('refresh','new_content.php');
</pre>
	</td>
</tr>
<tr>
	<td>resize</td>
    <td>options</td>
    <td>设置面板（panel）尺寸并做布局。Options 对象包含下列属性：<br>
	width：新的面板（panel）宽度<br>
	height：新的面板（panel）宽度<br>
	left：新的面板（panel）左边位置<br>
	top：新的面板（panel）顶部位置<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#pp').panel('resize',{
&nbsp;&nbsp;&nbsp;&nbsp;width: 600,
&nbsp;&nbsp;&nbsp;&nbsp;height: 400
});
</pre>
	</td>
</tr>
<tr>
	<td>move</td>
    <td>options</td>
    <td>移动面板（panel）到新位置。Options 对象包含下列属性：<br>
	left：新的面板（panel）左边位置<br>
	top：新的面板（panel）顶部位置
	</td>
</tr>
<tr>
	<td>maximize</td>
    <td>none</td>
    <td>面板（panel）适应它的容器的尺寸。</td>
</tr>
<tr>
	<td>minimize</td>
    <td>none</td>
    <td>最小化面板（panel）。</td>
</tr>
<tr>
	<td>restore</td>
    <td>none</td>
    <td>把最大化的面板（panel）还原为它原来的尺寸和位置。</td>
</tr>
<tr>
	<td>collapse</td>
    <td>animate</td>
    <td>折叠面板（panel）主体。</td>
</tr>
<tr>
	<td>expand</td>
    <td>animate</td>
    <td>展开面板（panel）主体。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）属性（property）。</td>
</tr>
<tr>
	<td>panel</td>
    <td>none</td>
    <td>返回外部面板（panel）对象。</td>
</tr>
<tr>
	<td>header</td>
    <td>none</td>
    <td>返回面板（panel）头部对象。</td>
</tr>
<tr>
	<td>body</td>
    <td>none</td>
    <td>返回面板（panel）主体对象。</td>
</tr>
<tr>
	<td>setTitle</td>
    <td>title</td>
    <td>设置头部的标题文本。</td>
</tr>
<tr>
	<td>open</td>
    <td>forceOpen</td>
    <td>当 forceOpen 参数设置为 true 时，就绕过 onBeforeOpen 回调函数打开面板（panel）。</td>
</tr>
<tr>
	<td>close</td>
    <td>forceClose</td>
    <td>当 forceClose 参数设置为 true 时，就绕过 onBeforeClose 回调函数关闭面板（panel）。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>forceDestroy</td>
    <td>当 forceDestroy 参数设置为 true 时，就绕过 onBeforeDestroy 回调函数销毁面板（panel）。</td>
</tr>
<tr>
	<td>refresh</td>
    <td>href</td>
    <td>刷新面板（panel）加载远程数据。如果分配了 'href' 参数，将重写旧的 'href' 属性。<br>
	代码实例：
<pre style="color:#006600">
// open a panel and then refresh its contents.
$('#pp').panel('open').panel('refresh');
// refresh contents with a new URL.
$('#pp').panel('open').panel('refresh','new_content.php');
</pre>
	</td>
</tr>
<tr>
	<td>resize</td>
    <td>options</td>
    <td>设置面板（panel）尺寸并做布局。Options 对象包含下列属性：<br>
	width：新的面板（panel）宽度<br>
	height：新的面板（panel）宽度<br>
	left：新的面板（panel）左边位置<br>
	top：新的面板（panel）顶部位置<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#pp').panel('resize',{
&nbsp;&nbsp;&nbsp;&nbsp;width: 600,
&nbsp;&nbsp;&nbsp;&nbsp;height: 400
});
</pre>
	</td>
</tr>
<tr>
	<td>move</td>
    <td>options</td>
    <td>移动面板（panel）到新位置。Options 对象包含下列属性：<br>
	left：新的面板（panel）左边位置<br>
	top：新的面板（panel）顶部位置
	</td>
</tr>
<tr>
	<td>maximize</td>
    <td>none</td>
    <td>面板（panel）适应它的容器的尺寸。</td>
</tr>
<tr>
	<td>minimize</td>
    <td>none</td>
    <td>最小化面板（panel）。</td>
</tr>
<tr>
	<td>restore</td>
    <td>none</td>
    <td>把最大化的面板（panel）还原为它原来的尺寸和位置。</td>
</tr>
<tr>
	<td>collapse</td>
    <td>animate</td>
    <td>折叠面板（panel）主体。</td>
</tr>
<tr>
	<td>expand</td>
    <td>animate</td>
    <td>展开面板（panel）主体。</td>
</tr>
</table></div>') where ID=5660;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>标签页（Tabs）容器的宽度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>标签页（Tabs）容器的高度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>plain</td>
    <td>boolean</td>
    <td>当设置为 true 时，就不用背景容器图片来呈现 tab 条。</td>
	<td>false</td>
</tr>
<tr>
	<td>fit</td>
    <td>boolean</td>
    <td>当设置为 true 时，就设置标签页（Tabs）容器的尺寸以适应它的父容器。</td>
	<td>false</td>
</tr>
<tr>
	<td>border</td>
    <td>boolean</td>
    <td>当设置为 true 时，就显示标签页（Tabs）容器边框。</td>
	<td>true</td>
</tr>
<tr>
	<td>scrollIncrement</td>
    <td>number</td>
    <td>每按一次 tab 滚动按钮，滚动的像素数。</td>
	<td>100</td>
</tr>
<tr>
	<td>scrollDuration</td>
    <td>number</td>
    <td>每一个滚动动画应该持续的毫秒数。</td>
	<td>400</td>
</tr>
<tr>
	<td>tools</td>
    <td>array,selector</td>
    <td>放置在头部的左侧或右侧的工具栏，可能的值：<br>
	1、数组，指示工具组，每个工具选项都和链接按钮（Linkbutton）一样。<br>
	2、选择器，指示包含工具的 &lt;div&gt;。<br>
	<br>
	代码实例：<br>
	通过数组定义工具。
<pre style="color:#006600">
$('#tt').tabs({
&nbsp;&nbsp;&nbsp;&nbsp;tools:[{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-add',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('add')
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-save',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('save')
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	通过已有的 DOM 容器定义工具。
<pre style="color:#006600">
$('#tt').tabs({
&nbsp;&nbsp;&nbsp;&nbsp;tools:'#tab-tools'
});
&lt;div id="tab-tools"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add"&gt;&lt;/a&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-save"&gt;&lt;/a&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>toolPosition</td>
    <td>string</td>
    <td>工具栏位置。可能的值：'left'、'right'。该属性自版本 1.3.2 起可用。</td>
	<td>right</td>
</tr>
<tr>
	<td>tabPosition</td>
    <td>string</td>
    <td>标签页（tab）位置。可能的值：'top'、'bottom'、'left'、'right'。该属性自版本 1.3.2 起可用。</td>
	<td>top</td>
</tr>
<tr>
	<td>headerWidth</td>
    <td>number</td>
    <td>标签页（tab）头部宽度，只有当 tabPosition 设置为 'left' 或 'right' 时才有效。该属性自版本 1.3.2 起可用。</td>
	<td>150</td>
</tr>
<tr>
	<td>tabWidth</td>
    <td>number</td>
    <td>tab 条的宽度。该属性自版本 1.3.4 起可用。</td>
	<td>auto</td>
</tr>
<tr>
	<td>tabHeight</td>
    <td>number</td>
    <td>tab 条的高度。该属性自版本 1.3.4 起可用。</td>
	<td>27</td>
</tr>
<tr>
	<td>selected</td>
    <td>number</td>
    <td>初始化选定的标签页索引。该属性自版本 1.3.5 起可用。</td>
	<td>0</td>
</tr>
<tr>
	<td>showHeader</td>
    <td>boolean</td>
    <td>当设置为 true 时，显示标签页（tab）头部。该属性自版本 1.3.5 起可用。</td>
	<td>true</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>标签页（Tabs）容器的宽度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>标签页（Tabs）容器的高度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>plain</td>
    <td>boolean</td>
    <td>当设置为 true 时，就不用背景容器图片来呈现 tab 条。</td>
	<td>false</td>
</tr>
<tr>
	<td>fit</td>
    <td>boolean</td>
    <td>当设置为 true 时，就设置标签页（Tabs）容器的尺寸以适应它的父容器。</td>
	<td>false</td>
</tr>
<tr>
	<td>border</td>
    <td>boolean</td>
    <td>当设置为 true 时，就显示标签页（Tabs）容器边框。</td>
	<td>true</td>
</tr>
<tr>
	<td>scrollIncrement</td>
    <td>number</td>
    <td>每按一次 tab 滚动按钮，滚动的像素数。</td>
	<td>100</td>
</tr>
<tr>
	<td>scrollDuration</td>
    <td>number</td>
    <td>每一个滚动动画应该持续的毫秒数。</td>
	<td>400</td>
</tr>
<tr>
	<td>tools</td>
    <td>array,selector</td>
    <td>放置在头部的左侧或右侧的工具栏，可能的值：<br>
	1、数组，指示工具组，每个工具选项都和链接按钮（Linkbutton）一样。<br>
	2、选择器，指示包含工具的 &lt;div&gt;。<br>
	<br>
	代码实例：<br>
	通过数组定义工具。
<pre style="color:#006600">
$('#tt').tabs({
&nbsp;&nbsp;&nbsp;&nbsp;tools:[{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-add',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('add')
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-save',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('save')
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	通过已有的 DOM 容器定义工具。
<pre style="color:#006600">
$('#tt').tabs({
&nbsp;&nbsp;&nbsp;&nbsp;tools:'#tab-tools'
});
&lt;div id="tab-tools"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add"&gt;&lt;/a&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-save"&gt;&lt;/a&gt;
&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>toolPosition</td>
    <td>string</td>
    <td>工具栏位置。可能的值：'left'、'right'。该属性自版本 1.3.2 起可用。</td>
	<td>right</td>
</tr>
<tr>
	<td>tabPosition</td>
    <td>string</td>
    <td>标签页（tab）位置。可能的值：'top'、'bottom'、'left'、'right'。该属性自版本 1.3.2 起可用。</td>
	<td>top</td>
</tr>
<tr>
	<td>headerWidth</td>
    <td>number</td>
    <td>标签页（tab）头部宽度，只有当 tabPosition 设置为 'left' 或 'right' 时才有效。该属性自版本 1.3.2 起可用。</td>
	<td>150</td>
</tr>
<tr>
	<td>tabWidth</td>
    <td>number</td>
    <td>tab 条的宽度。该属性自版本 1.3.4 起可用。</td>
	<td>auto</td>
</tr>
<tr>
	<td>tabHeight</td>
    <td>number</td>
    <td>tab 条的高度。该属性自版本 1.3.4 起可用。</td>
	<td>27</td>
</tr>
<tr>
	<td>selected</td>
    <td>number</td>
    <td>初始化选定的标签页索引。该属性自版本 1.3.5 起可用。</td>
	<td>0</td>
</tr>
<tr>
	<td>showHeader</td>
    <td>boolean</td>
    <td>当设置为 true 时，显示标签页（tab）头部。该属性自版本 1.3.5 起可用。</td>
	<td>true</td>
</tr>
</table></div>') where ID=5661;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onLoad</td>
    <td>panel</td>
    <td>当一个 ajax 标签页面板（tab panel）完成加载远程数据时触发。</td>
</tr>
<tr>
	<td>onSelect</td>
    <td>title,index</td>
    <td>当用户选择一个标签页面板（tab panel）时触发。</td>
</tr>
<tr>
	<td>onUnselect</td>
    <td>title,index</td>
    <td>当用户未选择一个标签页面板（tab panel）时触发。该事件自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>onBeforeClose</td>
    <td>title,index</td>
    <td>当一个标签页面板（tab panel）被关闭前触发，返回 false 就取消关闭动作。下面的实例演示如何在关闭标签页面板（tab panel）前显示确认对话框。
<pre style="color:#006600">
$('#tt').tabs({
  onBeforeClose: function(title){
&nbsp;&nbsp;&nbsp;&nbsp;return confirm('Are you sure you want to close ' + title);
  }
});
// using the async confirm dialog
$('#tt').tabs({
  onBeforeClose: function(title,index){
&nbsp;&nbsp;&nbsp;&nbsp;var target = this;
&nbsp;&nbsp;&nbsp;&nbsp;$.messager.confirm('Confirm','Are you sure you want to close '+title,function(r){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (r){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var opts = $(target).tabs('options');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var bc = opts.onBeforeClose;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;opts.onBeforeClose = function(){};  // allowed to close now
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(target).tabs('close',index);
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;opts.onBeforeClose = bc;  // restore the event function
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;});
&nbsp;&nbsp;&nbsp;&nbsp;return false;&nbsp;&nbsp;&nbsp;&nbsp;// prevent from closing
  }
});
</pre>
	</td>
</tr>
<tr>
	<td>onClose</td>
    <td>title,index</td>
    <td>当用户关闭一个标签页面板（tab panel）时触发。</td>
</tr>
<tr>
	<td>onAdd</td>
    <td>title,index</td>
    <td>当一个新的标签页面板（tab panel）被添加时触发。</td>
</tr>
<tr>
	<td>onUpdate</td>
    <td>title,index</td>
    <td>当一个标签页面板（tab panel）被更新时触发。</td>
</tr>
<tr>
	<td>onContextMenu</td>
    <td>e, title,index</td>
    <td>当一个标签页面板（tab panel）被右键点击时触发。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onLoad</td>
    <td>panel</td>
    <td>当一个 ajax 标签页面板（tab panel）完成加载远程数据时触发。</td>
</tr>
<tr>
	<td>onSelect</td>
    <td>title,index</td>
    <td>当用户选择一个标签页面板（tab panel）时触发。</td>
</tr>
<tr>
	<td>onUnselect</td>
    <td>title,index</td>
    <td>当用户未选择一个标签页面板（tab panel）时触发。该事件自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>onBeforeClose</td>
    <td>title,index</td>
    <td>当一个标签页面板（tab panel）被关闭前触发，返回 false 就取消关闭动作。下面的实例演示如何在关闭标签页面板（tab panel）前显示确认对话框。
<pre style="color:#006600">
$('#tt').tabs({
  onBeforeClose: function(title){
&nbsp;&nbsp;&nbsp;&nbsp;return confirm('Are you sure you want to close ' + title);
  }
});
// using the async confirm dialog
$('#tt').tabs({
  onBeforeClose: function(title,index){
&nbsp;&nbsp;&nbsp;&nbsp;var target = this;
&nbsp;&nbsp;&nbsp;&nbsp;$.messager.confirm('Confirm','Are you sure you want to close '+title,function(r){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (r){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var opts = $(target).tabs('options');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var bc = opts.onBeforeClose;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;opts.onBeforeClose = function(){};  // allowed to close now
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(target).tabs('close',index);
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;opts.onBeforeClose = bc;  // restore the event function
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;});
&nbsp;&nbsp;&nbsp;&nbsp;return false;&nbsp;&nbsp;&nbsp;&nbsp;// prevent from closing
  }
});
</pre>
	</td>
</tr>
<tr>
	<td>onClose</td>
    <td>title,index</td>
    <td>当用户关闭一个标签页面板（tab panel）时触发。</td>
</tr>
<tr>
	<td>onAdd</td>
    <td>title,index</td>
    <td>当一个新的标签页面板（tab panel）被添加时触发。</td>
</tr>
<tr>
	<td>onUpdate</td>
    <td>title,index</td>
    <td>当一个标签页面板（tab panel）被更新时触发。</td>
</tr>
<tr>
	<td>onContextMenu</td>
    <td>e, title,index</td>
    <td>当一个标签页面板（tab panel）被右键点击时触发。</td>
</tr>
</table></div>') where ID=5661;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回标签页（tabs）选项（options）。</td>
</tr>
<tr>
	<td>tabs</td>
    <td>none</td>
    <td>返回全部的标签页面板（tab panel）。</td>
</tr>
<tr>
	<td>resize</td>
    <td>none</td>
    <td>调整标签页（tabs）容器的尺寸并做布局。</td>
</tr>
<tr>
	<td>add</td>
    <td>options</td>
    <td>添加一个新的标签页面板（tab panel），options 参数是一个配置对象，更多详细信息请参见标签页面板（tab panel）属性。<br>
	当添加一个新的标签页面板（tab panel）时，它将被选中。<br>
	如需添加一个未选中的标签页面板（tab panel），请记得设置 'selected' 属性为 false。
<pre style="color:#006600">
// add a unselected tab panel
$('#tt').tabs('add',{
&nbsp;&nbsp;&nbsp;&nbsp;title: 'new tab',
&nbsp;&nbsp;&nbsp;&nbsp;selected: false
&nbsp;&nbsp;&nbsp;&nbsp;//...
});
</pre>
	</td>
</tr>
<tr>
	<td>close</td>
    <td>which</td>
    <td>关闭一个标签页面板（tab panel），'which' 参数可以是要被关闭的标签页面板（tab panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>getTab</td>
    <td>which</td>
    <td>获取指定的标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>getTabIndex</td>
    <td>tab</td>
    <td>获取指定的标签页面板（tab panel）索引。</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>获取选中的标签页面板（tab panel）。下面的实例演示如何获取选中的标签页面板（tab panel）的索引。
<pre style="color:#006600">
var tab = $('#tt').tabs('getSelected');
var index = $('#tt').tabs('getTabIndex',tab);
alert(index);
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>which</td>
    <td>选择一个标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>which</td>
    <td>选择一个标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>showHeader</td>
    <td>none</td>
    <td>显示标签页（tabs）头部。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>hideHeader</td>
    <td>none</td>
    <td>隐藏标签页（tabs）头部。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>exists</td>
    <td>which</td>
    <td>指示指定的面板是否已存在，'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>update</td>
    <td>param</td>
    <td>更新指定的标签页面板（tab panel），param 参数包含两个属性：<br>
	tab：被更新的标签页面板（tab panel）。<br>
	options：面板（panel）的选项（options）。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// update the selected panel with new title and content
var tab = $('#tt').tabs('getSelected');  // get selected panel
$('#tt').tabs('update', {
&nbsp;&nbsp;&nbsp;&nbsp;tab: tab,
&nbsp;&nbsp;&nbsp;&nbsp;options: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title: 'New Title',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;href: 'get_content.php'  // the new content URL
&nbsp;&nbsp;&nbsp;&nbsp;}
});

// call 'refresh' method for tab panel to update its content
var tab = $('#tt').tabs('getSelected');  // get selected panel
tab.panel('refresh', 'get_content.php');
</pre>
	</td>
</tr>
<tr>
	<td>enableTab</td>
    <td>which</td>
    <td>启用指定的标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。该方法自版本 1.3 起可用。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#tt').tabs('enableTab', 1);&nbsp;&nbsp;&nbsp;&nbsp;// enable the second tab panel
$('#tt').tabs('enableTab', 'Tab2');&nbsp;&nbsp;&nbsp;&nbsp;enable the tab panel that has 'Tab2' title
</pre>
	</td>
</tr>
<tr>
	<td>disableTab</td>
    <td>which</td>
    <td>禁用指定的标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。该方法自版本 1.3 起可用。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#tt').tabs('disableTab', 1);&nbsp;&nbsp;&nbsp;&nbsp;// disable the second tab panel.
</pre>
	</td>
</tr>
<tr>
	<td>scrollBy</td>
    <td>deltaX</td>
    <td>通过指定的像素数滚动标签页（tab）头部，负值表示滚动到右边，正值表示滚动到左边。该方法自版本 1.3.2 起可用。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// scroll the tab header to left
$('#tt').tabs('scroll', 10);
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回标签页（tabs）选项（options）。</td>
</tr>
<tr>
	<td>tabs</td>
    <td>none</td>
    <td>返回全部的标签页面板（tab panel）。</td>
</tr>
<tr>
	<td>resize</td>
    <td>none</td>
    <td>调整标签页（tabs）容器的尺寸并做布局。</td>
</tr>
<tr>
	<td>add</td>
    <td>options</td>
    <td>添加一个新的标签页面板（tab panel），options 参数是一个配置对象，更多详细信息请参见标签页面板（tab panel）属性。<br>
	当添加一个新的标签页面板（tab panel）时，它将被选中。<br>
	如需添加一个未选中的标签页面板（tab panel），请记得设置 'selected' 属性为 false。
<pre style="color:#006600">
// add a unselected tab panel
$('#tt').tabs('add',{
&nbsp;&nbsp;&nbsp;&nbsp;title: 'new tab',
&nbsp;&nbsp;&nbsp;&nbsp;selected: false
&nbsp;&nbsp;&nbsp;&nbsp;//...
});
</pre>
	</td>
</tr>
<tr>
	<td>close</td>
    <td>which</td>
    <td>关闭一个标签页面板（tab panel），'which' 参数可以是要被关闭的标签页面板（tab panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>getTab</td>
    <td>which</td>
    <td>获取指定的标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>getTabIndex</td>
    <td>tab</td>
    <td>获取指定的标签页面板（tab panel）索引。</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>获取选中的标签页面板（tab panel）。下面的实例演示如何获取选中的标签页面板（tab panel）的索引。
<pre style="color:#006600">
var tab = $('#tt').tabs('getSelected');
var index = $('#tt').tabs('getTabIndex',tab);
alert(index);
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>which</td>
    <td>选择一个标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>which</td>
    <td>选择一个标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>showHeader</td>
    <td>none</td>
    <td>显示标签页（tabs）头部。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>hideHeader</td>
    <td>none</td>
    <td>隐藏标签页（tabs）头部。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>exists</td>
    <td>which</td>
    <td>指示指定的面板是否已存在，'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>update</td>
    <td>param</td>
    <td>更新指定的标签页面板（tab panel），param 参数包含两个属性：<br>
	tab：被更新的标签页面板（tab panel）。<br>
	options：面板（panel）的选项（options）。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// update the selected panel with new title and content
var tab = $('#tt').tabs('getSelected');  // get selected panel
$('#tt').tabs('update', {
&nbsp;&nbsp;&nbsp;&nbsp;tab: tab,
&nbsp;&nbsp;&nbsp;&nbsp;options: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title: 'New Title',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;href: 'get_content.php'  // the new content URL
&nbsp;&nbsp;&nbsp;&nbsp;}
});

// call 'refresh' method for tab panel to update its content
var tab = $('#tt').tabs('getSelected');  // get selected panel
tab.panel('refresh', 'get_content.php');
</pre>
	</td>
</tr>
<tr>
	<td>enableTab</td>
    <td>which</td>
    <td>启用指定的标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。该方法自版本 1.3 起可用。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#tt').tabs('enableTab', 1);&nbsp;&nbsp;&nbsp;&nbsp;// enable the second tab panel
$('#tt').tabs('enableTab', 'Tab2');&nbsp;&nbsp;&nbsp;&nbsp;enable the tab panel that has 'Tab2' title
</pre>
	</td>
</tr>
<tr>
	<td>disableTab</td>
    <td>which</td>
    <td>禁用指定的标签页面板（tab panel），'which' 参数可以是标签页面板（tab panel）的标题（title）或索引（index）。该方法自版本 1.3 起可用。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#tt').tabs('disableTab', 1);&nbsp;&nbsp;&nbsp;&nbsp;// disable the second tab panel.
</pre>
	</td>
</tr>
<tr>
	<td>scrollBy</td>
    <td>deltaX</td>
    <td>通过指定的像素数滚动标签页（tab）头部，负值表示滚动到右边，正值表示滚动到左边。该方法自版本 1.3.2 起可用。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// scroll the tab header to left
$('#tt').tabs('scroll', 10);
</pre>
	</td>
</tr>
</table></div>') where ID=5661;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回折叠面板（accordion）的选项。</td>
</tr>
<tr>
	<td>panels</td>
    <td>none</td>
    <td>获取全部的面板（panel）。</td>
</tr>
<tr>
	<td>resize</td>
    <td>none</td>
    <td>调整折叠面板（accordion）的尺寸。</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>获取第一个选中的面板（panel）。</td>
</tr>
<tr>
	<td>getSelections</td>
    <td>none</td>
    <td>过去所有选中的面板（panel）。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>getPanel</td>
    <td>which</td>
    <td>获取指定的面板（panel）。'which' 参数可以是面板（panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>getPanelIndex</td>
    <td>panel</td>
    <td>获取指定的面板（panel）索引。该方法自版本 1.3 起可用。<br>
	下面的实例显示如何获取选中的面板（panel）索引。
<pre style="color:#006600">
var p = $('#aa').accordion('getSelected');
if (p){
&nbsp;&nbsp;&nbsp;&nbsp;var index = $('#aa').accordion('getPanelIndex', p);
&nbsp;&nbsp;&nbsp;&nbsp;alert(index);
}
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>which</td>
    <td>选择指定的面板（panel）。'which' 参数可以是面板（panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>which</td>
    <td>未选择指定的面板（panel）。'which' 参数可以是面板（panel）的标题（title）或索引（index）。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>add</td>
    <td>options</td>
    <td>添加一个新的面板（panel）。默认情况下，新添加的面板（panel）会被选中。如需添加一个未被选中的新面板（panel），请传递 'selected' 属性，并将其设置为 false。<br>
	代码实例：
<pre style="color:#006600">
$('#aa').accordion('add', {
&nbsp;&nbsp;&nbsp;&nbsp;title: 'New Title',
&nbsp;&nbsp;&nbsp;&nbsp;content: 'New Content',
&nbsp;&nbsp;&nbsp;&nbsp;selected: false
});
</pre>
	</td>
</tr>
<tr>
	<td>remove</td>
    <td>which</td>
    <td>移除指定的面板（panel）。'which' 参数可以是面板（panel）的标题（title）或索引（index）。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回折叠面板（accordion）的选项。</td>
</tr>
<tr>
	<td>panels</td>
    <td>none</td>
    <td>获取全部的面板（panel）。</td>
</tr>
<tr>
	<td>resize</td>
    <td>none</td>
    <td>调整折叠面板（accordion）的尺寸。</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>获取第一个选中的面板（panel）。</td>
</tr>
<tr>
	<td>getSelections</td>
    <td>none</td>
    <td>过去所有选中的面板（panel）。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>getPanel</td>
    <td>which</td>
    <td>获取指定的面板（panel）。'which' 参数可以是面板（panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>getPanelIndex</td>
    <td>panel</td>
    <td>获取指定的面板（panel）索引。该方法自版本 1.3 起可用。<br>
	下面的实例显示如何获取选中的面板（panel）索引。
<pre style="color:#006600">
var p = $('#aa').accordion('getSelected');
if (p){
&nbsp;&nbsp;&nbsp;&nbsp;var index = $('#aa').accordion('getPanelIndex', p);
&nbsp;&nbsp;&nbsp;&nbsp;alert(index);
}
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>which</td>
    <td>选择指定的面板（panel）。'which' 参数可以是面板（panel）的标题（title）或索引（index）。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>which</td>
    <td>未选择指定的面板（panel）。'which' 参数可以是面板（panel）的标题（title）或索引（index）。该方法自版本 1.3.5 起可用。</td>
</tr>
<tr>
	<td>add</td>
    <td>options</td>
    <td>添加一个新的面板（panel）。默认情况下，新添加的面板（panel）会被选中。如需添加一个未被选中的新面板（panel），请传递 'selected' 属性，并将其设置为 false。<br>
	代码实例：
<pre style="color:#006600">
$('#aa').accordion('add', {
&nbsp;&nbsp;&nbsp;&nbsp;title: 'New Title',
&nbsp;&nbsp;&nbsp;&nbsp;content: 'New Content',
&nbsp;&nbsp;&nbsp;&nbsp;selected: false
});
</pre>
	</td>
</tr>
<tr>
	<td>remove</td>
    <td>which</td>
    <td>移除指定的面板（panel）。'which' 参数可以是面板（panel）的标题（title）或索引（index）。</td>
</tr>
</table></div>') where ID=5662;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onShow</td>
    <td>none</td>
    <td>当菜单（menu）显示之后触发。</td>
</tr>
<tr>
	<td>onHide</td>
    <td>none</td>
    <td>当菜单（menu）隐藏之后触发。</td>
</tr>
<tr>
	<td>onClick</td>
    <td>item</td>
    <td>当点击菜单项（menu item）时触发。下面的实例演示如何处理所有菜单项点击：
<pre style="color:#006600">
&lt;div class="easyui-menu" data-options="onClick:menuHandler" style="width:120px;"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'new'"&gt;New&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'save',iconCls:'icon-save'"&gt;Save&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'print',iconCls:'icon-print'"&gt;Print&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div class="menu-sep"&gt;&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'exit'"&gt;Exit&lt;/div&gt;
&lt;/div&gt;
&lt;script type="text/javascript"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;function menuHandler(item){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(item.name)
&nbsp;&nbsp;&nbsp;&nbsp;}
&lt;/script&gt;
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onShow</td>
    <td>none</td>
    <td>当菜单（menu）显示之后触发。</td>
</tr>
<tr>
	<td>onHide</td>
    <td>none</td>
    <td>当菜单（menu）隐藏之后触发。</td>
</tr>
<tr>
	<td>onClick</td>
    <td>item</td>
    <td>当点击菜单项（menu item）时触发。下面的实例演示如何处理所有菜单项点击：
<pre style="color:#006600">
&lt;div class="easyui-menu" data-options="onClick:menuHandler" style="width:120px;"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'new'"&gt;New&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'save',iconCls:'icon-save'"&gt;Save&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'print',iconCls:'icon-print'"&gt;Print&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div class="menu-sep"&gt;&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div data-options="name:'exit'"&gt;Exit&lt;/div&gt;
&lt;/div&gt;
&lt;script type="text/javascript"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;function menuHandler(item){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(item.name)
&nbsp;&nbsp;&nbsp;&nbsp;}
&lt;/script&gt;
</pre>
	</td>
</tr>
</table></div>') where ID=5664;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>show</td>
    <td>pos</td>
    <td>在指定的位置显示菜单（menu）。<br>
		pos 参数有两个属性：<br>
		left：新的左边位置。<br>
		top：新的顶部位置。</td>
</tr>
<tr>
	<td>hide</td>
    <td>none</td>
    <td>隐藏菜单（menu）。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁菜单（menu）。</td>
</tr>
<tr>
	<td>getItem</td>
    <td>itemEl</td>
    <td>获取包含 'target' 属性（指示项目 DOM 元素）的菜单项（menu item）属性。下面的实例演示如何通过 id 获取指定的项目：
<pre style="color:#006600">
&lt;div id="mm" class="easyui-menu" style="width:120px"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;New&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div id="m-open"&gt;Open&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;Save&lt;/div&gt;
&lt;/div&gt;

var itemEl = $('#m-open')[0];  // the menu item element
var item = $('#mm').menu('getItem', itemEl);
console.log(item);
</pre>
	</td>
</tr>
<tr>
	<td>setText</td>
    <td>param</td>
    <td>给指定的菜单项（menu item）设置文本。'param' 参数包含两个属性：<br>
	target：DOM 对象，被设定的菜单项（menu item）。<br>
	text：string，新的文本值。<br>
	<br>
	代码实例：
<pre style="color:#006600">
var item = $('#mm').menu('findItem', 'Save');
$('#mm').menu('setText', {
&nbsp;&nbsp;&nbsp;&nbsp;target: item.target,
&nbsp;&nbsp;&nbsp;&nbsp;text: 'Saving'
});
</pre>
	</td>
</tr>
<tr>
	<td>setIcon</td>
    <td>param</td>
    <td>给指定的菜单项（menu item）设置图标。'param' 参数包含两个属性：<br>
	target：DOM 对象，即菜单项（menu item）。<br>
	iconCls：新图标的 CSS class。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#mm').menu('setIcon', {
&nbsp;&nbsp;&nbsp;&nbsp;target: $('#m-open')[0],
&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-closed'
});
</pre>
	</td>
</tr>
<tr>
	<td>findItem</td>
    <td>text</td>
    <td>找到指定的菜单项（menu item），返回对象与 getItem 方法相同。<br>
	代码实例：
<pre style="color:#006600">
// find 'Open' item and disable it
var item = $('#mm').menu('findItem', 'Open');
$('#mm').menu('disableItem', item.target);
</pre>
	</td>
</tr>
<tr>
	<td>appendItem</td>
    <td>options</td>
    <td>追加一个新的菜单项（menu item），'param' 参数指示新的项目属性。默认情况下，新增的项目将作为顶级菜单项（menu item）。如需追加一个子菜单项，需设置 'parent' 属性，用来指示已经有子项目的父项目元素。<br>
	代码实例：
<pre style="color:#006600">
// append a top menu item
$('#mm').menu('appendItem', {
&nbsp;&nbsp;&nbsp;&nbsp;text: 'New Item',
&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-ok',
&nbsp;&nbsp;&nbsp;&nbsp;onclick: function(){alert('New Item')}
});
// append a menu separator
$('#mm').menu('appendItem', {
&nbsp;&nbsp;&nbsp;&nbsp;separator: true
});
// append a sub menu item
var item = $('#mm').menu('findItem', 'Open');  // find 'Open' item
$('#mm').menu('appendItem', {
&nbsp;&nbsp;&nbsp;&nbsp;parent: item.target,  // the parent item element
&nbsp;&nbsp;&nbsp;&nbsp;text: 'Open Excel',
&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-excel',
&nbsp;&nbsp;&nbsp;&nbsp;onclick: function(){alert('Open Excel')}
});
</pre>
	</td>
</tr>
<tr>
	<td>removeItem</td>
    <td>itemEl</td>
    <td>移除指定的菜单项（menu item）。</td>
</tr>
<tr>
	<td>enableItem</td>
    <td>itemEl</td>
    <td>启用菜单项（menu item）。</td>
</tr>
<tr>
	<td>disableItem</td>
    <td>itemEl</td>
    <td>禁用菜单项（menu item）。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>show</td>
    <td>pos</td>
    <td>在指定的位置显示菜单（menu）。<br>
		pos 参数有两个属性：<br>
		left：新的左边位置。<br>
		top：新的顶部位置。</td>
</tr>
<tr>
	<td>hide</td>
    <td>none</td>
    <td>隐藏菜单（menu）。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁菜单（menu）。</td>
</tr>
<tr>
	<td>getItem</td>
    <td>itemEl</td>
    <td>获取包含 'target' 属性（指示项目 DOM 元素）的菜单项（menu item）属性。下面的实例演示如何通过 id 获取指定的项目：
<pre style="color:#006600">
&lt;div id="mm" class="easyui-menu" style="width:120px"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;New&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div id="m-open"&gt;Open&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;Save&lt;/div&gt;
&lt;/div&gt;

var itemEl = $('#m-open')[0];  // the menu item element
var item = $('#mm').menu('getItem', itemEl);
console.log(item);
</pre>
	</td>
</tr>
<tr>
	<td>setText</td>
    <td>param</td>
    <td>给指定的菜单项（menu item）设置文本。'param' 参数包含两个属性：<br>
	target：DOM 对象，被设定的菜单项（menu item）。<br>
	text：string，新的文本值。<br>
	<br>
	代码实例：
<pre style="color:#006600">
var item = $('#mm').menu('findItem', 'Save');
$('#mm').menu('setText', {
&nbsp;&nbsp;&nbsp;&nbsp;target: item.target,
&nbsp;&nbsp;&nbsp;&nbsp;text: 'Saving'
});
</pre>
	</td>
</tr>
<tr>
	<td>setIcon</td>
    <td>param</td>
    <td>给指定的菜单项（menu item）设置图标。'param' 参数包含两个属性：<br>
	target：DOM 对象，即菜单项（menu item）。<br>
	iconCls：新图标的 CSS class。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#mm').menu('setIcon', {
&nbsp;&nbsp;&nbsp;&nbsp;target: $('#m-open')[0],
&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-closed'
});
</pre>
	</td>
</tr>
<tr>
	<td>findItem</td>
    <td>text</td>
    <td>找到指定的菜单项（menu item），返回对象与 getItem 方法相同。<br>
	代码实例：
<pre style="color:#006600">
// find 'Open' item and disable it
var item = $('#mm').menu('findItem', 'Open');
$('#mm').menu('disableItem', item.target);
</pre>
	</td>
</tr>
<tr>
	<td>appendItem</td>
    <td>options</td>
    <td>追加一个新的菜单项（menu item），'param' 参数指示新的项目属性。默认情况下，新增的项目将作为顶级菜单项（menu item）。如需追加一个子菜单项，需设置 'parent' 属性，用来指示已经有子项目的父项目元素。<br>
	代码实例：
<pre style="color:#006600">
// append a top menu item
$('#mm').menu('appendItem', {
&nbsp;&nbsp;&nbsp;&nbsp;text: 'New Item',
&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-ok',
&nbsp;&nbsp;&nbsp;&nbsp;onclick: function(){alert('New Item')}
});
// append a menu separator
$('#mm').menu('appendItem', {
&nbsp;&nbsp;&nbsp;&nbsp;separator: true
});
// append a sub menu item
var item = $('#mm').menu('findItem', 'Open');  // find 'Open' item
$('#mm').menu('appendItem', {
&nbsp;&nbsp;&nbsp;&nbsp;parent: item.target,  // the parent item element
&nbsp;&nbsp;&nbsp;&nbsp;text: 'Open Excel',
&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-excel',
&nbsp;&nbsp;&nbsp;&nbsp;onclick: function(){alert('Open Excel')}
});
</pre>
	</td>
</tr>
<tr>
	<td>removeItem</td>
    <td>itemEl</td>
    <td>移除指定的菜单项（menu item）。</td>
</tr>
<tr>
	<td>enableItem</td>
    <td>itemEl</td>
    <td>启用菜单项（menu item）。</td>
</tr>
<tr>
	<td>disableItem</td>
    <td>itemEl</td>
    <td>禁用菜单项（menu item）。</td>
</tr>
</table></div>') where ID=5664;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）属性（property）。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用按钮。<br>
	代码实例：
<pre style="color:#006600">
$('#btn').linkbutton('disable');
</pre>
	</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用按钮。<br>
	代码实例：
<pre style="color:#006600">
$('#btn').linkbutton('enable');
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>none</td>
    <td>选中按钮。该方法自版本 1.3.3 起可用。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>none</td>
    <td>未选中按钮。该方法自版本 1.3.3 起可用。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）属性（property）。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用按钮。<br>
	代码实例：
<pre style="color:#006600">
$('#btn').linkbutton('disable');
</pre>
	</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用按钮。<br>
	代码实例：
<pre style="color:#006600">
$('#btn').linkbutton('enable');
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>none</td>
    <td>选中按钮。该方法自版本 1.3.3 起可用。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>none</td>
    <td>未选中按钮。该方法自版本 1.3.3 起可用。</td>
</tr>
</table></div>') where ID=5665;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用分割按钮（splitbutton）。代码实例：
<pre style="color:#006600">
$('#sb').splitbutton('disable');
</pre>
	</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用分割按钮（splitbutton）。代码实例：
<pre style="color:#006600">
$('#sb').splitbutton('enable');
</pre>
	</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁分割按钮（splitbutton）。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用分割按钮（splitbutton）。代码实例：
<pre style="color:#006600">
$('#sb').splitbutton('disable');
</pre>
	</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用分割按钮（splitbutton）。代码实例：
<pre style="color:#006600">
$('#sb').splitbutton('enable');
</pre>
	</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁分割按钮（splitbutton）。</td>
</tr>
</table></div>') where ID=5667;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>submit</td>
    <td>options</td>
    <td>做提交动作，options 参数是一个对象，它包含下列属性：<br>
	url：动作的 URL<br>
	onSubmit：提交之前的回调函数<br>
	success：提交成功之后的回调函数<br>
	<br>
	下面的实例演示如何提交一个有效表单，避免重复提交表单。
<pre style="color:#006600">
$.messager.progress();&nbsp;&nbsp;&nbsp;&nbsp;// display the progress bar
$('#ff').form('submit', {
&nbsp;&nbsp;&nbsp;&nbsp;url: ...,
&nbsp;&nbsp;&nbsp;&nbsp;onSubmit: function(){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var isValid = $(this).form('validate');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (!isValid){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$.messager.progress('close');&nbsp;&nbsp;&nbsp;&nbsp;// hide progress bar while the form is invalid
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return isValid;&nbsp;&nbsp;&nbsp;&nbsp;// return false will stop the form submission
&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;success: function(){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$.messager.progress('close');&nbsp;&nbsp;&nbsp;&nbsp;// hide progress bar while submit successfully
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>load</td>
    <td>data</td>
    <td>加载记录来填充表单。data 参数可以是一个字符串或者对象类型，字符串作为一个远程 URL，否则作为一个本地记录。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#ff').form('load','get_data.php');&nbsp;&nbsp;&nbsp;&nbsp;// load from URL

$('#ff').form('load',{
&nbsp;&nbsp;&nbsp;&nbsp;name:'name2',
&nbsp;&nbsp;&nbsp;&nbsp;email:'mymail@gmail.com',
&nbsp;&nbsp;&nbsp;&nbsp;subject:'subject2',
&nbsp;&nbsp;&nbsp;&nbsp;message:'message2',
&nbsp;&nbsp;&nbsp;&nbsp;language:5
});
</pre>
	</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除表单数据。</td>
</tr>
<tr>
	<td>reset</td>
    <td>none</td>
    <td>重置表单数据。该方法自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>validate</td>
    <td>none</td>
    <td>进行表单字段验证，当全部字段都有效时返回 true 。该方法和 validatebox 插件一起使用。</td>
</tr>
<tr>
	<td>enableValidation</td>
    <td>none</td>
    <td>启用验证。该方法自版本 1.3.4 起可用。</td>
</tr>
<tr>
	<td>disableValidation</td>
    <td>none</td>
    <td>禁用验证。该方法自版本 1.3.4 起可用。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>submit</td>
    <td>options</td>
    <td>做提交动作，options 参数是一个对象，它包含下列属性：<br>
	url：动作的 URL<br>
	onSubmit：提交之前的回调函数<br>
	success：提交成功之后的回调函数<br>
	<br>
	下面的实例演示如何提交一个有效表单，避免重复提交表单。
<pre style="color:#006600">
$.messager.progress();&nbsp;&nbsp;&nbsp;&nbsp;// display the progress bar
$('#ff').form('submit', {
&nbsp;&nbsp;&nbsp;&nbsp;url: ...,
&nbsp;&nbsp;&nbsp;&nbsp;onSubmit: function(){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var isValid = $(this).form('validate');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (!isValid){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$.messager.progress('close');&nbsp;&nbsp;&nbsp;&nbsp;// hide progress bar while the form is invalid
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return isValid;&nbsp;&nbsp;&nbsp;&nbsp;// return false will stop the form submission
&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;success: function(){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$.messager.progress('close');&nbsp;&nbsp;&nbsp;&nbsp;// hide progress bar while submit successfully
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>load</td>
    <td>data</td>
    <td>加载记录来填充表单。data 参数可以是一个字符串或者对象类型，字符串作为一个远程 URL，否则作为一个本地记录。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#ff').form('load','get_data.php');&nbsp;&nbsp;&nbsp;&nbsp;// load from URL

$('#ff').form('load',{
&nbsp;&nbsp;&nbsp;&nbsp;name:'name2',
&nbsp;&nbsp;&nbsp;&nbsp;email:'mymail@gmail.com',
&nbsp;&nbsp;&nbsp;&nbsp;subject:'subject2',
&nbsp;&nbsp;&nbsp;&nbsp;message:'message2',
&nbsp;&nbsp;&nbsp;&nbsp;language:5
});
</pre>
	</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除表单数据。</td>
</tr>
<tr>
	<td>reset</td>
    <td>none</td>
    <td>重置表单数据。该方法自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>validate</td>
    <td>none</td>
    <td>进行表单字段验证，当全部字段都有效时返回 true 。该方法和 validatebox 插件一起使用。</td>
</tr>
<tr>
	<td>enableValidation</td>
    <td>none</td>
    <td>启用验证。该方法自版本 1.3.4 起可用。</td>
</tr>
<tr>
	<td>disableValidation</td>
    <td>none</td>
    <td>禁用验证。该方法自版本 1.3.4 起可用。</td>
</tr>
</table></div>') where ID=5668;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>required</td>
    <td>boolean</td>
    <td>定义是否字段应被输入。</td>
	<td>false</td>
</tr>
<tr>
	<td>validType</td>
    <td>string,array</td>
    <td>定义字段的验证类型，比如 email、url，等等。可能的值：<br>
	1、验证类型字符串，应用单个验证规则。<br>
	2、验证类型数组，应用多个验证规则。单个字段上的多个验证规则自版本  1.3.2 起可用。<br>
	<br>
	代码实例：
<pre style="color:#006600">
&lt;input class="easyui-validatebox" data-options="required:true,validType:'url'"&gt;
&lt;input class="easyui-validatebox" data-options="
&nbsp;&nbsp;&nbsp;&nbsp;required:true,
&nbsp;&nbsp;&nbsp;&nbsp;validType:['email','length[0,20]']
"&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>delay</td>
    <td>number</td>
    <td>延迟验证最后的输入值。该属性自版本 1.3.2 起可用。</td>
	<td>200</td>
</tr>
<tr>
	<td>missingMessage</td>
    <td>string</td>
    <td>当文本框为空时出现的提示文本。</td>
	<td>该字段是必需的。</td>
</tr>
<tr>
	<td>invalidMessage</td>
    <td>string</td>
    <td>当文本框的内容无效时出现的提示文本。</td>
	<td>null</td>
</tr>
<tr>
	<td>tipPosition</td>
    <td>string</td>
    <td>定义当文本框的内容无效时提示消息的位置。可能的值：'left'、'right'。该属性自版本 1.3.2 起可用。</td>
	<td>right</td>
</tr>
<tr>
	<td>deltaX</td>
    <td>number</td>
    <td>在 X 方向的提示偏移。该属性自版本 1.3.3 起可用。</td>
	<td>0</td>
</tr>
<tr>
	<td>novalidate</td>
    <td>boolean</td>
    <td>当设置为 true 时，则禁用验证。该属性自版本 1.3.4 起可用。</td>
	<td>false</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>required</td>
    <td>boolean</td>
    <td>定义是否字段应被输入。</td>
	<td>false</td>
</tr>
<tr>
	<td>validType</td>
    <td>string,array</td>
    <td>定义字段的验证类型，比如 email、url，等等。可能的值：<br>
	1、验证类型字符串，应用单个验证规则。<br>
	2、验证类型数组，应用多个验证规则。单个字段上的多个验证规则自版本  1.3.2 起可用。<br>
	<br>
	代码实例：
<pre style="color:#006600">
&lt;input class="easyui-validatebox" data-options="required:true,validType:'url'"&gt;
&lt;input class="easyui-validatebox" data-options="
&nbsp;&nbsp;&nbsp;&nbsp;required:true,
&nbsp;&nbsp;&nbsp;&nbsp;validType:['email','length[0,20]']
"&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>delay</td>
    <td>number</td>
    <td>延迟验证最后的输入值。该属性自版本 1.3.2 起可用。</td>
	<td>200</td>
</tr>
<tr>
	<td>missingMessage</td>
    <td>string</td>
    <td>当文本框为空时出现的提示文本。</td>
	<td>该字段是必需的。</td>
</tr>
<tr>
	<td>invalidMessage</td>
    <td>string</td>
    <td>当文本框的内容无效时出现的提示文本。</td>
	<td>null</td>
</tr>
<tr>
	<td>tipPosition</td>
    <td>string</td>
    <td>定义当文本框的内容无效时提示消息的位置。可能的值：'left'、'right'。该属性自版本 1.3.2 起可用。</td>
	<td>right</td>
</tr>
<tr>
	<td>deltaX</td>
    <td>number</td>
    <td>在 X 方向的提示偏移。该属性自版本 1.3.3 起可用。</td>
	<td>0</td>
</tr>
<tr>
	<td>novalidate</td>
    <td>boolean</td>
    <td>当设置为 true 时，则禁用验证。该属性自版本 1.3.4 起可用。</td>
	<td>false</td>
</tr>
</table></div>') where ID=5669;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>组件的宽度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>组件的高度。该属性自版本 1.3.2 起可用。</td>
	<td>22</td>
</tr>
<tr>
	<td>panelWidth</td>
    <td>number</td>
    <td>下拉面板的宽度。</td>
	<td>null</td>
</tr>
<tr>
	<td>panelHeight</td>
    <td>number</td>
    <td>下拉面板的高度。</td>
	<td>200</td>
</tr>
<tr>
	<td>multiple</td>
    <td>boolean</td>
    <td>定义是否支持多选。</td>
	<td>false</td>
</tr>
<tr>
	<td>selectOnNavigation</td>
    <td>boolean</td>
    <td>定义当通过键盘导航项目时是否选择项目。该属性自版本 1.3.3 起可用。</td>
	<td>true</td>
</tr>
<tr>
	<td>separator</td>
    <td>string</td>
    <td>多选时文本的分隔符。</td>
	<td>,</td>
</tr>
<tr>
	<td>editable</td>
    <td>boolean</td>
    <td>定义用户是否可以往文本域中直接输入文字。</td>
	<td>true</td>
</tr>
<tr>
	<td>disabled</td>
    <td>boolean</td>
    <td>定义是否禁用文本域。</td>
	<td>false</td>
</tr>
<tr>
	<td>readonly</td>
    <td>boolean</td>
    <td>定义组件是否只读。该属性自版本 1.3.3 起可用。</td>
	<td>false</td>
</tr>
<tr>
	<td>hasDownArrow</td>
    <td>boolean</td>
    <td>定义是否显示向下箭头的按钮。</td>
	<td>true</td>
</tr>
<tr>
	<td>value</td>
    <td>string</td>
    <td>默认值。</td>
	<td></td>
</tr>
<tr>
	<td>delay</td>
    <td>number</td>
    <td>从最后一个键的输入事件起，延迟进行搜索。</td>
	<td>200</td>
</tr>
<tr>
	<td>keyHandler</td>
    <td>object</td>
    <td>当用户按键后调用的函数。默认的 keyHandler 定义如下：
<pre style="color:#006600">
keyHandler: {
&nbsp;&nbsp;&nbsp;&nbsp;up: function(){},
&nbsp;&nbsp;&nbsp;&nbsp;down: function(){},
&nbsp;&nbsp;&nbsp;&nbsp;enter: function(){},
&nbsp;&nbsp;&nbsp;&nbsp;query: function(q){}
}
</pre>
	</td>
	<td></td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>组件的宽度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>组件的高度。该属性自版本 1.3.2 起可用。</td>
	<td>22</td>
</tr>
<tr>
	<td>panelWidth</td>
    <td>number</td>
    <td>下拉面板的宽度。</td>
	<td>null</td>
</tr>
<tr>
	<td>panelHeight</td>
    <td>number</td>
    <td>下拉面板的高度。</td>
	<td>200</td>
</tr>
<tr>
	<td>multiple</td>
    <td>boolean</td>
    <td>定义是否支持多选。</td>
	<td>false</td>
</tr>
<tr>
	<td>selectOnNavigation</td>
    <td>boolean</td>
    <td>定义当通过键盘导航项目时是否选择项目。该属性自版本 1.3.3 起可用。</td>
	<td>true</td>
</tr>
<tr>
	<td>separator</td>
    <td>string</td>
    <td>多选时文本的分隔符。</td>
	<td>,</td>
</tr>
<tr>
	<td>editable</td>
    <td>boolean</td>
    <td>定义用户是否可以往文本域中直接输入文字。</td>
	<td>true</td>
</tr>
<tr>
	<td>disabled</td>
    <td>boolean</td>
    <td>定义是否禁用文本域。</td>
	<td>false</td>
</tr>
<tr>
	<td>readonly</td>
    <td>boolean</td>
    <td>定义组件是否只读。该属性自版本 1.3.3 起可用。</td>
	<td>false</td>
</tr>
<tr>
	<td>hasDownArrow</td>
    <td>boolean</td>
    <td>定义是否显示向下箭头的按钮。</td>
	<td>true</td>
</tr>
<tr>
	<td>value</td>
    <td>string</td>
    <td>默认值。</td>
	<td></td>
</tr>
<tr>
	<td>delay</td>
    <td>number</td>
    <td>从最后一个键的输入事件起，延迟进行搜索。</td>
	<td>200</td>
</tr>
<tr>
	<td>keyHandler</td>
    <td>object</td>
    <td>当用户按键后调用的函数。默认的 keyHandler 定义如下：
<pre style="color:#006600">
keyHandler: {
&nbsp;&nbsp;&nbsp;&nbsp;up: function(){},
&nbsp;&nbsp;&nbsp;&nbsp;down: function(){},
&nbsp;&nbsp;&nbsp;&nbsp;enter: function(){},
&nbsp;&nbsp;&nbsp;&nbsp;query: function(q){}
}
</pre>
	</td>
	<td></td>
</tr>
</table></div>') where ID=5670;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>panel</td>
    <td>none</td>
    <td>返回下拉面板对象。</td>
</tr>
<tr>
	<td>textbox</td>
    <td>none</td>
    <td>返回文本框对象。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁组件。</td>
</tr>
<tr>
	<td>resize</td>
    <td>width</td>
    <td>调整组件的宽度。</td>
</tr>
<tr>
	<td>showPanel</td>
    <td>none</td>
    <td>显示下拉面板。</td>
</tr>
<tr>
	<td>hidePanel</td>
    <td>none</td>
    <td>隐藏下拉面板。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用组件。</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用组件。</td>
</tr>
<tr>
	<td>readonly</td>
    <td>mode</td>
    <td>启用/禁用只读模式。该方法自版本 1.3.3 起可用。<br>
	用法实例：
<pre style="color:#006600">
$('#cc').combo('readonly');&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// enable readonly mode
$('#cc').combo('readonly', true);&nbsp;&nbsp;&nbsp;&nbsp;// enable readonly mode
$('#cc').combo('readonly', false);&nbsp;&nbsp;&nbsp;&nbsp;// disable readonly mode
</pre>
	</td>
</tr>
<tr>
	<td> validate </td>
    <td>none</td>
    <td>验证输入的值。</td>
</tr>
<tr>
	<td> isValid </td>
    <td>none</td>
    <td>返回验证结果。</td>
</tr>
<tr>
	<td> clear </td>
    <td>none</td>
    <td>清除组件的值。</td>
</tr>
<tr>
	<td> reset </td>
    <td>none</td>
    <td>重置组件的值。该方法自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td> getText </td>
    <td>none</td>
    <td>获取输入的文本。</td>
</tr>
<tr>
	<td> setText </td>
    <td>none</td>
    <td>设置文本值。</td>
</tr>
<tr>
	<td> getValues </td>
    <td>none</td>
    <td>获取组件的值的数组。</td>
</tr>
<tr>
	<td> setValues </td>
    <td>none</td>
    <td>设置组件的值的数组。</td>
</tr>
<tr>
	<td> getValue </td>
    <td>none</td>
    <td>获取组件的值。</td>
</tr>
<tr>
	<td> setValue </td>
    <td>none</td>
    <td>设置组件的值。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>panel</td>
    <td>none</td>
    <td>返回下拉面板对象。</td>
</tr>
<tr>
	<td>textbox</td>
    <td>none</td>
    <td>返回文本框对象。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁组件。</td>
</tr>
<tr>
	<td>resize</td>
    <td>width</td>
    <td>调整组件的宽度。</td>
</tr>
<tr>
	<td>showPanel</td>
    <td>none</td>
    <td>显示下拉面板。</td>
</tr>
<tr>
	<td>hidePanel</td>
    <td>none</td>
    <td>隐藏下拉面板。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用组件。</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用组件。</td>
</tr>
<tr>
	<td>readonly</td>
    <td>mode</td>
    <td>启用/禁用只读模式。该方法自版本 1.3.3 起可用。<br>
	用法实例：
<pre style="color:#006600">
$('#cc').combo('readonly');&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// enable readonly mode
$('#cc').combo('readonly', true);&nbsp;&nbsp;&nbsp;&nbsp;// enable readonly mode
$('#cc').combo('readonly', false);&nbsp;&nbsp;&nbsp;&nbsp;// disable readonly mode
</pre>
	</td>
</tr>
<tr>
	<td> validate </td>
    <td>none</td>
    <td>验证输入的值。</td>
</tr>
<tr>
	<td> isValid </td>
    <td>none</td>
    <td>返回验证结果。</td>
</tr>
<tr>
	<td> clear </td>
    <td>none</td>
    <td>清除组件的值。</td>
</tr>
<tr>
	<td> reset </td>
    <td>none</td>
    <td>重置组件的值。该方法自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td> getText </td>
    <td>none</td>
    <td>获取输入的文本。</td>
</tr>
<tr>
	<td> setText </td>
    <td>none</td>
    <td>设置文本值。</td>
</tr>
<tr>
	<td> getValues </td>
    <td>none</td>
    <td>获取组件的值的数组。</td>
</tr>
<tr>
	<td> setValues </td>
    <td>none</td>
    <td>设置组件的值的数组。</td>
</tr>
<tr>
	<td> getValue </td>
    <td>none</td>
    <td>获取组件的值。</td>
</tr>
<tr>
	<td> setValue </td>
    <td>none</td>
    <td>设置组件的值。</td>
</tr>
</table></div>') where ID=5670;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>valueField</td>
    <td>string</td>
    <td>绑定到该组合框（ComboBox）的 value 上的基础数据的名称。</td>
	<td>value</td>
</tr>
<tr>
	<td>textField</td>
    <td>string</td>
    <td>绑定到该组合框（ComboBox）的 text 上的基础数据的名称。</td>
	<td>text</td>
</tr>
<tr>
	<td>groupField</td>
    <td>string</td>
    <td>指示要被分组的字段。该属性自版本 1.3.4 起可用。</td>
	<td>null</td>
</tr>
<tr>
	<td>groupFormatter</td>
    <td>function(group)</td>
    <td>返回要显示在分组项目上的分组文本。该属性自版本 1.3.4 起可用。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox({
&nbsp;&nbsp;&nbsp;&nbsp;groupFormatter: function(group){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return '&lt;span style="color:red"&gt;' + group + '&lt;/span&gt;';
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>mode</td>
    <td>string</td>
    <td>定义在文本改变时如何加载列表数据。如果组合框（combobox）从服务器加载就设置为 'remote'。当设置为 'remote' 模式时，用户输入的值将会被作为名为 'q' 的 http 请求参数发送到服务器，以获取新的数据。</td>
	<td>local</td>
</tr>
<tr>
	<td>url</td>
    <td>string</td>
    <td>从远程加载列表数据的 URL 。</td>
	<td>null</td>
</tr>
<tr>
	<td>method</td>
    <td>string</td>
    <td>用来检索数据的 http 方法。</td>
	<td>post</td>
</tr>
<tr>
	<td>data</td>
    <td>array</td>
    <td>被加载的列表数据。<br>
	代码实例：
<pre style="color:#006600">
&lt;input class="easyui-combobox" data-options="
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;valueField: 'label',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;textField: 'value',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;label: 'java',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value: 'Java'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;label: 'perl',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value: 'Perl'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;label: 'ruby',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value: 'Ruby'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]" /&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>filter</td>
    <td>function</td>
    <td>定义当 'mode' 设置为 'local' 时如何过滤本地数据。该函数有两个参数：<br>
	q：用户输入的文本。<br>
	row：列表中的行数据。<br>
	返回 true 则允许显示该行。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox({
&nbsp;&nbsp;&nbsp;&nbsp;filter: function(q, row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var opts = $(this).combobox('options');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return row[opts.textField].indexOf(q) == 0;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>formatter</td>
    <td>function</td>
    <td>定义如何呈现行。该函数有一个参数：row。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox({
&nbsp;&nbsp;&nbsp;&nbsp;formatter: function(row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var opts = $(this).combobox('options');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return row[opts.textField];
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>loader</td>
    <td>function(param,success,error)</td>
    <td>定义如何从远程服务器加载数据。返回 false 则取消该动作。该函数有下列参数：<br>
	param：要传到远程服务器的参数对象。<br>
	success(data)：当获取数据成功时将被调用的回调函数。<br>
	error()：当获取数据失败时将被调用的回调函数。<br>
	</td>
	<td>json loader</td>
</tr>
<tr>
	<td>loadFilter</td>
    <td>function(data)</td>
    <td>返回要显示的过滤数据。该属性自版本 1.3.3 起可用。</td>
	<td></td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>valueField</td>
    <td>string</td>
    <td>绑定到该组合框（ComboBox）的 value 上的基础数据的名称。</td>
	<td>value</td>
</tr>
<tr>
	<td>textField</td>
    <td>string</td>
    <td>绑定到该组合框（ComboBox）的 text 上的基础数据的名称。</td>
	<td>text</td>
</tr>
<tr>
	<td>groupField</td>
    <td>string</td>
    <td>指示要被分组的字段。该属性自版本 1.3.4 起可用。</td>
	<td>null</td>
</tr>
<tr>
	<td>groupFormatter</td>
    <td>function(group)</td>
    <td>返回要显示在分组项目上的分组文本。该属性自版本 1.3.4 起可用。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox({
&nbsp;&nbsp;&nbsp;&nbsp;groupFormatter: function(group){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return '&lt;span style="color:red"&gt;' + group + '&lt;/span&gt;';
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>mode</td>
    <td>string</td>
    <td>定义在文本改变时如何加载列表数据。如果组合框（combobox）从服务器加载就设置为 'remote'。当设置为 'remote' 模式时，用户输入的值将会被作为名为 'q' 的 http 请求参数发送到服务器，以获取新的数据。</td>
	<td>local</td>
</tr>
<tr>
	<td>url</td>
    <td>string</td>
    <td>从远程加载列表数据的 URL 。</td>
	<td>null</td>
</tr>
<tr>
	<td>method</td>
    <td>string</td>
    <td>用来检索数据的 http 方法。</td>
	<td>post</td>
</tr>
<tr>
	<td>data</td>
    <td>array</td>
    <td>被加载的列表数据。<br>
	代码实例：
<pre style="color:#006600">
&lt;input class="easyui-combobox" data-options="
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;valueField: 'label',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;textField: 'value',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;label: 'java',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value: 'Java'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;label: 'perl',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value: 'Perl'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;label: 'ruby',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value: 'Ruby'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]" /&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>filter</td>
    <td>function</td>
    <td>定义当 'mode' 设置为 'local' 时如何过滤本地数据。该函数有两个参数：<br>
	q：用户输入的文本。<br>
	row：列表中的行数据。<br>
	返回 true 则允许显示该行。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox({
&nbsp;&nbsp;&nbsp;&nbsp;filter: function(q, row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var opts = $(this).combobox('options');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return row[opts.textField].indexOf(q) == 0;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>formatter</td>
    <td>function</td>
    <td>定义如何呈现行。该函数有一个参数：row。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox({
&nbsp;&nbsp;&nbsp;&nbsp;formatter: function(row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var opts = $(this).combobox('options');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return row[opts.textField];
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>loader</td>
    <td>function(param,success,error)</td>
    <td>定义如何从远程服务器加载数据。返回 false 则取消该动作。该函数有下列参数：<br>
	param：要传到远程服务器的参数对象。<br>
	success(data)：当获取数据成功时将被调用的回调函数。<br>
	error()：当获取数据失败时将被调用的回调函数。<br>
	</td>
	<td>json loader</td>
</tr>
<tr>
	<td>loadFilter</td>
    <td>function(data)</td>
    <td>返回要显示的过滤数据。该属性自版本 1.3.3 起可用。</td>
	<td></td>
</tr>
</table></div>') where ID=5671;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onBeforeLoad</td>
    <td>param</td>
    <td>在请求加载数据之前触发，返回 false 则取消加载动作。<br>
	代码实例：
<pre style="color:#006600">
// change the http request parameters before load data from server
$('#cc').combobox({
&nbsp;&nbsp;&nbsp;&nbsp;onBeforeLoad: function(param){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;param.id = 2;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;param.language = 'js';
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>onLoadSuccess</td>
    <td>none</td>
    <td>当远程数据加载成功时触发。</td>
</tr>
<tr>
	<td>onLoadError</td>
    <td>none</td>
    <td>当远程数据加载失败时触发。</td>
</tr>
<tr>
	<td>onSelect</td>
    <td>record</td>
    <td>当用户选择一个列表项时触发。</td>
</tr>
<tr>
	<td>onUnselect</td>
    <td>record</td>
    <td>当用户取消选择一个列表项时触发。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onBeforeLoad</td>
    <td>param</td>
    <td>在请求加载数据之前触发，返回 false 则取消加载动作。<br>
	代码实例：
<pre style="color:#006600">
// change the http request parameters before load data from server
$('#cc').combobox({
&nbsp;&nbsp;&nbsp;&nbsp;onBeforeLoad: function(param){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;param.id = 2;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;param.language = 'js';
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>onLoadSuccess</td>
    <td>none</td>
    <td>当远程数据加载成功时触发。</td>
</tr>
<tr>
	<td>onLoadError</td>
    <td>none</td>
    <td>当远程数据加载失败时触发。</td>
</tr>
<tr>
	<td>onSelect</td>
    <td>record</td>
    <td>当用户选择一个列表项时触发。</td>
</tr>
<tr>
	<td>onUnselect</td>
    <td>record</td>
    <td>当用户取消选择一个列表项时触发。</td>
</tr>
</table></div>') where ID=5671;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>getData</td>
    <td>none</td>
    <td>返回加载的数据。</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>加载本地列表数据。</td>
</tr>
<tr>
	<td>reload</td>
    <td>url</td>
    <td>请求远程的列表数据。传 'url' 参数来重写原始的 URL 值。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox('reload');  // reload list data using old URL
$('#cc').combobox('reload','get_data.php');  // reload list data using new URL
</pre>
	</td>
</tr>
<tr>
	<td>setValues</td>
    <td>values</td>
    <td>设置组合框（combobox）值的数组。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox('setValues', ['001','002']);
</pre>
	</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置组合框（combobox）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox('setValue', '001');
</pre>
	</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除组合框（combobox）的值。</td>
</tr>
<tr>
	<td>select</td>
    <td>value</td>
    <td>选择指定的选项。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>value</td>
    <td>取消选择指定的选项。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>getData</td>
    <td>none</td>
    <td>返回加载的数据。</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>加载本地列表数据。</td>
</tr>
<tr>
	<td>reload</td>
    <td>url</td>
    <td>请求远程的列表数据。传 'url' 参数来重写原始的 URL 值。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox('reload');  // reload list data using old URL
$('#cc').combobox('reload','get_data.php');  // reload list data using new URL
</pre>
	</td>
</tr>
<tr>
	<td>setValues</td>
    <td>values</td>
    <td>设置组合框（combobox）值的数组。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox('setValues', ['001','002']);
</pre>
	</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置组合框（combobox）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combobox('setValue', '001');
</pre>
	</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除组合框（combobox）的值。</td>
</tr>
<tr>
	<td>select</td>
    <td>value</td>
    <td>选择指定的选项。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>value</td>
    <td>取消选择指定的选项。</td>
</tr>
</table></div>') where ID=5671;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>tree</td>
    <td>none</td>
    <td>返回树（tree）对象。下面的实例演示如何取得选中的树节点。
<pre style="color:#006600">
var t = $('#cc').combotree('tree');&nbsp;&nbsp;&nbsp;&nbsp;// get the tree object
var n = t.tree('getSelected');&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// get selected node
alert(n.text);
</pre>
	</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>记住本地的树（tree）数据。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combotree('loadData', [{
&nbsp;&nbsp;&nbsp;&nbsp;id: 1,
&nbsp;&nbsp;&nbsp;&nbsp;text: 'Languages',
&nbsp;&nbsp;&nbsp;&nbsp;children: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 11,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Java'
&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 12,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'C++'
&nbsp;&nbsp;&nbsp;&nbsp;}]
}]);
</pre>
	</td>
</tr>
<tr>
	<td>reload</td>
    <td>url</td>
    <td>再一次请求远程的树（tree）数据。传 'url' 参数来重写原始的 URL 值。</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除组件的值。</td>
</tr>
<tr>
	<td>setValues</td>
    <td>values</td>
    <td>设置组件值的数组。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combotree('setValues', [1,3,21]);
</pre>
	</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置组件的值。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combotree('setValue', 6);
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>tree</td>
    <td>none</td>
    <td>返回树（tree）对象。下面的实例演示如何取得选中的树节点。
<pre style="color:#006600">
var t = $('#cc').combotree('tree');&nbsp;&nbsp;&nbsp;&nbsp;// get the tree object
var n = t.tree('getSelected');&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// get selected node
alert(n.text);
</pre>
	</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>记住本地的树（tree）数据。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combotree('loadData', [{
&nbsp;&nbsp;&nbsp;&nbsp;id: 1,
&nbsp;&nbsp;&nbsp;&nbsp;text: 'Languages',
&nbsp;&nbsp;&nbsp;&nbsp;children: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 11,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Java'
&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 12,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'C++'
&nbsp;&nbsp;&nbsp;&nbsp;}]
}]);
</pre>
	</td>
</tr>
<tr>
	<td>reload</td>
    <td>url</td>
    <td>再一次请求远程的树（tree）数据。传 'url' 参数来重写原始的 URL 值。</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除组件的值。</td>
</tr>
<tr>
	<td>setValues</td>
    <td>values</td>
    <td>设置组件值的数组。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combotree('setValues', [1,3,21]);
</pre>
	</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置组件的值。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combotree('setValue', 6);
</pre>
	</td>
</tr>
</table></div>') where ID=5672;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>loadMsg</td>
    <td>string</td>
    <td>当数据网格（datagrid）加载远程数据时显示的消息。</td>
	<td>null</td>
</tr>
<tr>
	<td>idField</td>
    <td>string</td>
    <td>id 的字段名。</td>
	<td>null</td>
</tr>
<tr>
	<td>textField</td>
    <td>string</td>
    <td>显示在文本框中的 text 字段名。</td>
	<td>null</td>
</tr>
<tr>
	<td>mode</td>
    <td>string</td>
    <td>定义当文本改变时如何加载数据网格（datagrid）数据。如果组合网格（combogrid）从服务器加载就设置为 'remote'。当设置为 'remote' 模式时，用户输入的值将会被作为名为 'q' 的 http 请求参数发送到服务器，以获取新的数据。</td>
	<td>local</td>
</tr>
<tr>
	<td>filter</td>
    <td>function(q, row)</td>
    <td>定义当 'mode' 设置为 'local' 时如何选择本地数据。返回 true 则选择该行。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combogrid({
&nbsp;&nbsp;&nbsp;&nbsp;filter: function(q, row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var opts = $(this).combogrid('options');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return row[opts.textField].indexOf(q) == 0;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td>100</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>loadMsg</td>
    <td>string</td>
    <td>当数据网格（datagrid）加载远程数据时显示的消息。</td>
	<td>null</td>
</tr>
<tr>
	<td>idField</td>
    <td>string</td>
    <td>id 的字段名。</td>
	<td>null</td>
</tr>
<tr>
	<td>textField</td>
    <td>string</td>
    <td>显示在文本框中的 text 字段名。</td>
	<td>null</td>
</tr>
<tr>
	<td>mode</td>
    <td>string</td>
    <td>定义当文本改变时如何加载数据网格（datagrid）数据。如果组合网格（combogrid）从服务器加载就设置为 'remote'。当设置为 'remote' 模式时，用户输入的值将会被作为名为 'q' 的 http 请求参数发送到服务器，以获取新的数据。</td>
	<td>local</td>
</tr>
<tr>
	<td>filter</td>
    <td>function(q, row)</td>
    <td>定义当 'mode' 设置为 'local' 时如何选择本地数据。返回 true 则选择该行。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combogrid({
&nbsp;&nbsp;&nbsp;&nbsp;filter: function(q, row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var opts = $(this).combogrid('options');
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return row[opts.textField].indexOf(q) == 0;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td>100</td>
</tr>
</table></div>') where ID=5673;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>grid</td>
    <td>none</td>
    <td>返回数据网格（datagrid）对象。下面的实例显示如何取得选中的行：
<pre style="color:#006600">
var g = $('#cc').combogrid('grid');&nbsp;&nbsp;&nbsp;&nbsp;// get datagrid object
var r = g.datagrid('getSelected');&nbsp;&nbsp;&nbsp;&nbsp;// get the selected row
alert(r.name);
</pre>
	</td>
</tr>
<tr>
	<td>setValues</td>
    <td>values</td>
    <td>设置组件值的数组。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combogrid('setValues', ['001','007']);
</pre>
	</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置组件的值。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combogrid('setValue', '002');
</pre>
	</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除组件的值。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>grid</td>
    <td>none</td>
    <td>返回数据网格（datagrid）对象。下面的实例显示如何取得选中的行：
<pre style="color:#006600">
var g = $('#cc').combogrid('grid');&nbsp;&nbsp;&nbsp;&nbsp;// get datagrid object
var r = g.datagrid('getSelected');&nbsp;&nbsp;&nbsp;&nbsp;// get the selected row
alert(r.name);
</pre>
	</td>
</tr>
<tr>
	<td>setValues</td>
    <td>values</td>
    <td>设置组件值的数组。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combogrid('setValues', ['001','007']);
</pre>
	</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置组件的值。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').combogrid('setValue', '002');
</pre>
	</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除组件的值。</td>
</tr>
</table></div>') where ID=5673;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁数字框（numberbox）对象。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用该域。</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用该域。</td>
</tr>
<tr>
	<td>fix</td>
    <td>none</td>
    <td>把值固定为有效的值。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>none</td>
    <td>设置数字框（numberbox）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#nn').numberbox('setValue', 206.12);
</pre>
	</td>
</tr>
<tr>
	<td>getValue</td>
    <td>none</td>
    <td>获取数字框（numberbox）的值。<br>
	代码实例：
<pre style="color:#006600">
var v = $('#nn').numberbox('getValue');
alert(v);
</pre>
	</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除数字框（numberbox）的值。</td>
</tr>
<tr>
	<td>reset</td>
    <td>none</td>
    <td>重置数字框（numberbox）的值。该方法自版本 1.3.2 起可用。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁数字框（numberbox）对象。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用该域。</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用该域。</td>
</tr>
<tr>
	<td>fix</td>
    <td>none</td>
    <td>把值固定为有效的值。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>none</td>
    <td>设置数字框（numberbox）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#nn').numberbox('setValue', 206.12);
</pre>
	</td>
</tr>
<tr>
	<td>getValue</td>
    <td>none</td>
    <td>获取数字框（numberbox）的值。<br>
	代码实例：
<pre style="color:#006600">
var v = $('#nn').numberbox('getValue');
alert(v);
</pre>
	</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除数字框（numberbox）的值。</td>
</tr>
<tr>
	<td>reset</td>
    <td>none</td>
    <td>重置数字框（numberbox）的值。该方法自版本 1.3.2 起可用。</td>
</tr>
</table></div>') where ID=5674;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>panelWidth</td>
    <td>number</td>
    <td>下拉日历面板的宽度。</td>
	<td>180</td>
</tr>
<tr>
	<td>panelHeight</td>
    <td>number</td>
    <td>下拉日历面板的高度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>currentText</td>
    <td>string</td>
    <td>当前日期按钮上显示的文本。</td>
	<td>Today</td>
</tr>
<tr>
	<td>closeText</td>
    <td>string</td>
    <td>关闭按钮上显示的文本。</td>
	<td>Close</td>
</tr>
<tr>
	<td>okText</td>
    <td>string</td>
    <td>确定按钮上显示的文本。</td>
	<td>Ok</td>
</tr>
<tr>
	<td>disabled</td>
    <td>boolean</td>
    <td>设置为 true 时禁用该域。</td>
	<td>false</td>
</tr>
<tr>
	<td>buttons</td>
    <td>array</td>
    <td>日历下面的按钮。该属性自版本 1.3.5 起可用。<br>
	代码实例：
<pre style="color:#006600">
var buttons = $.extend([], $.fn.datebox.defaults.buttons);
buttons.splice(1, 0, {
&nbsp;&nbsp;&nbsp;&nbsp;text: 'MyBtn',
&nbsp;&nbsp;&nbsp;&nbsp;handler: function(target){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('click MyBtn');
&nbsp;&nbsp;&nbsp;&nbsp;}
});
$('#dd').datebox({
&nbsp;&nbsp;&nbsp;&nbsp;buttons: buttons
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>sharedCalendar</td>
    <td>string,selector</td>
    <td>多个日期框（datebox）组件使用的共享日历。该属性自版本 1.3.5 起可用。<br>
	代码实例：
<pre style="color:#006600">
&lt;input class="easyui-datebox" sharedCalendar="#sc"&gt;
&lt;input class="easyui-datebox" sharedCalendar="#sc"&gt;
&lt;div id="sc" class="easyui-calendar"&gt;&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>formatter</td>
    <td>function</td>
    <td>格式化日期的函数，该函数有一个 'date' 参数，并返回一个字符串值。下面的实例演示如何重写默认的格式化（formatter）函数。
<pre style="color:#006600">
$.fn.datebox.defaults.formatter = function(date){
&nbsp;&nbsp;&nbsp;&nbsp;var y = date.getFullYear();
&nbsp;&nbsp;&nbsp;&nbsp;var m = date.getMonth()+1;
&nbsp;&nbsp;&nbsp;&nbsp;var d = date.getDate();
&nbsp;&nbsp;&nbsp;&nbsp;return m+'/'+d+'/'+y;
}
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>parser</td>
    <td>function</td>
    <td>解析日期字符串的函数，该函数有一个 'date' 字符串，并返回一个日期值。下面的实例演示如何重写默认的解析（parser）函数。
<pre style="color:#006600">
$.fn.datebox.defaults.parser = function(s){
&nbsp;&nbsp;&nbsp;&nbsp;var t = Date.parse(s);
&nbsp;&nbsp;&nbsp;&nbsp;if (!isNaN(t)){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return new Date(t);
&nbsp;&nbsp;&nbsp;&nbsp;} else {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return new Date();
&nbsp;&nbsp;&nbsp;&nbsp;}
}
</pre>
	</td>
	<td></td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>panelWidth</td>
    <td>number</td>
    <td>下拉日历面板的宽度。</td>
	<td>180</td>
</tr>
<tr>
	<td>panelHeight</td>
    <td>number</td>
    <td>下拉日历面板的高度。</td>
	<td>auto</td>
</tr>
<tr>
	<td>currentText</td>
    <td>string</td>
    <td>当前日期按钮上显示的文本。</td>
	<td>Today</td>
</tr>
<tr>
	<td>closeText</td>
    <td>string</td>
    <td>关闭按钮上显示的文本。</td>
	<td>Close</td>
</tr>
<tr>
	<td>okText</td>
    <td>string</td>
    <td>确定按钮上显示的文本。</td>
	<td>Ok</td>
</tr>
<tr>
	<td>disabled</td>
    <td>boolean</td>
    <td>设置为 true 时禁用该域。</td>
	<td>false</td>
</tr>
<tr>
	<td>buttons</td>
    <td>array</td>
    <td>日历下面的按钮。该属性自版本 1.3.5 起可用。<br>
	代码实例：
<pre style="color:#006600">
var buttons = $.extend([], $.fn.datebox.defaults.buttons);
buttons.splice(1, 0, {
&nbsp;&nbsp;&nbsp;&nbsp;text: 'MyBtn',
&nbsp;&nbsp;&nbsp;&nbsp;handler: function(target){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('click MyBtn');
&nbsp;&nbsp;&nbsp;&nbsp;}
});
$('#dd').datebox({
&nbsp;&nbsp;&nbsp;&nbsp;buttons: buttons
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>sharedCalendar</td>
    <td>string,selector</td>
    <td>多个日期框（datebox）组件使用的共享日历。该属性自版本 1.3.5 起可用。<br>
	代码实例：
<pre style="color:#006600">
&lt;input class="easyui-datebox" sharedCalendar="#sc"&gt;
&lt;input class="easyui-datebox" sharedCalendar="#sc"&gt;
&lt;div id="sc" class="easyui-calendar"&gt;&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>formatter</td>
    <td>function</td>
    <td>格式化日期的函数，该函数有一个 'date' 参数，并返回一个字符串值。下面的实例演示如何重写默认的格式化（formatter）函数。
<pre style="color:#006600">
$.fn.datebox.defaults.formatter = function(date){
&nbsp;&nbsp;&nbsp;&nbsp;var y = date.getFullYear();
&nbsp;&nbsp;&nbsp;&nbsp;var m = date.getMonth()+1;
&nbsp;&nbsp;&nbsp;&nbsp;var d = date.getDate();
&nbsp;&nbsp;&nbsp;&nbsp;return m+'/'+d+'/'+y;
}
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>parser</td>
    <td>function</td>
    <td>解析日期字符串的函数，该函数有一个 'date' 字符串，并返回一个日期值。下面的实例演示如何重写默认的解析（parser）函数。
<pre style="color:#006600">
$.fn.datebox.defaults.parser = function(s){
&nbsp;&nbsp;&nbsp;&nbsp;var t = Date.parse(s);
&nbsp;&nbsp;&nbsp;&nbsp;if (!isNaN(t)){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return new Date(t);
&nbsp;&nbsp;&nbsp;&nbsp;} else {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return new Date();
&nbsp;&nbsp;&nbsp;&nbsp;}
}
</pre>
	</td>
	<td></td>
</tr>
</table></div>') where ID=5675;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onSelect</td>
    <td>date</td>
    <td>当用户选择一个日期时触发。<br>
	代码实例：
<pre style="color:#006600">
$('#dd').datebox({
&nbsp;&nbsp;&nbsp;&nbsp;onSelect: function(date){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(date.getFullYear()+":"+(date.getMonth()+1)+":"+date.getDate());
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onSelect</td>
    <td>date</td>
    <td>当用户选择一个日期时触发。<br>
	代码实例：
<pre style="color:#006600">
$('#dd').datebox({
&nbsp;&nbsp;&nbsp;&nbsp;onSelect: function(date){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(date.getFullYear()+":"+(date.getMonth()+1)+":"+date.getDate());
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
</table></div>') where ID=5675;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>calendar</td>
    <td>none</td>
    <td>获取日历（calendar）对象。下面的实例演示如何获取日历（calendar）对象，然后重现它。 
<pre style="color:#006600">
// get the calendar object
var c = $('#dd').datebox('calendar');
// set the first day of week to monday
c.calendar({
&nbsp;&nbsp;&nbsp;&nbsp;firstDay: 1
});
</pre>
	</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置日期框（datebox）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#dd').datebox('setValue', '6/1/2012');&nbsp;&nbsp;&nbsp;&nbsp;// set datebox value
var v = $('#dd').datebox('getValue');&nbsp;&nbsp;&nbsp;&nbsp;// get datebox value
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>calendar</td>
    <td>none</td>
    <td>获取日历（calendar）对象。下面的实例演示如何获取日历（calendar）对象，然后重现它。 
<pre style="color:#006600">
// get the calendar object
var c = $('#dd').datebox('calendar');
// set the first day of week to monday
c.calendar({
&nbsp;&nbsp;&nbsp;&nbsp;firstDay: 1
});
</pre>
	</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置日期框（datebox）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#dd').datebox('setValue', '6/1/2012');&nbsp;&nbsp;&nbsp;&nbsp;// set datebox value
var v = $('#dd').datebox('getValue');&nbsp;&nbsp;&nbsp;&nbsp;// get datebox value
</pre>
	</td>
</tr>
</table></div>') where ID=5675;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>spinner</td>
    <td>none</td>
    <td>返回时间微调器（timespinner）对象。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置日期时间框（datetimebox）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');&nbsp;&nbsp;&nbsp;&nbsp;// set datetimebox value
var v = $('#dt').datetimebox('getValue');&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// get datetimebox value
alert(v);
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>spinner</td>
    <td>none</td>
    <td>返回时间微调器（timespinner）对象。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置日期时间框（datetimebox）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');&nbsp;&nbsp;&nbsp;&nbsp;// set datetimebox value
var v = $('#dt').datetimebox('getValue');&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// get datetimebox value
alert(v);
</pre>
	</td>
</tr>
</table></div>') where ID=5676;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>日历（calendar）组件的宽度。</td>
	<td>180</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>日历（calendar）组件的高度。</td>
	<td>180</td>
</tr>
<tr>
	<td>fit</td>
    <td>boolean</td>
    <td>当设置为 true 时，则设置日历的尺寸以适应它的父容器。</td>
	<td>false</td>
</tr>
<tr>
	<td>border</td>
    <td>boolean</td>
    <td>定义是否显示边框。</td>
	<td>true</td>
</tr>
<tr>
	<td>firstDay</td>
    <td>number</td>
    <td>定义每星期的第一天。星期日（Sunday）是 0，星期一（Monday）是 1，...</td>
	<td>0</td>
</tr>
<tr>
	<td>weeks</td>
    <td>array</td>
    <td>显示星期的列表。</td>
	<td>['S','M','T','W','T','F','S']</td>
</tr>
<tr>
	<td>months</td>
    <td>array</td>
    <td>显示月份的列表。</td>
	<td>['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']</td>
</tr>
<tr>
	<td>year</td>
    <td>number</td>
    <td>日历的年。下面的实例演示如何使用指定的年和月来创建一个日历。
<pre style="color:#006600">
&lt;div class="easyui-calendar" data-options="year:2012,month:6" /&gt;
</pre>
	</td>
	<td>当前年份（4 位）</td>
</tr>
<tr>
	<td>month</td>
    <td>number</td>
    <td>日历的月。</td>
	<td>当前月份（从 1 开始）</td>
</tr>
<tr>
	<td>current</td>
    <td>Date</td>
    <td>当前的日期。</td>
	<td>当前日期</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>日历（calendar）组件的宽度。</td>
	<td>180</td>
</tr>
<tr>
	<td>height</td>
    <td>number</td>
    <td>日历（calendar）组件的高度。</td>
	<td>180</td>
</tr>
<tr>
	<td>fit</td>
    <td>boolean</td>
    <td>当设置为 true 时，则设置日历的尺寸以适应它的父容器。</td>
	<td>false</td>
</tr>
<tr>
	<td>border</td>
    <td>boolean</td>
    <td>定义是否显示边框。</td>
	<td>true</td>
</tr>
<tr>
	<td>firstDay</td>
    <td>number</td>
    <td>定义每星期的第一天。星期日（Sunday）是 0，星期一（Monday）是 1，...</td>
	<td>0</td>
</tr>
<tr>
	<td>weeks</td>
    <td>array</td>
    <td>显示星期的列表。</td>
	<td>['S','M','T','W','T','F','S']</td>
</tr>
<tr>
	<td>months</td>
    <td>array</td>
    <td>显示月份的列表。</td>
	<td>['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']</td>
</tr>
<tr>
	<td>year</td>
    <td>number</td>
    <td>日历的年。下面的实例演示如何使用指定的年和月来创建一个日历。
<pre style="color:#006600">
&lt;div class="easyui-calendar" data-options="year:2012,month:6" /&gt;
</pre>
	</td>
	<td>当前年份（4 位）</td>
</tr>
<tr>
	<td>month</td>
    <td>number</td>
    <td>日历的月。</td>
	<td>当前月份（从 1 开始）</td>
</tr>
<tr>
	<td>current</td>
    <td>Date</td>
    <td>当前的日期。</td>
	<td>当前日期</td>
</tr>
</table></div>') where ID=5677;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onSelect</td>
    <td>date</td>
    <td>当用户选择一个日期时触发。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').calendar({
&nbsp;&nbsp;&nbsp;&nbsp;onSelect: function(date){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(date.getFullYear()+":"+(date.getMonth()+1)+":"+date.getDate());
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onSelect</td>
    <td>date</td>
    <td>当用户选择一个日期时触发。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').calendar({
&nbsp;&nbsp;&nbsp;&nbsp;onSelect: function(date){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(date.getFullYear()+":"+(date.getMonth()+1)+":"+date.getDate());
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
</table></div>') where ID=5677;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>resize</td>
    <td>none</td>
    <td>调整日历的尺寸。</tr>
<tr>
	<td>moveTo</td>
    <td>date</td>
    <td>移动日历到一个指定的日期。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').calendar('moveTo', new Date(2012, 6, 1));
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>resize</td>
    <td>none</td>
    <td>调整日历的尺寸。</tr>
<tr>
	<td>moveTo</td>
    <td>date</td>
    <td>移动日历到一个指定的日期。<br>
	代码实例：
<pre style="color:#006600">
$('#cc').calendar('moveTo', new Date(2012, 6, 1));
</pre>
	</td>
</tr>
</table></div>') where ID=5677;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁微调器（spinner）组件。</td>
</tr>
<tr>
	<td>resize</td>
    <td>width</td>
    <td>重置组件的宽度。通过传递 'width' 参数来重写初始宽度。<br>
	代码实例：
<pre style="color:#006600">
$('#ss').spinner('resize');  // resize with original width
$('#ss').spinner('resize', 200);  // resize with new width
</pre>
	</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用组件。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用组件。</td>
</tr>
<tr>
	<td>getValue</td>
    <td>none</td>
    <td>获取组件的值。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置组件的值。</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除组件的值。</td>
</tr>
<tr>
	<td>reset</td>
    <td>none</td>
    <td>重置组件的值。该方法自版本 1.3.2 起可用。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>destroy</td>
    <td>none</td>
    <td>销毁微调器（spinner）组件。</td>
</tr>
<tr>
	<td>resize</td>
    <td>width</td>
    <td>重置组件的宽度。通过传递 'width' 参数来重写初始宽度。<br>
	代码实例：
<pre style="color:#006600">
$('#ss').spinner('resize');  // resize with original width
$('#ss').spinner('resize', 200);  // resize with new width
</pre>
	</td>
</tr>
<tr>
	<td>enable</td>
    <td>none</td>
    <td>启用组件。</td>
</tr>
<tr>
	<td>disable</td>
    <td>none</td>
    <td>禁用组件。</td>
</tr>
<tr>
	<td>getValue</td>
    <td>none</td>
    <td>获取组件的值。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置组件的值。</td>
</tr>
<tr>
	<td>clear</td>
    <td>none</td>
    <td>清除组件的值。</td>
</tr>
<tr>
	<td>reset</td>
    <td>none</td>
    <td>重置组件的值。该方法自版本 1.3.2 起可用。</td>
</tr>
</table></div>') where ID=5678;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置数值微调器（numberspinner）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#ss').numberspinner('setValue', 8234725);  // set value
var v = $('#ss').numberspinner('getValue');  // get value
alert(v)
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置数值微调器（numberspinner）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#ss').numberspinner('setValue', 8234725);  // set value
var v = $('#ss').numberspinner('getValue');  // get value
alert(v)
</pre>
	</td>
</tr>
</table></div>') where ID=5679;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置时间微调器（timespinner）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#ss').timespinner('setValue', '17:45');  // set timespinner value
var v = $('#ss').timespinner('getValue');  // get timespinner value
alert(v);
</pre>
	</td>
</tr>
<tr>
	<td>getHours</td>
    <td>none</td>
    <td>获取当前的时钟的值。</td>
</tr>
<tr>
	<td>getMinutes</td>
    <td>none</td>
    <td>获取当前的分钟的值。</td>
</tr>
<tr>
	<td>getSeconds</td>
    <td>none</td>
    <td>获取当前的秒钟的值。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>setValue</td>
    <td>value</td>
    <td>设置时间微调器（timespinner）的值。<br>
	代码实例：
<pre style="color:#006600">
$('#ss').timespinner('setValue', '17:45');  // set timespinner value
var v = $('#ss').timespinner('getValue');  // get timespinner value
alert(v);
</pre>
	</td>
</tr>
<tr>
	<td>getHours</td>
    <td>none</td>
    <td>获取当前的时钟的值。</td>
</tr>
<tr>
	<td>getMinutes</td>
    <td>none</td>
    <td>获取当前的分钟的值。</td>
</tr>
<tr>
	<td>getSeconds</td>
    <td>none</td>
    <td>获取当前的秒钟的值。</td>
</tr>
</table></div>') where ID=5680;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>title</td>
    <td>string</td>
    <td>对话框的标题文本。</td>
	<td>New Dialog</td>
</tr>
<tr>
	<td>collapsible</td>
    <td>boolean</td>
    <td>定义是否显示折叠按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>minimizable</td>
    <td>boolean</td>
    <td>定义是否显示最小化按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>maximizable</td>
    <td>boolean</td>
    <td>定义是否显示最大化按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>resizable</td>
    <td>boolean</td>
    <td>定义对话框是否可调整尺寸。</td>
	<td>false</td>
</tr>
<tr>
	<td>toolbar</td>
    <td>array,selector</td>
    <td>对话框的顶部工具栏，可能的值：<br>
	1、数组，每个工具的选项都与链接按钮（linkbutton）一样。<br>
	2、选择器，指示工具栏。<br>
	<br>
	对话框工具栏可以在 &lt;div&gt;标签中声明：
<pre style="color:#006600">
&lt;div class="easyui-dialog" style="width:600px;height:300px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="title:'My Dialog',toolbar:'#tb',modal:true"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Dialog Content.
&lt;/div&gt;
&lt;div id="tb"&gt;
&lt;a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true"/a&gt;
&lt;a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-help',plain:true"/a&gt;
&lt;/div&gt;
</pre>
对话框工具栏也可以通过数组定义：
<pre style="color:#006600">
&lt;div class="easyui-dialog" style="width:600px;height:300px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="title:'My Dialog',modal:true,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toolbar:[{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text:'Edit',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-edit',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('edit')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text:'Help',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-help',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('help')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Dialog Content.
&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>buttons</td>
    <td>array,selector</td>
    <td>对话框的底部按钮，可能的值：<br>
	1、数组，每个按钮的选项都与链接按钮（linkbutton）一样。<br>
	2、选择器，指示按钮栏。<br>
	<br>
	按钮可以在 &lt;div&gt;标签中声明：
<pre style="color:#006600">
&lt;div class="easyui-dialog" style="width:600px;height:300px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="title:'My Dialog',buttons:'#bb',modal:true"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Dialog Content.
&lt;/div&gt;
&lt;div id="bb"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="easyui-linkbutton"&gt;Save&lt;/a&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="easyui-linkbutton"&gt;Close&lt;/a&gt;
&lt;/div&gt;
</pre>
按钮也可以通过数组定义：
<pre style="color:#006600">
&lt;div class="easyui-dialog" style="width:600px;height:300px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="title:'My Dialog',modal:true,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buttons:[{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text:'Save',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){...}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text:'Close',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){...}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Dialog Content.
&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>title</td>
    <td>string</td>
    <td>对话框的标题文本。</td>
	<td>New Dialog</td>
</tr>
<tr>
	<td>collapsible</td>
    <td>boolean</td>
    <td>定义是否显示折叠按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>minimizable</td>
    <td>boolean</td>
    <td>定义是否显示最小化按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>maximizable</td>
    <td>boolean</td>
    <td>定义是否显示最大化按钮。</td>
	<td>false</td>
</tr>
<tr>
	<td>resizable</td>
    <td>boolean</td>
    <td>定义对话框是否可调整尺寸。</td>
	<td>false</td>
</tr>
<tr>
	<td>toolbar</td>
    <td>array,selector</td>
    <td>对话框的顶部工具栏，可能的值：<br>
	1、数组，每个工具的选项都与链接按钮（linkbutton）一样。<br>
	2、选择器，指示工具栏。<br>
	<br>
	对话框工具栏可以在 &lt;div&gt;标签中声明：
<pre style="color:#006600">
&lt;div class="easyui-dialog" style="width:600px;height:300px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="title:'My Dialog',toolbar:'#tb',modal:true"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Dialog Content.
&lt;/div&gt;
&lt;div id="tb"&gt;
&lt;a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true"/a&gt;
&lt;a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-help',plain:true"/a&gt;
&lt;/div&gt;
</pre>
对话框工具栏也可以通过数组定义：
<pre style="color:#006600">
&lt;div class="easyui-dialog" style="width:600px;height:300px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="title:'My Dialog',modal:true,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toolbar:[{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text:'Edit',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-edit',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('edit')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text:'Help',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls:'icon-help',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){alert('help')}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Dialog Content.
&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>buttons</td>
    <td>array,selector</td>
    <td>对话框的底部按钮，可能的值：<br>
	1、数组，每个按钮的选项都与链接按钮（linkbutton）一样。<br>
	2、选择器，指示按钮栏。<br>
	<br>
	按钮可以在 &lt;div&gt;标签中声明：
<pre style="color:#006600">
&lt;div class="easyui-dialog" style="width:600px;height:300px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="title:'My Dialog',buttons:'#bb',modal:true"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Dialog Content.
&lt;/div&gt;
&lt;div id="bb"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="easyui-linkbutton"&gt;Save&lt;/a&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;a href="#" class="easyui-linkbutton"&gt;Close&lt;/a&gt;
&lt;/div&gt;
</pre>
按钮也可以通过数组定义：
<pre style="color:#006600">
&lt;div class="easyui-dialog" style="width:600px;height:300px"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data-options="title:'My Dialog',modal:true,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buttons:[{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text:'Save',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){...}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text:'Close',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler:function(){...}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Dialog Content.
&lt;/div&gt;
</pre>
	</td>
	<td>null</td>
</tr>
</table></div>') where ID=5683;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>$.messager.show</td>
    <td>options</td>
    <td>在屏幕的右下角显示一个消息窗口，options 参数是一个配置对象：<br>
	showType： 定义消息窗口如何显示。可用的值是：null、slide、fade、show。默认是 slide。<br>
	showSpeed： 定义消息窗口完成显示所需的以毫秒为单位的时间。默认是 600。<br>
	width：定义消息窗口的宽度。默认是 250。<br>
	height：定义消息窗口的高度。默认是 100。<br>
	title：头部面板上显示的标题文本。<br>
	msg：要显示的消息文本。<br>
	style：定义消息窗口的自定义样式。<br>
	timeout：如果定义为 0，除非用户关闭，消息窗口将不会关闭。如果定义为非 0 值，消息窗口将在超时后自动关闭。默认是 4 秒。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$.messager.show({
&nbsp;&nbsp;&nbsp;&nbsp;title:'My Title',
&nbsp;&nbsp;&nbsp;&nbsp;msg:'Message will be closed after 5 seconds.',
&nbsp;&nbsp;&nbsp;&nbsp;timeout:5000,
&nbsp;&nbsp;&nbsp;&nbsp;showType:'slide'
});
// show message window on top center
$.messager.show({
&nbsp;&nbsp;&nbsp;&nbsp;title:'My Title',
&nbsp;&nbsp;&nbsp;&nbsp;msg:'Message will be closed after 4 seconds.',
&nbsp;&nbsp;&nbsp;&nbsp;showType:'show',
&nbsp;&nbsp;&nbsp;&nbsp;style:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;right:'',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;top:document.body.scrollTop+document.documentElement.scrollTop,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bottom:''
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>$.messager.alert</td>
    <td>title, msg, icon, fn</td>
    <td>显示一个警告提示窗口。参数：<br>
	title：显示在头部面板上的标题文本。<br>
	msg：要显示的消息文本。<br>
	icon：要显示的图标图片。可用的值是：error、question、info、warning。<br>
	fn：当窗口关闭时触发的回调函数。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$.messager.alert('My Title','Here is a info message!','info');
</pre>
	</td>
</tr>
<tr>
	<td>$.messager.confirm</td>
    <td>title, msg, fn</td>
    <td>显示一个带"确定"和"取消"按钮的确认消息窗口。参数：<br>
	title：显示在头部面板上的标题文本。<br>
	msg：要显示的消息文本。<br>
	fn(b)：回调函数，当用户点击确认按钮时传递一个 true 参数给函数，否则给它传递一个 false 参数。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$.messager.confirm('Confirm', 'Are you sure to exit this system?', function(r){
&nbsp;&nbsp;&nbsp;&nbsp;if (r){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// exit action;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>$.messager.prompt</td>
    <td>title, msg, fn</td>
    <td>显示一个带"确定"和"取消"按钮的消息窗口，提示用户输入一些文本。参数：<br>
	title：显示在头部面板上的标题文本。<br>
	msg：要显示的消息文本。<br>
	fn(val)：带有用户输入的数值参数的回调函数。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$.messager.prompt('Prompt', 'Please enter your name:', function(r){
&nbsp;&nbsp;&nbsp;&nbsp;if (r){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('Your name is:' + r);
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>$.messager.progress</td>
    <td>options or method</td>
    <td>显示一个进度的消息窗口。<br>
	options 定义如下：<br>
	title：显示在头部面板上的标题文本，默认值是 '' 。<br>
	msg：消息框的主体文本，默认值是 '' 。<br>
	text：显示在进度条里的文本，默认值是 undefined 。<br>
	interval：每次进度更新之间以毫秒为单位的时间长度。默认值是 300。<br>
	<br>
	方法定义如下：
	bar：获取进度条（progressbar）对象。<br>
	close：关闭进度窗口。<br>
	<br>
	代码实例：<br>
	显示进度消息窗口。
<pre style="color:#006600">
$.messager.progress(); 
</pre>
	现在关闭消息窗口。
<pre style="color:#006600">
$.messager.progress('close');
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>$.messager.show</td>
    <td>options</td>
    <td>在屏幕的右下角显示一个消息窗口，options 参数是一个配置对象：<br>
	showType： 定义消息窗口如何显示。可用的值是：null、slide、fade、show。默认是 slide。<br>
	showSpeed： 定义消息窗口完成显示所需的以毫秒为单位的时间。默认是 600。<br>
	width：定义消息窗口的宽度。默认是 250。<br>
	height：定义消息窗口的高度。默认是 100。<br>
	title：头部面板上显示的标题文本。<br>
	msg：要显示的消息文本。<br>
	style：定义消息窗口的自定义样式。<br>
	timeout：如果定义为 0，除非用户关闭，消息窗口将不会关闭。如果定义为非 0 值，消息窗口将在超时后自动关闭。默认是 4 秒。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$.messager.show({
&nbsp;&nbsp;&nbsp;&nbsp;title:'My Title',
&nbsp;&nbsp;&nbsp;&nbsp;msg:'Message will be closed after 5 seconds.',
&nbsp;&nbsp;&nbsp;&nbsp;timeout:5000,
&nbsp;&nbsp;&nbsp;&nbsp;showType:'slide'
});
// show message window on top center
$.messager.show({
&nbsp;&nbsp;&nbsp;&nbsp;title:'My Title',
&nbsp;&nbsp;&nbsp;&nbsp;msg:'Message will be closed after 4 seconds.',
&nbsp;&nbsp;&nbsp;&nbsp;showType:'show',
&nbsp;&nbsp;&nbsp;&nbsp;style:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;right:'',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;top:document.body.scrollTop+document.documentElement.scrollTop,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bottom:''
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>$.messager.alert</td>
    <td>title, msg, icon, fn</td>
    <td>显示一个警告提示窗口。参数：<br>
	title：显示在头部面板上的标题文本。<br>
	msg：要显示的消息文本。<br>
	icon：要显示的图标图片。可用的值是：error、question、info、warning。<br>
	fn：当窗口关闭时触发的回调函数。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$.messager.alert('My Title','Here is a info message!','info');
</pre>
	</td>
</tr>
<tr>
	<td>$.messager.confirm</td>
    <td>title, msg, fn</td>
    <td>显示一个带"确定"和"取消"按钮的确认消息窗口。参数：<br>
	title：显示在头部面板上的标题文本。<br>
	msg：要显示的消息文本。<br>
	fn(b)：回调函数，当用户点击确认按钮时传递一个 true 参数给函数，否则给它传递一个 false 参数。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$.messager.confirm('Confirm', 'Are you sure to exit this system?', function(r){
&nbsp;&nbsp;&nbsp;&nbsp;if (r){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// exit action;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>$.messager.prompt</td>
    <td>title, msg, fn</td>
    <td>显示一个带"确定"和"取消"按钮的消息窗口，提示用户输入一些文本。参数：<br>
	title：显示在头部面板上的标题文本。<br>
	msg：要显示的消息文本。<br>
	fn(val)：带有用户输入的数值参数的回调函数。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$.messager.prompt('Prompt', 'Please enter your name:', function(r){
&nbsp;&nbsp;&nbsp;&nbsp;if (r){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert('Your name is:' + r);
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>$.messager.progress</td>
    <td>options or method</td>
    <td>显示一个进度的消息窗口。<br>
	options 定义如下：<br>
	title：显示在头部面板上的标题文本，默认值是 '' 。<br>
	msg：消息框的主体文本，默认值是 '' 。<br>
	text：显示在进度条里的文本，默认值是 undefined 。<br>
	interval：每次进度更新之间以毫秒为单位的时间长度。默认值是 300。<br>
	<br>
	方法定义如下：
	bar：获取进度条（progressbar）对象。<br>
	close：关闭进度窗口。<br>
	<br>
	代码实例：<br>
	显示进度消息窗口。
<pre style="color:#006600">
$.messager.progress(); 
</pre>
	现在关闭消息窗口。
<pre style="color:#006600">
$.messager.progress('close');
</pre>
	</td>
</tr>
</table></div>') where ID=5684;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>columns</td>
    <td>array</td>
    <td>数据网格（datagrid）的列（column）的配置对象，更多细节请参见列（column）属性。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>frozenColumns</td>
    <td>array</td>
    <td>和列（column）属性一样，但是这些列将被冻结在左边。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>fitColumns</td>
    <td>boolean</td>
    <td>设置为 true，则会自动扩大或缩小列的尺寸以适应网格的宽度并且防止水平滚动。</td>
	<td>false</td>
</tr>
<tr>
	<td>resizeHandle</td>
    <td>string</td>
    <td>调整列的位置，可用的值有：'left'、'right'、'both'。当设置为 'right' 时，用户可通过拖拽列头部的右边缘来调整列。<br>
	该属性自版本 1.3.2 起可用。
	</td>
	<td>right</td>
</tr>
<tr>
	<td>autoRowHeight</td>
    <td>boolean</td>
    <td>定义是否设置基于该行内容的行高度。设置为 false，则可以提高加载性能。</td>
	<td>true</td>
</tr>
<tr>
	<td>toolbar</td>
    <td>array,selector</td>
    <td>数据网格（datagrid）面板的头部工具栏。可能的值：<br>
	1、数组，每个工具选项与链接按钮（linkbutton）一样。<br>
	2、选择器，只是工具栏。<br>
	<br>
	在 &lt;div&gt; 标签内定义工具栏：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;toolbar: '#tb'
});
&lt;div id="tb"&gt;
&lt;a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true"&gt;&lt;/a&gt;
&lt;a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-help',plain:true"&gt;&lt;/a&gt;
&lt;/div&gt;
</pre>
	通过数组定义工具栏：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;toolbar: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-edit',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler: function(){alert('edit')}
&nbsp;&nbsp;&nbsp;&nbsp;},'-',{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-help',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler: function(){alert('help')}
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>striped</td>
    <td>boolean</td>
    <td>设置为 true，则把行条纹化。（即奇偶行使用不同背景色）</td>
	<td>false</td>
</tr>
<tr>
	<td>method</td>
    <td>string</td>
    <td>请求远程数据的方法（method）类型。</td>
	<td>post</td>
</tr>
<tr>
	<td>nowrap</td>
    <td>boolean</td>
    <td>设置为 true，则把数据显示在一行里。设置为 true 可提高加载性能。</td>
	<td>true</td>
</tr>
<tr>
	<td>idField</td>
    <td>string</td>
    <td>指示哪个字段是标识字段。</td>
	<td>null</td>
</tr>
<tr>
	<td>url</td>
    <td>string</td>
    <td>从远程站点请求数据的 URL。</td>
	<td>null</td>
</tr>
<tr>
	<td>data</td>
    <td>array,object</td>
    <td>要加载的数据。该属性自版本 1.3.2 起可用。<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;data: [
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{f1:'value11', f2:'value12'},
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{f1:'value21', f2:'value22'}
&nbsp;&nbsp;&nbsp;&nbsp;]
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>loadMsg</td>
    <td>string</td>
    <td>当从远程站点加载数据时，显示的提示消息。</td>
	<td>Processing, please wait …</td>
</tr>
<tr>
	<td>pagination</td>
    <td>boolean</td>
    <td>设置为 true，则在数据网格（datagrid）底部显示分页工具栏。</td>
	<td>false</td>
</tr>
<tr>
	<td>rownumbers</td>
    <td>boolean</td>
    <td>设置为 true，则显示带有行号的列。</td>
	<td>false</td>
</tr>
<tr>
	<td>singleSelect</td>
    <td>boolean</td>
    <td>设置为 true，则只允许选中一行。</td>
	<td>false</td>
</tr>
<tr>
	<td>checkOnSelect</td>
    <td>boolean</td>
    <td>如果设置为 true，当用户点击某一行时，则会选中/取消选中复选框。如果设置为 false 时，只有当用户点击了复选框时，才会选中/取消选中复选框。<br>
	该属性自版本 1.3 起可用。
	</td>
	<td>true</td>
</tr>
<tr>
	<td>selectOnCheck</td>
    <td>boolean</td>
    <td>如果设置为 true，点击复选框将会选中该行。如果设置为 false，选中该行将不会选中复选框。<br>
	该属性自版本 1.3 起可用。
	</td>
	<td>true</td>
</tr>
<tr>
	<td>pagePosition</td>
    <td>string</td>
    <td>定义分页栏的位置。可用的值有：'top'、'bottom'、'both'。<br>
	该属性自版本 1.3 起可用。
	</td>
	<td>bottom</td>
</tr>
<tr>
	<td>pageNumber</td>
    <td>number</td>
    <td>当设置了 pagination 属性时，初始化页码。</td>
	<td>1</td>
</tr>
<tr>
	<td>pageSize</td>
    <td>number</td>
    <td>当设置了 pagination 属性时，初始化页面尺寸。</td>
	<td>10</td>
</tr>
<tr>
	<td>pageList</td>
    <td>array</td>
    <td>当设置了 pagination 属性时，初始化页面尺寸的选择列表。</td>
	<td>[10,20,30,40,50]</td>
</tr>
<tr>
	<td>queryParams</td>
    <td>object</td>
    <td>当请求远程数据时，发送的额外参数。<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;queryParams: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'easyui',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;subject: 'datagrid'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td>{}</td>
</tr>
<tr>
	<td>sortName</td>
    <td>string</td>
    <td>定义可以排序的列。</td>
	<td>null</td>
</tr>
<tr>
	<td>sortOrder</td>
    <td>string</td>
    <td>定义列的排序顺序，只能用 'asc' 或 'desc'。</td>
	<td>asc</td>
</tr>
<tr>
	<td>multiSort</td>
    <td>boolean</td>
    <td>定义是否启用多列排序。该属性自版本 1.3.4 起可用。</td>
	<td>false</td>
</tr>
<tr>
	<td>remoteSort</td>
    <td>boolean</td>
    <td>定义是否从服务器排序数据。</td>
	<td>true</td>
</tr>
<tr>
	<td>showHeader</td>
    <td>boolean</td>
    <td>定义是否显示行的头部。</td>
	<td>true</td>
</tr>
<tr>
	<td>showFooter</td>
    <td>boolean</td>
    <td>定义是否显示行的底部。</td>
	<td>false</td>
</tr>
<tr>
	<td>scrollbarSize</td>
    <td>number</td>
    <td>滚动条宽度（当滚动条是垂直的时候）或者滚动条的高度（当滚动条是水平的时候）。</td>
	<td>18</td>
</tr>
<tr>
	<td>rowStyler</td>
    <td>function</td>
    <td>返回例如 'background:red' 的样式。该函数需要两个参数：<br>
	rowIndex：行的索引，从 0 开始。<br>
	rowData：该行相应的记录。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;rowStyler: function(index,row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (row.listprice&gt;80){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return 'background-color:#6293BB;color:#fff;'; // return inline style
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// the function can return predefined css class and inline style
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// return {class:'r1', style:{'color:#fff'}};&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>loader</td>
    <td>function</td>
    <td>定义如何从远程服务器加载数据。返回 false 则取消该动作。该函数有下列参数：<br>
	param：要传递到远程服务器的参数对象。<br>
	success(data)：当检索数据成功时调用的回调函数。<br>
	error()：当检索数据失败时调用的回调函数。<br>
	</td>
	<td>json loader</td>
</tr>
<tr>
	<td>loadFilter</td>
    <td>function</td>
    <td>返回要显示的过滤数据。该函数有一个参数 'data' ，表示原始数据。您可以把原始数据变成标准数据格式。该函数必须返回标准数据对象，含有 'total' 和 'rows' 属性。<br>
	代码实例：
<pre style="color:#006600">
// removing 'd' object from asp.net web service json output
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;loadFilter: function(data){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (data.d){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return data.d;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} else {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return data;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>editors</td>
    <td>object</td>
    <td>定义编辑行时的编辑器。</td>
	<td>predefined editors</td>
</tr>
<tr>
	<td>view</td>
    <td>object</td>
    <td>定义数据网格（datagrid）的视图。</td>
	<td>default view</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>columns</td>
    <td>array</td>
    <td>数据网格（datagrid）的列（column）的配置对象，更多细节请参见列（column）属性。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>frozenColumns</td>
    <td>array</td>
    <td>和列（column）属性一样，但是这些列将被冻结在左边。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>fitColumns</td>
    <td>boolean</td>
    <td>设置为 true，则会自动扩大或缩小列的尺寸以适应网格的宽度并且防止水平滚动。</td>
	<td>false</td>
</tr>
<tr>
	<td>resizeHandle</td>
    <td>string</td>
    <td>调整列的位置，可用的值有：'left'、'right'、'both'。当设置为 'right' 时，用户可通过拖拽列头部的右边缘来调整列。<br>
	该属性自版本 1.3.2 起可用。
	</td>
	<td>right</td>
</tr>
<tr>
	<td>autoRowHeight</td>
    <td>boolean</td>
    <td>定义是否设置基于该行内容的行高度。设置为 false，则可以提高加载性能。</td>
	<td>true</td>
</tr>
<tr>
	<td>toolbar</td>
    <td>array,selector</td>
    <td>数据网格（datagrid）面板的头部工具栏。可能的值：<br>
	1、数组，每个工具选项与链接按钮（linkbutton）一样。<br>
	2、选择器，只是工具栏。<br>
	<br>
	在 &lt;div&gt; 标签内定义工具栏：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;toolbar: '#tb'
});
&lt;div id="tb"&gt;
&lt;a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true"&gt;&lt;/a&gt;
&lt;a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-help',plain:true"&gt;&lt;/a&gt;
&lt;/div&gt;
</pre>
	通过数组定义工具栏：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;toolbar: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-edit',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler: function(){alert('edit')}
&nbsp;&nbsp;&nbsp;&nbsp;},'-',{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-help',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handler: function(){alert('help')}
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>striped</td>
    <td>boolean</td>
    <td>设置为 true，则把行条纹化。（即奇偶行使用不同背景色）</td>
	<td>false</td>
</tr>
<tr>
	<td>method</td>
    <td>string</td>
    <td>请求远程数据的方法（method）类型。</td>
	<td>post</td>
</tr>
<tr>
	<td>nowrap</td>
    <td>boolean</td>
    <td>设置为 true，则把数据显示在一行里。设置为 true 可提高加载性能。</td>
	<td>true</td>
</tr>
<tr>
	<td>idField</td>
    <td>string</td>
    <td>指示哪个字段是标识字段。</td>
	<td>null</td>
</tr>
<tr>
	<td>url</td>
    <td>string</td>
    <td>从远程站点请求数据的 URL。</td>
	<td>null</td>
</tr>
<tr>
	<td>data</td>
    <td>array,object</td>
    <td>要加载的数据。该属性自版本 1.3.2 起可用。<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;data: [
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{f1:'value11', f2:'value12'},
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{f1:'value21', f2:'value22'}
&nbsp;&nbsp;&nbsp;&nbsp;]
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>loadMsg</td>
    <td>string</td>
    <td>当从远程站点加载数据时，显示的提示消息。</td>
	<td>Processing, please wait …</td>
</tr>
<tr>
	<td>pagination</td>
    <td>boolean</td>
    <td>设置为 true，则在数据网格（datagrid）底部显示分页工具栏。</td>
	<td>false</td>
</tr>
<tr>
	<td>rownumbers</td>
    <td>boolean</td>
    <td>设置为 true，则显示带有行号的列。</td>
	<td>false</td>
</tr>
<tr>
	<td>singleSelect</td>
    <td>boolean</td>
    <td>设置为 true，则只允许选中一行。</td>
	<td>false</td>
</tr>
<tr>
	<td>checkOnSelect</td>
    <td>boolean</td>
    <td>如果设置为 true，当用户点击某一行时，则会选中/取消选中复选框。如果设置为 false 时，只有当用户点击了复选框时，才会选中/取消选中复选框。<br>
	该属性自版本 1.3 起可用。
	</td>
	<td>true</td>
</tr>
<tr>
	<td>selectOnCheck</td>
    <td>boolean</td>
    <td>如果设置为 true，点击复选框将会选中该行。如果设置为 false，选中该行将不会选中复选框。<br>
	该属性自版本 1.3 起可用。
	</td>
	<td>true</td>
</tr>
<tr>
	<td>pagePosition</td>
    <td>string</td>
    <td>定义分页栏的位置。可用的值有：'top'、'bottom'、'both'。<br>
	该属性自版本 1.3 起可用。
	</td>
	<td>bottom</td>
</tr>
<tr>
	<td>pageNumber</td>
    <td>number</td>
    <td>当设置了 pagination 属性时，初始化页码。</td>
	<td>1</td>
</tr>
<tr>
	<td>pageSize</td>
    <td>number</td>
    <td>当设置了 pagination 属性时，初始化页面尺寸。</td>
	<td>10</td>
</tr>
<tr>
	<td>pageList</td>
    <td>array</td>
    <td>当设置了 pagination 属性时，初始化页面尺寸的选择列表。</td>
	<td>[10,20,30,40,50]</td>
</tr>
<tr>
	<td>queryParams</td>
    <td>object</td>
    <td>当请求远程数据时，发送的额外参数。<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;queryParams: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'easyui',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;subject: 'datagrid'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td>{}</td>
</tr>
<tr>
	<td>sortName</td>
    <td>string</td>
    <td>定义可以排序的列。</td>
	<td>null</td>
</tr>
<tr>
	<td>sortOrder</td>
    <td>string</td>
    <td>定义列的排序顺序，只能用 'asc' 或 'desc'。</td>
	<td>asc</td>
</tr>
<tr>
	<td>multiSort</td>
    <td>boolean</td>
    <td>定义是否启用多列排序。该属性自版本 1.3.4 起可用。</td>
	<td>false</td>
</tr>
<tr>
	<td>remoteSort</td>
    <td>boolean</td>
    <td>定义是否从服务器排序数据。</td>
	<td>true</td>
</tr>
<tr>
	<td>showHeader</td>
    <td>boolean</td>
    <td>定义是否显示行的头部。</td>
	<td>true</td>
</tr>
<tr>
	<td>showFooter</td>
    <td>boolean</td>
    <td>定义是否显示行的底部。</td>
	<td>false</td>
</tr>
<tr>
	<td>scrollbarSize</td>
    <td>number</td>
    <td>滚动条宽度（当滚动条是垂直的时候）或者滚动条的高度（当滚动条是水平的时候）。</td>
	<td>18</td>
</tr>
<tr>
	<td>rowStyler</td>
    <td>function</td>
    <td>返回例如 'background:red' 的样式。该函数需要两个参数：<br>
	rowIndex：行的索引，从 0 开始。<br>
	rowData：该行相应的记录。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;rowStyler: function(index,row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (row.listprice&gt;80){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return 'background-color:#6293BB;color:#fff;'; // return inline style
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// the function can return predefined css class and inline style
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// return {class:'r1', style:{'color:#fff'}};&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>loader</td>
    <td>function</td>
    <td>定义如何从远程服务器加载数据。返回 false 则取消该动作。该函数有下列参数：<br>
	param：要传递到远程服务器的参数对象。<br>
	success(data)：当检索数据成功时调用的回调函数。<br>
	error()：当检索数据失败时调用的回调函数。<br>
	</td>
	<td>json loader</td>
</tr>
<tr>
	<td>loadFilter</td>
    <td>function</td>
    <td>返回要显示的过滤数据。该函数有一个参数 'data' ，表示原始数据。您可以把原始数据变成标准数据格式。该函数必须返回标准数据对象，含有 'total' 和 'rows' 属性。<br>
	代码实例：
<pre style="color:#006600">
// removing 'd' object from asp.net web service json output
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;loadFilter: function(data){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (data.d){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return data.d;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} else {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return data;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>editors</td>
    <td>object</td>
    <td>定义编辑行时的编辑器。</td>
	<td>predefined editors</td>
</tr>
<tr>
	<td>view</td>
    <td>object</td>
    <td>定义数据网格（datagrid）的视图。</td>
	<td>default view</td>
</tr>
</table></div>') where ID=5685;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>title</td>
    <td>string</td>
    <td>列的标题文本。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>field</td>
    <td>string</td>
    <td>列的字段名。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>列的宽度。如果未定义，则宽度会自动扩展以适应它的内容。没有定义宽度将会降低性能。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>rowspan</td>
    <td>number</td>
    <td>指示一个单元格占据多少行。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>colspan</td>
    <td>number</td>
    <td>指示一个单元格占据多少列。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>align</td>
    <td>string</td>
    <td>指示如何对齐该列的数据，可以用 'left'、'right'、'center'。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>halign</td>
    <td>string</td>
    <td>指示如何对齐该列的头部，可能的值：'left'、'right'、'center'。如果没有分配值，则头部对齐方式将与通过 'align' 属性定义的数据对齐方式一致。该属性自版本 1.3.2 起可用。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>sortable</td>
    <td>boolean</td>
    <td>设置为 true，则允许该列被排序。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>order</td>
    <td>string</td>
    <td>默认的排序顺序，只能用 'asc' 或 'desc'。该属性自版本 1.3.2 起可用。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>resizable</td>
    <td>boolean</td>
    <td>设置为 true，则允许该列可调整尺寸。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>fixed</td>
    <td>boolean</td>
    <td>设置为 true，则当 'fitColumns' 设置为 true 时放置调整宽度。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>hidden</td>
    <td>boolean</td>
    <td>设置为 true，则隐藏该列。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>checkbox</td>
    <td>boolean</td>
    <td>设置为 true，则显示复选框。复选框有固定宽度。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>formatter</td>
    <td>function</td>
    <td>单元格的格式化函数，需要三个参数：<br> 
	value：字段的值。<br>
	rowData：行的记录数据。<br>
	rowIndex：行的索引。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;columns:[[
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{field:'userId',title:'User', width:80,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formatter: function(value,row,index){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (row.user){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return row.user.name;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} else {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return value;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;]]
});
</pre>
	</td>
	<td>undefined</td>
</tr>
<tr>
	<td>styler</td>
    <td>function</td>
    <td>单元格的样式函数，返回样式字符串来自定义该单元格的样式，例如 'background:red' 。该函数需要三个参数：<br>
	value：字段的值。<br>
	rowData：行的记录数据。<br>
	rowIndex：行的索引。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;columns:[[
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{field:'listprice',title:'List Price', width:80, align:'right',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;styler: function(value,row,index){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (value &lt; 20){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return 'background-color:#ffee00;color:red;';
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// the function can return predefined css class and inline style
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// return {class:'c1',style:'color:red'}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;]]
});
</pre>
	</td>
	<td>undefined</td>
</tr>
<tr>
	<td>sorter</td>
    <td>function</td>
    <td>用于本地排序的自定义字段的排序函数，需要两个参数：<br>
	a：第一个字段值。<br>
	b：第二个字段值。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;remoteSort: false,
&nbsp;&nbsp;&nbsp;&nbsp;columns: [[
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{field:'date',title:'Date',width:80,sortable:true,align:'center',  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sorter:function(a,b){  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a = a.split('/');  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b = b.split('/');  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (a[2] == b[2]){  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (a[0] == b[0]){  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return (a[1]&gt;b[1]?1:-1);  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} else {  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return (a[0]&gt;b[0]?1:-1);  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} else {  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return (a[2]&gt;b[2]?1:-1);  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;]]
});
</pre>
	</td>
	<td>undefined</td>
</tr>
<tr>
	<td>editor</td>
    <td>string,object</td>
    <td>指示编辑类型。当是字符串（string）时则指编辑类型，当是对象（object）时则包含两个属性：<br>
	type：字符串，编辑类型，可能的类型：text、textarea、checkbox、numberbox、validatebox、datebox、combobox、combotree。<br>
	options：对象，编辑类型对应的编辑器选项。
	</td>
	<td>undefined</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>title</td>
    <td>string</td>
    <td>列的标题文本。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>field</td>
    <td>string</td>
    <td>列的字段名。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>width</td>
    <td>number</td>
    <td>列的宽度。如果未定义，则宽度会自动扩展以适应它的内容。没有定义宽度将会降低性能。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>rowspan</td>
    <td>number</td>
    <td>指示一个单元格占据多少行。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>colspan</td>
    <td>number</td>
    <td>指示一个单元格占据多少列。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>align</td>
    <td>string</td>
    <td>指示如何对齐该列的数据，可以用 'left'、'right'、'center'。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>halign</td>
    <td>string</td>
    <td>指示如何对齐该列的头部，可能的值：'left'、'right'、'center'。如果没有分配值，则头部对齐方式将与通过 'align' 属性定义的数据对齐方式一致。该属性自版本 1.3.2 起可用。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>sortable</td>
    <td>boolean</td>
    <td>设置为 true，则允许该列被排序。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>order</td>
    <td>string</td>
    <td>默认的排序顺序，只能用 'asc' 或 'desc'。该属性自版本 1.3.2 起可用。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>resizable</td>
    <td>boolean</td>
    <td>设置为 true，则允许该列可调整尺寸。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>fixed</td>
    <td>boolean</td>
    <td>设置为 true，则当 'fitColumns' 设置为 true 时放置调整宽度。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>hidden</td>
    <td>boolean</td>
    <td>设置为 true，则隐藏该列。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>checkbox</td>
    <td>boolean</td>
    <td>设置为 true，则显示复选框。复选框有固定宽度。</td>
	<td>undefined</td>
</tr>
<tr>
	<td>formatter</td>
    <td>function</td>
    <td>单元格的格式化函数，需要三个参数：<br> 
	value：字段的值。<br>
	rowData：行的记录数据。<br>
	rowIndex：行的索引。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;columns:[[
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{field:'userId',title:'User', width:80,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formatter: function(value,row,index){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (row.user){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return row.user.name;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} else {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return value;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;]]
});
</pre>
	</td>
	<td>undefined</td>
</tr>
<tr>
	<td>styler</td>
    <td>function</td>
    <td>单元格的样式函数，返回样式字符串来自定义该单元格的样式，例如 'background:red' 。该函数需要三个参数：<br>
	value：字段的值。<br>
	rowData：行的记录数据。<br>
	rowIndex：行的索引。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;columns:[[
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{field:'listprice',title:'List Price', width:80, align:'right',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;styler: function(value,row,index){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (value &lt; 20){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return 'background-color:#ffee00;color:red;';
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// the function can return predefined css class and inline style
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// return {class:'c1',style:'color:red'}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;]]
});
</pre>
	</td>
	<td>undefined</td>
</tr>
<tr>
	<td>sorter</td>
    <td>function</td>
    <td>用于本地排序的自定义字段的排序函数，需要两个参数：<br>
	a：第一个字段值。<br>
	b：第二个字段值。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;remoteSort: false,
&nbsp;&nbsp;&nbsp;&nbsp;columns: [[
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{field:'date',title:'Date',width:80,sortable:true,align:'center',  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sorter:function(a,b){  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a = a.split('/');  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b = b.split('/');  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (a[2] == b[2]){  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (a[0] == b[0]){  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return (a[1]&gt;b[1]?1:-1);  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} else {  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return (a[0]&gt;b[0]?1:-1);  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} else {  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return (a[2]&gt;b[2]?1:-1);  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;]]
});
</pre>
	</td>
	<td>undefined</td>
</tr>
<tr>
	<td>editor</td>
    <td>string,object</td>
    <td>指示编辑类型。当是字符串（string）时则指编辑类型，当是对象（object）时则包含两个属性：<br>
	type：字符串，编辑类型，可能的类型：text、textarea、checkbox、numberbox、validatebox、datebox、combobox、combotree。<br>
	options：对象，编辑类型对应的编辑器选项。
	</td>
	<td>undefined</td>
</tr>
</table></div>') where ID=5685;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onLoadSuccess</td>
    <td>data</td>
    <td>当数据加载成功时触发。</td>
</tr>
<tr>
	<td>onLoadError</td>
    <td>none</td>
    <td>加载远程数据发生某些错误时触发。</td>
</tr>
<tr>
	<td>onBeforeLoad</td>
    <td>param</td>
    <td>发送加载数据的请求前触发，如果返回 false 加载动作就会取消。</td>
</tr>
<tr>
	<td>onClickRow</td>
    <td>rowIndex, rowData</td>
    <td>当用户点击一行时触发，参数包括：<br>
	rowIndex：被点击行的索引，从 0 开始<br>
	rowData：被点击行对应的记录
	</td>
</tr>
<tr>
	<td>onDblClickRow</td>
    <td>rowIndex, rowData</td>
    <td>当用户双击一行时触发，参数包括：<br>
	rowIndex：被双击行的索引，从 0 开始<br>
	rowData：被双击行对应的记录
	</td>
</tr>
<tr>
	<td>onClickCell</td>
    <td>rowIndex, field, value</td>
    <td>当用户单击一个单元格时触发。</td>
</tr>
<tr>
	<td>onDblClickCell</td>
    <td>rowIndex, field, value</td>
    <td>当用户双击一个单元格时触发。<br>
	代码实例：
<pre style="color:#006600">
// when double click a cell, begin editing and make the editor get focus
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;onDblClickCell: function(index,field,value){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(this).datagrid('beginEdit', index);
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var ed = $(this).datagrid('getEditor', {index:index,field:field});
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(ed.target).focus();
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>onSortColumn</td>
    <td>sort, order</td>
    <td>当用户对一列进行排序时触发，参数包括：<br>
	sort：排序的列的字段名<br>
	order：排序的列的顺序
	</td>
</tr>
<tr>
	<td>onResizeColumn</td>
    <td>field, width</td>
    <td>当用户调整列的尺寸时触发。</td>
</tr>
<tr>
	<td>onSelect</td>
    <td>rowIndex, rowData</td>
    <td>当用户选中一行时触发，参数包括：<br>
	rowIndex：选中行的索引，从 0 开始<br>
	rowData：选中行对应的记录
	</td>
</tr>
<tr>
	<td>onUnselect</td>
    <td>rowIndex, rowData</td>
    <td>当用户取消选中一行时触发，参数包括：<br>
	rowIndex：取消选中行的索引，从 0 开始<br>
	rowData：取消选中行对应的记录
	</td>
</tr>
<tr>
	<td>onSelectAll</td>
    <td>rows</td>
    <td>当用户选中全部行时触发。</td>
</tr>
<tr>
	<td>onUnselectAll</td>
    <td>rows</td>
    <td>当用户取消选中全部行时触发。</td>
</tr>
<tr>
	<td>onCheck</td>
    <td>rowIndex,rowData</td>
    <td>当用户勾选一行时触发，参数包括：<br>
	rowIndex：勾选行的索引，从 0 开始<br>
	rowData：勾选行对应的记录<br>
	该事件自版本 1.3 起可用。 
	</td>
</tr>
<tr>
	<td>onUncheck</td>
    <td>rowIndex,rowData</td>
    <td>当用户取消勾选一行时触发，参数包括：<br>
	rowIndex：取消勾选行的索引，从 0 开始<br>
	rowData：取消勾选行对应的记录<br>
	该事件自版本 1.3 起可用。 
	</td>
</tr>
<tr>
	<td>onCheckAll</td>
    <td>rows</td>
    <td>当用户勾选全部行时触发。该事件自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>onUncheckAll</td>
    <td>rows</td>
    <td>当用户取消勾选全部行时触发。该事件自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>onBeforeEdit</td>
    <td>rowIndex, rowData</td>
    <td>当用户开始编辑一行时触发，参数包括：<br>
	rowIndex：编辑行的索引，从 0 开始<br>
	rowData：编辑行对应的记录
	</td>
</tr>
<tr>
	<td>onAfterEdit</td>
    <td>rowIndex, rowData, changes</td>
    <td>当用户完成编辑一行时触发，参数包括：<br>
	rowIndex：编辑行的索引，从 0 开始<br>
	rowData：编辑行对应的记录<br>
	changes：更改的字段/值对
	</td>
</tr>
<tr>
	<td>onCancelEdit</td>
    <td>rowIndex, rowData</td>
    <td>当用户取消编辑一行时触发，参数包括：<br>
	rowIndex：编辑行的索引，从 0 开始<br>
	rowData：编辑行对应的记录
	</td>
</tr>
<tr>
	<td>onHeaderContextMenu</td>
    <td>e, field</td>
    <td>当数据网格（datagrid）的头部被右键单击时触发。</td>
</tr>
<tr>
	<td>onRowContextMenu</td>
    <td>e, rowIndex, rowData</td>
    <td>当右键点击行时触发。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onLoadSuccess</td>
    <td>data</td>
    <td>当数据加载成功时触发。</td>
</tr>
<tr>
	<td>onLoadError</td>
    <td>none</td>
    <td>加载远程数据发生某些错误时触发。</td>
</tr>
<tr>
	<td>onBeforeLoad</td>
    <td>param</td>
    <td>发送加载数据的请求前触发，如果返回 false 加载动作就会取消。</td>
</tr>
<tr>
	<td>onClickRow</td>
    <td>rowIndex, rowData</td>
    <td>当用户点击一行时触发，参数包括：<br>
	rowIndex：被点击行的索引，从 0 开始<br>
	rowData：被点击行对应的记录
	</td>
</tr>
<tr>
	<td>onDblClickRow</td>
    <td>rowIndex, rowData</td>
    <td>当用户双击一行时触发，参数包括：<br>
	rowIndex：被双击行的索引，从 0 开始<br>
	rowData：被双击行对应的记录
	</td>
</tr>
<tr>
	<td>onClickCell</td>
    <td>rowIndex, field, value</td>
    <td>当用户单击一个单元格时触发。</td>
</tr>
<tr>
	<td>onDblClickCell</td>
    <td>rowIndex, field, value</td>
    <td>当用户双击一个单元格时触发。<br>
	代码实例：
<pre style="color:#006600">
// when double click a cell, begin editing and make the editor get focus
$('#dg').datagrid({
&nbsp;&nbsp;&nbsp;&nbsp;onDblClickCell: function(index,field,value){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(this).datagrid('beginEdit', index);
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var ed = $(this).datagrid('getEditor', {index:index,field:field});
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(ed.target).focus();
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>onSortColumn</td>
    <td>sort, order</td>
    <td>当用户对一列进行排序时触发，参数包括：<br>
	sort：排序的列的字段名<br>
	order：排序的列的顺序
	</td>
</tr>
<tr>
	<td>onResizeColumn</td>
    <td>field, width</td>
    <td>当用户调整列的尺寸时触发。</td>
</tr>
<tr>
	<td>onSelect</td>
    <td>rowIndex, rowData</td>
    <td>当用户选中一行时触发，参数包括：<br>
	rowIndex：选中行的索引，从 0 开始<br>
	rowData：选中行对应的记录
	</td>
</tr>
<tr>
	<td>onUnselect</td>
    <td>rowIndex, rowData</td>
    <td>当用户取消选中一行时触发，参数包括：<br>
	rowIndex：取消选中行的索引，从 0 开始<br>
	rowData：取消选中行对应的记录
	</td>
</tr>
<tr>
	<td>onSelectAll</td>
    <td>rows</td>
    <td>当用户选中全部行时触发。</td>
</tr>
<tr>
	<td>onUnselectAll</td>
    <td>rows</td>
    <td>当用户取消选中全部行时触发。</td>
</tr>
<tr>
	<td>onCheck</td>
    <td>rowIndex,rowData</td>
    <td>当用户勾选一行时触发，参数包括：<br>
	rowIndex：勾选行的索引，从 0 开始<br>
	rowData：勾选行对应的记录<br>
	该事件自版本 1.3 起可用。 
	</td>
</tr>
<tr>
	<td>onUncheck</td>
    <td>rowIndex,rowData</td>
    <td>当用户取消勾选一行时触发，参数包括：<br>
	rowIndex：取消勾选行的索引，从 0 开始<br>
	rowData：取消勾选行对应的记录<br>
	该事件自版本 1.3 起可用。 
	</td>
</tr>
<tr>
	<td>onCheckAll</td>
    <td>rows</td>
    <td>当用户勾选全部行时触发。该事件自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>onUncheckAll</td>
    <td>rows</td>
    <td>当用户取消勾选全部行时触发。该事件自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>onBeforeEdit</td>
    <td>rowIndex, rowData</td>
    <td>当用户开始编辑一行时触发，参数包括：<br>
	rowIndex：编辑行的索引，从 0 开始<br>
	rowData：编辑行对应的记录
	</td>
</tr>
<tr>
	<td>onAfterEdit</td>
    <td>rowIndex, rowData, changes</td>
    <td>当用户完成编辑一行时触发，参数包括：<br>
	rowIndex：编辑行的索引，从 0 开始<br>
	rowData：编辑行对应的记录<br>
	changes：更改的字段/值对
	</td>
</tr>
<tr>
	<td>onCancelEdit</td>
    <td>rowIndex, rowData</td>
    <td>当用户取消编辑一行时触发，参数包括：<br>
	rowIndex：编辑行的索引，从 0 开始<br>
	rowData：编辑行对应的记录
	</td>
</tr>
<tr>
	<td>onHeaderContextMenu</td>
    <td>e, field</td>
    <td>当数据网格（datagrid）的头部被右键单击时触发。</td>
</tr>
<tr>
	<td>onRowContextMenu</td>
    <td>e, rowIndex, rowData</td>
    <td>当右键点击行时触发。</td>
</tr>
</table></div>') where ID=5685;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>getPager</td>
    <td>none</td>
    <td>返回分页（pager）对象。</td>
</tr>
<tr>
	<td>getPanel</td>
    <td>none</td>
    <td>返回面板（panel）对象。</td>
</tr>
<tr>
	<td>getColumnFields</td>
    <td>frozen</td>
    <td>返回列的字段，如果 frozen 设置为 true，则冻结列的字段被返回。<br>
	代码实例：
<pre style="color:#006600">
var opts = $('#dg').datagrid('getColumnFields');&nbsp;&nbsp;&nbsp;&nbsp;// get unfrozen columns
var opts = $('#dg').datagrid('getColumnFields', true); // get frozen columns
</pre>
	</td>
</tr>
<tr>
	<td>getColumnOption</td>
    <td>field</td>
    <td>返回指定列的选项。</td>
</tr>
<tr>
	<td>resize</td>
    <td>param</td>
    <td>调整尺寸和布局。</td>
</tr>
<tr>
	<td>load</td>
    <td>param</td>
    <td>加载并显示第一页的行，如果指定 'param' 参数，它将替换 queryParams 属性。通常情况下，通过传递一些从参数进行查询，该方法被调用来从服务器加载新数据。
<pre style="color:#006600">
$('#dg').datagrid('load',{
&nbsp;&nbsp;&nbsp;&nbsp;code: '01',
&nbsp;&nbsp;&nbsp;&nbsp;name: 'name01'
});
</pre>
	</td>
</tr>
<tr>
	<td>reload</td>
    <td>param</td>
    <td>重新加载行，就像 load 方法一样，但是保持在当前页。</td>
</tr>
<tr>
	<td>reloadFooter</td>
    <td>footer</td>
    <td>重新加载底部的行。代码实例：
<pre style="color:#006600">
// update footer row values and then refresh
var rows = $('#dg').datagrid('getFooterRows');
rows[0]['name'] = 'new name';
rows[0]['salary'] = 60000;
$('#dg').datagrid('reloadFooter');

// update footer rows with new data
$('#dg').datagrid('reloadFooter',[
&nbsp;&nbsp;&nbsp;&nbsp;{name: 'name1', salary: 60000},
&nbsp;&nbsp;&nbsp;&nbsp;{name: 'name2', salary: 65000}
]);
</pre>
	</td>
</tr>
<tr>
	<td>loading</td>
    <td>none</td>
    <td>显示正在加载状态。</td>
</tr>
<tr>
	<td>loaded</td>
    <td>none</td>
    <td>隐藏正在加载状态。</td>
</tr>
<tr>
	<td>fitColumns</td>
    <td>none</td>
    <td>使列自动展开/折叠以适应数据网格（datagrid）的宽度。</td>
</tr>
<tr>
	<td>fixColumnSize</td>
    <td>field</td>
    <td>固定列的尺寸。如果 'field' 参数未设置，所有的列的尺寸将是固定的。<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid('fixColumnSize', 'name');  // fix the 'name' column size
$('#dg').datagrid('fixColumnSize');  // fix all columns size
</pre>
	</td>
</tr>
<tr>
	<td>fixRowHeight</td>
    <td>index</td>
    <td>固定指定行的高度。如果 'index' 参数未设置，所有的行的高度将是固定的。</td>
</tr>
<tr>
	<td>freezeRow</td>
    <td>index</td>
    <td>冻结指定的行，以便数据网格（datagrid）向下滚动时这些冻结行总是被显示在顶部。该方法自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>autoSizeColumn</td>
    <td>field</td>
    <td>调整列的宽度以适应内容。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>加载本地数据，旧的行会被移除。</td>
</tr>
<tr>
	<td>getData</td>
    <td>none</td>
    <td>返回加载的数据。</td>
</tr>
<tr>
	<td>getRows</td>
    <td>none</td>
    <td>返回当前页的行。</td>
</tr>
<tr>
	<td>getFooterRows</td>
    <td>none</td>
    <td>返回底部的行。</td>
</tr>
<tr>
	<td>getRowIndex</td>
    <td>row</td>
    <td>返回指定行的索引，row 参数可以是一个行记录或者一个 id 字段的值。</td>
</tr>
<tr>
	<td>getChecked</td>
    <td>none</td>
    <td>返回复选框选中的所有行。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>返回第一个选中的行或者 null。</td>
</tr>
<tr>
	<td>getSelections</td>
    <td>none</td>
    <td>返回所有选中的行，当没有选中的记录时，将返回空数组。</td>
</tr>
<tr>
	<td>clearSelections</td>
    <td>none</td>
    <td>清除所有的选择。</td>
</tr>
<tr>
	<td>clearChecked</td>
    <td>none</td>
    <td>清除所有勾选的行。该方法自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>scrollTo</td>
    <td>index</td>
    <td>滚动到指定行。该方法自版本 1.3.3 起可用。</td>
</tr>
<tr>
	<td>highlightRow</td>
    <td>index</td>
    <td>高亮显示一行。该方法自版本 1.3.3 起可用。</td>
</tr>
<tr>
	<td>selectAll</td>
    <td>none</td>
    <td>选中当前页所有的行。</td>
</tr>
<tr>
	<td>unselectAll</td>
    <td>none</td>
    <td>取消选中当前页所有的行。</td>
</tr>
<tr>
	<td>selectRow</td>
    <td>index</td>
    <td>选中一行，行索引从 0 开始。</td>
</tr>
<tr>
	<td>selectRecord</td>
    <td>idValue</td>
    <td>通过传递 id 的值做参数选中一行。</td>
</tr>
<tr>
	<td>unselectRow</td>
    <td>index</td>
    <td>取消选中一行。</td>
</tr>
<tr>
	<td>checkAll</td>
    <td>none</td>
    <td>勾选当前页所有的行。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>uncheckAll</td>
    <td>none</td>
    <td>取消勾选当前页所有的行。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>checkRow</td>
    <td>index</td>
    <td>勾选一行，行索引从 0 开始。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>uncheckRow</td>
    <td>index</td>
    <td>取消勾选一行，行索引从 0 开始。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>beginEdit</td>
    <td>index</td>
    <td>开始对一行进行编辑。</td>
</tr>
<tr>
	<td>endEdit</td>
    <td>index</td>
    <td>结束对一行进行编辑。</td>
</tr>
<tr>
	<td>cancelEdit</td>
    <td>index</td>
    <td>取消对一行进行编辑。</td>
</tr>
<tr>
	<td>getEditors</td>
    <td>index</td>
    <td>获取指定行的编辑器。每个编辑器有下列属性：<br>
	actions：编辑器能做的动作，与编辑器定义相同。<br>
	target：目标编辑器的 jQuery 对象。<br>
	field：字段名。<br>
	type：编辑器的类型，比如：'text'、'combobox'、'datebox'，等等。
	</td>
</tr>
<tr>
	<td>getEditor</td>
    <td>options</td>
    <td>获取指定的编辑器， options 参数包含两个属性：<br>
	index：行的索引。<br>
	field：字段名。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// get the datebox editor and change its value
var ed = $('#dg').datagrid('getEditor', {index:1,field:'birthday'});
$(ed.target).datebox('setValue', '5/4/2012');
</pre>
	</td>
</tr>
<tr>
	<td>refreshRow</td>
    <td>index</td>
    <td>刷新一行。</td>
</tr>
<tr>
	<td>validateRow</td>
    <td>index</td>
    <td>验证指定的行，有效时返回 true。</td>
</tr>
<tr>
	<td>updateRow</td>
    <td>param</td>
    <td>更新指定的行， param 参数包括下列属性：<br>
	index：要更新的行的索引。<br>
	row：新的行数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid('updateRow',{
&nbsp;&nbsp;&nbsp;&nbsp;index: 2,
&nbsp;&nbsp;&nbsp;&nbsp;row: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'new name',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note: 'new note message'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>appendRow</td>
    <td>row</td>
    <td>追加一个新行。新的行将被添加在最后的位置：
<pre style="color:#006600">
$('#dg').datagrid('appendRow',{
&nbsp;&nbsp;&nbsp;&nbsp;name: 'new name',
&nbsp;&nbsp;&nbsp;&nbsp;age: 30,
&nbsp;&nbsp;&nbsp;&nbsp;note: 'some messages'
});
</pre>
	</td>
</tr>
<tr>
	<td>insertRow</td>
    <td>param</td>
    <td>插入一个新行， param 参数包括下列属性：<br>
	index：插入进去的行的索引，如果没有定义，就追加该新行。<br>
	row：行的数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// insert a new row at second row position
$('#dg').datagrid('insertRow',{
&nbsp;&nbsp;&nbsp;&nbsp;index: 1,&nbsp;&nbsp;&nbsp;&nbsp;// index start with 0
&nbsp;&nbsp;&nbsp;&nbsp;row: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'new name',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;age: 30,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note: 'some messages'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>deleteRow</td>
    <td>index</td>
    <td>删除一行。</td>
</tr>
<tr>
	<td>getChanges</td>
    <td>type</td>
    <td>获取最后一次提交以来更改的行，type 参数表示更改的行的类型，可能的值是：inserted、deleted、updated，等等。当 type 参数没有分配时，返回所有改变的行。</td>
</tr>
<tr>
	<td>acceptChanges</td>
    <td>none</td>
    <td>提交自从被加载以来或最后一次调用 acceptChanges 以来所有更改的数据。</td>
</tr>
<tr>
	<td>rejectChanges</td>
    <td>none</td>
    <td>回滚自从创建以来或最后一次调用 acceptChanges 以来所有更改的数据。</td>
</tr>
<tr>
	<td>mergeCells</td>
    <td>options</td>
    <td>把一些单元格合并为一个单元格，options 参数包括下列特性：<br>
	index：列的索引。<br>
	field：字段名。<br>
	rowspan：合并跨越的行数。<br>
	colspan：合并跨越的列数。
	</td>
</tr>
<tr>
	<td>showColumn</td>
    <td>field</td>
    <td>显示指定的列。</td>
</tr>
<tr>
	<td>hideColumn</td>
    <td>field</td>
    <td>隐藏指定的列。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回选项（options）对象。</td>
</tr>
<tr>
	<td>getPager</td>
    <td>none</td>
    <td>返回分页（pager）对象。</td>
</tr>
<tr>
	<td>getPanel</td>
    <td>none</td>
    <td>返回面板（panel）对象。</td>
</tr>
<tr>
	<td>getColumnFields</td>
    <td>frozen</td>
    <td>返回列的字段，如果 frozen 设置为 true，则冻结列的字段被返回。<br>
	代码实例：
<pre style="color:#006600">
var opts = $('#dg').datagrid('getColumnFields');&nbsp;&nbsp;&nbsp;&nbsp;// get unfrozen columns
var opts = $('#dg').datagrid('getColumnFields', true); // get frozen columns
</pre>
	</td>
</tr>
<tr>
	<td>getColumnOption</td>
    <td>field</td>
    <td>返回指定列的选项。</td>
</tr>
<tr>
	<td>resize</td>
    <td>param</td>
    <td>调整尺寸和布局。</td>
</tr>
<tr>
	<td>load</td>
    <td>param</td>
    <td>加载并显示第一页的行，如果指定 'param' 参数，它将替换 queryParams 属性。通常情况下，通过传递一些从参数进行查询，该方法被调用来从服务器加载新数据。
<pre style="color:#006600">
$('#dg').datagrid('load',{
&nbsp;&nbsp;&nbsp;&nbsp;code: '01',
&nbsp;&nbsp;&nbsp;&nbsp;name: 'name01'
});
</pre>
	</td>
</tr>
<tr>
	<td>reload</td>
    <td>param</td>
    <td>重新加载行，就像 load 方法一样，但是保持在当前页。</td>
</tr>
<tr>
	<td>reloadFooter</td>
    <td>footer</td>
    <td>重新加载底部的行。代码实例：
<pre style="color:#006600">
// update footer row values and then refresh
var rows = $('#dg').datagrid('getFooterRows');
rows[0]['name'] = 'new name';
rows[0]['salary'] = 60000;
$('#dg').datagrid('reloadFooter');

// update footer rows with new data
$('#dg').datagrid('reloadFooter',[
&nbsp;&nbsp;&nbsp;&nbsp;{name: 'name1', salary: 60000},
&nbsp;&nbsp;&nbsp;&nbsp;{name: 'name2', salary: 65000}
]);
</pre>
	</td>
</tr>
<tr>
	<td>loading</td>
    <td>none</td>
    <td>显示正在加载状态。</td>
</tr>
<tr>
	<td>loaded</td>
    <td>none</td>
    <td>隐藏正在加载状态。</td>
</tr>
<tr>
	<td>fitColumns</td>
    <td>none</td>
    <td>使列自动展开/折叠以适应数据网格（datagrid）的宽度。</td>
</tr>
<tr>
	<td>fixColumnSize</td>
    <td>field</td>
    <td>固定列的尺寸。如果 'field' 参数未设置，所有的列的尺寸将是固定的。<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid('fixColumnSize', 'name');  // fix the 'name' column size
$('#dg').datagrid('fixColumnSize');  // fix all columns size
</pre>
	</td>
</tr>
<tr>
	<td>fixRowHeight</td>
    <td>index</td>
    <td>固定指定行的高度。如果 'index' 参数未设置，所有的行的高度将是固定的。</td>
</tr>
<tr>
	<td>freezeRow</td>
    <td>index</td>
    <td>冻结指定的行，以便数据网格（datagrid）向下滚动时这些冻结行总是被显示在顶部。该方法自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>autoSizeColumn</td>
    <td>field</td>
    <td>调整列的宽度以适应内容。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>加载本地数据，旧的行会被移除。</td>
</tr>
<tr>
	<td>getData</td>
    <td>none</td>
    <td>返回加载的数据。</td>
</tr>
<tr>
	<td>getRows</td>
    <td>none</td>
    <td>返回当前页的行。</td>
</tr>
<tr>
	<td>getFooterRows</td>
    <td>none</td>
    <td>返回底部的行。</td>
</tr>
<tr>
	<td>getRowIndex</td>
    <td>row</td>
    <td>返回指定行的索引，row 参数可以是一个行记录或者一个 id 字段的值。</td>
</tr>
<tr>
	<td>getChecked</td>
    <td>none</td>
    <td>返回复选框选中的所有行。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>返回第一个选中的行或者 null。</td>
</tr>
<tr>
	<td>getSelections</td>
    <td>none</td>
    <td>返回所有选中的行，当没有选中的记录时，将返回空数组。</td>
</tr>
<tr>
	<td>clearSelections</td>
    <td>none</td>
    <td>清除所有的选择。</td>
</tr>
<tr>
	<td>clearChecked</td>
    <td>none</td>
    <td>清除所有勾选的行。该方法自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>scrollTo</td>
    <td>index</td>
    <td>滚动到指定行。该方法自版本 1.3.3 起可用。</td>
</tr>
<tr>
	<td>highlightRow</td>
    <td>index</td>
    <td>高亮显示一行。该方法自版本 1.3.3 起可用。</td>
</tr>
<tr>
	<td>selectAll</td>
    <td>none</td>
    <td>选中当前页所有的行。</td>
</tr>
<tr>
	<td>unselectAll</td>
    <td>none</td>
    <td>取消选中当前页所有的行。</td>
</tr>
<tr>
	<td>selectRow</td>
    <td>index</td>
    <td>选中一行，行索引从 0 开始。</td>
</tr>
<tr>
	<td>selectRecord</td>
    <td>idValue</td>
    <td>通过传递 id 的值做参数选中一行。</td>
</tr>
<tr>
	<td>unselectRow</td>
    <td>index</td>
    <td>取消选中一行。</td>
</tr>
<tr>
	<td>checkAll</td>
    <td>none</td>
    <td>勾选当前页所有的行。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>uncheckAll</td>
    <td>none</td>
    <td>取消勾选当前页所有的行。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>checkRow</td>
    <td>index</td>
    <td>勾选一行，行索引从 0 开始。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>uncheckRow</td>
    <td>index</td>
    <td>取消勾选一行，行索引从 0 开始。该方法自版本 1.3 起可用。</td>
</tr>
<tr>
	<td>beginEdit</td>
    <td>index</td>
    <td>开始对一行进行编辑。</td>
</tr>
<tr>
	<td>endEdit</td>
    <td>index</td>
    <td>结束对一行进行编辑。</td>
</tr>
<tr>
	<td>cancelEdit</td>
    <td>index</td>
    <td>取消对一行进行编辑。</td>
</tr>
<tr>
	<td>getEditors</td>
    <td>index</td>
    <td>获取指定行的编辑器。每个编辑器有下列属性：<br>
	actions：编辑器能做的动作，与编辑器定义相同。<br>
	target：目标编辑器的 jQuery 对象。<br>
	field：字段名。<br>
	type：编辑器的类型，比如：'text'、'combobox'、'datebox'，等等。
	</td>
</tr>
<tr>
	<td>getEditor</td>
    <td>options</td>
    <td>获取指定的编辑器， options 参数包含两个属性：<br>
	index：行的索引。<br>
	field：字段名。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// get the datebox editor and change its value
var ed = $('#dg').datagrid('getEditor', {index:1,field:'birthday'});
$(ed.target).datebox('setValue', '5/4/2012');
</pre>
	</td>
</tr>
<tr>
	<td>refreshRow</td>
    <td>index</td>
    <td>刷新一行。</td>
</tr>
<tr>
	<td>validateRow</td>
    <td>index</td>
    <td>验证指定的行，有效时返回 true。</td>
</tr>
<tr>
	<td>updateRow</td>
    <td>param</td>
    <td>更新指定的行， param 参数包括下列属性：<br>
	index：要更新的行的索引。<br>
	row：新的行数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#dg').datagrid('updateRow',{
&nbsp;&nbsp;&nbsp;&nbsp;index: 2,
&nbsp;&nbsp;&nbsp;&nbsp;row: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'new name',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note: 'new note message'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>appendRow</td>
    <td>row</td>
    <td>追加一个新行。新的行将被添加在最后的位置：
<pre style="color:#006600">
$('#dg').datagrid('appendRow',{
&nbsp;&nbsp;&nbsp;&nbsp;name: 'new name',
&nbsp;&nbsp;&nbsp;&nbsp;age: 30,
&nbsp;&nbsp;&nbsp;&nbsp;note: 'some messages'
});
</pre>
	</td>
</tr>
<tr>
	<td>insertRow</td>
    <td>param</td>
    <td>插入一个新行， param 参数包括下列属性：<br>
	index：插入进去的行的索引，如果没有定义，就追加该新行。<br>
	row：行的数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// insert a new row at second row position
$('#dg').datagrid('insertRow',{
&nbsp;&nbsp;&nbsp;&nbsp;index: 1,&nbsp;&nbsp;&nbsp;&nbsp;// index start with 0
&nbsp;&nbsp;&nbsp;&nbsp;row: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'new name',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;age: 30,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note: 'some messages'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>deleteRow</td>
    <td>index</td>
    <td>删除一行。</td>
</tr>
<tr>
	<td>getChanges</td>
    <td>type</td>
    <td>获取最后一次提交以来更改的行，type 参数表示更改的行的类型，可能的值是：inserted、deleted、updated，等等。当 type 参数没有分配时，返回所有改变的行。</td>
</tr>
<tr>
	<td>acceptChanges</td>
    <td>none</td>
    <td>提交自从被加载以来或最后一次调用 acceptChanges 以来所有更改的数据。</td>
</tr>
<tr>
	<td>rejectChanges</td>
    <td>none</td>
    <td>回滚自从创建以来或最后一次调用 acceptChanges 以来所有更改的数据。</td>
</tr>
<tr>
	<td>mergeCells</td>
    <td>options</td>
    <td>把一些单元格合并为一个单元格，options 参数包括下列特性：<br>
	index：列的索引。<br>
	field：字段名。<br>
	rowspan：合并跨越的行数。<br>
	colspan：合并跨越的列数。
	</td>
</tr>
<tr>
	<td>showColumn</td>
    <td>field</td>
    <td>显示指定的列。</td>
</tr>
<tr>
	<td>hideColumn</td>
    <td>field</td>
    <td>隐藏指定的列。</td>
</tr>
</table></div>') where ID=5685;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>url</td>
    <td>string</td>
    <td>获取远程数据的 URL 。</td>
	<td>null</td>
</tr>
<tr>
	<td>method</td>
    <td>string</td>
    <td>检索数据的 http 方法（method）。</td>
	<td>post</td>
</tr>
<tr>
	<td>animate</td>
    <td>boolean</td>
    <td>定义当节点展开折叠时是否显示动画效果。</td>
	<td>false</td>
</tr>
<tr>
	<td>checkbox</td>
    <td>boolean</td>
    <td>定义是否在每个节点前边显示复选框。</td>
	<td>false</td>
</tr>
<tr>
	<td>cascadeCheck</td>
    <td>boolean</td>
    <td>定义是否级联检查。</td>
	<td>true</td>
</tr>
<tr>
	<td>onlyLeafCheck</td>
    <td>boolean</td>
    <td>定义是否只在叶节点前显示复选框。</td>
	<td>false</td>
</tr>
<tr>
	<td>lines</td>
    <td>boolean</td>
    <td>定义是否显示树线条。</td>
	<td>false</td>
</tr>
<tr>
	<td>dnd</td>
    <td>boolean</td>
    <td>定义是否启用拖放。</td>
	<td>false</td>
</tr>
<tr>
	<td>data</td>
    <td>array</td>
    <td>要加载的节点数据。
<pre style="color:#006600">
$('#tt').tree({
&nbsp;&nbsp;&nbsp;&nbsp;data: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Item1',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;state: 'closed',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;children: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Item11'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Item12'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]
&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Item2'
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>formatter</td>
    <td>function(node)</td>
    <td>定义如何呈现节点文本。<br>
	代码实例：
<pre style="color:#006600">
$('#tt').tree({
&nbsp;&nbsp;&nbsp;&nbsp;formatter:function(node){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return node.text;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>loader</td>
    <td>function(param,success,error)</td>
	<td>定义如何从远程服务器加载数据。返回 false 则取消该动作。该函数有下列参数：<br>
	param：要传递到远程服务器的参数对象。<br>
	success(data)：当检索数据成功时调用的回调函数。<br>
	error()：当检索数据失败时调用的回调函数。<br>
	</td>
	<td>json loader</td>
</tr>
<tr>
	<td>loadFilter</td>
    <td>function(data,parent)</td>
    <td>返回要显示的过滤数据。返回数据时以标准树格式返回的。该函数有下列参数：<br> 
	data：要加载的原始数据。<br>
	parent：DOM 对象，表示父节点。
	</td>
	<td></td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">类型</th>
    <th align="left">描述</th>
	<th align="left">默认值</th>
</tr>
<tr>
	<td>url</td>
    <td>string</td>
    <td>获取远程数据的 URL 。</td>
	<td>null</td>
</tr>
<tr>
	<td>method</td>
    <td>string</td>
    <td>检索数据的 http 方法（method）。</td>
	<td>post</td>
</tr>
<tr>
	<td>animate</td>
    <td>boolean</td>
    <td>定义当节点展开折叠时是否显示动画效果。</td>
	<td>false</td>
</tr>
<tr>
	<td>checkbox</td>
    <td>boolean</td>
    <td>定义是否在每个节点前边显示复选框。</td>
	<td>false</td>
</tr>
<tr>
	<td>cascadeCheck</td>
    <td>boolean</td>
    <td>定义是否级联检查。</td>
	<td>true</td>
</tr>
<tr>
	<td>onlyLeafCheck</td>
    <td>boolean</td>
    <td>定义是否只在叶节点前显示复选框。</td>
	<td>false</td>
</tr>
<tr>
	<td>lines</td>
    <td>boolean</td>
    <td>定义是否显示树线条。</td>
	<td>false</td>
</tr>
<tr>
	<td>dnd</td>
    <td>boolean</td>
    <td>定义是否启用拖放。</td>
	<td>false</td>
</tr>
<tr>
	<td>data</td>
    <td>array</td>
    <td>要加载的节点数据。
<pre style="color:#006600">
$('#tt').tree({
&nbsp;&nbsp;&nbsp;&nbsp;data: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Item1',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;state: 'closed',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;children: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Item11'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Item12'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]
&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'Item2'
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
	<td>null</td>
</tr>
<tr>
	<td>formatter</td>
    <td>function(node)</td>
    <td>定义如何呈现节点文本。<br>
	代码实例：
<pre style="color:#006600">
$('#tt').tree({
&nbsp;&nbsp;&nbsp;&nbsp;formatter:function(node){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return node.text;
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
	<td></td>
</tr>
<tr>
	<td>loader</td>
    <td>function(param,success,error)</td>
	<td>定义如何从远程服务器加载数据。返回 false 则取消该动作。该函数有下列参数：<br>
	param：要传递到远程服务器的参数对象。<br>
	success(data)：当检索数据成功时调用的回调函数。<br>
	error()：当检索数据失败时调用的回调函数。<br>
	</td>
	<td>json loader</td>
</tr>
<tr>
	<td>loadFilter</td>
    <td>function(data,parent)</td>
    <td>返回要显示的过滤数据。返回数据时以标准树格式返回的。该函数有下列参数：<br> 
	data：要加载的原始数据。<br>
	parent：DOM 对象，表示父节点。
	</td>
	<td></td>
</tr>
</table></div>') where ID=5687;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onClick</td>
    <td>node</td>
    <td>当用户点击一个节点时触发。代码实例：
<pre style="color:#006600">
$('#tt').tree({
&nbsp;&nbsp;&nbsp;&nbsp;onClick: function(node){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(node.text);  // alert node text property when clicked
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>onDblClick</td>
    <td>node</td>
    <td>当用户双击一个节点时触发。</td>
</tr>
<tr>
	<td>onBeforeLoad</td>
    <td>node, param</td>
    <td>当加载数据的请求发出前触发，返回 false 则取消加载动作。</td>
</tr>
<tr>
	<td>onLoadSuccess</td>
    <td>node, data</td>
    <td>当数据加载成功时触发。</td>
</tr>
<tr>
	<td>onLoadError</td>
    <td>arguments</td>
    <td>当数据加载失败时触发，arguments 参数与 jQuery.ajax 的 'error' 函数一样。</td>
</tr>
<tr>
	<td>onBeforeExpand</td>
    <td>node</td>
    <td>节点展开前触发，返回 false 则取消展开动作。</td>
</tr>
<tr>
	<td>onExpand</td>
    <td>node</td>
    <td>当节点展开时触发。</td>
</tr>
<tr>
	<td>onBeforeCollapse</td>
    <td>node</td>
    <td>节点折叠前触发，返回 false 则取消折叠动作。</td>
</tr>
<tr>
	<td>onCollapse</td>
    <td>node</td>
    <td>当节点折叠时触发。</td>
</tr>
<tr>
	<td>onBeforeCheck</td>
    <td>node, checked</td>
    <td>当用户点击复选框前触发，返回 false 则取消该选中动作。该事件自版本 1.3.1 起可用。</td>
</tr>
<tr>
	<td>onCheck</td>
    <td>node, checked</td>
    <td>当用户点击复选框时触发。</td>
</tr>
<tr>
	<td>onBeforeSelect</td>
    <td>node</td>
    <td>节点被选中前触发，返回 false 则取消选择动作。</td>
</tr>
<tr>
	<td>onSelect</td>
    <td>node</td>
    <td>当节点被选中时触发。</td>
</tr>
<tr>
	<td>onContextMenu</td>
    <td>e, node</td>
    <td>当右键点击节点时触发。代码实例：
<pre style="color:#006600">
// right click node and then display the context menu
$('#tt').tree({
&nbsp;&nbsp;&nbsp;&nbsp;onContextMenu: function(e, node){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.preventDefault();
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// select the node
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$('#tt').tree('select', node.target);
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// display context menu
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$('#mm').menu('show', {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;left: e.pageX,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;top: e.pageY
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});
&nbsp;&nbsp;&nbsp;&nbsp;}
});

// the context menu is defined as below:
&lt;div id="mm" class="easyui-menu" style="width:120px;"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div onclick="append()" data-options="iconCls:'icon-add'"&gt;Append&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div onclick="remove()" data-options="iconCls:'icon-remove'"&gt;Remove&lt;/div&gt;
&lt;/div&gt;
</pre>
	</td>
</tr>
<tr>
	<td>onBeforeDrag</td>
    <td>node</td>
    <td>当节点的拖拽开始时触发，返回 false 则禁止拖拽。该事件自版本 1.3.2 起可用。 </td>
</tr>
<tr>
	<td>onStartDrag</td>
    <td>node</td>
    <td>当开始拖拽节点时触发。该事件自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>onStopDrag</td>
    <td>node</td>
    <td>当停止拖拽节点时触发。该事件自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>onDragEnter</td>
    <td>target, source</td>
    <td>当节点被拖拽进入某个允许放置的目标节点时触发，返回 false 则禁止放置。<br>
	target：被放置的目标节点元素。<br>
	source：被拖拽的源节点。<br>
	该事件自版本 1.3.2 起可用。
	</td>
</tr>
<tr>
	<td>onDragOver</td>
    <td>target, source</td>
    <td>当节点被拖拽到允许放置的目标节点上时触发，返回 false 则禁止放置。<br>
	target：被放置的目标节点元素。<br>
	source：被拖拽的源节点。<br>
	该事件自版本 1.3.2 起可用。
	</td>
</tr>
<tr>
	<td>onDragLeave</td>
    <td>target, source</td>
    <td>当节点被拖拽离开允许放置的目标节点时触发。<br>
	target：被放置的目标节点元素。<br>
	source：被拖拽的源节点。<br>
	该事件自版本 1.3.2 起可用。
	</td>
</tr>
<tr>
	<td>onBeforeDrop</td>
    <td>target,source,point</td>
    <td>节点被放置之前触发，返回 false 则禁止放置。<br>
	target：DOM 对象，放置的目标节点。<br>
	source：源节点。<br>
	point：表示放置操作，可能的值是：'append'、'top' 或 'bottom'。<br>
	该事件自版本 1.3.2 起可用。
	</td>
</tr>
<tr>
	<td>onDrop</td>
    <td>target,source,point</td>
    <td>当节点被放置时触发。
	target：DOM 对象，放置的目标节点。<br>
	source：源节点。<br>
	point：表示放置操作，可能的值是：'append'、'top' 或 'bottom'。
	</td>
</tr>
<tr>
	<td>onBeforeEdit</td>
    <td>node</td>
    <td>编辑节点前触发。</td>
</tr>
<tr>
	<td>onAfterEdit</td>
    <td>node</td>
    <td>编辑节点后触发。</td>
</tr>
<tr>
	<td>onCancelEdit</td>
    <td>node</td>
    <td>当取消编辑动作时触发。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>onClick</td>
    <td>node</td>
    <td>当用户点击一个节点时触发。代码实例：
<pre style="color:#006600">
$('#tt').tree({
&nbsp;&nbsp;&nbsp;&nbsp;onClick: function(node){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(node.text);  // alert node text property when clicked
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	</td>
</tr>
<tr>
	<td>onDblClick</td>
    <td>node</td>
    <td>当用户双击一个节点时触发。</td>
</tr>
<tr>
	<td>onBeforeLoad</td>
    <td>node, param</td>
    <td>当加载数据的请求发出前触发，返回 false 则取消加载动作。</td>
</tr>
<tr>
	<td>onLoadSuccess</td>
    <td>node, data</td>
    <td>当数据加载成功时触发。</td>
</tr>
<tr>
	<td>onLoadError</td>
    <td>arguments</td>
    <td>当数据加载失败时触发，arguments 参数与 jQuery.ajax 的 'error' 函数一样。</td>
</tr>
<tr>
	<td>onBeforeExpand</td>
    <td>node</td>
    <td>节点展开前触发，返回 false 则取消展开动作。</td>
</tr>
<tr>
	<td>onExpand</td>
    <td>node</td>
    <td>当节点展开时触发。</td>
</tr>
<tr>
	<td>onBeforeCollapse</td>
    <td>node</td>
    <td>节点折叠前触发，返回 false 则取消折叠动作。</td>
</tr>
<tr>
	<td>onCollapse</td>
    <td>node</td>
    <td>当节点折叠时触发。</td>
</tr>
<tr>
	<td>onBeforeCheck</td>
    <td>node, checked</td>
    <td>当用户点击复选框前触发，返回 false 则取消该选中动作。该事件自版本 1.3.1 起可用。</td>
</tr>
<tr>
	<td>onCheck</td>
    <td>node, checked</td>
    <td>当用户点击复选框时触发。</td>
</tr>
<tr>
	<td>onBeforeSelect</td>
    <td>node</td>
    <td>节点被选中前触发，返回 false 则取消选择动作。</td>
</tr>
<tr>
	<td>onSelect</td>
    <td>node</td>
    <td>当节点被选中时触发。</td>
</tr>
<tr>
	<td>onContextMenu</td>
    <td>e, node</td>
    <td>当右键点击节点时触发。代码实例：
<pre style="color:#006600">
// right click node and then display the context menu
$('#tt').tree({
&nbsp;&nbsp;&nbsp;&nbsp;onContextMenu: function(e, node){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.preventDefault();
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// select the node
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$('#tt').tree('select', node.target);
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// display context menu
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$('#mm').menu('show', {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;left: e.pageX,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;top: e.pageY
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});
&nbsp;&nbsp;&nbsp;&nbsp;}
});

// the context menu is defined as below:
&lt;div id="mm" class="easyui-menu" style="width:120px;"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div onclick="append()" data-options="iconCls:'icon-add'"&gt;Append&lt;/div&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;div onclick="remove()" data-options="iconCls:'icon-remove'"&gt;Remove&lt;/div&gt;
&lt;/div&gt;
</pre>
	</td>
</tr>
<tr>
	<td>onBeforeDrag</td>
    <td>node</td>
    <td>当节点的拖拽开始时触发，返回 false 则禁止拖拽。该事件自版本 1.3.2 起可用。 </td>
</tr>
<tr>
	<td>onStartDrag</td>
    <td>node</td>
    <td>当开始拖拽节点时触发。该事件自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>onStopDrag</td>
    <td>node</td>
    <td>当停止拖拽节点时触发。该事件自版本 1.3.2 起可用。</td>
</tr>
<tr>
	<td>onDragEnter</td>
    <td>target, source</td>
    <td>当节点被拖拽进入某个允许放置的目标节点时触发，返回 false 则禁止放置。<br>
	target：被放置的目标节点元素。<br>
	source：被拖拽的源节点。<br>
	该事件自版本 1.3.2 起可用。
	</td>
</tr>
<tr>
	<td>onDragOver</td>
    <td>target, source</td>
    <td>当节点被拖拽到允许放置的目标节点上时触发，返回 false 则禁止放置。<br>
	target：被放置的目标节点元素。<br>
	source：被拖拽的源节点。<br>
	该事件自版本 1.3.2 起可用。
	</td>
</tr>
<tr>
	<td>onDragLeave</td>
    <td>target, source</td>
    <td>当节点被拖拽离开允许放置的目标节点时触发。<br>
	target：被放置的目标节点元素。<br>
	source：被拖拽的源节点。<br>
	该事件自版本 1.3.2 起可用。
	</td>
</tr>
<tr>
	<td>onBeforeDrop</td>
    <td>target,source,point</td>
    <td>节点被放置之前触发，返回 false 则禁止放置。<br>
	target：DOM 对象，放置的目标节点。<br>
	source：源节点。<br>
	point：表示放置操作，可能的值是：'append'、'top' 或 'bottom'。<br>
	该事件自版本 1.3.2 起可用。
	</td>
</tr>
<tr>
	<td>onDrop</td>
    <td>target,source,point</td>
    <td>当节点被放置时触发。
	target：DOM 对象，放置的目标节点。<br>
	source：源节点。<br>
	point：表示放置操作，可能的值是：'append'、'top' 或 'bottom'。
	</td>
</tr>
<tr>
	<td>onBeforeEdit</td>
    <td>node</td>
    <td>编辑节点前触发。</td>
</tr>
<tr>
	<td>onAfterEdit</td>
    <td>node</td>
    <td>编辑节点后触发。</td>
</tr>
<tr>
	<td>onCancelEdit</td>
    <td>node</td>
    <td>当取消编辑动作时触发。</td>
</tr>
</table></div>') where ID=5687;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回树的选项（options）。</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>加载树的数据。</td>
</tr>
<tr>
	<td>getNode</td>
    <td>target</td>
    <td>获取指定的节点对象。</td>
</tr>
<tr>
	<td>getData</td>
    <td>target</td>
    <td>获取指定的节点数据，包括它的子节点。</td>
</tr>
<tr>
	<td>reload</td>
    <td>target</td>
    <td>重新加载树的数据。</td>
</tr>
<tr>
	<td>getRoot</td>
    <td>none</td>
    <td>获取根节点，返回节点对象。</td>
</tr>
<tr>
	<td>getRoots</td>
    <td>none</td>
    <td>获取根节点，返回节点数组。</td>
</tr>
<tr>
	<td>getParent</td>
    <td>target</td>
    <td>获取父节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>getChildren</td>
    <td>target</td>
    <td>获取子节点， target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>getChecked</td>
    <td>state</td>
    <td>获取选中的节点。状态可用值有：'checked'、'unchecked'、'indeterminate'。如果状态未分配，则返回 'checked' 节点。<br>
	代码实例：
<pre style="color:#006600">
var nodes = $('#tt').tree('getChecked');&nbsp;&nbsp;&nbsp;&nbsp;// get checked nodes
var nodes = $('#tt').tree('getChecked', 'unchecked');&nbsp;&nbsp;&nbsp;&nbsp;// get unchecked nodes
var nodes = $('#tt').tree('getChecked', 'indeterminate');&nbsp;&nbsp;&nbsp;&nbsp;// get indeterminate nodes
var nodes = $('#tt').tree('getChecked', ['checked','indeterminate']);&nbsp;&nbsp;&nbsp;&nbsp;// get checked and indeterminate nodes
</pre>
	</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>获取选中的节点并返回它，如果没有选中节点，则返回 null。</td>
</tr>
<tr>
	<td>isLeaf</td>
    <td>target</td>
    <td>把指定的节点定义成叶节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>find</td>
    <td>id</td>
    <td>找到指定的节点并返回该节点对象。代码实例：
<pre style="color:#006600">
// find a node and then select it
var node = $('#tt').tree('find', 12);
$('#tt').tree('select', node.target);
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>target</td>
    <td>选中一个节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>check</td>
    <td>target</td>
    <td>把指定节点设置为勾选。</td>
</tr>
<tr>
	<td>uncheck</td>
    <td>target</td>
    <td>把指定节点设置为未勾选。</td>
</tr>
<tr>
	<td>collapse</td>
    <td>target</td>
    <td>折叠一个节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>expand</td>
    <td>target</td>
    <td>展开一个节点，target 参数表示节点的 DOM 对象。当节点关闭且没有子节点时，节点的 id 值（名为 'id' 参数）将被发送至服务器以请求子节点数据。</td>
</tr>
<tr>
	<td>collapseAll</td>
    <td>target</td>
    <td>折叠所有的节点。</td>
</tr>
<tr>
	<td>expandAll</td>
    <td>target</td>
    <td>展开所有的节点。</td>
</tr>
<tr>
	<td>expandTo</td>
    <td>target</td>
    <td>从根部展开一个指定的节点。</td>
</tr>
<tr>
	<td>scrollTo</td>
    <td>target</td>
    <td>滚动到指定节点。该方法自版本 1.3.4 起可用。</td>
</tr>
<tr>
	<td>append</td>
    <td>param</td>
    <td>追加一些子节点到一个父节点，param 参数有两个属性：<br>
	parent：DOM 对象，要追加到的父节点，如果没有分配，则追加为根节点。<br>
	data：数组，节点的数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// append some nodes to the selected node
var selected = $('#tt').tree('getSelected');
$('#tt').tree('append', {
&nbsp;&nbsp;&nbsp;&nbsp;parent: selected.target,
&nbsp;&nbsp;&nbsp;&nbsp;data: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 23,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node23'
&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node24',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;state: 'closed',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;children: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node241'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node242'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
</tr>
<tr>
	<td>toggle</td>
    <td>target</td>
    <td>切换节点的展开/折叠状态，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>insert</td>
    <td>param</td>
    <td>在指定节点的前边或后边插入一个节点，param 参数包括下列属性：<br>
	before：DOM 对象，前边插入的节点。<br>
	after：DOM 对象，后边插入的节点。<br>
	data：对象，节点数据。<br>
	<br>
	下面的代码演示了如何在选中节点之前插入一个新的节点：
<pre style="color:#006600">
var node = $('#tt').tree('getSelected');
if (node){
&nbsp;&nbsp;&nbsp;&nbsp;$('#tt').tree('insert', {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;before: node.target,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 21,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node text'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;});
}
</pre>
	</td>
</tr>
<tr>
	<td>remove</td>
    <td>target</td>
    <td>移除一个节点和它的子节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>pop</td>
    <td>target</td>
    <td>弹出一个节点和它的子节点，该方法和 remove 一样，但是返回了移除的节点数据。</td>
</tr>
<tr>
	<td>update</td>
    <td>param</td>
    <td>更新指定的节点，'param' 参数有下列属性：<br>
	target（DOM 对象，要被更新的节点）、id、text、iconCls、checked，等等。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// update the selected node text
var node = $('#tt').tree('getSelected');
if (node){
&nbsp;&nbsp;&nbsp;&nbsp;$('#tt').tree('update', {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;target: node.target,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'new text'
&nbsp;&nbsp;&nbsp;&nbsp;});
}
</pre>
	</td>
</tr>
<tr>
	<td>enableDnd</td>
    <td>none</td>
    <td>启用拖放功能。</td>
</tr>
<tr>
	<td>disableDnd</td>
    <td>none</td>
    <td>禁用拖放功能。</td>
</tr>
<tr>
	<td>beginEdit</td>
    <td>target</td>
    <td>开始编辑节点。</td>
</tr>
<tr>
	<td>endEdit</td>
    <td>target</td>
    <td>结束编辑节点。</td>
</tr>
<tr>
	<td>cancelEdit</td>
    <td>target</td>
    <td>取消编辑节点。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回树的选项（options）。</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>加载树的数据。</td>
</tr>
<tr>
	<td>getNode</td>
    <td>target</td>
    <td>获取指定的节点对象。</td>
</tr>
<tr>
	<td>getData</td>
    <td>target</td>
    <td>获取指定的节点数据，包括它的子节点。</td>
</tr>
<tr>
	<td>reload</td>
    <td>target</td>
    <td>重新加载树的数据。</td>
</tr>
<tr>
	<td>getRoot</td>
    <td>none</td>
    <td>获取根节点，返回节点对象。</td>
</tr>
<tr>
	<td>getRoots</td>
    <td>none</td>
    <td>获取根节点，返回节点数组。</td>
</tr>
<tr>
	<td>getParent</td>
    <td>target</td>
    <td>获取父节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>getChildren</td>
    <td>target</td>
    <td>获取子节点， target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>getChecked</td>
    <td>state</td>
    <td>获取选中的节点。状态可用值有：'checked'、'unchecked'、'indeterminate'。如果状态未分配，则返回 'checked' 节点。<br>
	代码实例：
<pre style="color:#006600">
var nodes = $('#tt').tree('getChecked');&nbsp;&nbsp;&nbsp;&nbsp;// get checked nodes
var nodes = $('#tt').tree('getChecked', 'unchecked');&nbsp;&nbsp;&nbsp;&nbsp;// get unchecked nodes
var nodes = $('#tt').tree('getChecked', 'indeterminate');&nbsp;&nbsp;&nbsp;&nbsp;// get indeterminate nodes
var nodes = $('#tt').tree('getChecked', ['checked','indeterminate']);&nbsp;&nbsp;&nbsp;&nbsp;// get checked and indeterminate nodes
</pre>
	</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>获取选中的节点并返回它，如果没有选中节点，则返回 null。</td>
</tr>
<tr>
	<td>isLeaf</td>
    <td>target</td>
    <td>把指定的节点定义成叶节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>find</td>
    <td>id</td>
    <td>找到指定的节点并返回该节点对象。代码实例：
<pre style="color:#006600">
// find a node and then select it
var node = $('#tt').tree('find', 12);
$('#tt').tree('select', node.target);
</pre>
	</td>
</tr>
<tr>
	<td>select</td>
    <td>target</td>
    <td>选中一个节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>check</td>
    <td>target</td>
    <td>把指定节点设置为勾选。</td>
</tr>
<tr>
	<td>uncheck</td>
    <td>target</td>
    <td>把指定节点设置为未勾选。</td>
</tr>
<tr>
	<td>collapse</td>
    <td>target</td>
    <td>折叠一个节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>expand</td>
    <td>target</td>
    <td>展开一个节点，target 参数表示节点的 DOM 对象。当节点关闭且没有子节点时，节点的 id 值（名为 'id' 参数）将被发送至服务器以请求子节点数据。</td>
</tr>
<tr>
	<td>collapseAll</td>
    <td>target</td>
    <td>折叠所有的节点。</td>
</tr>
<tr>
	<td>expandAll</td>
    <td>target</td>
    <td>展开所有的节点。</td>
</tr>
<tr>
	<td>expandTo</td>
    <td>target</td>
    <td>从根部展开一个指定的节点。</td>
</tr>
<tr>
	<td>scrollTo</td>
    <td>target</td>
    <td>滚动到指定节点。该方法自版本 1.3.4 起可用。</td>
</tr>
<tr>
	<td>append</td>
    <td>param</td>
    <td>追加一些子节点到一个父节点，param 参数有两个属性：<br>
	parent：DOM 对象，要追加到的父节点，如果没有分配，则追加为根节点。<br>
	data：数组，节点的数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// append some nodes to the selected node
var selected = $('#tt').tree('getSelected');
$('#tt').tree('append', {
&nbsp;&nbsp;&nbsp;&nbsp;parent: selected.target,
&nbsp;&nbsp;&nbsp;&nbsp;data: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 23,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node23'
&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node24',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;state: 'closed',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;children: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node241'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node242'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
</tr>
<tr>
	<td>toggle</td>
    <td>target</td>
    <td>切换节点的展开/折叠状态，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>insert</td>
    <td>param</td>
    <td>在指定节点的前边或后边插入一个节点，param 参数包括下列属性：<br>
	before：DOM 对象，前边插入的节点。<br>
	after：DOM 对象，后边插入的节点。<br>
	data：对象，节点数据。<br>
	<br>
	下面的代码演示了如何在选中节点之前插入一个新的节点：
<pre style="color:#006600">
var node = $('#tt').tree('getSelected');
if (node){
&nbsp;&nbsp;&nbsp;&nbsp;$('#tt').tree('insert', {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;before: node.target,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 21,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'node text'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;});
}
</pre>
	</td>
</tr>
<tr>
	<td>remove</td>
    <td>target</td>
    <td>移除一个节点和它的子节点，target 参数表示节点的 DOM 对象。</td>
</tr>
<tr>
	<td>pop</td>
    <td>target</td>
    <td>弹出一个节点和它的子节点，该方法和 remove 一样，但是返回了移除的节点数据。</td>
</tr>
<tr>
	<td>update</td>
    <td>param</td>
    <td>更新指定的节点，'param' 参数有下列属性：<br>
	target（DOM 对象，要被更新的节点）、id、text、iconCls、checked，等等。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// update the selected node text
var node = $('#tt').tree('getSelected');
if (node){
&nbsp;&nbsp;&nbsp;&nbsp;$('#tt').tree('update', {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;target: node.target,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text: 'new text'
&nbsp;&nbsp;&nbsp;&nbsp;});
}
</pre>
	</td>
</tr>
<tr>
	<td>enableDnd</td>
    <td>none</td>
    <td>启用拖放功能。</td>
</tr>
<tr>
	<td>disableDnd</td>
    <td>none</td>
    <td>禁用拖放功能。</td>
</tr>
<tr>
	<td>beginEdit</td>
    <td>target</td>
    <td>开始编辑节点。</td>
</tr>
<tr>
	<td>endEdit</td>
    <td>target</td>
    <td>结束编辑节点。</td>
</tr>
<tr>
	<td>cancelEdit</td>
    <td>target</td>
    <td>取消编辑节点。</td>
</tr>
</table></div>') where ID=5687;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回树形网格（treegrid）的选项（options）。</td>
</tr>
<tr>
	<td>resize</td>
    <td>options</td>
    <td>设置树形网格（treegrid）的尺寸， options 参数包含两个属性：<br>
	width：树形网格（treegrid）的新宽度。<br>
	height：树形网格（treegrid）的新高度。
	</td>
</tr>
<tr>
	<td>fixRowHeight</td>
    <td>id</td>
    <td>固定指定行的高度。</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>加载树形网格（treegrid）的数据。</td>
</tr>
<tr>
	<td>load</td>
    <td>param</td>
    <td>加载并显示第一页。该方法自版本 1.3.4 起可用。<br>
	代码实例：
<pre style="color:#006600">
// load and send some request parameters
$('#tg').treegrid('load', {
&nbsp;&nbsp;&nbsp;&nbsp;q: 'abc',
&nbsp;&nbsp;&nbsp;&nbsp;name: 'name1'
});
</pre>
	</td>
</tr>
<tr>
	<td>reload</td>
    <td>id</td>
    <td>重新加载树形网格（treegrid）的数据。如果传递了 'id' 参数，则重新加载树的指定行，否则重新加载树的所有行。<br>
	代码实例：
<pre style="color:#006600">
$('#tt').treegrid('reload', 2);&nbsp;&nbsp;&nbsp;&nbsp;// reload the row which value is equals to 2
$('#tt').treegrid('reload');&nbsp;&nbsp;&nbsp;&nbsp;// reload the all rows
$('#tt').treegrid('reload', {id:2, q:'abc'});  // reload the specified row with 'q' parameter passing to server
</pre>
	</td>
</tr>
<tr>
	<td>reloadFooter</td>
    <td>footer</td>
    <td>重新加载底部数据。</td>
</tr>
<tr>
	<td>getData</td>
    <td>none</td>
    <td>获取加载的数据。</td>
</tr>
<tr>
	<td>getFooterRows</td>
    <td>none</td>
    <td>获取底部数据。</td>
</tr>
<tr>
	<td>getRoot</td>
    <td>none</td>
    <td>获取根节点，返回节点对象。</td>
</tr>
<tr>
	<td>getRoots</td>
    <td>none</td>
    <td>获取根节点，返回节点数组。</td>
</tr>
<tr>
	<td>getParent</td>
    <td>id</td>
    <td>获取父节点。</td>
</tr>
<tr>
	<td>getChildren</td>
    <td>id</td>
    <td>获取子节点。</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>获取选中的节点并返回它，如果没有选中节点则返回 null。</td>
</tr>
<tr>
	<td>getSelections</td>
    <td>none</td>
    <td>获取所有选中的节点。</td>
</tr>
<tr>
	<td>getLevel</td>
    <td>id</td>
    <td>获取指定节点的层级。</td>
</tr>
<tr>
	<td>find</td>
    <td>id</td>
    <td>找到指定节点并返回该节点数据。</td>
</tr>
<tr>
	<td>select</td>
    <td>id</td>
    <td>选择节点。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>id</td>
    <td>取消选择节点。</td>
</tr>
<tr>
	<td>selectAll</td>
    <td>none</td>
    <td>选择所有节点。</td>
</tr>
<tr>
	<td>unselectAll</td>
    <td>none</td>
    <td>取消选择所有节点。</td>
</tr>
<tr>
	<td>collapse</td>
    <td>id</td>
    <td>折叠节点。</td>
</tr>
<tr>
	<td>expand</td>
    <td>id</td>
    <td>展开节点。</td>
</tr>
<tr>
	<td>collapseAll</td>
    <td>id</td>
    <td>折叠所有的节点。</td>
</tr>
<tr>
	<td>expandAll</td>
    <td>id</td>
    <td>展开所有的节点。</td>
</tr>
<tr>
	<td>expandTo</td>
    <td>id</td>
    <td>从根部展开一个指定的节点。</td>
</tr>
<tr>
	<td>toggle</td>
    <td>id</td>
    <td>切换节点的展开/折叠状态。</td>
</tr>
<tr>
	<td>append</td>
    <td>param</td>
    <td>追加一些子节点到一个父节点，'param' 参数包括下列属性：<br>
	parent：父节点的 id，如果没有分配，则追加为根节点。<br>
	data：数组，节点的数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// append some nodes to the selected row
var node = $('#tt').treegrid('getSelected');
$('#tt').treegrid('append',{
&nbsp;&nbsp;&nbsp;&nbsp;parent: node.id,  // the node has a 'id' value that defined through 'idField' property
&nbsp;&nbsp;&nbsp;&nbsp;data: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: '073',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'name73'
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
</tr>
<tr>
	<td>insert</td>
    <td>param</td>
    <td>在指定节点的前边或后边插入一个节点，'param' 参数包括下列属性：<br>
	before：前边插入的节点的 id 值。<br>
	after：后边插入的节点的 id 值。<br>
	data：新的节点数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// insert a new node before the selected node
var node = $('#tt').treegrid('getSelected');
if (node){
&nbsp;&nbsp;&nbsp;&nbsp;$('#tt').treegrid('insert', {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;before: node.id,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 38,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'name38'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;});
}
</pre>
	该方法自版本 1.3.1 起可用。 
	</td>
</tr>
<tr>
	<td>remove</td>
    <td>id</td>
    <td>移除节点和它的子节点。</td>
</tr>
<tr>
	<td>pop</td>
    <td>id</td>
    <td>弹出节点并在移除该节点后返回包含其子节点的节点数据。该方法自版本 1.3.1 起可用。</td>
</tr>
<tr>
	<td>refresh</td>
    <td>id</td>
    <td>刷新指定的节点。</td>
</tr>
<tr>
	<td>update</td>
    <td>param</td>
    <td>更新指定的节点。'param' 参数包括下列属性：<br>
	id：表示要被更新的节点的 id。<br>
	row：新的行数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#tt').treegrid('update',{
&nbsp;&nbsp;&nbsp;&nbsp;id: 2,
&nbsp;&nbsp;&nbsp;&nbsp;row: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'new name',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-save'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	该方法自版本 1.3.1 起可用。
	</td>
</tr>
<tr>
	<td>beginEdit</td>
    <td>id</td>
    <td>开始编辑节点。</td>
</tr>
<tr>
	<td>endEdit</td>
    <td>id</td>
    <td>结束编辑节点。</td>
</tr>
<tr>
	<td>cancelEdit</td>
    <td>id</td>
    <td>取消编辑节点。</td>
</tr>
<tr>
	<td>getEditors</td>
    <td>id</td>
    <td>获取指定行的编辑器。每个编辑器有下列属性：<br>
	actions：编辑器可以做的动作。<br>
	target：目标编辑器的 jQuery 对象。<br>
	field：字段名。<br>
	type：编辑器的类型。
	</td>
</tr>
<tr>
	<td>getEditor</td>
    <td>param</td>
    <td>获取指定的编辑器，param 参数包含两个属性：<br>
	id：行节点的 id。<br>
	field：字段名。
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th align="left">名称</th>
    <th align="left">参数</th>
    <th align="left">描述</th>
</tr>
<tr>
	<td>options</td>
    <td>none</td>
    <td>返回树形网格（treegrid）的选项（options）。</td>
</tr>
<tr>
	<td>resize</td>
    <td>options</td>
    <td>设置树形网格（treegrid）的尺寸， options 参数包含两个属性：<br>
	width：树形网格（treegrid）的新宽度。<br>
	height：树形网格（treegrid）的新高度。
	</td>
</tr>
<tr>
	<td>fixRowHeight</td>
    <td>id</td>
    <td>固定指定行的高度。</td>
</tr>
<tr>
	<td>loadData</td>
    <td>data</td>
    <td>加载树形网格（treegrid）的数据。</td>
</tr>
<tr>
	<td>load</td>
    <td>param</td>
    <td>加载并显示第一页。该方法自版本 1.3.4 起可用。<br>
	代码实例：
<pre style="color:#006600">
// load and send some request parameters
$('#tg').treegrid('load', {
&nbsp;&nbsp;&nbsp;&nbsp;q: 'abc',
&nbsp;&nbsp;&nbsp;&nbsp;name: 'name1'
});
</pre>
	</td>
</tr>
<tr>
	<td>reload</td>
    <td>id</td>
    <td>重新加载树形网格（treegrid）的数据。如果传递了 'id' 参数，则重新加载树的指定行，否则重新加载树的所有行。<br>
	代码实例：
<pre style="color:#006600">
$('#tt').treegrid('reload', 2);&nbsp;&nbsp;&nbsp;&nbsp;// reload the row which value is equals to 2
$('#tt').treegrid('reload');&nbsp;&nbsp;&nbsp;&nbsp;// reload the all rows
$('#tt').treegrid('reload', {id:2, q:'abc'});  // reload the specified row with 'q' parameter passing to server
</pre>
	</td>
</tr>
<tr>
	<td>reloadFooter</td>
    <td>footer</td>
    <td>重新加载底部数据。</td>
</tr>
<tr>
	<td>getData</td>
    <td>none</td>
    <td>获取加载的数据。</td>
</tr>
<tr>
	<td>getFooterRows</td>
    <td>none</td>
    <td>获取底部数据。</td>
</tr>
<tr>
	<td>getRoot</td>
    <td>none</td>
    <td>获取根节点，返回节点对象。</td>
</tr>
<tr>
	<td>getRoots</td>
    <td>none</td>
    <td>获取根节点，返回节点数组。</td>
</tr>
<tr>
	<td>getParent</td>
    <td>id</td>
    <td>获取父节点。</td>
</tr>
<tr>
	<td>getChildren</td>
    <td>id</td>
    <td>获取子节点。</td>
</tr>
<tr>
	<td>getSelected</td>
    <td>none</td>
    <td>获取选中的节点并返回它，如果没有选中节点则返回 null。</td>
</tr>
<tr>
	<td>getSelections</td>
    <td>none</td>
    <td>获取所有选中的节点。</td>
</tr>
<tr>
	<td>getLevel</td>
    <td>id</td>
    <td>获取指定节点的层级。</td>
</tr>
<tr>
	<td>find</td>
    <td>id</td>
    <td>找到指定节点并返回该节点数据。</td>
</tr>
<tr>
	<td>select</td>
    <td>id</td>
    <td>选择节点。</td>
</tr>
<tr>
	<td>unselect</td>
    <td>id</td>
    <td>取消选择节点。</td>
</tr>
<tr>
	<td>selectAll</td>
    <td>none</td>
    <td>选择所有节点。</td>
</tr>
<tr>
	<td>unselectAll</td>
    <td>none</td>
    <td>取消选择所有节点。</td>
</tr>
<tr>
	<td>collapse</td>
    <td>id</td>
    <td>折叠节点。</td>
</tr>
<tr>
	<td>expand</td>
    <td>id</td>
    <td>展开节点。</td>
</tr>
<tr>
	<td>collapseAll</td>
    <td>id</td>
    <td>折叠所有的节点。</td>
</tr>
<tr>
	<td>expandAll</td>
    <td>id</td>
    <td>展开所有的节点。</td>
</tr>
<tr>
	<td>expandTo</td>
    <td>id</td>
    <td>从根部展开一个指定的节点。</td>
</tr>
<tr>
	<td>toggle</td>
    <td>id</td>
    <td>切换节点的展开/折叠状态。</td>
</tr>
<tr>
	<td>append</td>
    <td>param</td>
    <td>追加一些子节点到一个父节点，'param' 参数包括下列属性：<br>
	parent：父节点的 id，如果没有分配，则追加为根节点。<br>
	data：数组，节点的数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// append some nodes to the selected row
var node = $('#tt').treegrid('getSelected');
$('#tt').treegrid('append',{
&nbsp;&nbsp;&nbsp;&nbsp;parent: node.id,  // the node has a 'id' value that defined through 'idField' property
&nbsp;&nbsp;&nbsp;&nbsp;data: [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: '073',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'name73'
&nbsp;&nbsp;&nbsp;&nbsp;}]
});
</pre>
	</td>
</tr>
<tr>
	<td>insert</td>
    <td>param</td>
    <td>在指定节点的前边或后边插入一个节点，'param' 参数包括下列属性：<br>
	before：前边插入的节点的 id 值。<br>
	after：后边插入的节点的 id 值。<br>
	data：新的节点数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
// insert a new node before the selected node
var node = $('#tt').treegrid('getSelected');
if (node){
&nbsp;&nbsp;&nbsp;&nbsp;$('#tt').treegrid('insert', {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;before: node.id,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id: 38,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'name38'
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;});
}
</pre>
	该方法自版本 1.3.1 起可用。 
	</td>
</tr>
<tr>
	<td>remove</td>
    <td>id</td>
    <td>移除节点和它的子节点。</td>
</tr>
<tr>
	<td>pop</td>
    <td>id</td>
    <td>弹出节点并在移除该节点后返回包含其子节点的节点数据。该方法自版本 1.3.1 起可用。</td>
</tr>
<tr>
	<td>refresh</td>
    <td>id</td>
    <td>刷新指定的节点。</td>
</tr>
<tr>
	<td>update</td>
    <td>param</td>
    <td>更新指定的节点。'param' 参数包括下列属性：<br>
	id：表示要被更新的节点的 id。<br>
	row：新的行数据。<br>
	<br>
	代码实例：
<pre style="color:#006600">
$('#tt').treegrid('update',{
&nbsp;&nbsp;&nbsp;&nbsp;id: 2,
&nbsp;&nbsp;&nbsp;&nbsp;row: {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: 'new name',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iconCls: 'icon-save'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
	该方法自版本 1.3.1 起可用。
	</td>
</tr>
<tr>
	<td>beginEdit</td>
    <td>id</td>
    <td>开始编辑节点。</td>
</tr>
<tr>
	<td>endEdit</td>
    <td>id</td>
    <td>结束编辑节点。</td>
</tr>
<tr>
	<td>cancelEdit</td>
    <td>id</td>
    <td>取消编辑节点。</td>
</tr>
<tr>
	<td>getEditors</td>
    <td>id</td>
    <td>获取指定行的编辑器。每个编辑器有下列属性：<br>
	actions：编辑器可以做的动作。<br>
	target：目标编辑器的 jQuery 对象。<br>
	field：字段名。<br>
	type：编辑器的类型。
	</td>
</tr>
<tr>
	<td>getEditor</td>
    <td>param</td>
    <td>获取指定的编辑器，param 参数包含两个属性：<br>
	id：行节点的 id。<br>
	field：字段名。
	</td>
</tr>
</table></div>') where ID=5688;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>类型</strong></th>
<th><strong>描述</strong></th>
<th><strong>默认值</strong></th>
</tr>
<tr>
<td>destroyMsg</td>
<td>object</td>
<td>当销毁一行时要显示的确认对话框消息。</td>
<td>
<pre style="color:#006600">
destroyMsg:{
&nbsp;&nbsp;&nbsp;&nbsp;norecord:{&nbsp;&nbsp;&nbsp;&nbsp;// when no record is selected
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title:'Warning',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msg:'No record is selected.'
&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;confirm:{&nbsp;&nbsp;&nbsp;&nbsp;// when select a row
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title:'Confirm',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msg:'Are you sure you want to delete?'
&nbsp;&nbsp;&nbsp;&nbsp;}
}
</pre>
</td>
</tr>
<tr>
<td>autoSave</td>
<td>boolean</td>
<td>设置为 true，则当点击数据网格外部时自动保存编辑行。</td>
<td>false</td>
</tr>
<tr>
<td>url</td>
<td>string</td>
<td>一个 URL，从服务器检索数据。</td>
<td>null</td>
</tr>
<tr>
<td>saveUrl</td>
<td>string</td>
<td>一个 URL，向服务器保存数据，并返回添加的行。</td>
<td>null</td>
</tr>
<tr>
<td>updateUrl</td>
<td>string</td>
<td>一个 URL，向服务器更新数据，并返回更新的行。</td>
<td>null</td>
</tr>
<tr>
<td>destroyUrl</td>
<td>string</td>
<td>一个 URL，向服务器传送 'id' 参数来销毁该行。</td>
<td>null</td>
</tr>
<tr>
<td>tree</td>
<td>selector</td>
<td>显示对应的树组件的树选择器。</td>
<td>null</td>
</tr>
<tr>
<td>treeUrl</td>
<td>string</td>
<td>一个 URL，检索树的数据。</td>
<td>null</td>
</tr>
<tr>
<td>treeDndUrl</td>
<td>string</td>
<td>一个 URL，处理拖放操作。</td>
<td>null</td>
</tr>
<tr>
<td>treeTextField</td>
<td>string</td>
<td>定义树的文本字段名称。</td>
<td>name</td>
</tr>
<tr>
<td>treeParentField</td>
<td>string</td>
<td>定义树的父节点字段名称。</td>
<td>parentId</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>类型</strong></th>
<th><strong>描述</strong></th>
<th><strong>默认值</strong></th>
</tr>
<tr>
<td>destroyMsg</td>
<td>object</td>
<td>当销毁一行时要显示的确认对话框消息。</td>
<td>
<pre style="color:#006600">
destroyMsg:{
&nbsp;&nbsp;&nbsp;&nbsp;norecord:{&nbsp;&nbsp;&nbsp;&nbsp;// when no record is selected
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title:'Warning',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msg:'No record is selected.'
&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;confirm:{&nbsp;&nbsp;&nbsp;&nbsp;// when select a row
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title:'Confirm',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msg:'Are you sure you want to delete?'
&nbsp;&nbsp;&nbsp;&nbsp;}
}
</pre>
</td>
</tr>
<tr>
<td>autoSave</td>
<td>boolean</td>
<td>设置为 true，则当点击数据网格外部时自动保存编辑行。</td>
<td>false</td>
</tr>
<tr>
<td>url</td>
<td>string</td>
<td>一个 URL，从服务器检索数据。</td>
<td>null</td>
</tr>
<tr>
<td>saveUrl</td>
<td>string</td>
<td>一个 URL，向服务器保存数据，并返回添加的行。</td>
<td>null</td>
</tr>
<tr>
<td>updateUrl</td>
<td>string</td>
<td>一个 URL，向服务器更新数据，并返回更新的行。</td>
<td>null</td>
</tr>
<tr>
<td>destroyUrl</td>
<td>string</td>
<td>一个 URL，向服务器传送 'id' 参数来销毁该行。</td>
<td>null</td>
</tr>
<tr>
<td>tree</td>
<td>selector</td>
<td>显示对应的树组件的树选择器。</td>
<td>null</td>
</tr>
<tr>
<td>treeUrl</td>
<td>string</td>
<td>一个 URL，检索树的数据。</td>
<td>null</td>
</tr>
<tr>
<td>treeDndUrl</td>
<td>string</td>
<td>一个 URL，处理拖放操作。</td>
<td>null</td>
</tr>
<tr>
<td>treeTextField</td>
<td>string</td>
<td>定义树的文本字段名称。</td>
<td>name</td>
</tr>
<tr>
<td>treeParentField</td>
<td>string</td>
<td>定义树的父节点字段名称。</td>
<td>parentId</td>
</tr>
</table></div>') where ID=5693;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>参数</strong></th>
<th><strong>描述</strong></th>
</tr>
<tr>
<td>onAdd</td>
<td>index,row</td>
<td>当添加一个新行时触发。</td>
</tr>
<tr>
<td>onEdit</td>
<td>index,row</td>
<td>当编辑一行时触发。</td>
</tr>
<tr>
<td>onBeforeSave</td>
<td>index</td>
<td>一行被保存之前触发，返回 false 则取消保存动作。</td>
</tr>
<tr>
<td>onSave</td>
<td>index,row</td>
<td>当保存一行时触发。</td>
</tr>
<tr>
<td>onDestroy</td>
<td>index,row</td>
<td>当销毁一行时触发。</td>
</tr>
<tr>
<td>onError</td>
<td>index,row</td>
<td>
当发生服务器错误时触发。<br/>
服务器应返回一个 'isError' 属性设置为 true 的行，表示发生错误。
<p>代码实例：</p>
<pre style="color:#006600">
//server side code
echo json_encode(array(
&nbsp;&nbsp;&nbsp;&nbsp;'isError' =&gt; true,
&nbsp;&nbsp;&nbsp;&nbsp;'msg' =&gt; 'error message.'
));
</pre>
<pre style="color:#006600">
//client side code
$('#dg').edatagrid({
&nbsp;&nbsp;&nbsp;&nbsp;onError: function(index,row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(row.msg);
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>参数</strong></th>
<th><strong>描述</strong></th>
</tr>
<tr>
<td>onAdd</td>
<td>index,row</td>
<td>当添加一个新行时触发。</td>
</tr>
<tr>
<td>onEdit</td>
<td>index,row</td>
<td>当编辑一行时触发。</td>
</tr>
<tr>
<td>onBeforeSave</td>
<td>index</td>
<td>一行被保存之前触发，返回 false 则取消保存动作。</td>
</tr>
<tr>
<td>onSave</td>
<td>index,row</td>
<td>当保存一行时触发。</td>
</tr>
<tr>
<td>onDestroy</td>
<td>index,row</td>
<td>当销毁一行时触发。</td>
</tr>
<tr>
<td>onError</td>
<td>index,row</td>
<td>
当发生服务器错误时触发。<br/>
服务器应返回一个 'isError' 属性设置为 true 的行，表示发生错误。
<p>代码实例：</p>
<pre style="color:#006600">
//server side code
echo json_encode(array(
&nbsp;&nbsp;&nbsp;&nbsp;'isError' =&gt; true,
&nbsp;&nbsp;&nbsp;&nbsp;'msg' =&gt; 'error message.'
));
</pre>
<pre style="color:#006600">
//client side code
$('#dg').edatagrid({
&nbsp;&nbsp;&nbsp;&nbsp;onError: function(index,row){
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert(row.msg);
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
</td>
</tr>
</table></div>') where ID=5693;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>参数</strong></th>
<th><strong>描述</strong></th>
</tr>
<tr>
<td>options</td>
<td>none</td>
<td>返回选项（options）对象。</td>
</tr>
<tr>
<td>enableEditing</td>
<td>none</td>
<td>启用数据网格（datagrid）编辑。</td>
</tr>
<tr>
<td>disableEditing</td>
<td>none</td>
<td>禁用数据网格（datagrid）编辑。</td>
</tr>
<tr>
<td>editRow</td>
<td>index</td>
<td>编辑指定的行。</td>
</tr>
<tr>
<td>addRow</td>
<td>index</td>
<td>
向指定的行索引添加一个新行。<br/>
如果 index 参数未指定，则向最后的位置追加一个新行。
<p>代码实例：</p>
<pre style="color:#006600">
// append an empty row
$('#dg').edatagrid('addRow');

// append an empty row as first row
$('#dg').edatagrid('addRow',0);

// insert a row with default values
$('#dg').edatagrid('addRow',{
&nbsp;&nbsp;&nbsp;&nbsp;index: 2,
&nbsp;&nbsp;&nbsp;&nbsp;row:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name:'name1',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;addr:'addr1'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
</td>
</tr>
<tr>
<td>saveRow</td>
<td>none</td>
<td>保存编辑行，发布到服务器。</td>
</tr>
<tr>
<td>cancelRow</td>
<td>none</td>
<td>取消编辑行。</td>
</tr>
<tr>
<td>destroyRow</td>
<td>index</td>
<td>
销毁当前选中的行。<br/>
如果 index 参数未指定，则销毁所有选中的行。
<p>代码实例：</p>
<pre style="color:#006600">
// destroy all the selected rows
$('#dg').edatagrid('destroyRow');

// destroy the first row
$('#dg').edatagrid('destroyRow', 0);

// destroy the specified rows
$('#dg').edatagrid('destroyRow', [3,4,5]);
</pre>
</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>参数</strong></th>
<th><strong>描述</strong></th>
</tr>
<tr>
<td>options</td>
<td>none</td>
<td>返回选项（options）对象。</td>
</tr>
<tr>
<td>enableEditing</td>
<td>none</td>
<td>启用数据网格（datagrid）编辑。</td>
</tr>
<tr>
<td>disableEditing</td>
<td>none</td>
<td>禁用数据网格（datagrid）编辑。</td>
</tr>
<tr>
<td>editRow</td>
<td>index</td>
<td>编辑指定的行。</td>
</tr>
<tr>
<td>addRow</td>
<td>index</td>
<td>
向指定的行索引添加一个新行。<br/>
如果 index 参数未指定，则向最后的位置追加一个新行。
<p>代码实例：</p>
<pre style="color:#006600">
// append an empty row
$('#dg').edatagrid('addRow');

// append an empty row as first row
$('#dg').edatagrid('addRow',0);

// insert a row with default values
$('#dg').edatagrid('addRow',{
&nbsp;&nbsp;&nbsp;&nbsp;index: 2,
&nbsp;&nbsp;&nbsp;&nbsp;row:{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name:'name1',
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;addr:'addr1'
&nbsp;&nbsp;&nbsp;&nbsp;}
});
</pre>
</td>
</tr>
<tr>
<td>saveRow</td>
<td>none</td>
<td>保存编辑行，发布到服务器。</td>
</tr>
<tr>
<td>cancelRow</td>
<td>none</td>
<td>取消编辑行。</td>
</tr>
<tr>
<td>destroyRow</td>
<td>index</td>
<td>
销毁当前选中的行。<br/>
如果 index 参数未指定，则销毁所有选中的行。
<p>代码实例：</p>
<pre style="color:#006600">
// destroy all the selected rows
$('#dg').edatagrid('destroyRow');

// destroy the first row
$('#dg').edatagrid('destroyRow', 0);

// destroy the specified rows
$('#dg').edatagrid('destroyRow', [3,4,5]);
</pre>
</td>
</tr>
</table></div>') where ID=5693;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>类型</strong></th>
<th><strong>描述</strong></th>
<th><strong>默认值</strong></th>
</tr>
<tr>
<td>filterMenuIconCls</td>
<td>string</td>
<td>过滤菜单项（指示要使用的项目）的图标 class。</td>
<td>icon-ok</td>
</tr>
<tr>
<td>filterBtnIconCls</td>
<td>string</td>
<td>过滤按钮的图标 class。</td>
<td>icon-filter</td>
</tr>
<tr>
<td>filterBtnPosition</td>
<td>string</td>
<td>过滤按钮的位置。可能的值是 'left' 和 'right'。</td>
<td>right</td>
</tr>
<tr>
<td>filterPosition</td>
<td>string</td>
<td>过滤栏相对于列的位置。可能的值是 'top' 和 'bottom'。</td>
<td>bottom</td>
</tr>
<tr>
<td>remoteFilter</td>
<td>boolean</td>
<td>
设置为 true 则执行远程过滤。<br/>
当启用时，'filterRules' 参数将发送到远程服务器。<br/>
'filterRules' 参数的值由 'filterStringify' 函数获得。
</td>
<td>false</td>
</tr>
<tr>
<td>filterDelay</td>
<td>number</td>
<td>从 'text' 过滤组件中最后一个键输入事件起，延迟执行过滤的时间。</td>
<td>400</td>
</tr>
<tr>
<td>filterRules</td>
<td>array</td>
<td>过滤规则的定义。每个规则包含 'field'、'op' 和 'value' 属性。</td>
<td>[]</td>
</tr>
<tr>
<td>filterStringify</td>
<td>function</td>
<td>把过滤规则字符串化的函数。</td>
<td>
<pre style="color:#006600">
function(data){
&nbsp;&nbsp;&nbsp;&nbsp;return JSON.stringify(data);
}
</pre>
</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>类型</strong></th>
<th><strong>描述</strong></th>
<th><strong>默认值</strong></th>
</tr>
<tr>
<td>filterMenuIconCls</td>
<td>string</td>
<td>过滤菜单项（指示要使用的项目）的图标 class。</td>
<td>icon-ok</td>
</tr>
<tr>
<td>filterBtnIconCls</td>
<td>string</td>
<td>过滤按钮的图标 class。</td>
<td>icon-filter</td>
</tr>
<tr>
<td>filterBtnPosition</td>
<td>string</td>
<td>过滤按钮的位置。可能的值是 'left' 和 'right'。</td>
<td>right</td>
</tr>
<tr>
<td>filterPosition</td>
<td>string</td>
<td>过滤栏相对于列的位置。可能的值是 'top' 和 'bottom'。</td>
<td>bottom</td>
</tr>
<tr>
<td>remoteFilter</td>
<td>boolean</td>
<td>
设置为 true 则执行远程过滤。<br/>
当启用时，'filterRules' 参数将发送到远程服务器。<br/>
'filterRules' 参数的值由 'filterStringify' 函数获得。
</td>
<td>false</td>
</tr>
<tr>
<td>filterDelay</td>
<td>number</td>
<td>从 'text' 过滤组件中最后一个键输入事件起，延迟执行过滤的时间。</td>
<td>400</td>
</tr>
<tr>
<td>filterRules</td>
<td>array</td>
<td>过滤规则的定义。每个规则包含 'field'、'op' 和 'value' 属性。</td>
<td>[]</td>
</tr>
<tr>
<td>filterStringify</td>
<td>function</td>
<td>把过滤规则字符串化的函数。</td>
<td>
<pre style="color:#006600">
function(data){
&nbsp;&nbsp;&nbsp;&nbsp;return JSON.stringify(data);
}
</pre>
</td>
</tr>
</table></div>') where ID=5695;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>参数</strong></th>
<th><strong>描述</strong></th>
</tr>
<tr>
<td>enableFilter</td>
<td>filters</td>
<td>
创建并启用过滤功能。<br/>
'filters' 参数是一个过滤配置的数组。<br/>
每一项包含下列属性：<br>
1) field：需要定义规则的域。<br>
2) type：过滤类型，可能的值：label、text、textarea、checkbox、numberbox、validatebox、datebox、combobox、combotree。<br>
3) options：过滤类型的选项。<br>
4) op：过滤操作，可能的值：contains、equal、notequal、beginwith、endwith、less、lessorequal、greater、greaterorequal。
<p>代码实例：</p>
<pre style="color:#006600">
$('#dg').datagrid('enableFilter');
$('#dg').datagrid('enableFilter', [{
&nbsp;&nbsp;&nbsp;&nbsp;field:'listprice',
&nbsp;&nbsp;&nbsp;&nbsp;type:'numberbox',
&nbsp;&nbsp;&nbsp;&nbsp;options:{precision:1},
&nbsp;&nbsp;&nbsp;&nbsp;op:['equal','notequal','less','greater']
}]);
</pre>
</td>
</tr>
<tr>
<td>addFilterRule</td>
<td>param</td>
<td>
添加一个过滤规则。
<pre style="color:#006600">
$('#dg').datagrid('addFilterRule', {
&nbsp;&nbsp;&nbsp;&nbsp;field: 'desp',
&nbsp;&nbsp;&nbsp;&nbsp;op: 'contains',
&nbsp;&nbsp;&nbsp;&nbsp;value: 'easyui'
});
</pre>
</td>
</tr>
<tr>
<td>removeFilterRule</td>
<td>field</td>
<td>
移除过滤规则。<br/>
如果 'field' 参数未指定，移除所有的过滤规则。
</td>
</tr>
<tr>
<td>doFilter</td>
<td>none</td>
<td>
基于过滤规则执行过滤。
</td>
</tr>
<tr>
<td>getFilterComponent</td>
<td>field</td>
<td>在指定的域上获取过滤组件。</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>参数</strong></th>
<th><strong>描述</strong></th>
</tr>
<tr>
<td>enableFilter</td>
<td>filters</td>
<td>
创建并启用过滤功能。<br/>
'filters' 参数是一个过滤配置的数组。<br/>
每一项包含下列属性：<br>
1) field：需要定义规则的域。<br>
2) type：过滤类型，可能的值：label、text、textarea、checkbox、numberbox、validatebox、datebox、combobox、combotree。<br>
3) options：过滤类型的选项。<br>
4) op：过滤操作，可能的值：contains、equal、notequal、beginwith、endwith、less、lessorequal、greater、greaterorequal。
<p>代码实例：</p>
<pre style="color:#006600">
$('#dg').datagrid('enableFilter');
$('#dg').datagrid('enableFilter', [{
&nbsp;&nbsp;&nbsp;&nbsp;field:'listprice',
&nbsp;&nbsp;&nbsp;&nbsp;type:'numberbox',
&nbsp;&nbsp;&nbsp;&nbsp;options:{precision:1},
&nbsp;&nbsp;&nbsp;&nbsp;op:['equal','notequal','less','greater']
}]);
</pre>
</td>
</tr>
<tr>
<td>addFilterRule</td>
<td>param</td>
<td>
添加一个过滤规则。
<pre style="color:#006600">
$('#dg').datagrid('addFilterRule', {
&nbsp;&nbsp;&nbsp;&nbsp;field: 'desp',
&nbsp;&nbsp;&nbsp;&nbsp;op: 'contains',
&nbsp;&nbsp;&nbsp;&nbsp;value: 'easyui'
});
</pre>
</td>
</tr>
<tr>
<td>removeFilterRule</td>
<td>field</td>
<td>
移除过滤规则。<br/>
如果 'field' 参数未指定，移除所有的过滤规则。
</td>
</tr>
<tr>
<td>doFilter</td>
<td>none</td>
<td>
基于过滤规则执行过滤。
</td>
</tr>
<tr>
<td>getFilterComponent</td>
<td>field</td>
<td>在指定的域上获取过滤组件。</td>
</tr>
</table></div>') where ID=5695;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>参数</strong></th>
<th><strong>描述</strong></th>
</tr>
<tr>
<td>enableDnd</td>
<td>index</td>
<td>
启用行的拖拽与放置。<br/>
'index' 参数指示要被拖拽与放置的行。<br/>
如果该参数未指定，则拖拽与放置所有行。
<p>代码实例：</p>
<pre style="color:#006600">
$('#dg').datagrid('enableDnd', 1);&nbsp;&nbsp;&nbsp;&nbsp;// enable dragging on second row
$('#dg').datagrid('enableDnd');&nbsp;&nbsp;&nbsp;&nbsp;// enable dragging all rows
</pre>
</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
<th><strong>名称</strong></th>
<th><strong>参数</strong></th>
<th><strong>描述</strong></th>
</tr>
<tr>
<td>enableDnd</td>
<td>index</td>
<td>
启用行的拖拽与放置。<br/>
'index' 参数指示要被拖拽与放置的行。<br/>
如果该参数未指定，则拖拽与放置所有行。
<p>代码实例：</p>
<pre style="color:#006600">
$('#dg').datagrid('enableDnd', 1);&nbsp;&nbsp;&nbsp;&nbsp;// enable dragging on second row
$('#dg').datagrid('enableDnd');&nbsp;&nbsp;&nbsp;&nbsp;// enable dragging all rows
</pre>
</td>
</tr>
</table></div>') where ID=5696;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody>
<tr>
<th width="10%">序号</th>
<th>类描述</th>
</tr>
<tr>
<td>1</td>
<td><strong>AbstractCollection </strong><br>

实现了大部分的集合接口。</td>
</tr>
<tr>
<td>2</td>
<td><strong>AbstractList </strong><br>

继承于AbstractCollection 并且实现了大部分List接口。</td>
</tr>
<tr>
<td>3</td>
<td><strong>AbstractSequentialList </strong><br>

继承于 AbstractList ，提供了对数据元素的链式访问而不是随机访问。</td>
</tr>
<tr>
<td>4</td>
<td>LinkedList<br>
<p>
该类实现了List接口，允许有null（空）元素。主要用于创建链表数据结构，该类没有同步方法，如果多个线程同时访问一个List，则必须自己实现访问同步，解决方法就是在创建List时候构造一个同步的List。例如：  
</p>
<pre>
Listlist=Collections.synchronizedList(newLinkedList(...));
</pre>
<p>LinkedList 查找效率低。</p>
</td>
</tr>
<tr>
<td>5</td>
<td>ArrayList<br>
<p>
该类也是实现了List的接口，实现了可变大小的数组，随机访问和遍历元素时，提供更好的性能。该类也是非同步的,在多线程的情况下不要使用。ArrayList 增长当前长度的50%，插入删除效率低。
</p></td>
</tr>
<tr>
<td>6</td>
<td><strong>AbstractSet </strong><br>

继承于AbstractCollection 并且实现了大部分Set接口。</td>
</tr>
<tr>
<td>7</td>
<td>HashSet<br><p>
该类实现了Set接口，不允许出现重复元素，不保证集合中元素的顺序，允许包含值为null的元素，但最多只能一个。</p></td>
</tr>
<tr>
<td>8</td>
<td>LinkedHashSet<br>

具有可预知迭代顺序的 <tt>Set</tt> 接口的哈希表和链接列表实现。</td>
</tr>
<tr>
<td>9</td>
<td>TreeSet<br>
<p>该类实现了Set接口，可以实现排序等功能。</p></td>
</tr>
<tr>
<td>10</td>
<td><strong>AbstractMap </strong><br>

实现了大部分的Map接口。</td>
</tr>
<tr>
<td>11</td>
<td>HashMap
<br>
HashMap 是一个散列表，它存储的内容是键值对(key-value)映射。<br>

该类实现了Map接口，根据键的HashCode值存储数据，具有很快的访问速度，最多允许一条记录的键为null，不支持线程同步。
</td>
</tr>
<tr>
<td>12</td>
<td>TreeMap
<br>
继承了AbstractMap，并且使用一颗树。</td>
</tr>
<tr>
<td>13</td>
<td>WeakHashMap
<br>
继承AbstractMap类，使用弱密钥的哈希表。</td>
</tr>
<tr>
<td>14</td>
<td>LinkedHashMap
<br>
继承于HashMap，使用元素的自然顺序对元素进行排序.</td>
</tr>
<tr>
<td>15</td>
<td>IdentityHashMap
<br>
继承AbstractMap类，比较文档时使用引用相等。</td>
</tr>
</tbody>
</table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody>
<tr>
<th width="10%">序号</th>
<th>类描述</th>
</tr>
<tr>
<td>1</td>
<td><strong>AbstractCollection </strong><br>

实现了大部分的集合接口。</td>
</tr>
<tr>
<td>2</td>
<td><strong>AbstractList </strong><br>

继承于AbstractCollection 并且实现了大部分List接口。</td>
</tr>
<tr>
<td>3</td>
<td><strong>AbstractSequentialList </strong><br>

继承于 AbstractList ，提供了对数据元素的链式访问而不是随机访问。</td>
</tr>
<tr>
<td>4</td>
<td>LinkedList<br>
<p>
该类实现了List接口，允许有null（空）元素。主要用于创建链表数据结构，该类没有同步方法，如果多个线程同时访问一个List，则必须自己实现访问同步，解决方法就是在创建List时候构造一个同步的List。例如：  
</p>
<pre>
Listlist=Collections.synchronizedList(newLinkedList(...));
</pre>
<p>LinkedList 查找效率低。</p>
</td>
</tr>
<tr>
<td>5</td>
<td>ArrayList<br>
<p>
该类也是实现了List的接口，实现了可变大小的数组，随机访问和遍历元素时，提供更好的性能。该类也是非同步的,在多线程的情况下不要使用。ArrayList 增长当前长度的50%，插入删除效率低。
</p></td>
</tr>
<tr>
<td>6</td>
<td><strong>AbstractSet </strong><br>

继承于AbstractCollection 并且实现了大部分Set接口。</td>
</tr>
<tr>
<td>7</td>
<td>HashSet<br><p>
该类实现了Set接口，不允许出现重复元素，不保证集合中元素的顺序，允许包含值为null的元素，但最多只能一个。</p></td>
</tr>
<tr>
<td>8</td>
<td>LinkedHashSet<br>

具有可预知迭代顺序的 <tt>Set</tt> 接口的哈希表和链接列表实现。</td>
</tr>
<tr>
<td>9</td>
<td>TreeSet<br>
<p>该类实现了Set接口，可以实现排序等功能。</p></td>
</tr>
<tr>
<td>10</td>
<td><strong>AbstractMap </strong><br>

实现了大部分的Map接口。</td>
</tr>
<tr>
<td>11</td>
<td>HashMap
<br>
HashMap 是一个散列表，它存储的内容是键值对(key-value)映射。<br>

该类实现了Map接口，根据键的HashCode值存储数据，具有很快的访问速度，最多允许一条记录的键为null，不支持线程同步。
</td>
</tr>
<tr>
<td>12</td>
<td>TreeMap
<br>
继承了AbstractMap，并且使用一颗树。</td>
</tr>
<tr>
<td>13</td>
<td>WeakHashMap
<br>
继承AbstractMap类，使用弱密钥的哈希表。</td>
</tr>
<tr>
<td>14</td>
<td>LinkedHashMap
<br>
继承于HashMap，使用元素的自然顺序对元素进行排序.</td>
</tr>
<tr>
<td>15</td>
<td>IdentityHashMap
<br>
继承AbstractMap类，比较文档时使用引用相等。</td>
</tr>
</tbody>
</table></div>') where ID=5753;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-addClasses">
	<td>addClasses</td>
    <td>Boolean</td>
	<td>如果设置为 <code>false</code>，将阻止 <code>ui-draggable</code> class 被添加。当在数百个元素上调用 <code>.draggable()</code> 时，这么设置有利于性能优化。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>addClasses</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ addClasses: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>addClasses</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var addClasses = $( ".selector" ).draggable( "option", "addClasses" );
 
// setter
$( ".selector" ).draggable( "option", "addClasses", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-appendTo">
	<td>appendTo</td>
    <td>jQuery 或 Element 或 Selector 或 String</td>
	<td>当拖拽时，draggable 助手（helper）要追加到哪一个元素。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>jQuery</strong>：一个 jQuery 对象，包含助手（helper）要追加到的元素。</li>
	<li><strong>Element</strong>：要追加助手（helper）的元素。</li>
	<li><strong>Selector</strong>：一个选择器，指定哪一个元素要追加助手（helper）。</li>
	<li><strong>String</strong>：字符串 <code>"parent"</code> 将促使助手（helper）成为 draggable 的同级。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>appendTo</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ appendTo: "body" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>appendTo</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var appendTo = $( ".selector" ).draggable( "option", "appendTo" );
 
// setter
$( ".selector" ).draggable( "option", "appendTo", "body" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"parent"</td>
</tr>
<tr id="option-axis">
	<td>axis</td>
    <td>String</td>
	<td>约束在水平轴 (x) 或垂直轴 (y) 上拖拽。可能的值：<code>"x"</code>, <code>"y"</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>axis</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ axis: "x" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>axis</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var axis = $( ".selector" ).draggable( "option", "axis" );
 
// setter
$( ".selector" ).draggable( "option", "axis", "x" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
    <td>Selector</td>
	<td>防止从指定的元素上开始拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ cancel: ".title" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).draggable( "option", "cancel" );
 
// setter
$( ".selector" ).draggable( "option", "cancel", ".title" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-connectToSortable">
	<td>connectToSortable</td>
    <td>Selector</td>
	<td>允许 draggable 放置在指定的 sortable 上。如果使用了该选项，一个 draggable 可被放置在一个 sortable 列表上，然后成为列表的一部分。注意：<code>helper</code> 选项必须设置为 <code>"clone"</code>，以便更好地工作。必须包含 <a href="/jqueryui/api-sortable.html">可排序小部件（Sortable Widget）</a>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>connectToSortable</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ connectToSortable: "#my-sortable" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>connectToSortable</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var connectToSortable = $( ".selector" ).draggable( "option", "connectToSortable" );
 
// setter
$( ".selector" ).draggable( "option", "connectToSortable", "#my-sortable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-containment">
	<td>containment</td>
    <td>Selector 或 Element 或 String 或 Array</td>
	<td>约束在指定元素或区域的边界内拖拽。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Selector</strong>：可拖拽元素将被包含在 selector 第一个元素的边界内。如果未找到元素，则不设置 containment。</li>
	<li><strong>Element</strong>：可拖拽元素将被包含在元素的边界。</li>
	<li><strong>String</strong>：可能的值：<code>"parent"</code>、<code>"document"</code>、<code>"window"</code>。</li>
	<li><strong>Array</strong>：一个数组， 以形式 <code>[ x1, y1, x2, y2 ]</code> 定义元素的边界。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>containment</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ containment: "parent" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>containment</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var containment = $( ".selector" ).draggable( "option", "containment" );
 
// setter
$( ".selector" ).draggable( "option", "containment", "parent" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cursor">
	<td>cursor</td>
    <td>String</td>
	<td>拖拽操作期间的 CSS 光标。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cursor</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ cursor: "crosshair" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cursor</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cursor = $( ".selector" ).draggable( "option", "cursor" );
 
// setter
$( ".selector" ).draggable( "option", "cursor", "crosshair" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"auto"</td>
</tr>
<tr id="option-cursorAt">
	<td>cursorAt</td>
    <td>Object</td>
	<td>设置拖拽助手（helper）相对于鼠标光标的偏移。坐标可通过一个或两个键的组合成一个哈希给出：<code>{ top, left, right, bottom }</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cursorAt</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ cursorAt: { left: 5 } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cursorAt</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cursorAt = $( ".selector" ).draggable( "option", "cursorAt" );
 
// setter
$( ".selector" ).draggable( "option", "cursorAt", { left: 5 } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Number</td>
	<td>鼠标按下后直到拖拽开始为止的时间，以毫秒计。该选项可以防止点击在某个元素上时不必要的拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ delay: 300 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).draggable( "option", "delay" );
 
// setter
$( ".selector" ).draggable( "option", "delay", 300 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 draggable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).draggable( "option", "disabled" );
 
// setter
$( ".selector" ).draggable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
    <td>Number</td>
	<td>鼠标按下后拖拽开始前必须移动的距离，以像素计。该选项可以防止点击在某个元素上时不必要的拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ distance: 10 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).draggable( "option", "distance" );
 
// setter
$( ".selector" ).draggable( "option", "distance", 10 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
<tr id="option-grid">
	<td>grid</td>
    <td>Array</td>
	<td>对齐拖拽助手（helper）到网格，每个 x 和 y 像素。数组形式必须是 <code>[ x, y ]</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>grid</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ grid: [ 50, 20 ] });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>grid</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var grid = $( ".selector" ).draggable( "option", "grid" );
 
// setter
$( ".selector" ).draggable( "option", "grid", [ 50, 20 ] );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-handle">
	<td>handle</td>
    <td>Selector 或 Element</td>
	<td>如果指定了该选项，则限制开始拖拽，除非鼠标在指定的元素上按下。只有可拖拽（draggable）元素的后代元素才允许被拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>handle</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ handle: "h2" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>handle</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var handle = $( ".selector" ).draggable( "option", "handle" );
 
// setter
$( ".selector" ).draggable( "option", "handle", "h2" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-helper">
	<td>helper</td>
    <td>String 或 Function()</td>
	<td>允许一个 helper 元素用于拖拽显示。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>String</strong>：如果设置为 <code>"clone"</code>，元素将被克隆，且克隆将被拖拽。</li>
	<li><strong>Function</strong>：一个函数，将返回拖拽时要使用的 DOMElement。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>helper</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ helper: "clone" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>helper</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var helper = $( ".selector" ).draggable( "option", "helper" );
 
// setter
$( ".selector" ).draggable( "option", "helper", "clone" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"original"</td>
</tr>
<tr id="option-iframeFix">
	<td>iframeFix</td>
    <td>Boolean 或 Selector</td>
	<td>防止拖拽期间 iframes 捕捉鼠标移动（mousemove ）事件。在与 <code>cursorAt</code> 选项结合使用时，或鼠标光标未覆盖在助手（helper）上时，非常有用。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>true</code> 时，透明遮罩将被放置在页面上所有 iframes 上。</li>
	<li><strong>Selector</strong>：匹配 selector 的任意 iframes 将被透明遮罩覆盖。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>iframeFix</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ iframeFix: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>iframeFix</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var iframeFix = $( ".selector" ).draggable( "option", "iframeFix" );
 
// setter
$( ".selector" ).draggable( "option", "iframeFix", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-opacity">
	<td>opacity</td>
    <td>Number</td>
	<td>当被拖拽时助手（helper）的不透明度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>opacity</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ opacity: 0.35 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>opacity</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var opacity = $( ".selector" ).draggable( "option", "opacity" );
 
// setter
$( ".selector" ).draggable( "option", "opacity", 0.35 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-refreshPositions">
	<td>refreshPositions</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，在每次鼠标移动（mousemove）时都会计算所有可放置的位置。注意：这解决了高度动态的问题，但是明显降低了性能。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>refreshPositions</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ refreshPositions: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>refreshPositions</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var refreshPositions = $( ".selector" ).draggable( "option", "refreshPositions" );
 
// setter
$( ".selector" ).draggable( "option", "refreshPositions", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-revert">
	<td>revert</td>
    <td>Boolean 或 String 或 Function()</td>
	<td>当拖拽停止时，元素是否还原到它的开始位置。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：如果设置为 <code>true</code>，元素总会还原。</li>
	<li><strong>String</strong>：如果设置为 <code>"invalid"</code>，还原仅在 draggable 未放置在 droppable 上时发生，如果设置为 <code>"valid"</code> 则相反。</li>
	<li><strong>Function</strong>：一个函数，确定元素是否还原到它的开始位置。该函数必须返回 <code>true</code> 才能还原元素。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>revert</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ revert: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>revert</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var revert = $( ".selector" ).draggable( "option", "revert" );
 
// setter
$( ".selector" ).draggable( "option", "revert", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-revertDuration">
	<td>revertDuration</td>
    <td>Number</td>
	<td>还原（revert）动画的持续时间，以毫秒计。如果 <code>revert</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>revertDuration</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ revertDuration: 200 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>revertDuration</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var revertDuration = $( ".selector" ).draggable( "option", "revertDuration" );
 
// setter
$( ".selector" ).draggable( "option", "revertDuration", 200 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>500</td>
</tr>
<tr id="option-scope">
	<td>scope</td>
    <td>String</td>
	<td>用于组合配套 draggable 和 droppable 项，除了 droppable 的 <code>accept</code> 选项之外。一个与 droppable 带有相同的 <code>scope</code> 值的 draggable 会被该 droppable 接受。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scope</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ scope: "tasks" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scope</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scope = $( ".selector" ).draggable( "option", "scope" );
 
// setter
$( ".selector" ).draggable( "option", "scope", "tasks" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"default"</td>
</tr>
<tr id="option-scroll">
	<td>scroll</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，当拖拽时容器会自动滚动。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scroll</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ scroll: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scroll</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scroll = $( ".selector" ).draggable( "option", "scroll" );
 
// setter
$( ".selector" ).draggable( "option", "scroll", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-scrollSensitivity">
	<td>scrollSensitivity</td>
    <td>Number</td>
	<td>从要滚动的视区边缘起的距离，以像素计。距离是相对于指针的，不是相对于 draggable。如果 <code>scroll</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scrollSensitivity</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ scrollSensitivity: 100 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scrollSensitivity</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scrollSensitivity = $( ".selector" ).draggable( "option", "scrollSensitivity" );
 
// setter
$( ".selector" ).draggable( "option", "scrollSensitivity", 100 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-scrollSpeed">
	<td>scrollSpeed</td>
    <td>Number</td>
	<td>当鼠标指针获取到在 <code>scrollSensitivity</code> 距离内时，窗体滚动的速度。如果 <code>scroll</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scrollSpeed</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ scrollSpeed: 100 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scrollSpeed</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scrollSpeed = $( ".selector" ).draggable( "option", "scrollSpeed" );
 
// setter
$( ".selector" ).draggable( "option", "scrollSpeed", 100 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-snap">
	<td>snap</td>
    <td>Boolean 或 Selector</td>
	<td>元素是否对齐到其他元素。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>true</code> 时，元素会对齐到其它可拖拽（draggable ）元素。</li>
	<li><strong>Selector</strong>：一个选择器，指定要对齐到哪个元素。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>snap</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ snap: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>snap</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var snap = $( ".selector" ).draggable( "option", "snap" );
 
// setter
$( ".selector" ).draggable( "option", "snap", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-snapMode">
	<td>snapMode</td>
    <td>String</td>
	<td>决定 draggable 将对齐到对齐元素的哪个边缘。如果 <code>snap</code> 选项是 <code>false</code> 则忽略。可能的值：<code>"inner"</code>、<code>"outer"</code>、<code>"both"</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>snapMode</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ snapMode: "inner" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>snapMode</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var snapMode = $( ".selector" ).draggable( "option", "snapMode" );
 
// setter
$( ".selector" ).draggable( "option", "snapMode", "inner" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"both"</td>
</tr>
<tr id="option-snapTolerance">
	<td>snapTolerance</td>
    <td>Number</td>
	<td>从要发生对齐的对齐元素边缘起的距离，以像素计。如果 <code>snap</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>snapTolerance</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ snapTolerance: 30 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>snapTolerance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var snapTolerance = $( ".selector" ).draggable( "option", "snapTolerance" );
 
// setter
$( ".selector" ).draggable( "option", "snapTolerance", 30 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-stack">
	<td>stack</td>
    <td>Selector</td>
	<td>控制匹配选择器（selector）的元素集合的 z-index，总是在当前拖拽项的前面，在类似窗口管理器这样的事物中非常有用。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>stack</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ stack: ".products" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>stack</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var stack = $( ".selector" ).draggable( "option", "stack" );
 
// setter
$( ".selector" ).draggable( "option", "stack", ".products" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-zIndex">
	<td>zIndex</td>
    <td>Number</td>
	<td>当被拖拽时，助手（helper）的 Z-index。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>zIndex</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ zIndex: 100 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>zIndex</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var zIndex = $( ".selector" ).draggable( "option", "zIndex" );
 
// setter
$( ".selector" ).draggable( "option", "zIndex", 100 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-addClasses">
	<td>addClasses</td>
    <td>Boolean</td>
	<td>如果设置为 <code>false</code>，将阻止 <code>ui-draggable</code> class 被添加。当在数百个元素上调用 <code>.draggable()</code> 时，这么设置有利于性能优化。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>addClasses</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ addClasses: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>addClasses</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var addClasses = $( ".selector" ).draggable( "option", "addClasses" );
 
// setter
$( ".selector" ).draggable( "option", "addClasses", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-appendTo">
	<td>appendTo</td>
    <td>jQuery 或 Element 或 Selector 或 String</td>
	<td>当拖拽时，draggable 助手（helper）要追加到哪一个元素。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>jQuery</strong>：一个 jQuery 对象，包含助手（helper）要追加到的元素。</li>
	<li><strong>Element</strong>：要追加助手（helper）的元素。</li>
	<li><strong>Selector</strong>：一个选择器，指定哪一个元素要追加助手（helper）。</li>
	<li><strong>String</strong>：字符串 <code>"parent"</code> 将促使助手（helper）成为 draggable 的同级。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>appendTo</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ appendTo: "body" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>appendTo</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var appendTo = $( ".selector" ).draggable( "option", "appendTo" );
 
// setter
$( ".selector" ).draggable( "option", "appendTo", "body" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"parent"</td>
</tr>
<tr id="option-axis">
	<td>axis</td>
    <td>String</td>
	<td>约束在水平轴 (x) 或垂直轴 (y) 上拖拽。可能的值：<code>"x"</code>, <code>"y"</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>axis</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ axis: "x" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>axis</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var axis = $( ".selector" ).draggable( "option", "axis" );
 
// setter
$( ".selector" ).draggable( "option", "axis", "x" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
    <td>Selector</td>
	<td>防止从指定的元素上开始拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ cancel: ".title" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).draggable( "option", "cancel" );
 
// setter
$( ".selector" ).draggable( "option", "cancel", ".title" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-connectToSortable">
	<td>connectToSortable</td>
    <td>Selector</td>
	<td>允许 draggable 放置在指定的 sortable 上。如果使用了该选项，一个 draggable 可被放置在一个 sortable 列表上，然后成为列表的一部分。注意：<code>helper</code> 选项必须设置为 <code>"clone"</code>，以便更好地工作。必须包含 <a href="/jqueryui/api-sortable.html">可排序小部件（Sortable Widget）</a>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>connectToSortable</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ connectToSortable: "#my-sortable" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>connectToSortable</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var connectToSortable = $( ".selector" ).draggable( "option", "connectToSortable" );
 
// setter
$( ".selector" ).draggable( "option", "connectToSortable", "#my-sortable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-containment">
	<td>containment</td>
    <td>Selector 或 Element 或 String 或 Array</td>
	<td>约束在指定元素或区域的边界内拖拽。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Selector</strong>：可拖拽元素将被包含在 selector 第一个元素的边界内。如果未找到元素，则不设置 containment。</li>
	<li><strong>Element</strong>：可拖拽元素将被包含在元素的边界。</li>
	<li><strong>String</strong>：可能的值：<code>"parent"</code>、<code>"document"</code>、<code>"window"</code>。</li>
	<li><strong>Array</strong>：一个数组， 以形式 <code>[ x1, y1, x2, y2 ]</code> 定义元素的边界。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>containment</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ containment: "parent" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>containment</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var containment = $( ".selector" ).draggable( "option", "containment" );
 
// setter
$( ".selector" ).draggable( "option", "containment", "parent" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cursor">
	<td>cursor</td>
    <td>String</td>
	<td>拖拽操作期间的 CSS 光标。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cursor</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ cursor: "crosshair" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cursor</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cursor = $( ".selector" ).draggable( "option", "cursor" );
 
// setter
$( ".selector" ).draggable( "option", "cursor", "crosshair" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"auto"</td>
</tr>
<tr id="option-cursorAt">
	<td>cursorAt</td>
    <td>Object</td>
	<td>设置拖拽助手（helper）相对于鼠标光标的偏移。坐标可通过一个或两个键的组合成一个哈希给出：<code>{ top, left, right, bottom }</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cursorAt</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ cursorAt: { left: 5 } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cursorAt</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cursorAt = $( ".selector" ).draggable( "option", "cursorAt" );
 
// setter
$( ".selector" ).draggable( "option", "cursorAt", { left: 5 } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Number</td>
	<td>鼠标按下后直到拖拽开始为止的时间，以毫秒计。该选项可以防止点击在某个元素上时不必要的拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ delay: 300 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).draggable( "option", "delay" );
 
// setter
$( ".selector" ).draggable( "option", "delay", 300 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 draggable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).draggable( "option", "disabled" );
 
// setter
$( ".selector" ).draggable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
    <td>Number</td>
	<td>鼠标按下后拖拽开始前必须移动的距离，以像素计。该选项可以防止点击在某个元素上时不必要的拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ distance: 10 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).draggable( "option", "distance" );
 
// setter
$( ".selector" ).draggable( "option", "distance", 10 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
<tr id="option-grid">
	<td>grid</td>
    <td>Array</td>
	<td>对齐拖拽助手（helper）到网格，每个 x 和 y 像素。数组形式必须是 <code>[ x, y ]</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>grid</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ grid: [ 50, 20 ] });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>grid</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var grid = $( ".selector" ).draggable( "option", "grid" );
 
// setter
$( ".selector" ).draggable( "option", "grid", [ 50, 20 ] );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-handle">
	<td>handle</td>
    <td>Selector 或 Element</td>
	<td>如果指定了该选项，则限制开始拖拽，除非鼠标在指定的元素上按下。只有可拖拽（draggable）元素的后代元素才允许被拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>handle</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ handle: "h2" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>handle</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var handle = $( ".selector" ).draggable( "option", "handle" );
 
// setter
$( ".selector" ).draggable( "option", "handle", "h2" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-helper">
	<td>helper</td>
    <td>String 或 Function()</td>
	<td>允许一个 helper 元素用于拖拽显示。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>String</strong>：如果设置为 <code>"clone"</code>，元素将被克隆，且克隆将被拖拽。</li>
	<li><strong>Function</strong>：一个函数，将返回拖拽时要使用的 DOMElement。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>helper</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ helper: "clone" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>helper</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var helper = $( ".selector" ).draggable( "option", "helper" );
 
// setter
$( ".selector" ).draggable( "option", "helper", "clone" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"original"</td>
</tr>
<tr id="option-iframeFix">
	<td>iframeFix</td>
    <td>Boolean 或 Selector</td>
	<td>防止拖拽期间 iframes 捕捉鼠标移动（mousemove ）事件。在与 <code>cursorAt</code> 选项结合使用时，或鼠标光标未覆盖在助手（helper）上时，非常有用。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>true</code> 时，透明遮罩将被放置在页面上所有 iframes 上。</li>
	<li><strong>Selector</strong>：匹配 selector 的任意 iframes 将被透明遮罩覆盖。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>iframeFix</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ iframeFix: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>iframeFix</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var iframeFix = $( ".selector" ).draggable( "option", "iframeFix" );
 
// setter
$( ".selector" ).draggable( "option", "iframeFix", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-opacity">
	<td>opacity</td>
    <td>Number</td>
	<td>当被拖拽时助手（helper）的不透明度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>opacity</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ opacity: 0.35 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>opacity</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var opacity = $( ".selector" ).draggable( "option", "opacity" );
 
// setter
$( ".selector" ).draggable( "option", "opacity", 0.35 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-refreshPositions">
	<td>refreshPositions</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，在每次鼠标移动（mousemove）时都会计算所有可放置的位置。注意：这解决了高度动态的问题，但是明显降低了性能。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>refreshPositions</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ refreshPositions: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>refreshPositions</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var refreshPositions = $( ".selector" ).draggable( "option", "refreshPositions" );
 
// setter
$( ".selector" ).draggable( "option", "refreshPositions", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-revert">
	<td>revert</td>
    <td>Boolean 或 String 或 Function()</td>
	<td>当拖拽停止时，元素是否还原到它的开始位置。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：如果设置为 <code>true</code>，元素总会还原。</li>
	<li><strong>String</strong>：如果设置为 <code>"invalid"</code>，还原仅在 draggable 未放置在 droppable 上时发生，如果设置为 <code>"valid"</code> 则相反。</li>
	<li><strong>Function</strong>：一个函数，确定元素是否还原到它的开始位置。该函数必须返回 <code>true</code> 才能还原元素。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>revert</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ revert: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>revert</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var revert = $( ".selector" ).draggable( "option", "revert" );
 
// setter
$( ".selector" ).draggable( "option", "revert", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-revertDuration">
	<td>revertDuration</td>
    <td>Number</td>
	<td>还原（revert）动画的持续时间，以毫秒计。如果 <code>revert</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>revertDuration</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ revertDuration: 200 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>revertDuration</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var revertDuration = $( ".selector" ).draggable( "option", "revertDuration" );
 
// setter
$( ".selector" ).draggable( "option", "revertDuration", 200 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>500</td>
</tr>
<tr id="option-scope">
	<td>scope</td>
    <td>String</td>
	<td>用于组合配套 draggable 和 droppable 项，除了 droppable 的 <code>accept</code> 选项之外。一个与 droppable 带有相同的 <code>scope</code> 值的 draggable 会被该 droppable 接受。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scope</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ scope: "tasks" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scope</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scope = $( ".selector" ).draggable( "option", "scope" );
 
// setter
$( ".selector" ).draggable( "option", "scope", "tasks" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"default"</td>
</tr>
<tr id="option-scroll">
	<td>scroll</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，当拖拽时容器会自动滚动。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scroll</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ scroll: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scroll</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scroll = $( ".selector" ).draggable( "option", "scroll" );
 
// setter
$( ".selector" ).draggable( "option", "scroll", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-scrollSensitivity">
	<td>scrollSensitivity</td>
    <td>Number</td>
	<td>从要滚动的视区边缘起的距离，以像素计。距离是相对于指针的，不是相对于 draggable。如果 <code>scroll</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scrollSensitivity</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ scrollSensitivity: 100 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scrollSensitivity</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scrollSensitivity = $( ".selector" ).draggable( "option", "scrollSensitivity" );
 
// setter
$( ".selector" ).draggable( "option", "scrollSensitivity", 100 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-scrollSpeed">
	<td>scrollSpeed</td>
    <td>Number</td>
	<td>当鼠标指针获取到在 <code>scrollSensitivity</code> 距离内时，窗体滚动的速度。如果 <code>scroll</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scrollSpeed</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ scrollSpeed: 100 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scrollSpeed</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scrollSpeed = $( ".selector" ).draggable( "option", "scrollSpeed" );
 
// setter
$( ".selector" ).draggable( "option", "scrollSpeed", 100 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-snap">
	<td>snap</td>
    <td>Boolean 或 Selector</td>
	<td>元素是否对齐到其他元素。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>true</code> 时，元素会对齐到其它可拖拽（draggable ）元素。</li>
	<li><strong>Selector</strong>：一个选择器，指定要对齐到哪个元素。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>snap</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ snap: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>snap</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var snap = $( ".selector" ).draggable( "option", "snap" );
 
// setter
$( ".selector" ).draggable( "option", "snap", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-snapMode">
	<td>snapMode</td>
    <td>String</td>
	<td>决定 draggable 将对齐到对齐元素的哪个边缘。如果 <code>snap</code> 选项是 <code>false</code> 则忽略。可能的值：<code>"inner"</code>、<code>"outer"</code>、<code>"both"</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>snapMode</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ snapMode: "inner" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>snapMode</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var snapMode = $( ".selector" ).draggable( "option", "snapMode" );
 
// setter
$( ".selector" ).draggable( "option", "snapMode", "inner" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"both"</td>
</tr>
<tr id="option-snapTolerance">
	<td>snapTolerance</td>
    <td>Number</td>
	<td>从要发生对齐的对齐元素边缘起的距离，以像素计。如果 <code>snap</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>snapTolerance</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ snapTolerance: 30 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>snapTolerance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var snapTolerance = $( ".selector" ).draggable( "option", "snapTolerance" );
 
// setter
$( ".selector" ).draggable( "option", "snapTolerance", 30 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-stack">
	<td>stack</td>
    <td>Selector</td>
	<td>控制匹配选择器（selector）的元素集合的 z-index，总是在当前拖拽项的前面，在类似窗口管理器这样的事物中非常有用。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>stack</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ stack: ".products" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>stack</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var stack = $( ".selector" ).draggable( "option", "stack" );
 
// setter
$( ".selector" ).draggable( "option", "stack", ".products" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-zIndex">
	<td>zIndex</td>
    <td>Number</td>
	<td>当被拖拽时，助手（helper）的 Z-index。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>zIndex</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({ zIndex: 100 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>zIndex</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var zIndex = $( ".selector" ).draggable( "option", "zIndex" );
 
// setter
$( ".selector" ).draggable( "option", "zIndex", 100 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
</table></div>') where ID=6197;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 draggable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 draggable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 draggable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).draggable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 draggable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).draggable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 draggable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 draggable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 draggable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).draggable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 draggable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 draggable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 draggable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).draggable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 draggable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).draggable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 draggable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 draggable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 draggable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).draggable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6197;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>dragcreate</td>
	<td>当 draggable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dragcreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dragcreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-drag">
	<td>drag( event, ui )</td>
    <td>drag</td>
	<td>在拖拽期间当鼠标移动时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 drag 回调的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({
  drag: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 drag 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "drag", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-start">
	<td>start( event, ui )</td>
    <td>dragstart</td>
	<td>当拖拽开始时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 start 回调的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({
  start: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dragstart 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dragstart", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-stop">
	<td>stop( event, ui )</td>
    <td>dragstop</td>
	<td>当拖拽停止时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 stop 回调的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({
  stop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dragstop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dragstop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>dragcreate</td>
	<td>当 draggable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dragcreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dragcreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-drag">
	<td>drag( event, ui )</td>
    <td>drag</td>
	<td>在拖拽期间当鼠标移动时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 drag 回调的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({
  drag: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 drag 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "drag", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-start">
	<td>start( event, ui )</td>
    <td>dragstart</td>
	<td>当拖拽开始时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 start 回调的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({
  start: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dragstart 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dragstart", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-stop">
	<td>stop( event, ui )</td>
    <td>dragstop</td>
	<td>当拖拽停止时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 stop 回调的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).draggable({
  stop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dragstop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dragstop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6197;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-accept">
	<td>accept</td>
    <td>Selector 或 Function()</td>
	<td>控制哪个可拖拽（draggable）元素可被 droppable 接受。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Selector</strong>：一个选择器，指定哪个可拖拽（draggable）元素可被 droppable 接受。</li>
	<li><strong>Function()</strong>：一个函数，将被页面上每个 draggable 调用（作为第一个参数传递给函数）。如果 draggable 被接受，该函数必须返回 <code>true</code>。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>accept</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ accept: ".special" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>accept</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var accept = $( ".selector" ).droppable( "option", "accept" );
 
// setter
$( ".selector" ).droppable( "option", "accept", ".special" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"*"</td>
</tr>
<tr id="option-activeClass">
	<td>activeClass</td>
    <td>String</td>
	<td>如果指定了该选项，当一个可接受的 draggable 被拖拽时，class 将被添加到 droppable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>activeClass</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ activeClass: "ui-state-highlight" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>activeClass</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var activeClass = $( ".selector" ).droppable( "option", "activeClass" );
 
// setter
$( ".selector" ).droppable( "option", "activeClass", "ui-state-highlight" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-addClasses">
	<td>addClasses</td>
    <td>Boolean</td>
	<td>如果设置为 <code>false</code>，将防止 <code>ui-droppable</code> class 被添加。这在数百个元素上调用 <code>.droppable()</code> 时有助于性能优化。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>addClasses</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ addClasses: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>addClasses</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var addClasses = $( ".selector" ).droppable( "option", "addClasses" );
 
// setter
$( ".selector" ).droppable( "option", "addClasses", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 droppable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).droppable( "option", "disabled" );
 
// setter
$( ".selector" ).droppable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-greedy">
	<td>greedy</td>
    <td>Boolean</td>
	<td>默认情况下，当一个元素被放置在嵌套是 droppable 上时，每个 droppable 将接收该元素。然而，通过设置该选项为 <code>true</code>，任何父元素的 droppable 将无法接收该元素。 <code>drop</code> 事件仍将照常，但会检查 <code>event.target</code> 以便查看哪个 droppable 接收 draggable 元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>greedy</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ greedy: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>greedy</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var greedy = $( ".selector" ).droppable( "option", "greedy" );
 
// setter
$( ".selector" ).droppable( "option", "greedy", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-hoverClass">
	<td>hoverClass</td>
    <td>String</td>
	<td>如果指定了该选项，当一个可接受 draggable 被覆盖在 droppable 上时，class 将被添加到 droppable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>hoverClass</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ hoverClass: "drop-hover" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>hoverClass</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var hoverClass = $( ".selector" ).droppable( "option", "hoverClass" );
 
// setter
$( ".selector" ).droppable( "option", "hoverClass", "drop-hover" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-scope">
	<td>scope</td>
    <td>String</td>
	<td>用于组合配套 draggable 和 droppable 项，除了 droppable 的 <code>accept</code> 选项之外。一个与 droppable 带有相同的 <code>scope</code> 值的 draggable 会被该 droppable 接受。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scope</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ scope: "tasks" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scope</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scope = $( ".selector" ).droppable( "option", "scope" );
 
// setter
$( ".selector" ).droppable( "option", "scope", "tasks" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"default"</td>
</tr>
<tr id="option-tolerance">
	<td>tolerance</td>
    <td>String</td>
	<td>指定用于测试一个 draggable 是否覆盖在一个 droppable 上的模式。可能的值：<br />
	<ul>
	<li><code>"fit"</code>：draggable 完全重叠在 droppable 上。</li>
	<li><code>"intersect"</code>：draggable 重叠在 droppable 上，两个方向上至少 50%。</li>
	<li><code>"pointer"</code>：鼠标指针重叠在 droppable 上。</li>
	<li><code>"touch"</code>：draggable 重叠在 droppable 上，任何数量皆可。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>tolerance</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ tolerance: "fit" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>tolerance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var tolerance = $( ".selector" ).droppable( "option", "tolerance" );
 
// setter
$( ".selector" ).droppable( "option", "tolerance", "fit" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"intersect"</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-accept">
	<td>accept</td>
    <td>Selector 或 Function()</td>
	<td>控制哪个可拖拽（draggable）元素可被 droppable 接受。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Selector</strong>：一个选择器，指定哪个可拖拽（draggable）元素可被 droppable 接受。</li>
	<li><strong>Function()</strong>：一个函数，将被页面上每个 draggable 调用（作为第一个参数传递给函数）。如果 draggable 被接受，该函数必须返回 <code>true</code>。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>accept</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ accept: ".special" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>accept</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var accept = $( ".selector" ).droppable( "option", "accept" );
 
// setter
$( ".selector" ).droppable( "option", "accept", ".special" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"*"</td>
</tr>
<tr id="option-activeClass">
	<td>activeClass</td>
    <td>String</td>
	<td>如果指定了该选项，当一个可接受的 draggable 被拖拽时，class 将被添加到 droppable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>activeClass</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ activeClass: "ui-state-highlight" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>activeClass</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var activeClass = $( ".selector" ).droppable( "option", "activeClass" );
 
// setter
$( ".selector" ).droppable( "option", "activeClass", "ui-state-highlight" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-addClasses">
	<td>addClasses</td>
    <td>Boolean</td>
	<td>如果设置为 <code>false</code>，将防止 <code>ui-droppable</code> class 被添加。这在数百个元素上调用 <code>.droppable()</code> 时有助于性能优化。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>addClasses</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ addClasses: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>addClasses</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var addClasses = $( ".selector" ).droppable( "option", "addClasses" );
 
// setter
$( ".selector" ).droppable( "option", "addClasses", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 droppable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).droppable( "option", "disabled" );
 
// setter
$( ".selector" ).droppable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-greedy">
	<td>greedy</td>
    <td>Boolean</td>
	<td>默认情况下，当一个元素被放置在嵌套是 droppable 上时，每个 droppable 将接收该元素。然而，通过设置该选项为 <code>true</code>，任何父元素的 droppable 将无法接收该元素。 <code>drop</code> 事件仍将照常，但会检查 <code>event.target</code> 以便查看哪个 droppable 接收 draggable 元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>greedy</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ greedy: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>greedy</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var greedy = $( ".selector" ).droppable( "option", "greedy" );
 
// setter
$( ".selector" ).droppable( "option", "greedy", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-hoverClass">
	<td>hoverClass</td>
    <td>String</td>
	<td>如果指定了该选项，当一个可接受 draggable 被覆盖在 droppable 上时，class 将被添加到 droppable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>hoverClass</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ hoverClass: "drop-hover" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>hoverClass</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var hoverClass = $( ".selector" ).droppable( "option", "hoverClass" );
 
// setter
$( ".selector" ).droppable( "option", "hoverClass", "drop-hover" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-scope">
	<td>scope</td>
    <td>String</td>
	<td>用于组合配套 draggable 和 droppable 项，除了 droppable 的 <code>accept</code> 选项之外。一个与 droppable 带有相同的 <code>scope</code> 值的 draggable 会被该 droppable 接受。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scope</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ scope: "tasks" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scope</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scope = $( ".selector" ).droppable( "option", "scope" );
 
// setter
$( ".selector" ).droppable( "option", "scope", "tasks" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"default"</td>
</tr>
<tr id="option-tolerance">
	<td>tolerance</td>
    <td>String</td>
	<td>指定用于测试一个 draggable 是否覆盖在一个 droppable 上的模式。可能的值：<br />
	<ul>
	<li><code>"fit"</code>：draggable 完全重叠在 droppable 上。</li>
	<li><code>"intersect"</code>：draggable 重叠在 droppable 上，两个方向上至少 50%。</li>
	<li><code>"pointer"</code>：鼠标指针重叠在 droppable 上。</li>
	<li><code>"touch"</code>：draggable 重叠在 droppable 上，任何数量皆可。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>tolerance</code> 选项的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({ tolerance: "fit" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>tolerance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var tolerance = $( ".selector" ).droppable( "option", "tolerance" );
 
// setter
$( ".selector" ).droppable( "option", "tolerance", "fit" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"intersect"</td>
</tr>
</table></div>') where ID=6198;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 droppable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 droppable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 droppable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).droppable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 droppable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).droppable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 droppable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 droppable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 droppable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).droppable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 droppable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 droppable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 droppable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).droppable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 droppable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).droppable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 droppable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 droppable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 droppable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).droppable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6198;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-activate">
	<td>activate( event, ui )</td>
    <td>dropactivate</td>
	<td>当一个可接受的 draggable 开始拖拽时触发。如果您想让 droppable 被放置时"点亮"，该选项就可以派上用场。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>draggable</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示 draggable 元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 activate 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  activate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>dropcreate</td>
	<td>当 droppable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropcreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropcreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-deactivate">
	<td>deactivate( event, ui )</td>
    <td>dropdeactivate</td>
	<td>当一个可接受的 draggable 停止拖拽时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>draggable</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示 draggable 元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 deactivate 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  deactivate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropdeactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropdeactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-drop">
	<td>drop( event, ui )</td>
    <td>drop</td>
	<td>当一个可接受的 draggable 被放置在 droppable（基于 <code><a href="#option-tolerance">tolerance</a></code> 选项）上时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>draggable</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示 draggable 元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 drop 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  drop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 drop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "drop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-out">
	<td>out( event, ui )</td>
    <td>dropout</td>
	<td>当 droppable 被拖拽出 droppable（基于 <code><a href="#option-tolerance">tolerance</a></code> 选项）时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 out 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  out: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropout 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropout", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-over">
	<td>over( event, ui )</td>
    <td>dropover</td>
	<td>当一个可接受的 draggable 被拖拽在 droppable（基于 <code><a href="#option-tolerance">tolerance</a></code> 选项）上时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>draggable</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示 draggable 元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 over 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  over: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropover 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropover", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-activate">
	<td>activate( event, ui )</td>
    <td>dropactivate</td>
	<td>当一个可接受的 draggable 开始拖拽时触发。如果您想让 droppable 被放置时"点亮"，该选项就可以派上用场。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>draggable</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示 draggable 元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 activate 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  activate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>dropcreate</td>
	<td>当 droppable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropcreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropcreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-deactivate">
	<td>deactivate( event, ui )</td>
    <td>dropdeactivate</td>
	<td>当一个可接受的 draggable 停止拖拽时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>draggable</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示 draggable 元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 deactivate 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  deactivate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropdeactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropdeactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-drop">
	<td>drop( event, ui )</td>
    <td>drop</td>
	<td>当一个可接受的 draggable 被放置在 droppable（基于 <code><a href="#option-tolerance">tolerance</a></code> 选项）上时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>draggable</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示 draggable 元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 drop 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  drop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 drop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "drop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-out">
	<td>out( event, ui )</td>
    <td>dropout</td>
	<td>当 droppable 被拖拽出 droppable（基于 <code><a href="#option-tolerance">tolerance</a></code> 选项）时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 out 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  out: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropout 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropout", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-over">
	<td>over( event, ui )</td>
    <td>dropover</td>
	<td>当一个可接受的 draggable 被拖拽在 droppable（基于 <code><a href="#option-tolerance">tolerance</a></code> 选项）上时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>draggable</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示 draggable 元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被拖拽的助手（helper）。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前 CSS 位置，比如 <code>{ top, left }</code> 对象。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：draggable 助手（helper）的当前偏移位置，比如 <code>{ top, left }</code> 对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 over 回调的 droppable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).droppable({
  over: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 dropover 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "dropover", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6198;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
    <td>Selector</td>
	<td>防止从指定的元素上开始交互。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 mouse：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse({ cancel: ".title" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).mouse( "option", "cancel" );
 
// setter
$( ".selector" ).mouse( "option", "cancel", ".title" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Number</td>
	<td>鼠标按下后直至交互开始的事件，以毫秒计。该选项可用于防止点击在一个元素上时不必要的交互。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 mouse：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse({ delay: 300 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).mouse( "option", "delay" );
 
// setter
$( ".selector" ).mouse( "option", "delay", 300 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
    <td>Number</td>
	<td>鼠标按下后交互开始前鼠标必须移动的距离，以像素计。该选项可用于防止点击在一个元素上时不必要的交互。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 mouse：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse({ distance: 10 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).mouse( "option", "distance" );
 
// setter
$( ".selector" ).mouse( "option", "distance", 10 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
    <td>Selector</td>
	<td>防止从指定的元素上开始交互。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 mouse：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse({ cancel: ".title" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).mouse( "option", "cancel" );
 
// setter
$( ".selector" ).mouse( "option", "cancel", ".title" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Number</td>
	<td>鼠标按下后直至交互开始的事件，以毫秒计。该选项可用于防止点击在一个元素上时不必要的交互。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 mouse：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse({ delay: 300 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).mouse( "option", "delay" );
 
// setter
$( ".selector" ).mouse( "option", "delay", 300 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
    <td>Number</td>
	<td>鼠标按下后交互开始前鼠标必须移动的距离，以像素计。该选项可用于防止点击在一个元素上时不必要的交互。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 mouse：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse({ distance: 10 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).mouse( "option", "distance" );
 
// setter
$( ".selector" ).mouse( "option", "distance", 10 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
</table></div>') where ID=6199;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-_mouseCapture">
	<td>_mouseCapture()</td>
	<td>Boolean</td>
	<td>决定交互是否应该基于交互的事件目标开始。默认的实现总是返回 <code>true</code>。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseCapture 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseCapture" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDelayMet">
	<td>_mouseDelayMet()</td>
	<td>Boolean</td>
	<td>决定 <code><a href="#option-delay">delay</a></code> 选项是否满足当前交互。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDelayMet 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDelayMet" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDestroy">
	<td>_mouseDestroy()</td>
	<td>jQuery (plugin only)</td>
	<td>销毁交互事件处理程序。这必须调用来自扩展的小部件的 <code>_destroy()</code> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDestroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDestroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDistanceMet">
	<td>_mouseDistanceMet()</td>
	<td>Boolean</td>
	<td>决定 <code><a href="#option-distance">distance</a></code> 选项是否满足当前交互。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDistanceMet 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDistanceMet" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDown">
	<td>_mouseDown()</td>
	<td>jQuery (plugin only)</td>
	<td>处理交互的开始。确认与主要的鼠标按钮关联的事件，确保 <code><a href="#option-delay">delay</a></code> 与 <code><a href="#option-distance">distance</a></code> 在交互启动之前得到满足。当交互已经准备开始，为要处理的扩展小部件调用 <code><a href="#method-_mouseStart">_mouseStart</a></code> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDown 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDown" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDrag">
	<td>_mouseDrag()</td>
	<td>jQuery (plugin only)</td>
	<td>扩展小部件应实现一个 <code>_mouseDrag()</code> 方法，来处理交互的每个移动。该方法将接收与鼠标移动相关联的鼠标事件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDrag 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDrag" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseInit">
	<td>_mouseInit()</td>
	<td>jQuery (plugin only)</td>
	<td>初始化交互事件处理程序。这必须调用来自扩展的小部件的 <code>_create()</code> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseInit 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseInit" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseMove">
	<td>_mouseMove()</td>
	<td>jQuery (plugin only)</td>
	<td>处理交互的每个移动。为要处理的扩展小部件调用 <a href="#method-_mouseDrag">_mouseDrag</a> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseMove 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseMove" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseStart">
	<td>_mouseStart()</td>
	<td>jQuery (plugin only)</td>
	<td>扩展小部件应实现一个 <code>_mouseStart()</code> 方法，来处理交互的开始。该方法将接收与交互开始相关联的鼠标事件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseStart 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseStart" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseStop">
	<td>_mouseStop()</td>
	<td>jQuery (plugin only)</td>
	<td>扩展小部件应实现一个 <code>_mouseStop()</code> 方法，来处理交互的结束。该方法将接收与交互结束相关联的鼠标事件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseStop 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseStop" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseUp">
	<td>_mouseUp()</td>
	<td>jQuery (plugin only)</td>
	<td>处理交互结束。对扩展小部件的处理调用 <a href="#method-_mouseStop">_mouseStop</a> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseUp 方法：</p>
<pre>
$( ".selector" ).mouse( "_mouseUp" );
</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-_mouseCapture">
	<td>_mouseCapture()</td>
	<td>Boolean</td>
	<td>决定交互是否应该基于交互的事件目标开始。默认的实现总是返回 <code>true</code>。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseCapture 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseCapture" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDelayMet">
	<td>_mouseDelayMet()</td>
	<td>Boolean</td>
	<td>决定 <code><a href="#option-delay">delay</a></code> 选项是否满足当前交互。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDelayMet 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDelayMet" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDestroy">
	<td>_mouseDestroy()</td>
	<td>jQuery (plugin only)</td>
	<td>销毁交互事件处理程序。这必须调用来自扩展的小部件的 <code>_destroy()</code> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDestroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDestroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDistanceMet">
	<td>_mouseDistanceMet()</td>
	<td>Boolean</td>
	<td>决定 <code><a href="#option-distance">distance</a></code> 选项是否满足当前交互。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDistanceMet 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDistanceMet" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDown">
	<td>_mouseDown()</td>
	<td>jQuery (plugin only)</td>
	<td>处理交互的开始。确认与主要的鼠标按钮关联的事件，确保 <code><a href="#option-delay">delay</a></code> 与 <code><a href="#option-distance">distance</a></code> 在交互启动之前得到满足。当交互已经准备开始，为要处理的扩展小部件调用 <code><a href="#method-_mouseStart">_mouseStart</a></code> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDown 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDown" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseDrag">
	<td>_mouseDrag()</td>
	<td>jQuery (plugin only)</td>
	<td>扩展小部件应实现一个 <code>_mouseDrag()</code> 方法，来处理交互的每个移动。该方法将接收与鼠标移动相关联的鼠标事件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseDrag 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseDrag" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseInit">
	<td>_mouseInit()</td>
	<td>jQuery (plugin only)</td>
	<td>初始化交互事件处理程序。这必须调用来自扩展的小部件的 <code>_create()</code> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseInit 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseInit" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseMove">
	<td>_mouseMove()</td>
	<td>jQuery (plugin only)</td>
	<td>处理交互的每个移动。为要处理的扩展小部件调用 <a href="#method-_mouseDrag">_mouseDrag</a> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseMove 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseMove" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseStart">
	<td>_mouseStart()</td>
	<td>jQuery (plugin only)</td>
	<td>扩展小部件应实现一个 <code>_mouseStart()</code> 方法，来处理交互的开始。该方法将接收与交互开始相关联的鼠标事件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseStart 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseStart" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseStop">
	<td>_mouseStop()</td>
	<td>jQuery (plugin only)</td>
	<td>扩展小部件应实现一个 <code>_mouseStop()</code> 方法，来处理交互的结束。该方法将接收与交互结束相关联的鼠标事件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseStop 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).mouse( "_mouseStop" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_mouseUp">
	<td>_mouseUp()</td>
	<td>jQuery (plugin only)</td>
	<td>处理交互结束。对扩展小部件的处理调用 <a href="#method-_mouseStop">_mouseStop</a> 方法。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 _mouseUp 方法：</p>
<pre>
$( ".selector" ).mouse( "_mouseUp" );
</pre>
	</td>
</tr>
</table></div>') where ID=6199;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-alsoResize">
	<td>alsoResize</td>
	<td>Selector 或 jQuery 或 Element</td>
	<td>一个或多个通过 resizable 元素进行同步调整尺寸的元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>alsoResize</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ alsoResize: "#mirror" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>alsoResize</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var alsoResize = $( ".selector" ).resizable( "option", "alsoResize" );
 
// setter
$( ".selector" ).resizable( "option", "alsoResize", "#mirror" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-animate">
	<td>animate</td>
	<td>Boolean</td>
	<td>调整尺寸后动态变化到最终尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>animate</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ animate: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>animate</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var animate = $( ".selector" ).resizable( "option", "animate" );
 
// setter
$( ".selector" ).resizable( "option", "animate", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-animateDuration">
	<td>animateDuration</td>
	<td>Number 或 String</td>
	<td>当使用 <code><a href="#option-animate">animate</a></code> 选项时，动画持续的时间。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Number</strong>：持续时间，以毫秒计。</li>
	<li><strong>String</strong>：一个命名的持续时间，比如 <code>"slow"</code> 或 <code>"fast"</code>。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>animateDuration</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ animateDuration: "fast" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>animateDuration</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var animateDuration = $( ".selector" ).resizable( "option", "animateDuration" );
 
// setter
$( ".selector" ).resizable( "option", "animateDuration", "fast" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"slow"</td>
</tr>
<tr id="option-animateEasing">
	<td>animateEasing</td>
	<td>String</td>
	<td>当使用 <code><a href="#option-animate">animate</a></code> 选项时要使用的 <a href="/jqueryui/api-easings.html">Easings</a>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>animateEasing</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ animateEasing: "easeOutBounce" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>animateEasing</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var animateEasing = $( ".selector" ).resizable( "option", "animateEasing" );
 
// setter
$( ".selector" ).resizable( "option", "animateEasing", "easeOutBounce" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"swing"</td>
</tr>
<tr id="option-aspectRatio">
	<td>aspectRatio</td>
	<td>Boolean 或 Number</td>
	<td>元素是否应该被限制在一个特定的长宽比。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>true</code> 时，元素将保持其原有的长宽比。</li>
	<li><strong>Number</strong>：强制在调整尺寸时元素保持特定的长宽比。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>aspectRatio</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ aspectRatio: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>aspectRatio</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var aspectRatio = $( ".selector" ).resizable( "option", "aspectRatio" );
 
// setter
$( ".selector" ).resizable( "option", "aspectRatio", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-autoHide">
	<td>autoHide</td>
	<td>Boolean</td>
	<td>当用户鼠标没有悬浮在元素上时是否隐藏手柄。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>autoHide</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ autoHide: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>autoHide</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var autoHide = $( ".selector" ).resizable( "option", "autoHide" );
 
// setter
$( ".selector" ).resizable( "option", "autoHide", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
	<td>Selector</td>
	<td>防止从指定的元素上开始调整尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ cancel: ".cancel" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).resizable( "option", "cancel" );
 
// setter
$( ".selector" ).resizable( "option", "cancel", ".cancel" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-containment">
	<td>containment</td>
	<td>Selector 或 Element 或 String</td>
	<td>约束在指定元素或区域的边界内调整尺寸。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Selector</strong>：可调整尺寸元素将被包含在 selector 第一个元素的边界内。如果未找到元素，则不设置 containment。</li>
	<li><strong>Element</strong>：可调整尺寸元素将被包含在元素的边界内。</li>
	<li><strong>String</strong>：可能的值：<code>"parent"</code>、<code>"document"</code>。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>containment</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ containment: "parent" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>containment</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var containment = $( ".selector" ).resizable( "option", "containment" );
 
// setter
$( ".selector" ).resizable( "option", "containment", "parent" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
	<td>Number</td>
	<td>鼠标按下后直到调整尺寸开始为止的时间，以毫秒计。如果指定了该选项，调整只有在鼠标移动超过时间后才开始。该选项可以防止点击在某个元素上时不必要的调整尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ delay: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).resizable( "option", "delay" );
 
// setter
$( ".selector" ).resizable( "option", "delay", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 resizable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).resizable( "option", "disabled" );
 
// setter
$( ".selector" ).resizable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
    <td>Number</td>
	<td>鼠标按下后调整尺寸开始前必须移动的距离，以像素计。如果指定了该选项，调整只有在鼠标移动超过距离后才开始。该选项可以防止点击在某个元素上时不必要的调整尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ distance: 30 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).resizable( "option", "distance" );
 
// setter
$( ".selector" ).resizable( "option", "distance", 30 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
<tr id="option-ghost">
	<td>ghost</td>
	<td>Boolean</td>
	<td>如果设置为 <code>true</code>，则为调整尺寸显示一个半透明的辅助元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>ghost</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ ghost: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>ghost</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var ghost = $( ".selector" ).resizable( "option", "ghost" );
 
// setter
$( ".selector" ).resizable( "option", "ghost", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-grid">
	<td>grid</td>
	<td>Array</td>
	<td>把可调整尺寸元素对齐到网格，每个 x 和 y 像素。数组形式必须是 <code>[ x, y ]</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>grid</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ grid: [ 20, 10 ] });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>grid</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var grid = $( ".selector" ).resizable( "option", "grid" );
 
// setter
$( ".selector" ).resizable( "option", "grid", [ 20, 10 ] );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-handles">
	<td>handles</td>
    <td>String 或 Object</td>
	<td>可用于调整尺寸的处理程序。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>String</strong>：一个逗号分隔的列表，列表值为下面所列出的任意值：n, e, s, w, ne, se, sw, nw, all。必要的处理程序由插件自动生成。</li>
	<li><strong>Object</strong>：支持下面的键：{ n, e, s, w, ne, se, sw, nw }。任何指定的值应该是一个匹配作为处理程序使用的 resizable 的子元素的 jQuery 选择器。</li>
	</ul>
	<p><strong>注释：</strong>当生成您自己的处理程序时，每个处理程序必须有 <code>ui-resizable-handle</code> class，也可以是适当的 <code>appropriate ui-resizable-{direction}</code> class，比如 ui-resizable-s。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>handles</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ handles: "n, e, s, w" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>handles</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var handles = $( ".selector" ).resizable( "option", "handles" );
 
// setter
$( ".selector" ).resizable( "option", "handles", "n, e, s, w" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"e, s, se"</td>
</tr>
<tr id="option-helper">
	<td>helper</td>
    <td>String</td>
	<td>一个将被添加到代理元素的 class 名称，用于描绘调整手柄拖拽过程中调整的轮廓。一旦调整完成，原来的元素则被重新定义尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>helper</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ helper: "resizable-helper" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>helper</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var helper = $( ".selector" ).resizable( "option", "helper" );
 
// setter
$( ".selector" ).resizable( "option", "helper", "resizable-helper" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-maxHeight">
	<td>maxHeight</td>
	<td>Number</td>
	<td>resizable 允许被调整到的最大高度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>maxHeight</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ maxHeight: 300 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>maxHeight</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var maxHeight = $( ".selector" ).resizable( "option", "maxHeight" );
 
// setter
$( ".selector" ).resizable( "option", "maxHeight", 300 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-maxWidth">
	<td>maxWidth</td>
	<td>Number</td>
	<td>resizable 允许被调整到的最大宽度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>maxWidth</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ maxWidth: 300 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>maxWidth</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var maxWidth = $( ".selector" ).resizable( "option", "maxWidth" );
 
// setter
$( ".selector" ).resizable( "option", "maxWidth", 300 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-minHeight">
	<td>minHeight</td>
	<td>Number</td>
	<td>resizable 允许被调整到的最小高度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>minHeight</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ minHeight: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>minHeight</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var minHeight = $( ".selector" ).resizable( "option", "minHeight" );
 
// setter
$( ".selector" ).resizable( "option", "minHeight", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>10</td>
</tr>
<tr id="option-minWidth">
	<td>minWidth</td>
	<td>Number</td>
	<td>resizable 允许被调整到的最小宽度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>minWidth</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ minWidth: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>minWidth</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var minWidth = $( ".selector" ).resizable( "option", "minWidth" );
 
// setter
$( ".selector" ).resizable( "option", "minWidth", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>10</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-alsoResize">
	<td>alsoResize</td>
	<td>Selector 或 jQuery 或 Element</td>
	<td>一个或多个通过 resizable 元素进行同步调整尺寸的元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>alsoResize</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ alsoResize: "#mirror" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>alsoResize</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var alsoResize = $( ".selector" ).resizable( "option", "alsoResize" );
 
// setter
$( ".selector" ).resizable( "option", "alsoResize", "#mirror" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-animate">
	<td>animate</td>
	<td>Boolean</td>
	<td>调整尺寸后动态变化到最终尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>animate</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ animate: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>animate</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var animate = $( ".selector" ).resizable( "option", "animate" );
 
// setter
$( ".selector" ).resizable( "option", "animate", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-animateDuration">
	<td>animateDuration</td>
	<td>Number 或 String</td>
	<td>当使用 <code><a href="#option-animate">animate</a></code> 选项时，动画持续的时间。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Number</strong>：持续时间，以毫秒计。</li>
	<li><strong>String</strong>：一个命名的持续时间，比如 <code>"slow"</code> 或 <code>"fast"</code>。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>animateDuration</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ animateDuration: "fast" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>animateDuration</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var animateDuration = $( ".selector" ).resizable( "option", "animateDuration" );
 
// setter
$( ".selector" ).resizable( "option", "animateDuration", "fast" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"slow"</td>
</tr>
<tr id="option-animateEasing">
	<td>animateEasing</td>
	<td>String</td>
	<td>当使用 <code><a href="#option-animate">animate</a></code> 选项时要使用的 <a href="/jqueryui/api-easings.html">Easings</a>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>animateEasing</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ animateEasing: "easeOutBounce" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>animateEasing</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var animateEasing = $( ".selector" ).resizable( "option", "animateEasing" );
 
// setter
$( ".selector" ).resizable( "option", "animateEasing", "easeOutBounce" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"swing"</td>
</tr>
<tr id="option-aspectRatio">
	<td>aspectRatio</td>
	<td>Boolean 或 Number</td>
	<td>元素是否应该被限制在一个特定的长宽比。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>true</code> 时，元素将保持其原有的长宽比。</li>
	<li><strong>Number</strong>：强制在调整尺寸时元素保持特定的长宽比。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>aspectRatio</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ aspectRatio: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>aspectRatio</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var aspectRatio = $( ".selector" ).resizable( "option", "aspectRatio" );
 
// setter
$( ".selector" ).resizable( "option", "aspectRatio", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-autoHide">
	<td>autoHide</td>
	<td>Boolean</td>
	<td>当用户鼠标没有悬浮在元素上时是否隐藏手柄。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>autoHide</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ autoHide: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>autoHide</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var autoHide = $( ".selector" ).resizable( "option", "autoHide" );
 
// setter
$( ".selector" ).resizable( "option", "autoHide", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
	<td>Selector</td>
	<td>防止从指定的元素上开始调整尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ cancel: ".cancel" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).resizable( "option", "cancel" );
 
// setter
$( ".selector" ).resizable( "option", "cancel", ".cancel" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-containment">
	<td>containment</td>
	<td>Selector 或 Element 或 String</td>
	<td>约束在指定元素或区域的边界内调整尺寸。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Selector</strong>：可调整尺寸元素将被包含在 selector 第一个元素的边界内。如果未找到元素，则不设置 containment。</li>
	<li><strong>Element</strong>：可调整尺寸元素将被包含在元素的边界内。</li>
	<li><strong>String</strong>：可能的值：<code>"parent"</code>、<code>"document"</code>。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>containment</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ containment: "parent" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>containment</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var containment = $( ".selector" ).resizable( "option", "containment" );
 
// setter
$( ".selector" ).resizable( "option", "containment", "parent" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
	<td>Number</td>
	<td>鼠标按下后直到调整尺寸开始为止的时间，以毫秒计。如果指定了该选项，调整只有在鼠标移动超过时间后才开始。该选项可以防止点击在某个元素上时不必要的调整尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ delay: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).resizable( "option", "delay" );
 
// setter
$( ".selector" ).resizable( "option", "delay", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 resizable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).resizable( "option", "disabled" );
 
// setter
$( ".selector" ).resizable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
    <td>Number</td>
	<td>鼠标按下后调整尺寸开始前必须移动的距离，以像素计。如果指定了该选项，调整只有在鼠标移动超过距离后才开始。该选项可以防止点击在某个元素上时不必要的调整尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ distance: 30 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).resizable( "option", "distance" );
 
// setter
$( ".selector" ).resizable( "option", "distance", 30 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
<tr id="option-ghost">
	<td>ghost</td>
	<td>Boolean</td>
	<td>如果设置为 <code>true</code>，则为调整尺寸显示一个半透明的辅助元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>ghost</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ ghost: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>ghost</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var ghost = $( ".selector" ).resizable( "option", "ghost" );
 
// setter
$( ".selector" ).resizable( "option", "ghost", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-grid">
	<td>grid</td>
	<td>Array</td>
	<td>把可调整尺寸元素对齐到网格，每个 x 和 y 像素。数组形式必须是 <code>[ x, y ]</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>grid</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ grid: [ 20, 10 ] });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>grid</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var grid = $( ".selector" ).resizable( "option", "grid" );
 
// setter
$( ".selector" ).resizable( "option", "grid", [ 20, 10 ] );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-handles">
	<td>handles</td>
    <td>String 或 Object</td>
	<td>可用于调整尺寸的处理程序。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>String</strong>：一个逗号分隔的列表，列表值为下面所列出的任意值：n, e, s, w, ne, se, sw, nw, all。必要的处理程序由插件自动生成。</li>
	<li><strong>Object</strong>：支持下面的键：{ n, e, s, w, ne, se, sw, nw }。任何指定的值应该是一个匹配作为处理程序使用的 resizable 的子元素的 jQuery 选择器。</li>
	</ul>
	<p><strong>注释：</strong>当生成您自己的处理程序时，每个处理程序必须有 <code>ui-resizable-handle</code> class，也可以是适当的 <code>appropriate ui-resizable-{direction}</code> class，比如 ui-resizable-s。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>handles</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ handles: "n, e, s, w" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>handles</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var handles = $( ".selector" ).resizable( "option", "handles" );
 
// setter
$( ".selector" ).resizable( "option", "handles", "n, e, s, w" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"e, s, se"</td>
</tr>
<tr id="option-helper">
	<td>helper</td>
    <td>String</td>
	<td>一个将被添加到代理元素的 class 名称，用于描绘调整手柄拖拽过程中调整的轮廓。一旦调整完成，原来的元素则被重新定义尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>helper</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ helper: "resizable-helper" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>helper</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var helper = $( ".selector" ).resizable( "option", "helper" );
 
// setter
$( ".selector" ).resizable( "option", "helper", "resizable-helper" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-maxHeight">
	<td>maxHeight</td>
	<td>Number</td>
	<td>resizable 允许被调整到的最大高度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>maxHeight</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ maxHeight: 300 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>maxHeight</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var maxHeight = $( ".selector" ).resizable( "option", "maxHeight" );
 
// setter
$( ".selector" ).resizable( "option", "maxHeight", 300 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-maxWidth">
	<td>maxWidth</td>
	<td>Number</td>
	<td>resizable 允许被调整到的最大宽度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>maxWidth</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ maxWidth: 300 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>maxWidth</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var maxWidth = $( ".selector" ).resizable( "option", "maxWidth" );
 
// setter
$( ".selector" ).resizable( "option", "maxWidth", 300 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-minHeight">
	<td>minHeight</td>
	<td>Number</td>
	<td>resizable 允许被调整到的最小高度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>minHeight</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ minHeight: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>minHeight</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var minHeight = $( ".selector" ).resizable( "option", "minHeight" );
 
// setter
$( ".selector" ).resizable( "option", "minHeight", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>10</td>
</tr>
<tr id="option-minWidth">
	<td>minWidth</td>
	<td>Number</td>
	<td>resizable 允许被调整到的最小宽度。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>minWidth</code> 选项的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({ minWidth: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>minWidth</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var minWidth = $( ".selector" ).resizable( "option", "minWidth" );
 
// setter
$( ".selector" ).resizable( "option", "minWidth", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>10</td>
</tr>
</table></div>') where ID=6200;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 resizable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 resizable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 resizable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).resizable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 resizable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).resizable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 resizable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 resizable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 resizable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).resizable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 resizable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 resizable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 resizable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).resizable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 resizable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).resizable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 resizable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 resizable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 resizable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).resizable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6200;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>resizecreate</td>
	<td>当 resizable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 resizecreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "resizecreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-resize">
	<td>resize( event, ui )</td>
	<td>resize</td>
	<td>在调整尺寸期间当调整手柄拖拽时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>element</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示要被调整尺寸的元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被调整尺寸的助手（helper）。
		</li>
		<li><strong>originalElement</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被包裹之前的原始元素。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：resizable 调整前的位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalSize</strong><br />
		类型：Object<br />
		描述：resizable 调整前的尺寸，表示为 <code>{ width, height }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>size</strong><br />
		类型：Object<br />
		描述：当前尺寸，表示为 <code>{ width, height }</code>。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 resize 回调的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({
  resize: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 resize 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "resize", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-start">
	<td>start( event, ui )</td>
    <td>resizestart</td>
	<td>当调整尺寸操作开始时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>element</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示要被调整尺寸的元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被调整尺寸的助手（helper）。
		</li>
		<li><strong>originalElement</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被包裹之前的原始元素。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：resizable 调整前的位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalSize</strong><br />
		类型：Object<br />
		描述：resizable 调整前的尺寸，表示为 <code>{ width, height }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>size</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ width, height }</code>。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 start 回调的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({
  start: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 resizestart 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "resizestart", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-stop">
	<td>stop( event, ui )</td>
    <td>resizestop</td>
	<td>当调整尺寸操作停止时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>element</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示要被调整尺寸的元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被调整尺寸的助手（helper）。
		</li>
		<li><strong>originalElement</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被包裹之前的原始元素。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：resizable 调整前的位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalSize</strong><br />
		类型：Object<br />
		描述：resizable 调整前的尺寸，表示为 <code>{ width, height }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>size</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ width, height }</code>。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 stop 回调的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({
  stop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 resizestop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "resizestop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>resizecreate</td>
	<td>当 resizable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 resizecreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "resizecreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-resize">
	<td>resize( event, ui )</td>
	<td>resize</td>
	<td>在调整尺寸期间当调整手柄拖拽时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>element</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示要被调整尺寸的元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被调整尺寸的助手（helper）。
		</li>
		<li><strong>originalElement</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被包裹之前的原始元素。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：resizable 调整前的位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalSize</strong><br />
		类型：Object<br />
		描述：resizable 调整前的尺寸，表示为 <code>{ width, height }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>size</strong><br />
		类型：Object<br />
		描述：当前尺寸，表示为 <code>{ width, height }</code>。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 resize 回调的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({
  resize: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 resize 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "resize", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-start">
	<td>start( event, ui )</td>
    <td>resizestart</td>
	<td>当调整尺寸操作开始时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>element</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示要被调整尺寸的元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被调整尺寸的助手（helper）。
		</li>
		<li><strong>originalElement</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被包裹之前的原始元素。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：resizable 调整前的位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalSize</strong><br />
		类型：Object<br />
		描述：resizable 调整前的尺寸，表示为 <code>{ width, height }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>size</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ width, height }</code>。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 start 回调的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({
  start: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 resizestart 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "resizestart", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-stop">
	<td>stop( event, ui )</td>
    <td>resizestop</td>
	<td>当调整尺寸操作停止时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>element</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示要被调整尺寸的元素。
		</li>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被调整尺寸的助手（helper）。
		</li>
		<li><strong>originalElement</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被包裹之前的原始元素。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：resizable 调整前的位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalSize</strong><br />
		类型：Object<br />
		描述：resizable 调整前的尺寸，表示为 <code>{ width, height }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>size</strong><br />
		类型：Object<br />
		描述：当前位置，表示为 <code>{ width, height }</code>。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 stop 回调的 resizable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).resizable({
  stop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 resizestop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "resizestop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6200;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-appendTo">
	<td>appendTo</td>
	<td>Selector</td>
	<td>选择助手（套索）要被添加到哪一个元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>appendTo</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ appendTo: "#someElem" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>appendTo</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var appendTo = $( ".selector" ).selectable( "option", "appendTo" );
 
// setter
$( ".selector" ).selectable( "option", "appendTo", "#someElem" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"body"</td>
</tr>
<tr id="option-autoRefresh">
	<td>autoRefresh</td>
	<td>Boolean</td>
	<td>该选项决定是否在每个选择操作的开始时更新（重新计算）每个选择项的位置和尺寸。如果您有多个项目，您可能要设置该选项为 false，并手动调用 <code><a href="#method-refresh">refresh()</a></code> 方法。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>autoRefresh</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ autoRefresh: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>autoRefresh</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var autoRefresh = $( ".selector" ).selectable( "option", "autoRefresh" );
 
// setter
$( ".selector" ).selectable( "option", "autoRefresh", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
	<td>Selector</td>
	<td>防止从匹配选择器的元素上开始选择。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ cancel: "a,.cancel" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).selectable( "option", "cancel" );
 
// setter
$( ".selector" ).selectable( "option", "cancel", "a,.cancel" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Number</td>
	<td>鼠标按下后直到选择开始的时间，以毫秒计。该选项可以防止点击在某个元素上时不必要的选择。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ delay: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).selectable( "option", "delay" );
 
// setter
$( ".selector" ).selectable( "option", "delay", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 selectable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).selectable( "option", "disabled" );
 
// setter
$( ".selector" ).selectable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
    <td>Number</td>
	<td>鼠标按下后选择开始前必须移动的距离，以像素计。如果指定了该选项，选择只有在鼠标拖拽超出指定距离时才会开始。该选项可以防止点击在某个元素上时不必要的选择。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ distance: 30 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).selectable( "option", "distance" );
 
// setter
$( ".selector" ).selectable( "option", "distance", 30 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-filter">
	<td>filter</td>
	<td>Selector</td>
	<td>要制作选择项（可被选择的）的匹配的子元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>filter</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ filter: "li" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>filter</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var filter = $( ".selector" ).selectable( "option", "filter" );
 
// setter
$( ".selector" ).selectable( "option", "filter", "li" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"*"</td>
</tr>
<tr id="option-tolerance">
	<td>tolerance</td>
	<td>String</td>
	<td>指定用于测试套索是否选择一个项目的模式。可能的值：<br />
	<ul>
	<li><code>"fit"</code>：套索完全重叠在项目上。</li>
	<li><code>"touch"</code>：套索重叠在项目上，任何比例皆可。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>tolerance</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ tolerance: "fit" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>tolerance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var tolerance = $( ".selector" ).selectable( "option", "tolerance" );
 
// setter
$( ".selector" ).selectable( "option", "tolerance", "fit" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"touch"</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-appendTo">
	<td>appendTo</td>
	<td>Selector</td>
	<td>选择助手（套索）要被添加到哪一个元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>appendTo</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ appendTo: "#someElem" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>appendTo</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var appendTo = $( ".selector" ).selectable( "option", "appendTo" );
 
// setter
$( ".selector" ).selectable( "option", "appendTo", "#someElem" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"body"</td>
</tr>
<tr id="option-autoRefresh">
	<td>autoRefresh</td>
	<td>Boolean</td>
	<td>该选项决定是否在每个选择操作的开始时更新（重新计算）每个选择项的位置和尺寸。如果您有多个项目，您可能要设置该选项为 false，并手动调用 <code><a href="#method-refresh">refresh()</a></code> 方法。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>autoRefresh</code> 选项的 draggable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ autoRefresh: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>autoRefresh</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var autoRefresh = $( ".selector" ).selectable( "option", "autoRefresh" );
 
// setter
$( ".selector" ).selectable( "option", "autoRefresh", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
	<td>Selector</td>
	<td>防止从匹配选择器的元素上开始选择。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ cancel: "a,.cancel" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).selectable( "option", "cancel" );
 
// setter
$( ".selector" ).selectable( "option", "cancel", "a,.cancel" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Number</td>
	<td>鼠标按下后直到选择开始的时间，以毫秒计。该选项可以防止点击在某个元素上时不必要的选择。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ delay: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).selectable( "option", "delay" );
 
// setter
$( ".selector" ).selectable( "option", "delay", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 selectable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).selectable( "option", "disabled" );
 
// setter
$( ".selector" ).selectable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
    <td>Number</td>
	<td>鼠标按下后选择开始前必须移动的距离，以像素计。如果指定了该选项，选择只有在鼠标拖拽超出指定距离时才会开始。该选项可以防止点击在某个元素上时不必要的选择。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ distance: 30 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).selectable( "option", "distance" );
 
// setter
$( ".selector" ).selectable( "option", "distance", 30 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-filter">
	<td>filter</td>
	<td>Selector</td>
	<td>要制作选择项（可被选择的）的匹配的子元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>filter</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ filter: "li" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>filter</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var filter = $( ".selector" ).selectable( "option", "filter" );
 
// setter
$( ".selector" ).selectable( "option", "filter", "li" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"*"</td>
</tr>
<tr id="option-tolerance">
	<td>tolerance</td>
	<td>String</td>
	<td>指定用于测试套索是否选择一个项目的模式。可能的值：<br />
	<ul>
	<li><code>"fit"</code>：套索完全重叠在项目上。</li>
	<li><code>"touch"</code>：套索重叠在项目上，任何比例皆可。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>tolerance</code> 选项的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({ tolerance: "fit" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>tolerance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var tolerance = $( ".selector" ).selectable( "option", "tolerance" );
 
// setter
$( ".selector" ).selectable( "option", "tolerance", "fit" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"touch"</td>
</tr>
</table></div>') where ID=6201;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 selectable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 selectable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 selectable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).selectable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 selectable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).selectable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 selectable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 selectable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refresh">
	<td>refresh()</td>
	<td>jQuery (plugin only)</td>
	<td>更新每个选择项元素的位置和尺寸。当 <code><a href="#option-autoRefresh">autoRefresh</a></code> 选项被设置为 false 时，该方法可用于手动重新计算每个选择项的位置和尺寸。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refresh 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "refresh" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 selectable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).selectable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 selectable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 selectable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 selectable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).selectable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 selectable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).selectable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 selectable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 selectable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refresh">
	<td>refresh()</td>
	<td>jQuery (plugin only)</td>
	<td>更新每个选择项元素的位置和尺寸。当 <code><a href="#option-autoRefresh">autoRefresh</a></code> 选项被设置为 false 时，该方法可用于手动重新计算每个选择项的位置和尺寸。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refresh 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable( "refresh" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 selectable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).selectable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6201;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>selectablecreate</td>
	<td>当 selectable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectablecreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectablecreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-selected">
	<td>selected( event, ui )</td>
	<td>selectableselected</td>
	<td>当每个元素被添加选择时，在选择操作结尾触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>selected</strong><br />
		类型：Element<br />
		描述：被选择的可选择项目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 selected 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  selected: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectableselected 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectableselected", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-selecting">
	<td>selecting( event, ui )</td>
	<td>selectableselecting</td>
	<td>当每个元素被添加选择时，在选择操作期间触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>selecting</strong><br />
		类型：Element<br />
		描述：正被选择的当前可选择项目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 selecting 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  selecting: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectableselecting 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectableselecting", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-start">
	<td>start( event, ui )</td>
    <td>selectablestart</td>
	<td>在选择操作开头触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 start 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  start: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectablestart 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectablestart", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-stop">
	<td>stop( event, ui )</td>
    <td>selectablestop</td>
	<td>在选择操作结尾触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 stop 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  stop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectablestop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectablestop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-unselected">
	<td>unselected( event, ui )</td>
	<td>selectableunselected</td>
	<td>当每个元素从选择中被移除时，在选择操作结尾触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>unselected</strong><br />
		类型：Element<br />
		描述：未被选择的可选择项目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 unselected 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  unselected: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectableunselected 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectableunselected", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-unselecting">
	<td>unselecting( event, ui )</td>
	<td>selectableunselecting</td>
	<td>当每个元素从选择中被移除时，在选择操作期间触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>unselecting</strong><br />
		类型：Element<br />
		描述：正未被选择的当前可选择项目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 unselecting 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  unselecting: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectableunselecting 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectableunselecting", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>selectablecreate</td>
	<td>当 selectable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectablecreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectablecreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-selected">
	<td>selected( event, ui )</td>
	<td>selectableselected</td>
	<td>当每个元素被添加选择时，在选择操作结尾触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>selected</strong><br />
		类型：Element<br />
		描述：被选择的可选择项目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 selected 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  selected: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectableselected 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectableselected", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-selecting">
	<td>selecting( event, ui )</td>
	<td>selectableselecting</td>
	<td>当每个元素被添加选择时，在选择操作期间触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>selecting</strong><br />
		类型：Element<br />
		描述：正被选择的当前可选择项目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 selecting 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  selecting: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectableselecting 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectableselecting", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-start">
	<td>start( event, ui )</td>
    <td>selectablestart</td>
	<td>在选择操作开头触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 start 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  start: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectablestart 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectablestart", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-stop">
	<td>stop( event, ui )</td>
    <td>selectablestop</td>
	<td>在选择操作结尾触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 stop 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  stop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectablestop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectablestop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-unselected">
	<td>unselected( event, ui )</td>
	<td>selectableunselected</td>
	<td>当每个元素从选择中被移除时，在选择操作结尾触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>unselected</strong><br />
		类型：Element<br />
		描述：未被选择的可选择项目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 unselected 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  unselected: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectableunselected 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectableunselected", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-unselecting">
	<td>unselecting( event, ui )</td>
	<td>selectableunselecting</td>
	<td>当每个元素从选择中被移除时，在选择操作期间触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>unselecting</strong><br />
		类型：Element<br />
		描述：正未被选择的当前可选择项目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 unselecting 回调的 selectable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).selectable({
  unselecting: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 selectableunselecting 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "selectableunselecting", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6201;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-appendTo">
	<td>appendTo</td>
	<td>jQuery 或 Element 或 Selector 或 String</td>
	<td>当拖拽时，通过鼠标移动的助手被追加到哪里（例如，解决 overlap/zIndex 问题）。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>jQuery</strong>：一个 jQuery 对象，包含助手（helper）要追加到的元素。</li>
	<li><strong>Element</strong>：要追加助手（helper）的元素。</li>
	<li><strong>Selector</strong>：一个选择器，指定哪个元素要追加助手（helper）。</li>
	<li><strong>String</strong>：字符串 <code>"parent"</code> 将促使助手（helper）成为 sortable 项目的同级。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>appendTo</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ appendTo: document.body });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>appendTo</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var appendTo = $( ".selector" ).sortable( "option", "appendTo" );
 
// setter
$( ".selector" ).sortable( "option", "appendTo", document.body );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"parent"</td>
</tr>
<tr id="option-axis">
	<td>axis</td>
    <td>String</td>
	<td>如果定义了该选项，项目只能在水平或垂直方向上被拖拽。可能的值：<code>"x"</code>, <code>"y"</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>axis</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ axis: "x" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>axis</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var axis = $( ".selector" ).sortable( "option", "axis" );
 
// setter
$( ".selector" ).sortable( "option", "axis", "x" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
    <td>Selector</td>
	<td>防止从匹配选择器的元素上开始排序。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ cancel: "a,button" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).sortable( "option", "cancel" );
 
// setter
$( ".selector" ).sortable( "option", "cancel", "a,button" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-connectWith">
	<td>connectWith</td>
	<td>Selector</td>
	<td>列表中的项目需被连接的其他 sortable 元素的选择器。这是一个单向关系，如果您想要项目被双向连接，必须在两个 sortable 元素上都设置 <code>connectWith</code> 选项。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>connectWith</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ connectWith: "#shopping-cart" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>connectWith</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var connectWith = $( ".selector" ).sortable( "option", "connectWith" );
 
// setter
$( ".selector" ).sortable( "option", "connectWith", "#shopping-cart" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-containment">
	<td>containment</td>
    <td>Element 或 Selector 或 String</td>
	<td>定义拖拽时，sortable 项目被约束的边界。<br />
	<p>注释：为 containment 指定的元素必须有一个已计算的宽度和高度（尽管它不需要显式）。例如，如果您有 <code>float: left</code> sortable 子元素，并指定了 <code>containment: "parent"</code>，请确保在 sortable/parent 容器上有 <code>float: left</code> ，否则它将有 <code>height: 0</code>，导致未定义的行为。</p>
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Element</strong>：一个要作为容器使用的元素。</li>
	<li><strong>Selector</strong>：一个选择器，指定一个要作为容器使用的元素。</li>
	<li><strong>String</strong>：一个字符串，标识一个要作为容器使用的元素。可能的值：<code>"parent"</code>、<code>"document"</code>、<code>"window"</code>。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>containment</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ containment: "parent" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>containment</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var containment = $( ".selector" ).sortable( "option", "containment" );
 
// setter
$( ".selector" ).sortable( "option", "containment", "parent" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cursor">
	<td>cursor</td>
    <td>String</td>
	<td>定义当排序时被显示的光标。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cursor</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ cursor: "move" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cursor</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cursor = $( ".selector" ).sortable( "option", "cursor" );
 
// setter
$( ".selector" ).sortable( "option", "cursor", "move" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"auto"</td>
</tr>
<tr id="option-cursorAt">
	<td>cursorAt</td>
    <td>Object</td>
	<td>移动排序元素或助手（helper），这样光标总是出现，以便从相同的位置进行拖拽。坐标可通过一个或两个键的组合成一个哈希给出：<code>{ top, left, right, bottom }</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cursorAt</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ cursorAt: { left: 5 } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cursorAt</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cursorAt = $( ".selector" ).sortable( "option", "cursorAt" );
 
// setter
$( ".selector" ).sortable( "option", "cursorAt", { left: 5 } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Integer</td>
	<td>鼠标按下后直到排序开始的时间，以毫秒计。该选项可以防止点击在某个元素上时不必要的拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ delay: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).sortable( "option", "delay" );
 
// setter
$( ".selector" ).sortable( "option", "delay", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 sortable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).sortable( "option", "disabled" );
 
// setter
$( ".selector" ).sortable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
	<td>Number</td>
	<td>鼠标按下后排序开始前必须移动的距离，以像素计。如果指定了该选项，排序只有在鼠标拖拽超出指定距离时才会开始。该选项可以用于允许在一个手柄内的元素上点击。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ distance: 5 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).sortable( "option", "distance" );
 
// setter
$( ".selector" ).sortable( "option", "distance", 5 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
<tr id="option-dropOnEmpty">
	<td>dropOnEmpty</td>
	<td>Boolean</td>
	<td>如果为 <code>false</code>，从该 sortable 的项目不能被放置在空连接的 sortable 上（请查看 <code><a href="#option-connectWith">connectWith</a></code> 选项）。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>dropOnEmpty</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ dropOnEmpty: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>dropOnEmpty</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var dropOnEmpty = $( ".selector" ).sortable( "option", "dropOnEmpty" );
 
// setter
$( ".selector" ).sortable( "option", "dropOnEmpty", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-forceHelperSize">
	<td>forceHelperSize</td>
	<td>Boolean</td>
	<td>如果为 <code>true</code>，强制助手（helper）有一个尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>forceHelperSize</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ forceHelperSize: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>forceHelperSize</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var forceHelperSize = $( ".selector" ).sortable( "option", "forceHelperSize" );
 
// setter
$( ".selector" ).sortable( "option", "forceHelperSize", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-forcePlaceholderSize">
	<td>forcePlaceholderSize</td>
	<td>Boolean</td>
	<td>如果为 <code>true</code>，强制占位符（placeholder）有一个尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>forcePlaceholderSize</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ forcePlaceholderSize: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>forcePlaceholderSize</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var forcePlaceholderSize = $( ".selector" ).sortable( "option", "forcePlaceholderSize" );
 
// setter
$( ".selector" ).sortable( "option", "forcePlaceholderSize", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-grid">
	<td>grid</td>
    <td>Array</td>
	<td>对齐排序元素或助手（helper）到网格，每个 x 和 y 像素。数组形式必须是 <code>[ x, y ]</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>grid</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ grid: [ 20, 10 ] });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>grid</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var grid = $( ".selector" ).sortable( "option", "grid" );
 
// setter
$( ".selector" ).sortable( "option", "grid", [ 20, 10 ] );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-handle">
	<td>handle</td>
    <td>Selector 或 Element</td>
	<td>如果指定了该选项，则限制在指定的元素上开始排序。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>handle</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ handle: ".handle" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>handle</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var handle = $( ".selector" ).sortable( "option", "handle" );
 
// setter
$( ".selector" ).sortable( "option", "handle", ".handle" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-helper">
	<td>helper</td>
    <td>String 或 Function()</td>
	<td>允许一个 helper 元素用于拖拽显示。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>String</strong>：如果设置为 <code>"clone"</code>，元素将被克隆，且克隆将被拖拽。</li>
	<li><strong>Function</strong>：一个函数，将返回拖拽时要使用的 DOMElement。函数接收事件，且元素正被排序。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>helper</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ helper: "clone" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>helper</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var helper = $( ".selector" ).sortable( "option", "helper" );
 
// setter
$( ".selector" ).sortable( "option", "helper", "clone" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"original"</td>
</tr>
<tr id="option-items">
	<td>items</td>
	<td>Selector</td>
	<td>指定元素内的哪一个项目应是 sortable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>items</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ items: "&gt; li" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>items</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var items = $( ".selector" ).sortable( "option", "items" );
 
// setter
$( ".selector" ).sortable( "option", "items", "&gt; li" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"> *"</td>
</tr>
<tr id="option-opacity">
	<td>opacity</td>
    <td>Number</td>
	<td>当排序时助手（helper）的不透明度。从 <code>0.01</code> 到 <code>1</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>opacity</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ opacity: 0.5 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>opacity</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var opacity = $( ".selector" ).sortable( "option", "opacity" );
 
// setter
$( ".selector" ).sortable( "option", "opacity", 0.5 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-placeholder">
	<td>placeholder</td>
	<td>String</td>
	<td>要应用的 class 名称，否则为白色空白。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>placeholder</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ placeholder: "sortable-placeholder" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>placeholder</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var placeholder = $( ".selector" ).sortable( "option", "placeholder" );
 
// setter
$( ".selector" ).sortable( "option", "placeholder", "sortable-placeholder" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-revert">
	<td>revert</td>
    <td>Boolean 或 Number</td>
	<td>sortable 项目是否使用一个流畅的动画还原到它的新位置。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>true</code>，该项目将会使用动画，动画使用默认的持续时间。</li>
	<li><strong>Number</strong>：动画的持续时间，以毫秒计。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>revert</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ revert: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>revert</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var revert = $( ".selector" ).sortable( "option", "revert" );
 
// setter
$( ".selector" ).sortable( "option", "revert", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-scroll">
	<td>scroll</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，当到达边缘时页面会滚动。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scroll</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ scroll: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scroll</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scroll = $( ".selector" ).sortable( "option", "scroll" );
 
// setter
$( ".selector" ).sortable( "option", "scroll", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-scrollSensitivity">
	<td>scrollSensitivity</td>
    <td>Number</td>
	<td>定义鼠标距离边缘多少距离时开始滚动。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scrollSensitivity</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ scrollSensitivity: 10 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scrollSensitivity</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scrollSensitivity = $( ".selector" ).sortable( "option", "scrollSensitivity" );
 
// setter
$( ".selector" ).sortable( "option", "scrollSensitivity", 10 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-scrollSpeed">
	<td>scrollSpeed</td>
    <td>Number</td>
	<td>当鼠标指针获取到在 <code><a href="#option-scrollSensitivity">scrollSensitivity</a></code> 距离内时，窗体滚动的速度。如果 <code>scroll</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scrollSpeed</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ scrollSpeed: 40 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scrollSpeed</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scrollSpeed = $( ".selector" ).sortable( "option", "scrollSpeed" );
 
// setter
$( ".selector" ).sortable( "option", "scrollSpeed", 40 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-tolerance">
	<td>tolerance</td>
	<td>String</td>
	<td>指定用于测试项目被移动时是否覆盖在另一个项目上的模式。可能的值：<br />
	<ul>
	<li><code>"intersect"</code>：项目至少 50% 重叠在其他项目上。</li>
	<li><code>"pointer"</code>：鼠标指针重叠在其他项目上。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>tolerance</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ tolerance: "pointer" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>tolerance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var tolerance = $( ".selector" ).sortable( "option", "tolerance" );
 
// setter
$( ".selector" ).sortable( "option", "tolerance", "pointer" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"intersect"</td>
</tr>
<tr id="option-zIndex">
	<td>zIndex</td>
    <td>Integer</td>
	<td>当被排序时，元素/助手（helper）的 Z-index。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>zIndex</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ zIndex: 9999 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>zIndex</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var zIndex = $( ".selector" ).sortable( "option", "zIndex" );
 
// setter
$( ".selector" ).sortable( "option", "zIndex", 9999 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1000</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-appendTo">
	<td>appendTo</td>
	<td>jQuery 或 Element 或 Selector 或 String</td>
	<td>当拖拽时，通过鼠标移动的助手被追加到哪里（例如，解决 overlap/zIndex 问题）。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>jQuery</strong>：一个 jQuery 对象，包含助手（helper）要追加到的元素。</li>
	<li><strong>Element</strong>：要追加助手（helper）的元素。</li>
	<li><strong>Selector</strong>：一个选择器，指定哪个元素要追加助手（helper）。</li>
	<li><strong>String</strong>：字符串 <code>"parent"</code> 将促使助手（helper）成为 sortable 项目的同级。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>appendTo</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ appendTo: document.body });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>appendTo</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var appendTo = $( ".selector" ).sortable( "option", "appendTo" );
 
// setter
$( ".selector" ).sortable( "option", "appendTo", document.body );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"parent"</td>
</tr>
<tr id="option-axis">
	<td>axis</td>
    <td>String</td>
	<td>如果定义了该选项，项目只能在水平或垂直方向上被拖拽。可能的值：<code>"x"</code>, <code>"y"</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>axis</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ axis: "x" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>axis</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var axis = $( ".selector" ).sortable( "option", "axis" );
 
// setter
$( ".selector" ).sortable( "option", "axis", "x" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cancel">
	<td>cancel</td>
    <td>Selector</td>
	<td>防止从匹配选择器的元素上开始排序。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cancel</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ cancel: "a,button" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cancel</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cancel = $( ".selector" ).sortable( "option", "cancel" );
 
// setter
$( ".selector" ).sortable( "option", "cancel", "a,button" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"input, textarea, button, select, option"</td>
</tr>
<tr id="option-connectWith">
	<td>connectWith</td>
	<td>Selector</td>
	<td>列表中的项目需被连接的其他 sortable 元素的选择器。这是一个单向关系，如果您想要项目被双向连接，必须在两个 sortable 元素上都设置 <code>connectWith</code> 选项。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>connectWith</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ connectWith: "#shopping-cart" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>connectWith</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var connectWith = $( ".selector" ).sortable( "option", "connectWith" );
 
// setter
$( ".selector" ).sortable( "option", "connectWith", "#shopping-cart" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-containment">
	<td>containment</td>
    <td>Element 或 Selector 或 String</td>
	<td>定义拖拽时，sortable 项目被约束的边界。<br />
	<p>注释：为 containment 指定的元素必须有一个已计算的宽度和高度（尽管它不需要显式）。例如，如果您有 <code>float: left</code> sortable 子元素，并指定了 <code>containment: "parent"</code>，请确保在 sortable/parent 容器上有 <code>float: left</code> ，否则它将有 <code>height: 0</code>，导致未定义的行为。</p>
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Element</strong>：一个要作为容器使用的元素。</li>
	<li><strong>Selector</strong>：一个选择器，指定一个要作为容器使用的元素。</li>
	<li><strong>String</strong>：一个字符串，标识一个要作为容器使用的元素。可能的值：<code>"parent"</code>、<code>"document"</code>、<code>"window"</code>。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>containment</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ containment: "parent" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>containment</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var containment = $( ".selector" ).sortable( "option", "containment" );
 
// setter
$( ".selector" ).sortable( "option", "containment", "parent" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-cursor">
	<td>cursor</td>
    <td>String</td>
	<td>定义当排序时被显示的光标。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cursor</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ cursor: "move" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cursor</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cursor = $( ".selector" ).sortable( "option", "cursor" );
 
// setter
$( ".selector" ).sortable( "option", "cursor", "move" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"auto"</td>
</tr>
<tr id="option-cursorAt">
	<td>cursorAt</td>
    <td>Object</td>
	<td>移动排序元素或助手（helper），这样光标总是出现，以便从相同的位置进行拖拽。坐标可通过一个或两个键的组合成一个哈希给出：<code>{ top, left, right, bottom }</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>cursorAt</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ cursorAt: { left: 5 } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>cursorAt</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var cursorAt = $( ".selector" ).sortable( "option", "cursorAt" );
 
// setter
$( ".selector" ).sortable( "option", "cursorAt", { left: 5 } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Integer</td>
	<td>鼠标按下后直到排序开始的时间，以毫秒计。该选项可以防止点击在某个元素上时不必要的拖拽。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ delay: 150 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).sortable( "option", "delay" );
 
// setter
$( ".selector" ).sortable( "option", "delay", 150 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 sortable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).sortable( "option", "disabled" );
 
// setter
$( ".selector" ).sortable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-distance">
	<td>distance</td>
	<td>Number</td>
	<td>鼠标按下后排序开始前必须移动的距离，以像素计。如果指定了该选项，排序只有在鼠标拖拽超出指定距离时才会开始。该选项可以用于允许在一个手柄内的元素上点击。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>distance</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ distance: 5 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>distance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var distance = $( ".selector" ).sortable( "option", "distance" );
 
// setter
$( ".selector" ).sortable( "option", "distance", 5 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
<tr id="option-dropOnEmpty">
	<td>dropOnEmpty</td>
	<td>Boolean</td>
	<td>如果为 <code>false</code>，从该 sortable 的项目不能被放置在空连接的 sortable 上（请查看 <code><a href="#option-connectWith">connectWith</a></code> 选项）。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>dropOnEmpty</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ dropOnEmpty: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>dropOnEmpty</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var dropOnEmpty = $( ".selector" ).sortable( "option", "dropOnEmpty" );
 
// setter
$( ".selector" ).sortable( "option", "dropOnEmpty", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-forceHelperSize">
	<td>forceHelperSize</td>
	<td>Boolean</td>
	<td>如果为 <code>true</code>，强制助手（helper）有一个尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>forceHelperSize</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ forceHelperSize: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>forceHelperSize</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var forceHelperSize = $( ".selector" ).sortable( "option", "forceHelperSize" );
 
// setter
$( ".selector" ).sortable( "option", "forceHelperSize", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-forcePlaceholderSize">
	<td>forcePlaceholderSize</td>
	<td>Boolean</td>
	<td>如果为 <code>true</code>，强制占位符（placeholder）有一个尺寸。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>forcePlaceholderSize</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ forcePlaceholderSize: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>forcePlaceholderSize</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var forcePlaceholderSize = $( ".selector" ).sortable( "option", "forcePlaceholderSize" );
 
// setter
$( ".selector" ).sortable( "option", "forcePlaceholderSize", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-grid">
	<td>grid</td>
    <td>Array</td>
	<td>对齐排序元素或助手（helper）到网格，每个 x 和 y 像素。数组形式必须是 <code>[ x, y ]</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>grid</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ grid: [ 20, 10 ] });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>grid</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var grid = $( ".selector" ).sortable( "option", "grid" );
 
// setter
$( ".selector" ).sortable( "option", "grid", [ 20, 10 ] );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-handle">
	<td>handle</td>
    <td>Selector 或 Element</td>
	<td>如果指定了该选项，则限制在指定的元素上开始排序。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>handle</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ handle: ".handle" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>handle</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var handle = $( ".selector" ).sortable( "option", "handle" );
 
// setter
$( ".selector" ).sortable( "option", "handle", ".handle" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-helper">
	<td>helper</td>
    <td>String 或 Function()</td>
	<td>允许一个 helper 元素用于拖拽显示。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>String</strong>：如果设置为 <code>"clone"</code>，元素将被克隆，且克隆将被拖拽。</li>
	<li><strong>Function</strong>：一个函数，将返回拖拽时要使用的 DOMElement。函数接收事件，且元素正被排序。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>helper</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ helper: "clone" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>helper</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var helper = $( ".selector" ).sortable( "option", "helper" );
 
// setter
$( ".selector" ).sortable( "option", "helper", "clone" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"original"</td>
</tr>
<tr id="option-items">
	<td>items</td>
	<td>Selector</td>
	<td>指定元素内的哪一个项目应是 sortable。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>items</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ items: "&gt; li" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>items</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var items = $( ".selector" ).sortable( "option", "items" );
 
// setter
$( ".selector" ).sortable( "option", "items", "&gt; li" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"> *"</td>
</tr>
<tr id="option-opacity">
	<td>opacity</td>
    <td>Number</td>
	<td>当排序时助手（helper）的不透明度。从 <code>0.01</code> 到 <code>1</code>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>opacity</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ opacity: 0.5 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>opacity</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var opacity = $( ".selector" ).sortable( "option", "opacity" );
 
// setter
$( ".selector" ).sortable( "option", "opacity", 0.5 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-placeholder">
	<td>placeholder</td>
	<td>String</td>
	<td>要应用的 class 名称，否则为白色空白。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>placeholder</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ placeholder: "sortable-placeholder" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>placeholder</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var placeholder = $( ".selector" ).sortable( "option", "placeholder" );
 
// setter
$( ".selector" ).sortable( "option", "placeholder", "sortable-placeholder" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-revert">
	<td>revert</td>
    <td>Boolean 或 Number</td>
	<td>sortable 项目是否使用一个流畅的动画还原到它的新位置。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>true</code>，该项目将会使用动画，动画使用默认的持续时间。</li>
	<li><strong>Number</strong>：动画的持续时间，以毫秒计。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>revert</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ revert: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>revert</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var revert = $( ".selector" ).sortable( "option", "revert" );
 
// setter
$( ".selector" ).sortable( "option", "revert", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-scroll">
	<td>scroll</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，当到达边缘时页面会滚动。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scroll</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ scroll: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scroll</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scroll = $( ".selector" ).sortable( "option", "scroll" );
 
// setter
$( ".selector" ).sortable( "option", "scroll", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
<tr id="option-scrollSensitivity">
	<td>scrollSensitivity</td>
    <td>Number</td>
	<td>定义鼠标距离边缘多少距离时开始滚动。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scrollSensitivity</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ scrollSensitivity: 10 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scrollSensitivity</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scrollSensitivity = $( ".selector" ).sortable( "option", "scrollSensitivity" );
 
// setter
$( ".selector" ).sortable( "option", "scrollSensitivity", 10 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-scrollSpeed">
	<td>scrollSpeed</td>
    <td>Number</td>
	<td>当鼠标指针获取到在 <code><a href="#option-scrollSensitivity">scrollSensitivity</a></code> 距离内时，窗体滚动的速度。如果 <code>scroll</code> 选项是 <code>false</code> 则忽略。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>scrollSpeed</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ scrollSpeed: 40 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>scrollSpeed</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var scrollSpeed = $( ".selector" ).sortable( "option", "scrollSpeed" );
 
// setter
$( ".selector" ).sortable( "option", "scrollSpeed", 40 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>20</td>
</tr>
<tr id="option-tolerance">
	<td>tolerance</td>
	<td>String</td>
	<td>指定用于测试项目被移动时是否覆盖在另一个项目上的模式。可能的值：<br />
	<ul>
	<li><code>"intersect"</code>：项目至少 50% 重叠在其他项目上。</li>
	<li><code>"pointer"</code>：鼠标指针重叠在其他项目上。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>tolerance</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ tolerance: "pointer" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>tolerance</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var tolerance = $( ".selector" ).sortable( "option", "tolerance" );
 
// setter
$( ".selector" ).sortable( "option", "tolerance", "pointer" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"intersect"</td>
</tr>
<tr id="option-zIndex">
	<td>zIndex</td>
    <td>Integer</td>
	<td>当被排序时，元素/助手（helper）的 Z-index。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>zIndex</code> 选项的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({ zIndex: 9999 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>zIndex</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var zIndex = $( ".selector" ).sortable( "option", "zIndex" );
 
// setter
$( ".selector" ).sortable( "option", "zIndex", 9999 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1000</td>
</tr>
</table></div>') where ID=6202;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-cancel">
	<td>cancel()</td>
	<td>jQuery (plugin only)</td>
	<td>当前排序开始时，取消一个在当前 sortable 中的改变，且恢复到之前的状态。在 stop 和 receive 回调函数中非常有用。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 cancel 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "cancel" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 sortable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 sortable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 sortable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).sortable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 draggable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).sortable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 sortable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 sortable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refresh">
	<td>refresh()</td>
	<td>jQuery (plugin only)</td>
	<td>刷新 sortable 项目。触发所有 sortable 项目重新加载，导致新的项目被认可。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refresh 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "refresh" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refreshPositions">
	<td>refreshPositions()</td>
	<td>jQuery (plugin only)</td>
	<td>刷新 sortable 项目的缓存位置。调用该方法刷新所有 sortable 的已缓存的项目位置。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refreshPositions 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "refreshPositions" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-serialize">
	<td>serialize( options )</td>
	<td>String</td>
	<td>序列化 sortable 的项目 <code>id</code> 为一个 form/ajax 可提交的字符串。调用该方法会产生一个可被追加到任何 url 中的哈希，以便简单地把一个新的项目顺序提交回服务器。<br />
	<p>默认情况下，它通过每个项目的 <code>id</code> 进行工作，id 格式为 <code>"setname_number"</code>，且它会产生一个形如 <code>"setname[]=number&setname[]=number"</code> 的哈希。</p>
	<p>注释：如果序列化返回一个空的字符串，请确认 <code>id</code> 属性包含一个下划线（_）。形式必须是 <code>"set_number"</code>。例如，一个 <code>id</code> 属性为 <code>"foo_1"</code>、<code>"foo_5"</code>、<code>"foo_2"</code> 的 3 元素列表将序列化为 <code>"foo[]=1&foo[]=5&foo[]=2"</code>。您可以使用下划线（_）、等号（=）或连字符（-）来分隔集合和数字。例如，<code>"foo=1"</code>、<code>"foo-1"</code>、<code>"foo_1"</code> 所有都序列化为 <code>"foo[]=1"</code>。</p>
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	要自定义序列化的选项。
		<ul>
		<li><strong>key</strong>（默认值：<code>属性中分隔符前面的部分</code>）<br />
		类型：String<br />
		描述：把 <code>part1[]</code> 替换为指定的值。
		</li>
		<li><strong>attribute</strong>（默认值：<code>"id"</code>）<br />
		类型：String<br />
		描述：值要使用的属性名称。
		</li>
		<li><strong>expression</strong>（默认值：<code>/(.+)[-=_](.+)/</code>）<br />
		类型：RegExp<br />
		描述：用于把属性值分隔为键和值两部分的正则表达式。
		</li>
		</ul>	
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 serialize 方法：</p>
	<pre style="white-space: pre-wrap;">
var sorted = $( ".selector" ).sortable( "serialize", { key: "sort" } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-toArray">
	<td>toArray( options )</td>
	<td>Array</td>
	<td>序列化 sortable 的项目 <code>id</code> 为一个字符串的数组。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	要自定义序列化的选项。
		<ul>
		<li><strong>attribute</strong>（默认值：<code>"id"</code>）<br />
		类型：String<br />
		描述：值要使用的属性名称。
		</li>
		</ul>	
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 toArray 方法：</p>
	<pre style="white-space: pre-wrap;">
var sortedIDs = $( ".selector" ).sortable( "toArray" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 sortable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).sortable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-cancel">
	<td>cancel()</td>
	<td>jQuery (plugin only)</td>
	<td>当前排序开始时，取消一个在当前 sortable 中的改变，且恢复到之前的状态。在 stop 和 receive 回调函数中非常有用。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 cancel 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "cancel" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 sortable 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 sortable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 sortable。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).sortable( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 draggable 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).sortable( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 sortable 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 sortable 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refresh">
	<td>refresh()</td>
	<td>jQuery (plugin only)</td>
	<td>刷新 sortable 项目。触发所有 sortable 项目重新加载，导致新的项目被认可。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refresh 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "refresh" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refreshPositions">
	<td>refreshPositions()</td>
	<td>jQuery (plugin only)</td>
	<td>刷新 sortable 项目的缓存位置。调用该方法刷新所有 sortable 的已缓存的项目位置。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refreshPositions 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable( "refreshPositions" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-serialize">
	<td>serialize( options )</td>
	<td>String</td>
	<td>序列化 sortable 的项目 <code>id</code> 为一个 form/ajax 可提交的字符串。调用该方法会产生一个可被追加到任何 url 中的哈希，以便简单地把一个新的项目顺序提交回服务器。<br />
	<p>默认情况下，它通过每个项目的 <code>id</code> 进行工作，id 格式为 <code>"setname_number"</code>，且它会产生一个形如 <code>"setname[]=number&setname[]=number"</code> 的哈希。</p>
	<p>注释：如果序列化返回一个空的字符串，请确认 <code>id</code> 属性包含一个下划线（_）。形式必须是 <code>"set_number"</code>。例如，一个 <code>id</code> 属性为 <code>"foo_1"</code>、<code>"foo_5"</code>、<code>"foo_2"</code> 的 3 元素列表将序列化为 <code>"foo[]=1&foo[]=5&foo[]=2"</code>。您可以使用下划线（_）、等号（=）或连字符（-）来分隔集合和数字。例如，<code>"foo=1"</code>、<code>"foo-1"</code>、<code>"foo_1"</code> 所有都序列化为 <code>"foo[]=1"</code>。</p>
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	要自定义序列化的选项。
		<ul>
		<li><strong>key</strong>（默认值：<code>属性中分隔符前面的部分</code>）<br />
		类型：String<br />
		描述：把 <code>part1[]</code> 替换为指定的值。
		</li>
		<li><strong>attribute</strong>（默认值：<code>"id"</code>）<br />
		类型：String<br />
		描述：值要使用的属性名称。
		</li>
		<li><strong>expression</strong>（默认值：<code>/(.+)[-=_](.+)/</code>）<br />
		类型：RegExp<br />
		描述：用于把属性值分隔为键和值两部分的正则表达式。
		</li>
		</ul>	
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 serialize 方法：</p>
	<pre style="white-space: pre-wrap;">
var sorted = $( ".selector" ).sortable( "serialize", { key: "sort" } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-toArray">
	<td>toArray( options )</td>
	<td>Array</td>
	<td>序列化 sortable 的项目 <code>id</code> 为一个字符串的数组。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	要自定义序列化的选项。
		<ul>
		<li><strong>attribute</strong>（默认值：<code>"id"</code>）<br />
		类型：String<br />
		描述：值要使用的属性名称。
		</li>
		</ul>	
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 toArray 方法：</p>
	<pre style="white-space: pre-wrap;">
var sortedIDs = $( ".selector" ).sortable( "toArray" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 sortable 元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).sortable( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6202;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-activate">
	<td>activate( event, ui )</td>
	<td>sortactivate</td>
	<td>当使用被连接列表时触发该事件，每个被连接列表在拖拽开始时接收它。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 activate 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
activate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-beforeStop">
	<td>beforeStop( event, ui )</td>
	<td>sortbeforestop</td>
	<td>当排序停止时触发该事件，除了当占位符（placeholder）/助手（helper）仍然可用时。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 beforeStop 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
beforeStop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortbeforestop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortbeforestop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-change">
	<td>change( event, ui )</td>
	<td>sortchange</td>
	<td>在排序期间触发该事件，除了当 DOM 位置改变时。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 change 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
change: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortchange 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortchange", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
	<td>sortcreate</td>
	<td>当 droppable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortcreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortcreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-deactivate">
	<td>deactivate( event, ui )</td>
	<td>sortdeactivate</td>
	<td>当排序停止时触发该事件，该事件传播到所有可能的连接列表。。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 deactivate 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
deactivate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortdeactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortdeactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-out">
	<td>out( event, ui )</td>
	<td>sortout</td>
	<td>当一个 sortable 项目从一个 sortable 列表移除时触发该事件。<br />
	<p>注释：当一个 sortable 项目被撤销时也会触发该事件。</p>
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 out 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
out: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortout 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortout", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-over">
	<td>over( event, ui )</td>
	<td>sortover</td>
	<td>当一个 sortable 项目移动到一个 sortable 列表时触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 over 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
out: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortover 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortout", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-receive">
	<td>receive( event, ui )</td>
	<td>sortreceive</td>
	<td>当来自一个连接的 sortable 列表的一个项目被放置到另一个列表时触发该事件。后者是事件目标。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 receive 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
receive: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortreceive 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortreceive", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-remove">
	<td>remove( event, ui )</td>
	<td>sortremove</td>
	<td>当来自一个连接的 sortable 列表的一个项目被放置到另一个列表时触发该事件。前者是事件目标。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 remove 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
remove: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortremove 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortremove", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-sort">
	<td>sort( event, ui )</td>
	<td>sort</td>
	<td>在排序期间触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 sort 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
sort: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sort 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sort", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-start">
	<td>start( event, ui )</td>
	<td>sortstart</td>
	<td>当排序开始时触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 start 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
start: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortstart 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortstart", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-stop">
	<td>stop( event, ui )</td>
	<td>sortstop</td>
	<td>当排序停止时触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 stop 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
stop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortstop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortstop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-update">
	<td>update( event, ui )</td>
	<td>sortupdate</td>
	<td>当用户停止排序且 DOM 位置改变时触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 update 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
update: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortupdate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortupdate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-activate">
	<td>activate( event, ui )</td>
	<td>sortactivate</td>
	<td>当使用被连接列表时触发该事件，每个被连接列表在拖拽开始时接收它。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 activate 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
activate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-beforeStop">
	<td>beforeStop( event, ui )</td>
	<td>sortbeforestop</td>
	<td>当排序停止时触发该事件，除了当占位符（placeholder）/助手（helper）仍然可用时。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 beforeStop 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
beforeStop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortbeforestop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortbeforestop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-change">
	<td>change( event, ui )</td>
	<td>sortchange</td>
	<td>在排序期间触发该事件，除了当 DOM 位置改变时。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 change 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
change: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortchange 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortchange", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
	<td>sortcreate</td>
	<td>当 droppable 被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortcreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortcreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-deactivate">
	<td>deactivate( event, ui )</td>
	<td>sortdeactivate</td>
	<td>当排序停止时触发该事件，该事件传播到所有可能的连接列表。。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 deactivate 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
deactivate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortdeactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortdeactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-out">
	<td>out( event, ui )</td>
	<td>sortout</td>
	<td>当一个 sortable 项目从一个 sortable 列表移除时触发该事件。<br />
	<p>注释：当一个 sortable 项目被撤销时也会触发该事件。</p>
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 out 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
out: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortout 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortout", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-over">
	<td>over( event, ui )</td>
	<td>sortover</td>
	<td>当一个 sortable 项目移动到一个 sortable 列表时触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 over 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
out: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortover 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortout", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-receive">
	<td>receive( event, ui )</td>
	<td>sortreceive</td>
	<td>当来自一个连接的 sortable 列表的一个项目被放置到另一个列表时触发该事件。后者是事件目标。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 receive 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
receive: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortreceive 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortreceive", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-remove">
	<td>remove( event, ui )</td>
	<td>sortremove</td>
	<td>当来自一个连接的 sortable 列表的一个项目被放置到另一个列表时触发该事件。前者是事件目标。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 remove 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
remove: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortremove 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortremove", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-sort">
	<td>sort( event, ui )</td>
	<td>sort</td>
	<td>在排序期间触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 sort 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
sort: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sort 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sort", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-start">
	<td>start( event, ui )</td>
	<td>sortstart</td>
	<td>当排序开始时触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 start 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
start: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortstart 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortstart", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-stop">
	<td>stop( event, ui )</td>
	<td>sortstop</td>
	<td>当排序停止时触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 stop 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
stop: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortstop 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortstop", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-update">
	<td>update( event, ui )</td>
	<td>sortupdate</td>
	<td>当用户停止排序且 DOM 位置改变时触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>helper</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被排序的助手（helper）。
		</li>
		<li><strong>item</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示当前被拖拽的元素。
		</li>
		<li><strong>offset</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前的绝对位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>position</strong><br />
		类型：Object<br />
		描述：助手（helper）的当前位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>originalPosition</strong><br />
		类型：Object<br />
		描述：元素的原始位置，表示为 <code>{ top, left }</code>。
		</li>
		<li><strong>sender</strong><br />
		类型：jQuery<br />
		描述：如果从一个 sortable 移动到另一个 sortable，项目来自的那个 sortable。
		</li>
		<li><strong>placeholder</strong><br />
		类型：jQuery<br />
		描述：jQuery 对象，表示被作为占位符使用的元素。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 update 回调的 sortable：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).sortable({
update: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 sortupdate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "sortupdate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6202;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-disable">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该小部件。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的小部件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).widget({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).widget( "option", "disabled" );
 
// setter
$( ".selector" ).widget( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-hide">
	<td>hide</td>
	<td>Boolean 或 Number 或 String 或 Object</td>
	<td>是否使用动画隐藏元素，以及如何动画隐藏元素。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>false</code> 时，则不使用动画，元素会立即隐藏。当设置为 <code>true</code> 时，元素会使用默认的持续时间和默认的 easing 淡出。</li>
	<li><strong>Number</strong>：元素将使用指定的持续时间和默认的 easing 淡出。</li>
	<li><strong>String</strong>：元素将使用指定的特效隐藏。该值可以是一个内建的 jQuery 动画方法名称，比如 <code>"slideUp"</code>，也可以是一个 <a href="/jqueryui/api-category-effects.html">jQuery UI 特效</a> 的名称，比如<code>"fold"</code>。以上两种情况的特效将使用默认的持续时间和默认的 easing。</li>
	<li><strong>Object</strong>：如果值是一个对象，则 <code>effect</code>、<code>delay</code>、<code>duration</code> 和 <code>easing</code> 属性会被提供。如果 <code>effect</code> 属性包含 jQuery 方法的名称，则使用该方法，否则，它被认为是一个 jQuery UI 特效的名称。当使用一个支持额外设置的 jQuery UI 特效时，您可以在对象中包含这些设置，且它们会被传递给特效。如果 <code>duration</code> 或 <code>easing</code> 被省略，则使用默认值。如果 <code>effect</code> 被省略，则使用 <code>"fadeOut"</code>。如果 <code>delay</code> 被省略，则不使用延迟。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>hide</code> 选项的小部件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).widget({ hide: { effect: "explode", duration: 1000 } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>hide</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var hide = $( ".selector" ).widget( "option", "hide" );
 
// setter
$( ".selector" ).widget( "option", "hide", { effect: "explode", duration: 1000 } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-show">
	<td>show</td>
	<td>Boolean 或 Number 或 String 或 Object</td>
	<td>是否使用动画显示元素，以及如何动画显示元素。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>false</code> 时，则不使用动画，元素会立即显示。当设置为 <code>true</code> 时，元素会使用默认的持续时间和默认的 easing 淡入。</li>
	<li><strong>Number</strong>：元素将使用指定的持续时间和默认的 easing 淡入。</li>
	<li><strong>String</strong>：元素将使用指定的特效显示。该值可以是一个内建的 jQuery 动画方法名称，比如 <code>"slideDown"</code>，也可以是一个 <a href="/jqueryui/api-category-effects.html">jQuery UI 特效</a> 的名称，比如<code>"fold"</code>。以上两种情况的特效将使用默认的持续时间和默认的 easing。</li>
	<li><strong>Object</strong>：如果值是一个对象，则 <code>effect</code>、<code>delay</code>、<code>duration</code> 和 <code>easing</code> 属性会被提供。如果 <code>effect</code> 属性包含 jQuery 方法的名称，则使用该方法，否则，它被认为是一个 jQuery UI 特效的名称。当使用一个支持额外设置的 jQuery UI 特效时，您可以在对象中包含这些设置，且它们会被传递给特效。如果 <code>duration</code> 或 <code>easing</code> 被省略，则使用默认值。如果 <code>effect</code> 被省略，则使用 <code>"fadeIn"</code>。如果 <code>delay</code> 被省略，则不使用延迟。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>show</code> 选项的小部件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).widget({ show: { effect: "blind", duration: 800 } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>show</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var show = $( ".selector" ).widget( "option", "show" );
 
// setter
$( ".selector" ).widget( "option", "show", { effect: "blind", duration: 800 } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-disable">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该小部件。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的小部件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).widget({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).widget( "option", "disabled" );
 
// setter
$( ".selector" ).widget( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-hide">
	<td>hide</td>
	<td>Boolean 或 Number 或 String 或 Object</td>
	<td>是否使用动画隐藏元素，以及如何动画隐藏元素。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>false</code> 时，则不使用动画，元素会立即隐藏。当设置为 <code>true</code> 时，元素会使用默认的持续时间和默认的 easing 淡出。</li>
	<li><strong>Number</strong>：元素将使用指定的持续时间和默认的 easing 淡出。</li>
	<li><strong>String</strong>：元素将使用指定的特效隐藏。该值可以是一个内建的 jQuery 动画方法名称，比如 <code>"slideUp"</code>，也可以是一个 <a href="/jqueryui/api-category-effects.html">jQuery UI 特效</a> 的名称，比如<code>"fold"</code>。以上两种情况的特效将使用默认的持续时间和默认的 easing。</li>
	<li><strong>Object</strong>：如果值是一个对象，则 <code>effect</code>、<code>delay</code>、<code>duration</code> 和 <code>easing</code> 属性会被提供。如果 <code>effect</code> 属性包含 jQuery 方法的名称，则使用该方法，否则，它被认为是一个 jQuery UI 特效的名称。当使用一个支持额外设置的 jQuery UI 特效时，您可以在对象中包含这些设置，且它们会被传递给特效。如果 <code>duration</code> 或 <code>easing</code> 被省略，则使用默认值。如果 <code>effect</code> 被省略，则使用 <code>"fadeOut"</code>。如果 <code>delay</code> 被省略，则不使用延迟。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>hide</code> 选项的小部件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).widget({ hide: { effect: "explode", duration: 1000 } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>hide</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var hide = $( ".selector" ).widget( "option", "hide" );
 
// setter
$( ".selector" ).widget( "option", "hide", { effect: "explode", duration: 1000 } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-show">
	<td>show</td>
	<td>Boolean 或 Number 或 String 或 Object</td>
	<td>是否使用动画显示元素，以及如何动画显示元素。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：当设置为 <code>false</code> 时，则不使用动画，元素会立即显示。当设置为 <code>true</code> 时，元素会使用默认的持续时间和默认的 easing 淡入。</li>
	<li><strong>Number</strong>：元素将使用指定的持续时间和默认的 easing 淡入。</li>
	<li><strong>String</strong>：元素将使用指定的特效显示。该值可以是一个内建的 jQuery 动画方法名称，比如 <code>"slideDown"</code>，也可以是一个 <a href="/jqueryui/api-category-effects.html">jQuery UI 特效</a> 的名称，比如<code>"fold"</code>。以上两种情况的特效将使用默认的持续时间和默认的 easing。</li>
	<li><strong>Object</strong>：如果值是一个对象，则 <code>effect</code>、<code>delay</code>、<code>duration</code> 和 <code>easing</code> 属性会被提供。如果 <code>effect</code> 属性包含 jQuery 方法的名称，则使用该方法，否则，它被认为是一个 jQuery UI 特效的名称。当使用一个支持额外设置的 jQuery UI 特效时，您可以在对象中包含这些设置，且它们会被传递给特效。如果 <code>duration</code> 或 <code>easing</code> 被省略，则使用默认值。如果 <code>effect</code> 被省略，则使用 <code>"fadeIn"</code>。如果 <code>delay</code> 被省略，则不使用延迟。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>show</code> 选项的小部件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).widget({ show: { effect: "blind", duration: 800 } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>show</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var show = $( ".selector" ).widget( "option", "show" );
 
// setter
$( ".selector" ).widget( "option", "show", { effect: "blind", duration: 800 } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
</table></div>') where ID=6218;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-_create">
	<td>_create()</td>
	<td>jQuery (plugin only)</td>
	<td><code>_create()</code> 方法是小部件的构造函数。没有参数，但是 <code>this.element</code> 和 <code>this.options</code> 已经设置。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>基于一个选项设置小部件元素的背景颜色。</p>
	<pre style="white-space: pre-wrap;">
_create: function() {
  this.element.css( "background-color", this.options.color );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_delay">
	<td>_delay( fn [, delay ] )</td>
	<td>Number</td>
	<td>在指定延迟后调用提供的函数。保持 <code>this</code> 上下文正确。本质上是 <code>setTimeout()</code>。<br />
	使用 <code>clearTimeout()</code> 返回超时 ID。<br />
	<ul>
	<li><strong>fn</strong><br />
	类型：Function() 或 String<br />
	描述：要调用的函数。也可以是小部件上方法的名称。
	</li>
	<li><strong>delay</strong><br />
	类型：Number<br />
	描述：调用函数前等待的毫秒数，默认为 <code>0</code>。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>100 毫秒后在小部件上调用 <code>_foo()</code> 方法。</p>
	<pre style="white-space: pre-wrap;">
this._delay( this._foo, 100 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_destroy">
	<td>_destroy()</td>
	<td>jQuery (plugin only)</td>
	<td>公共的 <a href="#method-destroy"><code>destroy()</code></a> 方法清除所有的公共数据、事件等等。代表了定制、指定小部件、清理的 <code>_destroy()</code>。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当小部件被销毁时，从小部件的元素移除一个 class。</p>
	<pre style="white-space: pre-wrap;">
_destroy: function() {
  this.element.removeClass( "my-widget" );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_focusable">
	<td>_focusable( element )</td>
	<td>jQuery (plugin only)</td>
	<td>建立聚焦在元素上时要应用 <code>ui-state-focus</code> class 的 <code>element</code>。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要应用 focusable 行为的元素。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>向小部件内的一组元素应用 focusable 样式：</p>
	<pre style="white-space: pre-wrap;">
this._focusable( this.element.find( ".my-items" ) );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_getCreateEventData">
	<td>_getCreateEventData()</td>
	<td>Object</td>
	<td>所有的小部件触发 <a href="#event-create"><code>create</code></a> 事件。默认情况下，事件中不提供任何的数据，但是该方法会返回一个对象，作为 <code>create</code> 事件的数据被传递。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>向 <code>create</code> 事件处理程序传递小部件的选项，作为参数。</p>
	<pre style="white-space: pre-wrap;">
_getCreateEventData: function() {
  return this.options;
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_getCreateOptions">
	<td>_getCreateOptions()</td>
	<td>Object</td>
	<td>该方法允许小部件在初始化期间为定义选项定义一个自定义的方法。用户提供的选项会覆盖该方法返回的选项，即会覆盖默认的选项。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>让小部件元素的 id 属性作为选项可用。</p>
	<pre style="white-space: pre-wrap;">
_getCreateOptions: function() {
  return { id: this.element.attr( "id" ) };
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_hide">
	<td>_hide( element, option [, callback ] )</td>
	<td>jQuery (plugin only)</td>
	<td>使用内置的动画方法或使用自定义的特效隐藏一个元素。如需了解可能的 <code>option</code> 值，请查看 <a href="#option-hide">hide</a>。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要隐藏的元素。
	</li>
	<li><strong>option</strong><br />
	类型：Object<br />
	描述：定义如何隐藏元素的设置。
	</li>
	<li><strong>callback</strong><br />
	类型：Function()<br />
	描述：元素完全隐藏后要调用的回调。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>为自定义动画传递 <code>hide</code> 选项。</p>
	<pre style="white-space: pre-wrap;">
this._hide( this.element, this.options.hide, function() {
 
  // Remove the element from the DOM when it's fully hidden.
  $( this ).remove();
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_hoverable">
	<td>_hoverable( element )</td>
	<td>jQuery (plugin only)</td>
	<td>建立悬浮在元素上时要应用 <code>ui-state-hover</code> class 的 <code>element</code>。事件处理程序在销毁时自动清理。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要应用 hoverable 行为的元素。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当悬浮在元素上时，向元素内所有的 <code>div</code> 应用 hoverable 样式。</p>
	<pre style="white-space: pre-wrap;">
this._hoverable( this.element.find( "div" ) );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_init">
	<td>_init()</td>
	<td>jQuery (plugin only)</td>
	<td>小部件初始化的理念与创建不同。任何时候不带参数的调用插件或者只带一个选项哈希的调用插件，初始化小部件。当小部件被创建时会包含这个方法。
	<p>注释：如果存在不带参数成功调用小部件时要执行的逻辑动作，初始化只能在这时处理。</p>
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>如果设置了 <code>autoOpen</code> 选项，则调用 <code>open()</code> 方法。</p>
	<pre style="white-space: pre-wrap;">
_init: function() {
  if ( this.options.autoOpen ) {
    this.open();
  }
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_off">
	<td>_off( element, eventName )</td>
	<td>jQuery (plugin only)</td>
	<td>从指定的元素取消绑定事件处理程序。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要取消绑定事件处理程序的元素。不像 <code>_on()</code> 方法，<code>_off()</code> 方法中元素是必需的。
	</li>
	<li><strong>eventName</strong><br />
	类型：String<br />
	描述：一个或多个空格分隔的事件类型。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>从小部件的元素上取消绑定所有 click 事件。</p>
	<pre style="white-space: pre-wrap;">
this._off( this.element, "click" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_on">
	<td>_on( [suppressDisabledCheck ] [, element ], handlers )</td>
	<td>jQuery (plugin only)</td>
	<td>授权通过事件名称内的选择器被支持，例如 <code>"click .foo"</code>。<code>_on()</code> 方法提供了一些直接事件绑定的好处：<br />
	<ul>
	<li>保持处理程序内适当的 <code>this</code> 上下文。
	</li>
	<li>自动处理禁用的部件：如果小部件被禁用或事件发生在带有 <code>ui-state-disabled</code> class 的元素上，则不调用事件处理程序。可以被 <code>suppressDisabledCheck</code> 参数重写。
	</li>
	<li>事件处理程序会自动添加命名空间，在销毁时会自自动清理。
	</li>
	<li><strong>suppressDisabledCheck</strong>（默认值：<code>false</code>）<br />
	类型：Boolean<br />
	描述：是否要绕过禁用的检查。
	</li>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要绑定事件处理程序的元素。如果未提供元素，<code>this.element</code> 用于未授权的事件，<a href="#method-widget">widget 元素</a> 用于授权的事件。
	</li>
	<li><strong>handlers</strong><br />
	类型：Object<br />
	描述：一个 map，其中字符串键代表事件类型，可选的选择器用于授权，值代表事件调用的处理函数。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>放置小部件元素内所有被点击的链接的默认行为。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  "click a": function( event ) {
    event.preventDefault();
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_setOption">
	<td>_setOption( key, value )</td>
	<td>jQuery (plugin only)</td>
	<td>为每个独立的选项调用 <a href="#method-_setOptions"><code>_setOptions()</code></a> 方法。小部件状态随着改变而更新。<br />
	<ul>
	<li><strong>key</strong><br />
	类型：String<br />
	描述：要设置的选项名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当小部件的 <code>height</code> 或 <code>width</code> 选项改变时，更新小部件的元素。</p>
	<pre style="white-space: pre-wrap;">
_setOption: function( key, value ) {
  if ( key === "width" ) {
    this.element.width( value );
  }
  if ( key === "height" ) {
    this.element.height( value );
  }
  this._super( key, value );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_setOptions">
	<td>_setOptions( options )</td>
	<td>jQuery (plugin only)</td>
	<td>当调用 <a href="#method-option"><code>option()</code></a> 方法时调用，无论以什么形式调用 <code>option()</code>。如果您要根据多个选项的改变而改变处理器密集型，重载该方法是很有用的。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>如果小部件的 <code>height</code> 或 <code>width</code> 选项改变，调用 <code>resize</code> 方法。</p>
	<pre style="white-space: pre-wrap;">
_setOptions: function( options ) {
  var that = this,
    resize = false;
 
  $.each( options, function( key, value ) {
    that._setOption( key, value );
    if ( key === "height" || key === "width" ) {
      resize = true;
    }
  });
 
  if ( resize ) {
    this.resize();
  }
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_show">
	<td>_show( element, option [, callback ] )</td>
	<td>jQuery (plugin only)</td>
	<td>使用内置的动画方法或使用自定义的特效显示一个元素。如需了解可能的 <code>option</code> 值，请查看 <a href="#option-show">show</a>。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要显示的元素。
	</li>
	<li><strong>option</strong><br />
	类型：Object<br />
	描述：定义如何显示元素的设置。
	</li>
	<li><strong>callback</strong><br />
	类型：Function()<br />
	描述：元素完全显示后要调用的回调。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>为自定义动画传递 <code>show</code> 选项。</p>
	<pre style="white-space: pre-wrap;">
this._show( this.element, this.options.show, function() {
 
  // Focus the element when it's fully visible.
  this.focus();
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_super">
	<td>_super( [arg ] [, ... ] )</td>
	<td>jQuery (plugin only)</td>
	<td>从父部件中调用相同名称的方法，带有任意指定的参数。本质上是 <code>.call()</code>。<br />
	<ul>
	<li><strong>arg</strong><br />
	类型：Object<br />
	描述：要传递给父部件的方法的零到多个参数。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>处理 <code>title</code> 选项更新，并调用付部件的 <code>_setOption()</code> 来更新选项的内部存储。</p>
	<pre style="white-space: pre-wrap;">
_setOption: function( key, value ) {
  if ( key === "title" ) {
    this.element.find( "h3" ).text( value );
  }
  this._super( key, value );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_superApply">
	<td>_superApply( arguments )</td>
	<td>jQuery (plugin only)</td>
	<td>从父部件中调用相同名称的方法，带有参数的数组。本质上是 <code>.apply()</code>。<br />
	<ul>
	<li><strong>arguments</strong><br />
	类型：Array<br />
	描述：要传递给父部件的方法的参数数组。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>处理 <code>title</code> 选项更新，并调用付部件的 <code>_setOption()</code> 来更新选项的内部存储。</p>
	<pre style="white-space: pre-wrap;">
_setOption: function( key, value ) {
  if ( key === "title" ) {
    this.element.find( "h3" ).text( value );
  }
  this._superApply( arguments );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_trigger">
	<td>_trigger( type [, event ] [, data ] )</td>
	<td>Boolean</td>
	<td>触发一个事件及其相关的回调。带有该名称的选项与作为回调被调用的类型相等。<br />
	<p>事件名称是小部件名称和类型的小写字母串。</p>
	<p>注释：当提供数据时，您必须提供所有三个参数。如果没有传递事件，则传递 <code>null</code>。</p>
	<p>如果默认行为是阻止的，则返回 <code>false</code>，否则返回 <code>true</code>。当处理程序返回 <code>false</code> 时或调用 <code>event.preventDefault()</code> 时，则阻止默认行为发生。</p>
	<ul>
	<li><strong>type</strong><br />
	类型：String<br />
	描述：<code>type</code> 应该匹配回调选项的名称。完整的事件类型会自动生成。
	</li>
	<li><strong>event</strong><br />
	类型：Event<br />
	描述：导致该事件发生的原始事件，想听众提供上下文时很有用。
	</li>
	<li><strong>data</strong><br />
	类型：Object<br />
	描述：一个与事件相关的数据哈希。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当按下一个键时，触发 <code>search</code> 事件。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  keydown: function( event ) {
 
    // Pass the original event so that the custom search event has
    // useful information, such as keyCode
    this._trigger( "search", event, {
 
      // Pass additional information unique to this event
      value: this.element.val()
    });
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除小部件功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当点击小部件的任意锚点时销毁小部件。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  "click a": function( event ) {
    event.preventDefault();
    this.destroy();
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用小部件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当点击小部件的任意锚点时禁用小部件。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  "click a": function( event ) {
    event.preventDefault();
    this.disable();
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用小部件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当点击小部件的任意锚点时启用小部件。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  "click a": function( event ) {
    event.preventDefault();
    this.enable();
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td id="method-option">option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>获得 <code>width</code> 选项的值。</p>
	<pre style="white-space: pre-wrap;">
this.option( "width" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前小部件选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>记录每个小部件选项的键/值对，用于调试。</p>
	<pre style="white-space: pre-wrap;">
var options = this.option();
for ( var key in options ) {
  console.log( key, options[ key ] );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的小部件选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>设置 <code>width</code> 选项为 <code>500</code>。</p>
	<pre style="white-space: pre-wrap;">
this.option( "width", 500 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为小部件设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>设置 <code>height</code> 和 <code>width</code> 选项为 <code>500</code>。</p>
	<pre style="white-space: pre-wrap;">
this.option({
  width: 500,
  height: 500
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含原始元素或其他相关的生成元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当创建小部件时，在小部件的原始元素周围放置一个红色的边框。</p>
	<pre style="white-space: pre-wrap;">
_create: function() {
  this.widget().css( "border", "2px solid red" );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-_create">
	<td>_create()</td>
	<td>jQuery (plugin only)</td>
	<td><code>_create()</code> 方法是小部件的构造函数。没有参数，但是 <code>this.element</code> 和 <code>this.options</code> 已经设置。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>基于一个选项设置小部件元素的背景颜色。</p>
	<pre style="white-space: pre-wrap;">
_create: function() {
  this.element.css( "background-color", this.options.color );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_delay">
	<td>_delay( fn [, delay ] )</td>
	<td>Number</td>
	<td>在指定延迟后调用提供的函数。保持 <code>this</code> 上下文正确。本质上是 <code>setTimeout()</code>。<br />
	使用 <code>clearTimeout()</code> 返回超时 ID。<br />
	<ul>
	<li><strong>fn</strong><br />
	类型：Function() 或 String<br />
	描述：要调用的函数。也可以是小部件上方法的名称。
	</li>
	<li><strong>delay</strong><br />
	类型：Number<br />
	描述：调用函数前等待的毫秒数，默认为 <code>0</code>。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>100 毫秒后在小部件上调用 <code>_foo()</code> 方法。</p>
	<pre style="white-space: pre-wrap;">
this._delay( this._foo, 100 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_destroy">
	<td>_destroy()</td>
	<td>jQuery (plugin only)</td>
	<td>公共的 <a href="#method-destroy"><code>destroy()</code></a> 方法清除所有的公共数据、事件等等。代表了定制、指定小部件、清理的 <code>_destroy()</code>。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当小部件被销毁时，从小部件的元素移除一个 class。</p>
	<pre style="white-space: pre-wrap;">
_destroy: function() {
  this.element.removeClass( "my-widget" );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_focusable">
	<td>_focusable( element )</td>
	<td>jQuery (plugin only)</td>
	<td>建立聚焦在元素上时要应用 <code>ui-state-focus</code> class 的 <code>element</code>。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要应用 focusable 行为的元素。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>向小部件内的一组元素应用 focusable 样式：</p>
	<pre style="white-space: pre-wrap;">
this._focusable( this.element.find( ".my-items" ) );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_getCreateEventData">
	<td>_getCreateEventData()</td>
	<td>Object</td>
	<td>所有的小部件触发 <a href="#event-create"><code>create</code></a> 事件。默认情况下，事件中不提供任何的数据，但是该方法会返回一个对象，作为 <code>create</code> 事件的数据被传递。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>向 <code>create</code> 事件处理程序传递小部件的选项，作为参数。</p>
	<pre style="white-space: pre-wrap;">
_getCreateEventData: function() {
  return this.options;
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_getCreateOptions">
	<td>_getCreateOptions()</td>
	<td>Object</td>
	<td>该方法允许小部件在初始化期间为定义选项定义一个自定义的方法。用户提供的选项会覆盖该方法返回的选项，即会覆盖默认的选项。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>让小部件元素的 id 属性作为选项可用。</p>
	<pre style="white-space: pre-wrap;">
_getCreateOptions: function() {
  return { id: this.element.attr( "id" ) };
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_hide">
	<td>_hide( element, option [, callback ] )</td>
	<td>jQuery (plugin only)</td>
	<td>使用内置的动画方法或使用自定义的特效隐藏一个元素。如需了解可能的 <code>option</code> 值，请查看 <a href="#option-hide">hide</a>。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要隐藏的元素。
	</li>
	<li><strong>option</strong><br />
	类型：Object<br />
	描述：定义如何隐藏元素的设置。
	</li>
	<li><strong>callback</strong><br />
	类型：Function()<br />
	描述：元素完全隐藏后要调用的回调。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>为自定义动画传递 <code>hide</code> 选项。</p>
	<pre style="white-space: pre-wrap;">
this._hide( this.element, this.options.hide, function() {
 
  // Remove the element from the DOM when it's fully hidden.
  $( this ).remove();
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_hoverable">
	<td>_hoverable( element )</td>
	<td>jQuery (plugin only)</td>
	<td>建立悬浮在元素上时要应用 <code>ui-state-hover</code> class 的 <code>element</code>。事件处理程序在销毁时自动清理。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要应用 hoverable 行为的元素。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当悬浮在元素上时，向元素内所有的 <code>div</code> 应用 hoverable 样式。</p>
	<pre style="white-space: pre-wrap;">
this._hoverable( this.element.find( "div" ) );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_init">
	<td>_init()</td>
	<td>jQuery (plugin only)</td>
	<td>小部件初始化的理念与创建不同。任何时候不带参数的调用插件或者只带一个选项哈希的调用插件，初始化小部件。当小部件被创建时会包含这个方法。
	<p>注释：如果存在不带参数成功调用小部件时要执行的逻辑动作，初始化只能在这时处理。</p>
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>如果设置了 <code>autoOpen</code> 选项，则调用 <code>open()</code> 方法。</p>
	<pre style="white-space: pre-wrap;">
_init: function() {
  if ( this.options.autoOpen ) {
    this.open();
  }
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_off">
	<td>_off( element, eventName )</td>
	<td>jQuery (plugin only)</td>
	<td>从指定的元素取消绑定事件处理程序。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要取消绑定事件处理程序的元素。不像 <code>_on()</code> 方法，<code>_off()</code> 方法中元素是必需的。
	</li>
	<li><strong>eventName</strong><br />
	类型：String<br />
	描述：一个或多个空格分隔的事件类型。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>从小部件的元素上取消绑定所有 click 事件。</p>
	<pre style="white-space: pre-wrap;">
this._off( this.element, "click" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_on">
	<td>_on( [suppressDisabledCheck ] [, element ], handlers )</td>
	<td>jQuery (plugin only)</td>
	<td>授权通过事件名称内的选择器被支持，例如 <code>"click .foo"</code>。<code>_on()</code> 方法提供了一些直接事件绑定的好处：<br />
	<ul>
	<li>保持处理程序内适当的 <code>this</code> 上下文。
	</li>
	<li>自动处理禁用的部件：如果小部件被禁用或事件发生在带有 <code>ui-state-disabled</code> class 的元素上，则不调用事件处理程序。可以被 <code>suppressDisabledCheck</code> 参数重写。
	</li>
	<li>事件处理程序会自动添加命名空间，在销毁时会自自动清理。
	</li>
	<li><strong>suppressDisabledCheck</strong>（默认值：<code>false</code>）<br />
	类型：Boolean<br />
	描述：是否要绕过禁用的检查。
	</li>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要绑定事件处理程序的元素。如果未提供元素，<code>this.element</code> 用于未授权的事件，<a href="#method-widget">widget 元素</a> 用于授权的事件。
	</li>
	<li><strong>handlers</strong><br />
	类型：Object<br />
	描述：一个 map，其中字符串键代表事件类型，可选的选择器用于授权，值代表事件调用的处理函数。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>放置小部件元素内所有被点击的链接的默认行为。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  "click a": function( event ) {
    event.preventDefault();
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_setOption">
	<td>_setOption( key, value )</td>
	<td>jQuery (plugin only)</td>
	<td>为每个独立的选项调用 <a href="#method-_setOptions"><code>_setOptions()</code></a> 方法。小部件状态随着改变而更新。<br />
	<ul>
	<li><strong>key</strong><br />
	类型：String<br />
	描述：要设置的选项名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当小部件的 <code>height</code> 或 <code>width</code> 选项改变时，更新小部件的元素。</p>
	<pre style="white-space: pre-wrap;">
_setOption: function( key, value ) {
  if ( key === "width" ) {
    this.element.width( value );
  }
  if ( key === "height" ) {
    this.element.height( value );
  }
  this._super( key, value );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_setOptions">
	<td>_setOptions( options )</td>
	<td>jQuery (plugin only)</td>
	<td>当调用 <a href="#method-option"><code>option()</code></a> 方法时调用，无论以什么形式调用 <code>option()</code>。如果您要根据多个选项的改变而改变处理器密集型，重载该方法是很有用的。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>如果小部件的 <code>height</code> 或 <code>width</code> 选项改变，调用 <code>resize</code> 方法。</p>
	<pre style="white-space: pre-wrap;">
_setOptions: function( options ) {
  var that = this,
    resize = false;
 
  $.each( options, function( key, value ) {
    that._setOption( key, value );
    if ( key === "height" || key === "width" ) {
      resize = true;
    }
  });
 
  if ( resize ) {
    this.resize();
  }
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_show">
	<td>_show( element, option [, callback ] )</td>
	<td>jQuery (plugin only)</td>
	<td>使用内置的动画方法或使用自定义的特效显示一个元素。如需了解可能的 <code>option</code> 值，请查看 <a href="#option-show">show</a>。<br />
	<ul>
	<li><strong>element</strong><br />
	类型：jQuery<br />
	描述：要显示的元素。
	</li>
	<li><strong>option</strong><br />
	类型：Object<br />
	描述：定义如何显示元素的设置。
	</li>
	<li><strong>callback</strong><br />
	类型：Function()<br />
	描述：元素完全显示后要调用的回调。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>为自定义动画传递 <code>show</code> 选项。</p>
	<pre style="white-space: pre-wrap;">
this._show( this.element, this.options.show, function() {
 
  // Focus the element when it's fully visible.
  this.focus();
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_super">
	<td>_super( [arg ] [, ... ] )</td>
	<td>jQuery (plugin only)</td>
	<td>从父部件中调用相同名称的方法，带有任意指定的参数。本质上是 <code>.call()</code>。<br />
	<ul>
	<li><strong>arg</strong><br />
	类型：Object<br />
	描述：要传递给父部件的方法的零到多个参数。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>处理 <code>title</code> 选项更新，并调用付部件的 <code>_setOption()</code> 来更新选项的内部存储。</p>
	<pre style="white-space: pre-wrap;">
_setOption: function( key, value ) {
  if ( key === "title" ) {
    this.element.find( "h3" ).text( value );
  }
  this._super( key, value );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_superApply">
	<td>_superApply( arguments )</td>
	<td>jQuery (plugin only)</td>
	<td>从父部件中调用相同名称的方法，带有参数的数组。本质上是 <code>.apply()</code>。<br />
	<ul>
	<li><strong>arguments</strong><br />
	类型：Array<br />
	描述：要传递给父部件的方法的参数数组。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>处理 <code>title</code> 选项更新，并调用付部件的 <code>_setOption()</code> 来更新选项的内部存储。</p>
	<pre style="white-space: pre-wrap;">
_setOption: function( key, value ) {
  if ( key === "title" ) {
    this.element.find( "h3" ).text( value );
  }
  this._superApply( arguments );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_trigger">
	<td>_trigger( type [, event ] [, data ] )</td>
	<td>Boolean</td>
	<td>触发一个事件及其相关的回调。带有该名称的选项与作为回调被调用的类型相等。<br />
	<p>事件名称是小部件名称和类型的小写字母串。</p>
	<p>注释：当提供数据时，您必须提供所有三个参数。如果没有传递事件，则传递 <code>null</code>。</p>
	<p>如果默认行为是阻止的，则返回 <code>false</code>，否则返回 <code>true</code>。当处理程序返回 <code>false</code> 时或调用 <code>event.preventDefault()</code> 时，则阻止默认行为发生。</p>
	<ul>
	<li><strong>type</strong><br />
	类型：String<br />
	描述：<code>type</code> 应该匹配回调选项的名称。完整的事件类型会自动生成。
	</li>
	<li><strong>event</strong><br />
	类型：Event<br />
	描述：导致该事件发生的原始事件，想听众提供上下文时很有用。
	</li>
	<li><strong>data</strong><br />
	类型：Object<br />
	描述：一个与事件相关的数据哈希。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当按下一个键时，触发 <code>search</code> 事件。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  keydown: function( event ) {
 
    // Pass the original event so that the custom search event has
    // useful information, such as keyCode
    this._trigger( "search", event, {
 
      // Pass additional information unique to this event
      value: this.element.val()
    });
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除小部件功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当点击小部件的任意锚点时销毁小部件。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  "click a": function( event ) {
    event.preventDefault();
    this.destroy();
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用小部件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当点击小部件的任意锚点时禁用小部件。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  "click a": function( event ) {
    event.preventDefault();
    this.disable();
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用小部件。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当点击小部件的任意锚点时启用小部件。</p>
	<pre style="white-space: pre-wrap;">
this._on( this.element, {
  "click a": function( event ) {
    event.preventDefault();
    this.enable();
  }
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td id="method-option">option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>获得 <code>width</code> 选项的值。</p>
	<pre style="white-space: pre-wrap;">
this.option( "width" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前小部件选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>记录每个小部件选项的键/值对，用于调试。</p>
	<pre style="white-space: pre-wrap;">
var options = this.option();
for ( var key in options ) {
  console.log( key, options[ key ] );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的小部件选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>设置 <code>width</code> 选项为 <code>500</code>。</p>
	<pre style="white-space: pre-wrap;">
this.option( "width", 500 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为小部件设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>设置 <code>height</code> 和 <code>width</code> 选项为 <code>500</code>。</p>
	<pre style="white-space: pre-wrap;">
this.option({
  width: 500,
  height: 500
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含原始元素或其他相关的生成元素的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>当创建小部件时，在小部件的原始元素周围放置一个红色的边框。</p>
	<pre style="white-space: pre-wrap;">
_create: function() {
  this.widget().css( "border", "2px solid red" );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6218;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
	<td>widgetcreate</td>
	<td>当小部件被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的小部件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).widget({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 widgetcreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "widgetcreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
	<td>widgetcreate</td>
	<td>当小部件被创建时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的小部件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).widget({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 widgetcreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "widgetcreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6218;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-active">
	<td>active</td>
    <td>Boolean 或 Integer</td>
	<td>当前打开哪一个面板。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：设置 <code>active</code> 为 <code>false</code> 将折叠所有的面板。这要求 <a href="#option-collapsible"><code>collapsible</code></a> 选项必须为 <code>true</code>。</li>
	<li><strong>Integer</strong>：激活打开的面板索引，以零为基础。负值则表示从最后一个面板后退选择面板。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>active</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ active: 2 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>active</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var active = $( ".selector" ).accordion( "option", "active" );
 
// setter
$( ".selector" ).accordion( "option", "active", 2 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-animate">
	<td>animate</td>
    <td>Boolean 或 Number 或 String 或 Object</td>
	<td>是否使用动画改变面板，且如何使用动画改变面板。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：<code>false</code> 值将禁用动画。</li>
	<li><strong>Number</strong>：easing 默认的持续时间，以毫秒计。</li>
	<li><strong>String</strong>：默认的持续时间要使用的 <a href="/jqueryui/api-easings.html">easing</a> 名称。
	<li><strong>Object</strong>：<code>easing</code> 和 <code>duration</code> 属性的动画设置。</li>
		<ul>
		<li>上面任意的选项都可以包含 <code>down</code> 属性。</li>
		<li>当被激活的面板有一个比当前激活面板较低的指数时，发生 "Down" 动画。</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>animate</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ animate: "bounceslide" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>animate</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var animate = $( ".selector" ).accordion( "option", "animate" );
 
// setter
$( ".selector" ).accordion( "option", "animate", "bounceslide" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>{}</td>
</tr>
<tr id="option-collapsible">
	<td>collapsible</td>
    <td>Boolean</td>
	<td>所有部分是否都可以马上关闭。允许折叠激活的部分。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>collapsible</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ collapsible: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>collapsible</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var collapsible = $( ".selector" ).accordion( "option", "collapsible" );
 
// setter
$( ".selector" ).accordion( "option", "collapsible", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 accordion。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).accordion( "option", "disabled" );
 
// setter
$( ".selector" ).accordion( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-event">
	<td>event</td>
    <td>String</td>
	<td>accordion 头部会作出反应的事件，用以激活相关的面板。可以指定多个事件，用空格间隔。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>event</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ event: "mouseover" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>event</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var event = $( ".selector" ).accordion( "option", "event" );
 
// setter
$( ".selector" ).accordion( "option", "event", "mouseover" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"click"</td>
</tr>
<tr id="option-header">
	<td>header</td>
    <td>Selector</td>
	<td>标题元素的选择器，通过主要 accordion 元素上的 .find() 进行应用。内容面板必须是紧跟在与其相关的标题后的同级元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>header</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ header: "h3" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>header</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var header = $( ".selector" ).accordion( "option", "header" );
 
// setter
$( ".selector" ).accordion( "option", "header", "h3" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"> li > :first-child,> :not(li):even"</td>
</tr>
<tr id="option-heightStyle">
	<td>heightStyle</td>
    <td>String</td>
	<td>控制 accordion 和每个面板的高度。可能的值：<br />
	<ul>
	<li><code>"auto"</code>：所有的面板将会被设置为最高的面板的高度。</li>
	<li><code>"fill"</code>：基于 accordion 的父元素的高度，扩展到可用的高度。</li>
	<li><code>"content"</code>：每个面板的高度取决于它的内容。</li>	
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>heightStyle</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ heightStyle: "fill" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>heightStyle</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var heightStyle = $( ".selector" ).accordion( "option", "heightStyle" );
 
// setter
$( ".selector" ).accordion( "option", "heightStyle", "fill" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"auto"</td>
</tr>
<tr id="option-icons">
	<td>icons</td>
    <td>Object</td>
	<td>标题要使用的图标，与 <a href="/jqueryui/api-icons.html">jQuery UI CSS 框架提供的图标（Icons）</a> 匹配。设置为 <code>false</code> 则不显示图标。<br />
	<ul>
	<li>header （string，默认值："ui-icon-triangle-1-e"）</li>
	<li>activeHeader （string，默认值："ui-icon-triangle-1-s"）</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>icons</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>icons</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var icons = $( ".selector" ).accordion( "option", "icons" );
 
// setter
$( ".selector" ).accordion( "option", "icons", { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>{ "header": "ui-icon-triangle-1-e", "activeHeader": "ui-icon-triangle-1-s" }</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-active">
	<td>active</td>
    <td>Boolean 或 Integer</td>
	<td>当前打开哪一个面板。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：设置 <code>active</code> 为 <code>false</code> 将折叠所有的面板。这要求 <a href="#option-collapsible"><code>collapsible</code></a> 选项必须为 <code>true</code>。</li>
	<li><strong>Integer</strong>：激活打开的面板索引，以零为基础。负值则表示从最后一个面板后退选择面板。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>active</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ active: 2 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>active</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var active = $( ".selector" ).accordion( "option", "active" );
 
// setter
$( ".selector" ).accordion( "option", "active", 2 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>0</td>
</tr>
<tr id="option-animate">
	<td>animate</td>
    <td>Boolean 或 Number 或 String 或 Object</td>
	<td>是否使用动画改变面板，且如何使用动画改变面板。<br />
	<p><strong>支持多个类型：</strong></p>
	<ul>
	<li><strong>Boolean</strong>：<code>false</code> 值将禁用动画。</li>
	<li><strong>Number</strong>：easing 默认的持续时间，以毫秒计。</li>
	<li><strong>String</strong>：默认的持续时间要使用的 <a href="/jqueryui/api-easings.html">easing</a> 名称。
	<li><strong>Object</strong>：<code>easing</code> 和 <code>duration</code> 属性的动画设置。</li>
		<ul>
		<li>上面任意的选项都可以包含 <code>down</code> 属性。</li>
		<li>当被激活的面板有一个比当前激活面板较低的指数时，发生 "Down" 动画。</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>animate</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ animate: "bounceslide" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>animate</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var animate = $( ".selector" ).accordion( "option", "animate" );
 
// setter
$( ".selector" ).accordion( "option", "animate", "bounceslide" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>{}</td>
</tr>
<tr id="option-collapsible">
	<td>collapsible</td>
    <td>Boolean</td>
	<td>所有部分是否都可以马上关闭。允许折叠激活的部分。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>collapsible</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ collapsible: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>collapsible</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var collapsible = $( ".selector" ).accordion( "option", "collapsible" );
 
// setter
$( ".selector" ).accordion( "option", "collapsible", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 accordion。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).accordion( "option", "disabled" );
 
// setter
$( ".selector" ).accordion( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-event">
	<td>event</td>
    <td>String</td>
	<td>accordion 头部会作出反应的事件，用以激活相关的面板。可以指定多个事件，用空格间隔。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>event</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ event: "mouseover" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>event</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var event = $( ".selector" ).accordion( "option", "event" );
 
// setter
$( ".selector" ).accordion( "option", "event", "mouseover" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"click"</td>
</tr>
<tr id="option-header">
	<td>header</td>
    <td>Selector</td>
	<td>标题元素的选择器，通过主要 accordion 元素上的 .find() 进行应用。内容面板必须是紧跟在与其相关的标题后的同级元素。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>header</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ header: "h3" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>header</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var header = $( ".selector" ).accordion( "option", "header" );
 
// setter
$( ".selector" ).accordion( "option", "header", "h3" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"> li > :first-child,> :not(li):even"</td>
</tr>
<tr id="option-heightStyle">
	<td>heightStyle</td>
    <td>String</td>
	<td>控制 accordion 和每个面板的高度。可能的值：<br />
	<ul>
	<li><code>"auto"</code>：所有的面板将会被设置为最高的面板的高度。</li>
	<li><code>"fill"</code>：基于 accordion 的父元素的高度，扩展到可用的高度。</li>
	<li><code>"content"</code>：每个面板的高度取决于它的内容。</li>	
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>heightStyle</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ heightStyle: "fill" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>heightStyle</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var heightStyle = $( ".selector" ).accordion( "option", "heightStyle" );
 
// setter
$( ".selector" ).accordion( "option", "heightStyle", "fill" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>"auto"</td>
</tr>
<tr id="option-icons">
	<td>icons</td>
    <td>Object</td>
	<td>标题要使用的图标，与 <a href="/jqueryui/api-icons.html">jQuery UI CSS 框架提供的图标（Icons）</a> 匹配。设置为 <code>false</code> 则不显示图标。<br />
	<ul>
	<li>header （string，默认值："ui-icon-triangle-1-e"）</li>
	<li>activeHeader （string，默认值："ui-icon-triangle-1-s"）</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>icons</code> 选项的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({ icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>icons</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var icons = $( ".selector" ).accordion( "option", "icons" );
 
// setter
$( ".selector" ).accordion( "option", "icons", { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>{ "header": "ui-icon-triangle-1-e", "activeHeader": "ui-icon-triangle-1-s" }</td>
</tr>
</table></div>') where ID=6220;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 accordion 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 accordion。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 accordion。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).accordion( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 accordion 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).accordion( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 accordion 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 accordion 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refresh">
	<td>refresh()</td>
	<td>jQuery (plugin only)</td>
	<td>处理任何在 DOM 中直接添加或移除的标题和面板，并重新计算 accordion 的高度。结果取决于内容和 <code><a href="#option-heightStyle ">heightStyle </a></code> 选项。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refresh 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "refresh" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 accordion 的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).accordion( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 accordion 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 accordion。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 accordion。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).accordion( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 accordion 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).accordion( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 accordion 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 accordion 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refresh">
	<td>refresh()</td>
	<td>jQuery (plugin only)</td>
	<td>处理任何在 DOM 中直接添加或移除的标题和面板，并重新计算 accordion 的高度。结果取决于内容和 <code><a href="#option-heightStyle ">heightStyle </a></code> 选项。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refresh 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion( "refresh" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 accordion 的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).accordion( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6220;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-activate">
	<td>activate( event, ui )</td>
	<td>accordionactivate</td>
	<td>面板被激活后触发（在动画完成之后）。如果 accordion 之前是折叠的，则 <code>ui.oldHeader</code> 和 <code>ui.oldPanel</code> 将是空的 jQuery 对象。如果 accordion 正在折叠，则 <code>ui.newHeader</code> 和 <code>ui.newPanel</code> 将是空的 jQuery 对象。<br />
	<p><strong>注意：由于 <code>activate</code> 事件只有在面板激活时才能触发，当创建 accordion 部件时，最初的面板不会触发该事件。如果您需要一个用于部件创建的钩，请使用 <code>create</code> 事件。</strong></p>
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>newHeader</strong><br />
		类型：jQuery<br />
		描述：刚被激活的标题。
		</li>
		<li><strong>oldHeader</strong><br />
		类型：jQuery<br />
		描述：刚被取消激活的标题。
		</li>
		<li><strong>newPanel</strong><br />
		类型：jQuery<br />
		描述：刚被激活的面板。
		</li>
		<li><strong>oldPanel</strong><br />
		类型：jQuery<br />
		描述：刚被取消激活的面板。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 activate 回调的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({
  activate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 accordionactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "accordionactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-beforeActivate">
	<td>beforeActivate( event, ui )</td>
	<td>accordionbeforeactivate</td>
	<td>面板被激活前直接触发。可以取消以防止面板被激活。如果 accordion 当前是折叠的，则 <code>ui.oldHeader</code> 和 <code>ui.oldPanel</code> 将是空的 jQuery 对象。如果 accordion 正在折叠，则 <code>ui.newHeader</code> 和 <code>ui.newPanel</code> 将是空的 jQuery 对象。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>newHeader</strong><br />
		类型：jQuery<br />
		描述：将被激活的标题。
		</li>
		<li><strong>oldHeader</strong><br />
		类型：jQuery<br />
		描述：将被取消激活的标题。
		</li>
		<li><strong>newPanel</strong><br />
		类型：jQuery<br />
		描述：将被激活的面板。
		</li>
		<li><strong>oldPanel</strong><br />
		类型：jQuery<br />
		描述：将被取消激活的面板。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 beforeActivate 回调的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({
  beforeActivate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 accordionbeforeactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "accordionbeforeactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
	<td>accordioncreate</td>
	<td>当创建 accordion 时触发。如果 accordion 是折叠的，<code>ui.header</code> 和 <code>ui.panel</code> 将是空的 jQuery 对象。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>header</strong><br />
		类型：jQuery<br />
		描述：激活的标题。
		</li>
		<li><strong>panel</strong><br />
		类型：jQuery<br />
		描述：激活的面板。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 accordioncreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "accordioncreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-activate">
	<td>activate( event, ui )</td>
	<td>accordionactivate</td>
	<td>面板被激活后触发（在动画完成之后）。如果 accordion 之前是折叠的，则 <code>ui.oldHeader</code> 和 <code>ui.oldPanel</code> 将是空的 jQuery 对象。如果 accordion 正在折叠，则 <code>ui.newHeader</code> 和 <code>ui.newPanel</code> 将是空的 jQuery 对象。<br />
	<p><strong>注意：由于 <code>activate</code> 事件只有在面板激活时才能触发，当创建 accordion 部件时，最初的面板不会触发该事件。如果您需要一个用于部件创建的钩，请使用 <code>create</code> 事件。</strong></p>
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>newHeader</strong><br />
		类型：jQuery<br />
		描述：刚被激活的标题。
		</li>
		<li><strong>oldHeader</strong><br />
		类型：jQuery<br />
		描述：刚被取消激活的标题。
		</li>
		<li><strong>newPanel</strong><br />
		类型：jQuery<br />
		描述：刚被激活的面板。
		</li>
		<li><strong>oldPanel</strong><br />
		类型：jQuery<br />
		描述：刚被取消激活的面板。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 activate 回调的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({
  activate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 accordionactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "accordionactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-beforeActivate">
	<td>beforeActivate( event, ui )</td>
	<td>accordionbeforeactivate</td>
	<td>面板被激活前直接触发。可以取消以防止面板被激活。如果 accordion 当前是折叠的，则 <code>ui.oldHeader</code> 和 <code>ui.oldPanel</code> 将是空的 jQuery 对象。如果 accordion 正在折叠，则 <code>ui.newHeader</code> 和 <code>ui.newPanel</code> 将是空的 jQuery 对象。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>newHeader</strong><br />
		类型：jQuery<br />
		描述：将被激活的标题。
		</li>
		<li><strong>oldHeader</strong><br />
		类型：jQuery<br />
		描述：将被取消激活的标题。
		</li>
		<li><strong>newPanel</strong><br />
		类型：jQuery<br />
		描述：将被激活的面板。
		</li>
		<li><strong>oldPanel</strong><br />
		类型：jQuery<br />
		描述：将被取消激活的面板。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 beforeActivate 回调的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({
  beforeActivate: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 accordionbeforeactivate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "accordionbeforeactivate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
	<td>accordioncreate</td>
	<td>当创建 accordion 时触发。如果 accordion 是折叠的，<code>ui.header</code> 和 <code>ui.panel</code> 将是空的 jQuery 对象。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>header</strong><br />
		类型：jQuery<br />
		描述：激活的标题。
		</li>
		<li><strong>panel</strong><br />
		类型：jQuery<br />
		描述：激活的面板。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 accordion：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).accordion({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 accordioncreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "accordioncreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6220;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate" id="options">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-appendTo">
	<td>appendTo</td>
    <td>Selector</td>
	<td>菜单应该被附加到哪一个元素。当该值为 <code>null</code> 时，输入域的父元素将检查 <code>ui-front</code> class。如果找到带有 <code>ui-front</code> class 的元素，菜单将被附加到该元素。如果未找到带有 <code>ui-front</code> class 的元素，不管值为多少，菜单将被附加到 body。<br />
	<p><strong>注意：当建议菜单打开时，<code>appendTo</code> 选项不应改变。</strong></p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>appendTo</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ appendTo: "#someElem" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>appendTo</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var appendTo = $( ".selector" ).autocomplete( "option", "appendTo" );
 
// setter
$( ".selector" ).autocomplete( "option", "appendTo", "#someElem" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-autoFocus">
	<td>autoFocus</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，当菜单显示时，第一个条目将自动获得焦点。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>autoFocus</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ autoFocus: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>autoFocus</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var autoFocus = $( ".selector" ).autocomplete( "option", "autoFocus" );
 
// setter
$( ".selector" ).autocomplete( "option", "autoFocus", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Integer</td>
	<td>按键和执行搜索之间的延迟，以毫秒计。对于本地数据，采用零延迟是有意义的（更具响应性），但对于远程数据会产生大量的负荷，同时降低了响应性。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ delay: 500 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).autocomplete( "option", "delay" );
 
// setter
$( ".selector" ).autocomplete( "option", "delay", 500 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>300</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 autocomplete。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).autocomplete( "option", "disabled" );
 
// setter
$( ".selector" ).autocomplete( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-minLength">
	<td>minLength</td>
    <td>Integer</td>
	<td>执行搜索前用户必须输入的最小字符数。对于仅带有几项条目的本地数据，通常设置为零，但是当单个字符搜索会匹配几千项条目时，设置个高数值是很有必要的。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>minLength</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ minLength: 0 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>minLength</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var minLength = $( ".selector" ).autocomplete( "option", "minLength" );
 
// setter
$( ".selector" ).autocomplete( "option", "minLength", 0 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
<tr id="option-position">
	<td>position</td>
    <td>Object</td>
	<td>标识建议菜单的位置与相关的 input 元素有关系。<code>of</code> 选项默认为 input 元素，但是您可以指定另一个定位元素。如需了解各种选项的更多细节，请查看 <a href="/jqueryui/api-position.html"> jQuery UI 定位（Position）</a>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>position</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ position: { my : "right top", at: "right bottom" } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>position</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var position = $( ".selector" ).autocomplete( "option", "position" );
 
// setter
$( ".selector" ).autocomplete( "option", "position", { my : "right top", at: "right bottom" } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>{ my: "left top", at: "left bottom", collision: "none" }</td>
</tr>
<tr id="option-source">
	<td>source</td>
    <td>Array 或 String 或 Function( Object request, Function response( Object data ) )</td>
	<td>定义要使用的数据，必须指定。<br />
	<p>独立于您要使用的变量，标签总是被视为文本。如果您想要标签被视为 html，您可以使用 <a href="https://github.com/scottgonzalez/jquery-ui-extensions/blob/master/src/autocomplete/jquery.ui.autocomplete.html.js" target="_blank" rel="noopener noreferrer">Scott González' html 扩展</a>。演示侧重于 <code>source</code> 选项的不同变量 - 您可以查找其中匹配您的使用情况的那个，并查看相关代码。</p>
	<p><strong>支持多个类型：</strong></p>
	<ul>
		<li><strong>Array</strong>：可用于本地数据的一个数组。支持两种格式：
		<ul>
			<li>字符串数组：<code>[ "Choice1", "Choice2" ]</code></li>
			<li>带有 <code>label</code> 和 <code>value</code> 属性的对象数组：<code>[ { label: "Choice1", value: "value1" }, ... ]</code></li>
		</ul>
		label 属性显示在建议菜单中。当用户选择一个条目时，value 将被插入到 input 元素中。如果只是指定了一个属性，则该属性将被视为 label 和 value，例如，如果您只提供了 <code>value</code> 属性，value 也会被视为标签。
		</li>
		<li><strong>String</strong>：当使用一个字符串，Autocomplete 插件希望该字符串指向一个能返回 JSON 数据的 URL 资源。它可以是在相同的主机上，也可以是在不同的主机上（必须提供 JSONP）。Autocomplete 插件不过滤结果，而是通过一个 <code>term</code> 字段添加了一个查询字符串，用于服务器端脚本过滤结果。例如，如果 <code>source</code> 选项设置为 <code>"http://example.com"</code>，且用户键入了 <code>foo</code>，GET 请求则为 <code>http://example.com?term=foo</code>。数据本身的格式可以与前面描述的本地数据的格式相同。</li>
		<li><strong>Function</strong>：第三个变量，一个回调函数，提供最大的灵活性，可用于连接任何数据源到 Autocomplete。回调函数接受两个参数：
		<ul>
			<li>一个 <code>request</code> 对象，带有一个 <code>term</code> 属性，表示当前文本输入中的值。例如，如果用户在 city 字段输入 <code>"new yo"</code>，则 Autocomplete term 等同于 <code>"new yo"</code>。</li>
			<li>一个 <code>response</code> 回调函数，提供单个参数：建议给用户的数据。该数据应基于被提供的 term 进行过滤，且可以是上面描述的本地数据的任何格式。用于在请求期间提供自定义 source 回调来处理错误。即使遇到错误，您也必须调用 <code>response</code> 回调函数。这就确保了小部件总是正确的状态。</li>
		</ul>
		<p>当过滤本地数据时，您可以使用内置的 <code>$.ui.autocomplete.escapeRegex</code> 函数。它会接受一个字符串参数，转义所有的正则表达式字符，让结果安全地传递到 <code>new RegExp()</code>。</p>
		</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>source</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ source: [ "c++", "java", "php", "coldfusion", "javascript", "asp", "ruby" ] });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>source</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var source = $( ".selector" ).autocomplete( "option", "source" );
 
// setter
$( ".selector" ).autocomplete( "option", "source", [ "c++", "java", "php", "coldfusion", "javascript", "asp", "ruby" ] );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>none; must be specified</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate" id="options">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-appendTo">
	<td>appendTo</td>
    <td>Selector</td>
	<td>菜单应该被附加到哪一个元素。当该值为 <code>null</code> 时，输入域的父元素将检查 <code>ui-front</code> class。如果找到带有 <code>ui-front</code> class 的元素，菜单将被附加到该元素。如果未找到带有 <code>ui-front</code> class 的元素，不管值为多少，菜单将被附加到 body。<br />
	<p><strong>注意：当建议菜单打开时，<code>appendTo</code> 选项不应改变。</strong></p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>appendTo</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ appendTo: "#someElem" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>appendTo</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var appendTo = $( ".selector" ).autocomplete( "option", "appendTo" );
 
// setter
$( ".selector" ).autocomplete( "option", "appendTo", "#someElem" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-autoFocus">
	<td>autoFocus</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，当菜单显示时，第一个条目将自动获得焦点。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>autoFocus</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ autoFocus: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>autoFocus</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var autoFocus = $( ".selector" ).autocomplete( "option", "autoFocus" );
 
// setter
$( ".selector" ).autocomplete( "option", "autoFocus", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-delay">
	<td>delay</td>
    <td>Integer</td>
	<td>按键和执行搜索之间的延迟，以毫秒计。对于本地数据，采用零延迟是有意义的（更具响应性），但对于远程数据会产生大量的负荷，同时降低了响应性。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>delay</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ delay: 500 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>delay</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var delay = $( ".selector" ).autocomplete( "option", "delay" );
 
// setter
$( ".selector" ).autocomplete( "option", "delay", 500 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>300</td>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 autocomplete。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).autocomplete( "option", "disabled" );
 
// setter
$( ".selector" ).autocomplete( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-minLength">
	<td>minLength</td>
    <td>Integer</td>
	<td>执行搜索前用户必须输入的最小字符数。对于仅带有几项条目的本地数据，通常设置为零，但是当单个字符搜索会匹配几千项条目时，设置个高数值是很有必要的。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>minLength</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ minLength: 0 });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>minLength</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var minLength = $( ".selector" ).autocomplete( "option", "minLength" );
 
// setter
$( ".selector" ).autocomplete( "option", "minLength", 0 );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>1</td>
</tr>
<tr id="option-position">
	<td>position</td>
    <td>Object</td>
	<td>标识建议菜单的位置与相关的 input 元素有关系。<code>of</code> 选项默认为 input 元素，但是您可以指定另一个定位元素。如需了解各种选项的更多细节，请查看 <a href="/jqueryui/api-position.html"> jQuery UI 定位（Position）</a>。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>position</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ position: { my : "right top", at: "right bottom" } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>position</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var position = $( ".selector" ).autocomplete( "option", "position" );
 
// setter
$( ".selector" ).autocomplete( "option", "position", { my : "right top", at: "right bottom" } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>{ my: "left top", at: "left bottom", collision: "none" }</td>
</tr>
<tr id="option-source">
	<td>source</td>
    <td>Array 或 String 或 Function( Object request, Function response( Object data ) )</td>
	<td>定义要使用的数据，必须指定。<br />
	<p>独立于您要使用的变量，标签总是被视为文本。如果您想要标签被视为 html，您可以使用 <a href="https://github.com/scottgonzalez/jquery-ui-extensions/blob/master/src/autocomplete/jquery.ui.autocomplete.html.js" target="_blank" rel="noopener noreferrer">Scott González' html 扩展</a>。演示侧重于 <code>source</code> 选项的不同变量 - 您可以查找其中匹配您的使用情况的那个，并查看相关代码。</p>
	<p><strong>支持多个类型：</strong></p>
	<ul>
		<li><strong>Array</strong>：可用于本地数据的一个数组。支持两种格式：
		<ul>
			<li>字符串数组：<code>[ "Choice1", "Choice2" ]</code></li>
			<li>带有 <code>label</code> 和 <code>value</code> 属性的对象数组：<code>[ { label: "Choice1", value: "value1" }, ... ]</code></li>
		</ul>
		label 属性显示在建议菜单中。当用户选择一个条目时，value 将被插入到 input 元素中。如果只是指定了一个属性，则该属性将被视为 label 和 value，例如，如果您只提供了 <code>value</code> 属性，value 也会被视为标签。
		</li>
		<li><strong>String</strong>：当使用一个字符串，Autocomplete 插件希望该字符串指向一个能返回 JSON 数据的 URL 资源。它可以是在相同的主机上，也可以是在不同的主机上（必须提供 JSONP）。Autocomplete 插件不过滤结果，而是通过一个 <code>term</code> 字段添加了一个查询字符串，用于服务器端脚本过滤结果。例如，如果 <code>source</code> 选项设置为 <code>"http://example.com"</code>，且用户键入了 <code>foo</code>，GET 请求则为 <code>http://example.com?term=foo</code>。数据本身的格式可以与前面描述的本地数据的格式相同。</li>
		<li><strong>Function</strong>：第三个变量，一个回调函数，提供最大的灵活性，可用于连接任何数据源到 Autocomplete。回调函数接受两个参数：
		<ul>
			<li>一个 <code>request</code> 对象，带有一个 <code>term</code> 属性，表示当前文本输入中的值。例如，如果用户在 city 字段输入 <code>"new yo"</code>，则 Autocomplete term 等同于 <code>"new yo"</code>。</li>
			<li>一个 <code>response</code> 回调函数，提供单个参数：建议给用户的数据。该数据应基于被提供的 term 进行过滤，且可以是上面描述的本地数据的任何格式。用于在请求期间提供自定义 source 回调来处理错误。即使遇到错误，您也必须调用 <code>response</code> 回调函数。这就确保了小部件总是正确的状态。</li>
		</ul>
		<p>当过滤本地数据时，您可以使用内置的 <code>$.ui.autocomplete.escapeRegex</code> 函数。它会接受一个字符串参数，转义所有的正则表达式字符，让结果安全地传递到 <code>new RegExp()</code>。</p>
		</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>source</code> 选项的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({ source: [ "c++", "java", "php", "coldfusion", "javascript", "asp", "ruby" ] });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>source</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var source = $( ".selector" ).autocomplete( "option", "source" );
 
// setter
$( ".selector" ).autocomplete( "option", "source", [ "c++", "java", "php", "coldfusion", "javascript", "asp", "ruby" ] );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>none; must be specified</td>
</tr>
</table></div>') where ID=6221;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate" id="methods">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-close">
	<td>close()</td>
    <td>jQuery (plugin only)</td>
	<td>关闭 Autocomplete 菜单。当与 <code><a href="#method-search" target="_blank" rel="noopener noreferrer">search</a></code> 方法结合使用时，可用于关闭打开的菜单。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 close 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "close" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 autocomplete 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 autocomplete。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 autocomplete。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).autocomplete( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 autocomplete 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).autocomplete( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 autocomplete 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 autocomplete 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-search">
	<td>search( [value ] )</td>
    <td>jQuery (plugin only)</td>
	<td>触发 <a href="#event-search"><code>search</code></a> 事件，如果该事件未被取消则调用数据源。当被点击时，可被类似选择框按钮用来打开建议。当不带参数调用该方法时，则使用当前输入的值。可带一个空字符串和 <code>minLength: 0</code> 进行调用，来显示所有的条目。<br />
	<ul>
	<li><strong>value</strong><br />
	类型：String
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 search 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "search", "" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含菜单元素的 <code>jQuery</code> 对象。虽然菜单项不断地被创建和销毁。菜单元素本身会在初始化时创建，并不断的重复使用。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate" id="methods">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-close">
	<td>close()</td>
    <td>jQuery (plugin only)</td>
	<td>关闭 Autocomplete 菜单。当与 <code><a href="#method-search" target="_blank" rel="noopener noreferrer">search</a></code> 方法结合使用时，可用于关闭打开的菜单。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 close 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "close" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 autocomplete 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 autocomplete。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 autocomplete。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).autocomplete( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 autocomplete 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).autocomplete( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 autocomplete 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 autocomplete 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-search">
	<td>search( [value ] )</td>
    <td>jQuery (plugin only)</td>
	<td>触发 <a href="#event-search"><code>search</code></a> 事件，如果该事件未被取消则调用数据源。当被点击时，可被类似选择框按钮用来打开建议。当不带参数调用该方法时，则使用当前输入的值。可带一个空字符串和 <code>minLength: 0</code> 进行调用，来显示所有的条目。<br />
	<ul>
	<li><strong>value</strong><br />
	类型：String
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 search 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "search", "" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含菜单元素的 <code>jQuery</code> 对象。虽然菜单项不断地被创建和销毁。菜单元素本身会在初始化时创建，并不断的重复使用。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6221;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate" id="extension-points">
<tr>
	<th width="15%">扩展点</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr>
	<td colspan="3">
	自动完成部件（Autocomplete Widget）通过 <a href="/jqueryui/api-jQuery-widget.html">部件库（Widget Factory）</a> 创建的，且可被扩展。当对部件进行扩展时，您可以重载或者添加扩展部件的行为。下面提供的方法作为扩展点，与上面列出的 <a href="#methods">插件方法</a> 具有相同的 API 稳定性。如需了解更多有关小部件扩展的知识，请查看 <a href="/jqueryui/jqueryui-widget-extend.html">通过部件库（Widget Factory）扩展小部件</a>.
	</td>
</tr>
<tr id="method-_renderItem">
	<td>_renderItem( ul, item )</td>
    <td>jQuery</td>
	<td>Method that controls the creation of each option in the widget's menu. The method must create a new <code>&lt;li&gt;</code> element, append it to the menu, and return it.<br />
	<p><em>Note: At this time the<code>&lt;ul&gt;</code> element created must contain an <code>&lt;a&gt;</code> element for compatibility with the <a href="/menu/">menu</a> widget. See the example below.</em></p>
	<ul>
	<li><strong>ul</strong><br />
	类型：jQuery<br />
	描述：新创建的 <code>&lt;li&gt;</code> 元素必须追加到的 <code>&lt;ul&gt;</code> 元素。
	</li>
	<li><strong>item</strong><br />
	类型：Object<br />
		<ul>
		<li><strong>label</strong><br />
		类型：String<br />
		描述：条目显示的字符串。
		</li>
		<li><strong>item</strong><br />
		类型：String<br />
		描述：当条目被选中时插入到输入框中的值。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>添加条目的值作为 <code>&lt;li&gt;</code> 上的 data 属性。</p>
	<pre style="white-space: pre-wrap;">
_renderItem: function( ul, item ) {
  return $( "&lt;li&gt;" )
    .attr( "data-value", item.value )
    .append( $( "&lt;a&gt;" ).text( item.label ) )
    .appendTo( ul );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_renderMenu">
	<td>_renderMenu( ul, items )</td>
    <td>jQuery (plugin only)</td>
	<td>该方法负责在菜单显示前调整菜单尺寸。菜单元素可通过 <code>this.menu.element</code> 使用。<br />
	<ul>
	<li><strong>ul</strong><br />
	类型：jQuery<br />
	描述：一个要作为小部件的菜单使用的空的 <code>&lt;ul&gt;</code> 元素。
	</li>
	<li><strong>items</strong><br />
	类型：Array<br />
	描述：一个数组，数组元素为匹配用户输入的条目。每个条目是一个带有 <code>label</code> 和 <code>value</code> 属性的对象。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>添加一个 CSS class 名称到旧的菜单项。</p>
	<pre style="white-space: pre-wrap;">
_renderMenu: function( ul, items ) {
  var that = this;
  $.each( items, function( index, item ) {
    that._renderItemData( ul, item );
  });
  $( ul ).find( "li:odd" ).addClass( "odd" );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_resizeMenu">
	<td>_resizeMenu()</td>
    <td>jQuery (plugin only)</td>
	<td>该方法负责在菜单显示前调整菜单尺寸。菜单元素可通过 <code>this.menu.element</code> 使用。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>菜单总是显示为 500 像素宽。</p>
	<pre style="white-space: pre-wrap;">
_resizeMenu: function() {
  this.menu.element.outerWidth( 500 );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate" id="extension-points">
<tr>
	<th width="15%">扩展点</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr>
	<td colspan="3">
	自动完成部件（Autocomplete Widget）通过 <a href="/jqueryui/api-jQuery-widget.html">部件库（Widget Factory）</a> 创建的，且可被扩展。当对部件进行扩展时，您可以重载或者添加扩展部件的行为。下面提供的方法作为扩展点，与上面列出的 <a href="#methods">插件方法</a> 具有相同的 API 稳定性。如需了解更多有关小部件扩展的知识，请查看 <a href="/jqueryui/jqueryui-widget-extend.html">通过部件库（Widget Factory）扩展小部件</a>.
	</td>
</tr>
<tr id="method-_renderItem">
	<td>_renderItem( ul, item )</td>
    <td>jQuery</td>
	<td>Method that controls the creation of each option in the widget's menu. The method must create a new <code>&lt;li&gt;</code> element, append it to the menu, and return it.<br />
	<p><em>Note: At this time the<code>&lt;ul&gt;</code> element created must contain an <code>&lt;a&gt;</code> element for compatibility with the <a href="/menu/">menu</a> widget. See the example below.</em></p>
	<ul>
	<li><strong>ul</strong><br />
	类型：jQuery<br />
	描述：新创建的 <code>&lt;li&gt;</code> 元素必须追加到的 <code>&lt;ul&gt;</code> 元素。
	</li>
	<li><strong>item</strong><br />
	类型：Object<br />
		<ul>
		<li><strong>label</strong><br />
		类型：String<br />
		描述：条目显示的字符串。
		</li>
		<li><strong>item</strong><br />
		类型：String<br />
		描述：当条目被选中时插入到输入框中的值。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>添加条目的值作为 <code>&lt;li&gt;</code> 上的 data 属性。</p>
	<pre style="white-space: pre-wrap;">
_renderItem: function( ul, item ) {
  return $( "&lt;li&gt;" )
    .attr( "data-value", item.value )
    .append( $( "&lt;a&gt;" ).text( item.label ) )
    .appendTo( ul );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_renderMenu">
	<td>_renderMenu( ul, items )</td>
    <td>jQuery (plugin only)</td>
	<td>该方法负责在菜单显示前调整菜单尺寸。菜单元素可通过 <code>this.menu.element</code> 使用。<br />
	<ul>
	<li><strong>ul</strong><br />
	类型：jQuery<br />
	描述：一个要作为小部件的菜单使用的空的 <code>&lt;ul&gt;</code> 元素。
	</li>
	<li><strong>items</strong><br />
	类型：Array<br />
	描述：一个数组，数组元素为匹配用户输入的条目。每个条目是一个带有 <code>label</code> 和 <code>value</code> 属性的对象。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>添加一个 CSS class 名称到旧的菜单项。</p>
	<pre style="white-space: pre-wrap;">
_renderMenu: function( ul, items ) {
  var that = this;
  $.each( items, function( index, item ) {
    that._renderItemData( ul, item );
  });
  $( ul ).find( "li:odd" ).addClass( "odd" );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-_resizeMenu">
	<td>_resizeMenu()</td>
    <td>jQuery (plugin only)</td>
	<td>该方法负责在菜单显示前调整菜单尺寸。菜单元素可通过 <code>this.menu.element</code> 使用。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>菜单总是显示为 500 像素宽。</p>
	<pre style="white-space: pre-wrap;">
_resizeMenu: function() {
  this.menu.element.outerWidth( 500 );
}
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6221;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate" id="events">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-change">
	<td>change( event, ui )</td>
	<td>autocompletechange</td>
	<td>如果输入域的值改变则触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>item</strong><br />
		类型：Object<br />
		描述：从菜单中选择的条目，否则属性为 <code>null</code>。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 change 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  change: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompletechange 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompletechange", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-close">
	<td>close( event, ui )</td>
    <td>autocompleteclose</td>
	<td>当菜单隐藏时触发。不是每一个 <code>close</code> 事件都伴随着 <code>change</code> 事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 close 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  close: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompleteclose 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompleteclose", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>autocompletecreate</td>
	<td>当创建 autocomplete 时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompletecreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompletecreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-focus">
	<td>focus( event, ui )</td>
    <td>autocompletefocus</td>
	<td>当焦点移动到一个条目上（未选择）时触发。默认的动作是把文本域中的值替换为获得焦点的条目的值，即使该事件是通过键盘交互触发的。取消该事件会阻止值被更新，但不会阻止菜单项获得焦点。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>item</strong><br />
		类型：Object<br />
		描述：获得焦点的条目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 focus 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  focus: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompletefocus 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompletefocus", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-open">
	<td>open( event, ui )</td>
    <td>autocompleteopen</td>
	<td>当打开建议菜单或者更新建议菜单时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 open 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  open: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompleteopen 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompleteopen", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-response">
	<td>response( event, ui )</td>
    <td>autocompleteresponse</td>
	<td>在搜索完成后菜单显示前触发。用于建议数据的本地操作，其中自定义的 <a href="#option-source"><code>source</code></a> 选项回调不是必需的。该事件总是在搜索完成时触发，如果搜索无结果或者禁用了 Autocomplete，导致菜单未显示，该事件一样会被触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>content</strong><br />
		类型：Array<br />
		描述：包含响应数据，且可被修改来改变显示结果。该数据已经标准化，所以如果您要修改数据，请确保每个条目都包含 <code>value</code> 和 <code>label</code> 属性。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 response 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  response: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompleteresponse 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompleteresponse", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-search">
	<td>search( event, ui )</td>
    <td>autocompletesearch</td>
	<td>在搜索执行前满足 <a href="#option-minLength"><code>minLength</code></a> 和 <a href="#option-delay"><code>delay</code></a> 后触发。如果取消该事件，则不会提交请求，也不会提供建议条目。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 search 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  search: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompletesearch 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompletesearch", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-select">
	<td>select( event, ui )</td>
    <td>autocompleteselect</td>
	<td>当从菜单中选择条目时触发。默认的动作是把文本域中的值替换为被选中的条目的值。取消该事件会阻止值被更新，但不会阻止菜单关闭。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>item</strong><br />
		类型：Object<br />
		描述：一个带有被选项的 <code>label</code> 和 <code>value</code> 属性的对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 select 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  select: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompleteselect 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompleteselect", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate" id="events">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-change">
	<td>change( event, ui )</td>
	<td>autocompletechange</td>
	<td>如果输入域的值改变则触发该事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>item</strong><br />
		类型：Object<br />
		描述：从菜单中选择的条目，否则属性为 <code>null</code>。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 change 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  change: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompletechange 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompletechange", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-close">
	<td>close( event, ui )</td>
    <td>autocompleteclose</td>
	<td>当菜单隐藏时触发。不是每一个 <code>close</code> 事件都伴随着 <code>change</code> 事件。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 close 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  close: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompleteclose 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompleteclose", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
    <td>autocompletecreate</td>
	<td>当创建 autocomplete 时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompletecreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompletecreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-focus">
	<td>focus( event, ui )</td>
    <td>autocompletefocus</td>
	<td>当焦点移动到一个条目上（未选择）时触发。默认的动作是把文本域中的值替换为获得焦点的条目的值，即使该事件是通过键盘交互触发的。取消该事件会阻止值被更新，但不会阻止菜单项获得焦点。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>item</strong><br />
		类型：Object<br />
		描述：获得焦点的条目。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 focus 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  focus: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompletefocus 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompletefocus", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-open">
	<td>open( event, ui )</td>
    <td>autocompleteopen</td>
	<td>当打开建议菜单或者更新建议菜单时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 open 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  open: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompleteopen 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompleteopen", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-response">
	<td>response( event, ui )</td>
    <td>autocompleteresponse</td>
	<td>在搜索完成后菜单显示前触发。用于建议数据的本地操作，其中自定义的 <a href="#option-source"><code>source</code></a> 选项回调不是必需的。该事件总是在搜索完成时触发，如果搜索无结果或者禁用了 Autocomplete，导致菜单未显示，该事件一样会被触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>content</strong><br />
		类型：Array<br />
		描述：包含响应数据，且可被修改来改变显示结果。该数据已经标准化，所以如果您要修改数据，请确保每个条目都包含 <code>value</code> 和 <code>label</code> 属性。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 response 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  response: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompleteresponse 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompleteresponse", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-search">
	<td>search( event, ui )</td>
    <td>autocompletesearch</td>
	<td>在搜索执行前满足 <a href="#option-minLength"><code>minLength</code></a> 和 <a href="#option-delay"><code>delay</code></a> 后触发。如果取消该事件，则不会提交请求，也不会提供建议条目。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 search 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  search: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompletesearch 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompletesearch", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="event-select">
	<td>select( event, ui )</td>
    <td>autocompleteselect</td>
	<td>当从菜单中选择条目时触发。默认的动作是把文本域中的值替换为被选中的条目的值。取消该事件会阻止值被更新，但不会阻止菜单关闭。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
		<ul>
		<li><strong>item</strong><br />
		类型：Object<br />
		描述：一个带有被选项的 <code>label</code> 和 <code>value</code> 属性的对象。
		</li>
		</ul>
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 select 回调的 autocomplete：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).autocomplete({
  select: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 autocompleteselect 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "autocompleteselect", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6221;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 button。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).button( "option", "disabled" );
 
// setter
$( ".selector" ).button( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-icons">
	<td>icons</td>
    <td>Object</td>
	<td>要显示的图标，包括带有文本的图标和不带有文本的图标（查看 <a href="#option-text"><code>text</code></a> 选项）。默认情况下 ，主图标显示在标签文本的左边，副图标显示在右边。显示位置可通过 CSS 进行控制。<br />
	<p><code>primary</code> 和 <code>secondary</code> 属性值必须是 <a href="/jqueryui/api-icons.html">图标 class 名称</a>，例如，<code>"ui-icon-gear"</code>。如果只使用一个图标，则 <code>icons: { primary: "ui-icon-locked" }</code>。如果使用两个图标，则 <code>icons: { primary: "ui-icon-gear", secondary: "ui-icon-triangle-1-s" }</code>。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>icons</code> 选项的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({ icons: { primary: "ui-icon-gear", secondary: "ui-icon-triangle-1-s" } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var icons = $( ".selector" ).button( "option", "icons" );
 
// setter
$( ".selector" ).button( "option", "icons", { primary: "ui-icon-gear", secondary: "ui-icon-triangle-1-s" } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>{ primary: null, secondary: null }</td>
</tr>
<tr id="option-label">
	<td>label</td>
    <td>String</td>
	<td>要显示在按钮中的文本。当未指定时（<code>null</code>），则使用元素的 HTML 内容，或者如果元素是一个 submit 或 reset 类型的 input 元素，则使用它的 <code>value</code> 属性，或者如果元素是一个 radio 或 checkbox 类型的 input 元素，则使用相关的 label 元素的 HTML 内容。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>label</code> 选项的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({ label: "custom label" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>label</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var label = $( ".selector" ).button( "option", "label" );
 
// setter
$( ".selector" ).button( "option", "label", "custom label" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-text">
	<td>text</td>
    <td>Boolean</td>
	<td>是否显示标签。当设置为 <code>false</code> 时，不显示文本，但是此时必须启用 <a href="#options-icons"><code>icons</code></a> 选项，否则 <code>text</code> 选项将被忽略。 <br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>text</code> 选项的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({ text: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>text</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var text = $( ".selector" ).button( "option", "text" );
 
// setter
$( ".selector" ).button( "option", "text", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">选项</th>
	<th width="15%">类型</th>
    <th width="60%">描述</th>
	<th width="10%">默认值</th>
</tr>
<tr id="option-disabled">
	<td>disabled</td>
    <td>Boolean</td>
	<td>如果设置为 <code>true</code>，则禁用该 button。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>disabled</code> 选项的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({ disabled: true });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var disabled = $( ".selector" ).button( "option", "disabled" );
 
// setter
$( ".selector" ).button( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>false</td>
</tr>
<tr id="option-icons">
	<td>icons</td>
    <td>Object</td>
	<td>要显示的图标，包括带有文本的图标和不带有文本的图标（查看 <a href="#option-text"><code>text</code></a> 选项）。默认情况下 ，主图标显示在标签文本的左边，副图标显示在右边。显示位置可通过 CSS 进行控制。<br />
	<p><code>primary</code> 和 <code>secondary</code> 属性值必须是 <a href="/jqueryui/api-icons.html">图标 class 名称</a>，例如，<code>"ui-icon-gear"</code>。如果只使用一个图标，则 <code>icons: { primary: "ui-icon-locked" }</code>。如果使用两个图标，则 <code>icons: { primary: "ui-icon-gear", secondary: "ui-icon-triangle-1-s" }</code>。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>icons</code> 选项的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({ icons: { primary: "ui-icon-gear", secondary: "ui-icon-triangle-1-s" } });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>disabled</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var icons = $( ".selector" ).button( "option", "icons" );
 
// setter
$( ".selector" ).button( "option", "icons", { primary: "ui-icon-gear", secondary: "ui-icon-triangle-1-s" } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>{ primary: null, secondary: null }</td>
</tr>
<tr id="option-label">
	<td>label</td>
    <td>String</td>
	<td>要显示在按钮中的文本。当未指定时（<code>null</code>），则使用元素的 HTML 内容，或者如果元素是一个 submit 或 reset 类型的 input 元素，则使用它的 <code>value</code> 属性，或者如果元素是一个 radio 或 checkbox 类型的 input 元素，则使用相关的 label 元素的 HTML 内容。<br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>label</code> 选项的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({ label: "custom label" });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>label</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var label = $( ".selector" ).button( "option", "label" );
 
// setter
$( ".selector" ).button( "option", "label", "custom label" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>null</td>
</tr>
<tr id="option-text">
	<td>text</td>
    <td>Boolean</td>
	<td>是否显示标签。当设置为 <code>false</code> 时，不显示文本，但是此时必须启用 <a href="#options-icons"><code>icons</code></a> 选项，否则 <code>text</code> 选项将被忽略。 <br />
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 <code>text</code> 选项的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({ text: false });
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>在初始化后，获取或设置 <code>text</code> 选项：</p>
	<pre style="white-space: pre-wrap;">
// getter
var text = $( ".selector" ).button( "option", "text" );
 
// setter
$( ".selector" ).button( "option", "text", false );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
	<td>true</td>
</tr>
</table></div>') where ID=6222;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 button 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 button。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 button。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).button( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 button 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).button( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 button 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 button 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refresh">
	<td>refresh()</td>
	<td>jQuery (plugin only)</td>
	<td>刷新按钮的视觉状态。用于在以编程方式改变原生元素的选中状态或禁用状态后更新按钮状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refresh 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "refresh" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 button 的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).button( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">方法</th>
	<th width="15%">返回</th>
    <th width="70%">描述</th>
</tr>
<tr id="method-destroy">
	<td>destroy()</td>
    <td>jQuery (plugin only)</td>
	<td>完全移除 button 功能。这会把元素返回到它的预初始化状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 destroy 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "destroy" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-disable">
	<td>disable()</td>
    <td>jQuery (plugin only)</td>
	<td>禁用 button。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 disable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "disable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-enable">
	<td>enable()</td>
    <td>jQuery (plugin only)</td>
	<td>启用 button。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 enable 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "enable" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-option">
	<td>option( optionName )</td>
    <td>Object</td>
	<td>获取当前与指定的 <code>optionName</code> 关联的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要获取的选项的名称。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var isDisabled = $( ".selector" ).button( "option", "disabled" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option()</td>
    <td>PlainObject</td>
	<td>获取一个包含键/值对的对象，键/值对表示当前 button 选项哈希。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
var options = $( ".selector" ).button( "option" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( optionName, value )</td>
    <td>jQuery (plugin only)</td>
	<td>设置与指定的 <code>optionName</code> 关联的 button 选项的值。<br />
	<ul>
	<li><strong>optionName</strong><br />
	类型：String<br />
	描述：要设置的选项的名称。
	</li>
	<li><strong>value</strong><br />
	类型：Object<br />
	描述：要为选项设置的值。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "option", "disabled", true );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr>
	<td>option( options )</td>
    <td>jQuery (plugin only)</td>
	<td>为 button 设置一个或多个选项。<br />
	<ul>
	<li><strong>options</strong><br />
	类型：Object<br />
	描述：要设置的 option-value 对。
	</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用该方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "option", { disabled: true } );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-refresh">
	<td>refresh()</td>
	<td>jQuery (plugin only)</td>
	<td>刷新按钮的视觉状态。用于在以编程方式改变原生元素的选中状态或禁用状态后更新按钮状态。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 refresh 方法：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button( "refresh" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
<tr id="method-widget">
	<td>widget()</td>
    <td>jQuery</td>
	<td>返回一个包含 button 的 <code>jQuery</code> 对象。<br />
	<ul>
	<li>该方法不接受任何参数。</li>
	</ul>
	<p><strong>代码实例：</strong></p>
	<p>调用 widget 方法：</p>
	<pre style="white-space: pre-wrap;">
var widget = $( ".selector" ).button( "widget" );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6222;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
	<td>buttoncreate</td>
	<td>当创建按钮 button 时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 buttoncreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "buttoncreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr>
	<th width="15%">事件</th>
	<th width="15%">类型</th>
    <th width="70%">描述</th>
</tr>
<tr id="event-create">
	<td>create( event, ui )</td>
	<td>buttoncreate</td>
	<td>当创建按钮 button 时触发。<br />
	<ul>
	<li><strong>event</strong><br />
	类型：Event
	</li>
	<li><strong>ui</strong><br />
	类型：Object
	</li>
	</ul>
	<p>注意：<code>ui</code> 对象是空的，这里包含它是为了与其他事件保持一致性。</p>
	<p><strong>代码实例：</strong></p>
	<p>初始化带有指定 create 回调的 button：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).button({
  create: function( event, ui ) {}
});
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	<p>绑定一个事件监听器到 buttoncreate 事件：</p>
	<pre style="white-space: pre-wrap;">
$( ".selector" ).on( "buttoncreate", function( event, ui ) {} );
&nbsp;&nbsp;&nbsp;&nbsp;</pre>
	</td>
</tr>
</table></div>') where ID=6222;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:12%">函数</th>
<th>描述</th>
<th>实例</th>
</tr>
<tr>
<td>ASCII(s)</td>
<td>返回字符串 s 的第一个字符的 ASCII 码。</td>
<td>
<p>返回 CustomerName 字段第一个字母的 ASCII 码：</p>
<pre>SELECT ASCII(CustomerName) AS NumCodeOfFirstChar
FROM Customers;</pre></td>
</tr>
<tr>
<td>CHAR_LENGTH(s)</td>
<td>返回字符串 s 的字符数</td>
<td>
<p>返回字符串 RUNOOB 的字符数</p>
<pre>
SELECT CHAR_LENGTH("RUNOOB") AS LengthOfString;
</pre>
</td>
</tr>
<tr>
<td>CHARACTER_LENGTH(s)</td>
<td>返回字符串 s 的字符数</td>
<td>
<p>返回字符串 RUNOOB 的字符数</p>
<pre>
SELECT CHARACTER_LENGTH("RUNOOB") AS LengthOfString;
</pre>
</td>
</tr>
<tr>
<td>CONCAT(s1,s2...sn)</td>
<td>字符串 s1,s2 等多个字符串合并为一个字符串</td>
<td>
<p>合并多个字符串</p>
<pre>
SELECT CONCAT("SQL ", "Runoob ", "Gooogle ", "Facebook") AS ConcatenatedString;
</pre>
</td>
</tr>
<tr>
<td>CONCAT_WS(x, s1,s2...sn)</td>
<td>同 CONCAT(s1,s2,...) 函数，但是每个字符串之间要加上 x，x 可以是分隔符</td>
<td>
<p>合并多个字符串，并添加分隔符：</p>
<pre>
SELECT CONCAT_WS("-", "SQL", "Tutorial", "is", "fun!")AS ConcatenatedString;
</pre>
</td>
</tr>
<tr>
<td>FIELD(s,s1,s2...)</td>
<td>返回第一个字符串 s 在字符串列表(s1,s2...)中的位置</td>
<td>
<p>返回字符串 c 在列表值中的位置：</p>
<pre>
SELECT FIELD("c", "a", "b", "c", "d", "e");
</pre>
</td>
</tr>
<tr>
<td>FIND_IN_SET(s1,s2)</td>
<td>返回在字符串s2中与s1匹配的字符串的位置</td>
<td>
<p>返回字符串 c 在指定字符串中的位置：</p>
<pre>
SELECT FIND_IN_SET("c", "a,b,c,d,e");
</pre>
</td>
</tr>
<tr>
<td>FORMAT(x,n)</td>
<td>函数可以将数字 x 进行格式化 "#,###.##", 将 x 保留到小数点后 n 位，最后一位四舍五入。
</td>
<td>
<p>格式化数字 "#,###.##" 形式：</p>
<pre>
SELECT FORMAT(250500.5634, 2);     -- 输出 250,500.56
</pre>
</td>
</tr>
<tr>
<td>INSERT(s1,x,len,s2)</td>
<td>字符串 s2 替换 s1 的 x 位置开始长度为 len 的字符串</td>
<td>
<p>从字符串第一个位置开始的 6 个字符替换为 runoob：</p>
<pre>
SELECT INSERT("google.com", 1, 6, "runnob");  -- 输出：panku.in
</pre>
</td>
</tr>
<tr>
<td>LOCATE(s1,s)</td>
<td>从字符串 s 中获取 s1 的开始位置</td>
<td>
<p>获取 b 在字符串 abc 中的位置：</p>
<pre>
SELECT LOCATE('st','myteststring');  -- 5
</pre>
<p>返回字符串 abc 中 b 的位置：</p>

<pre>SELECT LOCATE('b', 'abc') -- 2</pre>
</td>
</tr>
<tr>
<td>LCASE(s)</td>
<td>将字符串 s 的所有字母变成小写字母</td>
<td>
<p>字符串 RUNOOB 转换为小写：</p>
<pre>
SELECT LCASE('RUNOOB') -- runoob
</pre>
</td>
</tr>
<tr>
<td>LEFT(s,n)</td>
<td>返回字符串 s 的前 n 个字符</td>
<td>
<p>返回字符串 runoob 中的前两个字符：</p>
<pre>
SELECT LEFT('runoob',2) -- ru
</pre>

</td>
</tr>


<tr>
<td>LOWER(s)</td>
<td>将字符串 s 的所有字母变成小写字母</td>
<td>
<p>字符串 RUNOOB 转换为小写：</p>
<pre>
SELECT LOWER('RUNOOB') -- runoob
</pre>
</td>
</tr>
<tr>
<td>LPAD(s1,len,s2)</td>
<td>在字符串 s1 的开始处填充字符串 s2，使字符串长度达到 len</td>
<td>
<p>将字符串 xx 填充到 abc 字符串的开始处：</p>
<pre>
SELECT LPAD('abc',5,'xx') -- xxabc
</pre>
</td>
</tr>
<tr>
<td>LTRIM(s)</td>
<td>去掉字符串 s 开始处的空格</td>
<td>
<p>去掉字符串 RUNOOB开始处的空格：</p>
<pre>
SELECT LTRIM("    RUNOOB") AS LeftTrimmedString;-- RUNOOB
</pre>
</td>
</tr>
<tr>
<td>MID(s,n,len)</td>
<td>从字符串 s 的 n 位置截取长度为 len 的子字符串，同 SUBSTRING(s,n,len)</td>
<td>
<p>从字符串 RUNOOB 中的第 2 个位置截取 3个 字符：</p>
<pre>
SELECT MID("RUNOOB", 2, 3) AS ExtractString; -- UNO
</pre>
</td>
</tr>
<tr>
<td>POSITION(s1 IN s)</td>
<td>从字符串 s 中获取 s1 的开始位置</td>
<td>
<p>返回字符串 abc 中 b 的位置：</p>
<pre>
SELECT POSITION('b' in 'abc') -- 2
</pre>
</td>
</tr>
<tr>
<td>REPEAT(s,n)</td>
<td>将字符串 s 重复 n 次 </td>
<td>
<p>将字符串 runoob 重复三次：</p>
<pre>
SELECT REPEAT('runoob',3) -- runoobrunoobrunoob
</pre>
</td>
</tr>
<tr>
<td>REPLACE(s,s1,s2)</td>
<td>将字符串 s2 替代字符串 s 中的字符串 s1</td>
<td>
<p>将字符串 abc 中的字符 a 替换为字符 x：</p>
<pre>
SELECT REPLACE('abc','a','x') --xbc
</pre>
</td>
</tr>
<tr>
<td>REVERSE(s)</td>
<td>将字符串s的顺序反过来</td>
<td>
<p>将字符串 abc 的顺序反过来：</p>
<pre>
SELECT REVERSE('abc') -- cba
</pre>
</td>
</tr>
<tr>
<td>RIGHT(s,n)</td>
<td>返回字符串 s 的后 n 个字符</td>
<td>
<p>返回字符串 runoob 的后两个字符：</p>
<pre>
SELECT RIGHT('runoob',2) -- ob
</pre>
</td>
</tr>
<tr>
<td>RPAD(s1,len,s2)</td>
<td>在字符串 s1 的结尾处添加字符串 s2，使字符串的长度达到 len</td>
<td>
<p>将字符串 xx 填充到 abc 字符串的结尾处：</p>
<pre>
SELECT RPAD('abc',5,'xx') -- abcxx
</pre>
</td>
</tr>
<tr>
<td>RTRIM(s)</td>
<td>去掉字符串 s 结尾处的空格</td>
<td>
<p>去掉字符串 RUNOOB 的末尾空格：</p>
<pre>
SELECT RTRIM("RUNOOB     ") AS RightTrimmedString;   -- RUNOOB
</pre>
</td>
</tr>
<tr>
<td>SPACE(n)</td>
<td>返回 n 个空格</td>
<td>
<p>返回 10 个空格：</p>
<pre>
SELECT SPACE(10);
</pre>
</td>
</tr>
<tr>
<td>STRCMP(s1,s2)</td>
<td>比较字符串 s1 和 s2，如果 s1 与 s2 相等返回 0 ，如果 s1&gt;s2 返回 1，如果 s1&lt;s2 返回 -1</td>
<td>
<p>比较字符串：</p>
<pre>
SELECT STRCMP("runoob", "runoob");  -- 0
</pre>
</td>
</tr>
<tr>
<td>SUBSTR(s, start, length)</td>
<td>从字符串 s 的 start 位置截取长度为 length 的子字符串</td>
<td>
<p>从字符串 RUNOOB 中的第 2 个位置截取 3个 字符：</p>
<pre>
SELECT SUBSTR("RUNOOB", 2, 3) AS ExtractString; -- UNO
</pre>
</td>
</tr>
<tr>
<td>SUBSTRING(s, start, length)</td>
<td>从字符串 s 的 start 位置截取长度为 length 的子字符串</td>
<td>
<p>从字符串 RUNOOB 中的第 2 个位置截取 3个 字符：</p>
<pre>
SELECT SUBSTRING("RUNOOB", 2, 3) AS ExtractString; -- UNO
</pre>
</td>
</tr>
<tr>
<td>SUBSTRING_INDEX(s, delimiter, number)</td>
<td>返回从字符串 s 的第 number 个出现的分隔符 delimiter 之后的子串。<br>
如果 number 是正数，返回第 number 个字符左边的字符串。<br>

如果 number 是负数，返回第(number 的绝对值(从右边数))个字符右边的字符串。

</td>
<td>
<pre>
SELECT SUBSTRING_INDEX('a*b','*',1) -- a
SELECT SUBSTRING_INDEX('a*b','*',-1)&nbsp;&nbsp;&nbsp;&nbsp;-- b
SELECT SUBSTRING_INDEX(SUBSTRING_INDEX('a*b*c*d*e','*',3),'*',-1)&nbsp;&nbsp;&nbsp;&nbsp;-- c
</pre>
</td>
</tr>
<tr>
<td>TRIM(s)</td>
<td>去掉字符串 s 开始和结尾处的空格</td>
<td>
<p>去掉字符串 RUNOOB 的首尾空格：</p>
<pre>
SELECT TRIM('    RUNOOB    ') AS TrimmedString;
</pre>
</td>
</tr>
<tr>
<td>UCASE(s)</td>
<td>将字符串转换为大写</td>
<td>
<p>将字符串 runoob 转换为大写：</p>
<pre>
SELECT UCASE("runoob"); -- RUNOOB
</pre>
</td>
</tr>
<tr>
<td>UPPER(s)</td>
<td>将字符串转换为大写</td>
<td>
<p>将字符串 runoob 转换为大写：</p>
<pre>
SELECT UPPER("runoob"); -- RUNOOB
</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:12%">函数</th>
<th>描述</th>
<th>实例</th>
</tr>
<tr>
<td>ASCII(s)</td>
<td>返回字符串 s 的第一个字符的 ASCII 码。</td>
<td>
<p>返回 CustomerName 字段第一个字母的 ASCII 码：</p>
<pre>SELECT ASCII(CustomerName) AS NumCodeOfFirstChar
FROM Customers;</pre></td>
</tr>
<tr>
<td>CHAR_LENGTH(s)</td>
<td>返回字符串 s 的字符数</td>
<td>
<p>返回字符串 RUNOOB 的字符数</p>
<pre>
SELECT CHAR_LENGTH("RUNOOB") AS LengthOfString;
</pre>
</td>
</tr>
<tr>
<td>CHARACTER_LENGTH(s)</td>
<td>返回字符串 s 的字符数</td>
<td>
<p>返回字符串 RUNOOB 的字符数</p>
<pre>
SELECT CHARACTER_LENGTH("RUNOOB") AS LengthOfString;
</pre>
</td>
</tr>
<tr>
<td>CONCAT(s1,s2...sn)</td>
<td>字符串 s1,s2 等多个字符串合并为一个字符串</td>
<td>
<p>合并多个字符串</p>
<pre>
SELECT CONCAT("SQL ", "Runoob ", "Gooogle ", "Facebook") AS ConcatenatedString;
</pre>
</td>
</tr>
<tr>
<td>CONCAT_WS(x, s1,s2...sn)</td>
<td>同 CONCAT(s1,s2,...) 函数，但是每个字符串之间要加上 x，x 可以是分隔符</td>
<td>
<p>合并多个字符串，并添加分隔符：</p>
<pre>
SELECT CONCAT_WS("-", "SQL", "Tutorial", "is", "fun!")AS ConcatenatedString;
</pre>
</td>
</tr>
<tr>
<td>FIELD(s,s1,s2...)</td>
<td>返回第一个字符串 s 在字符串列表(s1,s2...)中的位置</td>
<td>
<p>返回字符串 c 在列表值中的位置：</p>
<pre>
SELECT FIELD("c", "a", "b", "c", "d", "e");
</pre>
</td>
</tr>
<tr>
<td>FIND_IN_SET(s1,s2)</td>
<td>返回在字符串s2中与s1匹配的字符串的位置</td>
<td>
<p>返回字符串 c 在指定字符串中的位置：</p>
<pre>
SELECT FIND_IN_SET("c", "a,b,c,d,e");
</pre>
</td>
</tr>
<tr>
<td>FORMAT(x,n)</td>
<td>函数可以将数字 x 进行格式化 "#,###.##", 将 x 保留到小数点后 n 位，最后一位四舍五入。
</td>
<td>
<p>格式化数字 "#,###.##" 形式：</p>
<pre>
SELECT FORMAT(250500.5634, 2);     -- 输出 250,500.56
</pre>
</td>
</tr>
<tr>
<td>INSERT(s1,x,len,s2)</td>
<td>字符串 s2 替换 s1 的 x 位置开始长度为 len 的字符串</td>
<td>
<p>从字符串第一个位置开始的 6 个字符替换为 runoob：</p>
<pre>
SELECT INSERT("google.com", 1, 6, "runnob");  -- 输出：panku.in
</pre>
</td>
</tr>
<tr>
<td>LOCATE(s1,s)</td>
<td>从字符串 s 中获取 s1 的开始位置</td>
<td>
<p>获取 b 在字符串 abc 中的位置：</p>
<pre>
SELECT LOCATE('st','myteststring');  -- 5
</pre>
<p>返回字符串 abc 中 b 的位置：</p>

<pre>SELECT LOCATE('b', 'abc') -- 2</pre>
</td>
</tr>
<tr>
<td>LCASE(s)</td>
<td>将字符串 s 的所有字母变成小写字母</td>
<td>
<p>字符串 RUNOOB 转换为小写：</p>
<pre>
SELECT LCASE('RUNOOB') -- runoob
</pre>
</td>
</tr>
<tr>
<td>LEFT(s,n)</td>
<td>返回字符串 s 的前 n 个字符</td>
<td>
<p>返回字符串 runoob 中的前两个字符：</p>
<pre>
SELECT LEFT('runoob',2) -- ru
</pre>

</td>
</tr>


<tr>
<td>LOWER(s)</td>
<td>将字符串 s 的所有字母变成小写字母</td>
<td>
<p>字符串 RUNOOB 转换为小写：</p>
<pre>
SELECT LOWER('RUNOOB') -- runoob
</pre>
</td>
</tr>
<tr>
<td>LPAD(s1,len,s2)</td>
<td>在字符串 s1 的开始处填充字符串 s2，使字符串长度达到 len</td>
<td>
<p>将字符串 xx 填充到 abc 字符串的开始处：</p>
<pre>
SELECT LPAD('abc',5,'xx') -- xxabc
</pre>
</td>
</tr>
<tr>
<td>LTRIM(s)</td>
<td>去掉字符串 s 开始处的空格</td>
<td>
<p>去掉字符串 RUNOOB开始处的空格：</p>
<pre>
SELECT LTRIM("    RUNOOB") AS LeftTrimmedString;-- RUNOOB
</pre>
</td>
</tr>
<tr>
<td>MID(s,n,len)</td>
<td>从字符串 s 的 n 位置截取长度为 len 的子字符串，同 SUBSTRING(s,n,len)</td>
<td>
<p>从字符串 RUNOOB 中的第 2 个位置截取 3个 字符：</p>
<pre>
SELECT MID("RUNOOB", 2, 3) AS ExtractString; -- UNO
</pre>
</td>
</tr>
<tr>
<td>POSITION(s1 IN s)</td>
<td>从字符串 s 中获取 s1 的开始位置</td>
<td>
<p>返回字符串 abc 中 b 的位置：</p>
<pre>
SELECT POSITION('b' in 'abc') -- 2
</pre>
</td>
</tr>
<tr>
<td>REPEAT(s,n)</td>
<td>将字符串 s 重复 n 次 </td>
<td>
<p>将字符串 runoob 重复三次：</p>
<pre>
SELECT REPEAT('runoob',3) -- runoobrunoobrunoob
</pre>
</td>
</tr>
<tr>
<td>REPLACE(s,s1,s2)</td>
<td>将字符串 s2 替代字符串 s 中的字符串 s1</td>
<td>
<p>将字符串 abc 中的字符 a 替换为字符 x：</p>
<pre>
SELECT REPLACE('abc','a','x') --xbc
</pre>
</td>
</tr>
<tr>
<td>REVERSE(s)</td>
<td>将字符串s的顺序反过来</td>
<td>
<p>将字符串 abc 的顺序反过来：</p>
<pre>
SELECT REVERSE('abc') -- cba
</pre>
</td>
</tr>
<tr>
<td>RIGHT(s,n)</td>
<td>返回字符串 s 的后 n 个字符</td>
<td>
<p>返回字符串 runoob 的后两个字符：</p>
<pre>
SELECT RIGHT('runoob',2) -- ob
</pre>
</td>
</tr>
<tr>
<td>RPAD(s1,len,s2)</td>
<td>在字符串 s1 的结尾处添加字符串 s2，使字符串的长度达到 len</td>
<td>
<p>将字符串 xx 填充到 abc 字符串的结尾处：</p>
<pre>
SELECT RPAD('abc',5,'xx') -- abcxx
</pre>
</td>
</tr>
<tr>
<td>RTRIM(s)</td>
<td>去掉字符串 s 结尾处的空格</td>
<td>
<p>去掉字符串 RUNOOB 的末尾空格：</p>
<pre>
SELECT RTRIM("RUNOOB     ") AS RightTrimmedString;   -- RUNOOB
</pre>
</td>
</tr>
<tr>
<td>SPACE(n)</td>
<td>返回 n 个空格</td>
<td>
<p>返回 10 个空格：</p>
<pre>
SELECT SPACE(10);
</pre>
</td>
</tr>
<tr>
<td>STRCMP(s1,s2)</td>
<td>比较字符串 s1 和 s2，如果 s1 与 s2 相等返回 0 ，如果 s1&gt;s2 返回 1，如果 s1&lt;s2 返回 -1</td>
<td>
<p>比较字符串：</p>
<pre>
SELECT STRCMP("runoob", "runoob");  -- 0
</pre>
</td>
</tr>
<tr>
<td>SUBSTR(s, start, length)</td>
<td>从字符串 s 的 start 位置截取长度为 length 的子字符串</td>
<td>
<p>从字符串 RUNOOB 中的第 2 个位置截取 3个 字符：</p>
<pre>
SELECT SUBSTR("RUNOOB", 2, 3) AS ExtractString; -- UNO
</pre>
</td>
</tr>
<tr>
<td>SUBSTRING(s, start, length)</td>
<td>从字符串 s 的 start 位置截取长度为 length 的子字符串</td>
<td>
<p>从字符串 RUNOOB 中的第 2 个位置截取 3个 字符：</p>
<pre>
SELECT SUBSTRING("RUNOOB", 2, 3) AS ExtractString; -- UNO
</pre>
</td>
</tr>
<tr>
<td>SUBSTRING_INDEX(s, delimiter, number)</td>
<td>返回从字符串 s 的第 number 个出现的分隔符 delimiter 之后的子串。<br>
如果 number 是正数，返回第 number 个字符左边的字符串。<br>

如果 number 是负数，返回第(number 的绝对值(从右边数))个字符右边的字符串。

</td>
<td>
<pre>
SELECT SUBSTRING_INDEX('a*b','*',1) -- a
SELECT SUBSTRING_INDEX('a*b','*',-1)&nbsp;&nbsp;&nbsp;&nbsp;-- b
SELECT SUBSTRING_INDEX(SUBSTRING_INDEX('a*b*c*d*e','*',3),'*',-1)&nbsp;&nbsp;&nbsp;&nbsp;-- c
</pre>
</td>
</tr>
<tr>
<td>TRIM(s)</td>
<td>去掉字符串 s 开始和结尾处的空格</td>
<td>
<p>去掉字符串 RUNOOB 的首尾空格：</p>
<pre>
SELECT TRIM('    RUNOOB    ') AS TrimmedString;
</pre>
</td>
</tr>
<tr>
<td>UCASE(s)</td>
<td>将字符串转换为大写</td>
<td>
<p>将字符串 runoob 转换为大写：</p>
<pre>
SELECT UCASE("runoob"); -- RUNOOB
</pre>
</td>
</tr>
<tr>
<td>UPPER(s)</td>
<td>将字符串转换为大写</td>
<td>
<p>将字符串 runoob 转换为大写：</p>
<pre>
SELECT UPPER("runoob"); -- RUNOOB
</pre>
</td>
</tr>
</tbody></table></div>') where ID=6292;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:25%">函数名</th>
<th>描述</th>
<th>实例</th>
</tr>
<tr>
<td>ABS(x)</td>
<td>返回 x 的绝对值　　</td>
<td>
<p>返回 -1 的绝对值：</p>
<pre>
SELECT ABS(-1) -- 返回1
</pre>
</td>
</tr>
<tr>
<td>ACOS(x)</td>
<td>求 x 的反余弦值(参数是弧度)</td>
<td>
<pre>
SELECT ACOS(0.25);
</pre>
</td>
</tr>
<tr>
<td>ASIN(x)</td>
<td>求反正弦值(参数是弧度)</td>
<td>
<pre>
SELECT ASIN(0.25);
</pre>
</td>
</tr>
<tr>
<td>ATAN(x)</td>
<td>求反正切值(参数是弧度)</td>
<td>
<pre>
SELECT ATAN(2.5);
</pre>
</td>
</tr>
<tr>
<td>ATAN2(n, m)</td>
<td>求反正切值(参数是弧度)</td>
<td>
<pre>
SELECT ATAN2(-0.8, 2);
</pre>
</td>
</tr>
<tr>
<td>AVG(expression)</td>
<td>返回一个表达式的平均值，expression 是一个字段</td>
<td>
<p>返回 Products 表中Price 字段的平均值：</p>
<pre>
SELECT AVG(Price) AS AveragePrice FROM Products;
</pre>
</td>
</tr>
<tr>
<td>CEIL(x)</td>
<td>返回大于或等于 x 的最小整数　</td>
<td>
<pre>
SELECT CEIL(1.5) -- 返回2
</pre>
</td>
</tr>
<tr>
<td>CEILING(x)	
</td>
<td>返回大于或等于 x 的最小整数　</td>
<td>
<pre>
SELECT CEIL(1.5) -- 返回2
</pre>
</td>
</tr>
<tr>
<td>COS(x)</td>
<td>求余弦值(参数是弧度)</td>
<td>
<pre>
SELECT COS(2);
</pre>
</td>
</tr>
<tr>
<td>COT(x)</td>
<td>求余切值(参数是弧度)</td>
<td>
<pre>
SELECT COT(6);
</pre>
</td>
</tr>
<tr>
<td>COUNT(expression)</td>
<td>返回查询的记录总数，expression 参数是一个字段或者 * 号</td>
<td>
<p>返回 Products 表中 products 字段总共有多少条记录：</p>
<pre>
SELECT COUNT(ProductID) AS NumberOfProducts FROM Products;
</pre>
</td>
</tr>
<tr>
<td>DEGREES(x)</td>
<td>将弧度转换为角度　　</td>
<td>
<pre>
SELECT DEGREES(3.1415926535898) -- 180
</pre>
</td>
</tr>
<tr>
<td>n DIV m</td>
<td>整除，n 为被除数，m 为除数</td>
<td>
<p>计算 10 除于 5：</p>
<pre>
SELECT 10 DIV 5;  -- 2
</pre>
</td>
</tr>
<tr>
<td>EXP(x)</td>
<td>返回 e 的 x 次方　　</td>
<td>
<p>计算 e 的三次方：</p>
<pre>
SELECT EXP(3) -- 20.085536923188
</pre>
</td>
</tr>
<tr>
<td>FLOOR(x)</td>
<td>返回小于或等于 x 的最大整数　　</td>
<td>
<p>小于或等于 1.5 的整数：</p>
<pre>
SELECT FLOOR(1.5) -- 返回1
</pre>
</td>
</tr>
<tr>
<td>GREATEST(expr1, expr2, expr3, ...)</td>
<td>返回列表中的最大值</td>
<td>
<p>返回以下数字列表中的最大值：</p>
<pre>
SELECT GREATEST(3, 12, 34, 8, 25); -- 34
</pre>
<p>返回以下字符串列表中的最大值：</p><pre>
SELECT GREATEST("Google", "Runoob", "Apple");   -- Runoob
</pre>

</td>
</tr>
<tr>
<td>LEAST(expr1, expr2, expr3, ...)</td>
<td>返回列表中的最小值</td>
<td>
<p>返回以下数字列表中的最小值：</p>
<pre>
SELECT LEAST(3, 12, 34, 8, 25); -- 3
</pre>
<p>返回以下字符串列表中的最小值：</p><pre>
SELECT LEAST("Google", "Runoob", "Apple");   -- Apple
</pre>

</td>
</tr>
<tr>
<td><a href="func_mysql_ln.asp">LN</a></td>
<td>返回数字的自然对数</td>
<td>
<p>返回 2 的自然对数：</p>
<pre>
SELECT LN(2);  -- 0.6931471805599453
</pre>

</td>
</tr>
<tr>
<td>LOG(x)</td>
<td>返回自然对数(以 e 为底的对数)　　</td>
<td>
<pre>
SELECT LOG(20.085536923188) -- 3
</pre>

</td>
</tr>
<tr>
<td>LOG10(x)</td>
<td>返回以 10 为底的对数　　</td>
<td>
<pre>
SELECT LOG10(100) -- 2</pre>

</td>
</tr>
<tr>
<td>LOG2(x)</td>
<td>返回以 2 为底的对数</td>
<td>
<p>返回以 2 为底 6 的对数：</p>
<pre>
SELECT LOG2(6);  -- 2.584962500721156
</pre>

</td>
</tr>
<tr>
<td>MAX(expression)</td>
<td>返回字段 expression 中的最大值</td>
<td>
<p>返回数据表 Products 中字段 Price 的最大值：</p>
<pre>
SELECT MAX(Price) AS LargestPrice FROM Products;
</pre>

</td>
</tr>
<tr>
<td>MIN(expression)</td>
<td>返回字段 expression 中的最小值</td>
<td>
<p>返回数据表 Products 中字段 Price 的最小值：</p>
<pre>
SELECT MIN(Price) AS LargestPrice FROM Products;
</pre>

</tr>
<tr>
<td>MOD(x,y)</td>
<td>返回 x 除以 y 以后的余数　</td>
<td>
<p>5 除于 2 的余数：</p>
<pre>
SELECT MOD(5,2) -- 1
</pre>

</td>
</tr>
<tr>
<td>PI()</td>
<td>返回圆周率(3.141593）　　</td>
<td>
<pre>
SELECT PI() --3.141593
</pre>

</td>
</tr>
<tr>
<td>POW(x,y)</td>
<td>返回 x 的 y 次方　</td>
<td>
<p>2 的 3 次方：</p>
<pre>
SELECT POW(2,3) -- 8
</pre>

</td>
</tr>
<tr>
<td>POWER(x,y)</td>
<td>返回 x 的 y 次方　</td>
<td>
<p>2 的 3 次方：</p>
<pre>
SELECT POWER(2,3) -- 8
</pre>
</tr>
<tr>
<td>RADIANS(x)</td>
<td>将角度转换为弧度　　</td>
<td>
<p>180 度转换为弧度：</p>
<pre>
SELECT RADIANS(180) -- 3.1415926535898
</pre>

</td>
</tr>
<tr>
<td>RAND()</td>
<td>返回 0 到 1 的随机数　　</td>
<td>
<pre>
SELECT RAND() --0.93099315644334
</pre>

</td>
</tr>
<tr>
<td>ROUND(x)</td>
<td>返回离 x 最近的整数</td>
<td>
<pre>
SELECT ROUND(1.23456) --1
</pre>

</td>
</tr>
<tr>
<td>SIGN(x)</td>
<td>返回 x 的符号，x 是负数、0、正数分别返回 -1、0 和 1　</td>
<td>
<pre>
SELECT SIGN(-10) -- (-1)
</pre>

</td>
</tr>
<tr>
<td>SIN(x)</td>
<td>求正弦值(参数是弧度)　　</td>
<td>
<pre>
SELECT SIN(RADIANS(30)) -- 0.5
</pre>

</td>
</tr>
<tr>
<td>SQRT(x)</td>
<td>返回x的平方根　　</td>

<td>
<p>25 的平方根：</p>
<pre>
SELECT SQRT(25) -- 5
</pre>

</td>
</tr>
<tr>
<td>SUM(expression)</td>
<td>返回指定字段的总和</td>
<td>
<p>计算 OrderDetails 表中字段 Quantity 的总和：</p>
<pre>
SELECT SUM(Quantity) AS TotalItemsOrdered FROM OrderDetails;
</pre>

</td>
</tr>
<tr>
<td>TAN(x)</td>
<td>求正切值(参数是弧度)</td>
<td>
<pre>
SELECT TAN(1.75);  -- -5.52037992250933
</pre>

</td>
</tr>
<tr>
<td>TRUNCATE(x,y)</td>
<td>返回数值 x 保留到小数点后 y 位的值（与 ROUND 最大的区别是不会进行四舍五入）</td>
<td>
<pre>
SELECT TRUNCATE(1.23456,3) -- 1.234
</pre>

</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:25%">函数名</th>
<th>描述</th>
<th>实例</th>
</tr>
<tr>
<td>ABS(x)</td>
<td>返回 x 的绝对值　　</td>
<td>
<p>返回 -1 的绝对值：</p>
<pre>
SELECT ABS(-1) -- 返回1
</pre>
</td>
</tr>
<tr>
<td>ACOS(x)</td>
<td>求 x 的反余弦值(参数是弧度)</td>
<td>
<pre>
SELECT ACOS(0.25);
</pre>
</td>
</tr>
<tr>
<td>ASIN(x)</td>
<td>求反正弦值(参数是弧度)</td>
<td>
<pre>
SELECT ASIN(0.25);
</pre>
</td>
</tr>
<tr>
<td>ATAN(x)</td>
<td>求反正切值(参数是弧度)</td>
<td>
<pre>
SELECT ATAN(2.5);
</pre>
</td>
</tr>
<tr>
<td>ATAN2(n, m)</td>
<td>求反正切值(参数是弧度)</td>
<td>
<pre>
SELECT ATAN2(-0.8, 2);
</pre>
</td>
</tr>
<tr>
<td>AVG(expression)</td>
<td>返回一个表达式的平均值，expression 是一个字段</td>
<td>
<p>返回 Products 表中Price 字段的平均值：</p>
<pre>
SELECT AVG(Price) AS AveragePrice FROM Products;
</pre>
</td>
</tr>
<tr>
<td>CEIL(x)</td>
<td>返回大于或等于 x 的最小整数　</td>
<td>
<pre>
SELECT CEIL(1.5) -- 返回2
</pre>
</td>
</tr>
<tr>
<td>CEILING(x)	
</td>
<td>返回大于或等于 x 的最小整数　</td>
<td>
<pre>
SELECT CEIL(1.5) -- 返回2
</pre>
</td>
</tr>
<tr>
<td>COS(x)</td>
<td>求余弦值(参数是弧度)</td>
<td>
<pre>
SELECT COS(2);
</pre>
</td>
</tr>
<tr>
<td>COT(x)</td>
<td>求余切值(参数是弧度)</td>
<td>
<pre>
SELECT COT(6);
</pre>
</td>
</tr>
<tr>
<td>COUNT(expression)</td>
<td>返回查询的记录总数，expression 参数是一个字段或者 * 号</td>
<td>
<p>返回 Products 表中 products 字段总共有多少条记录：</p>
<pre>
SELECT COUNT(ProductID) AS NumberOfProducts FROM Products;
</pre>
</td>
</tr>
<tr>
<td>DEGREES(x)</td>
<td>将弧度转换为角度　　</td>
<td>
<pre>
SELECT DEGREES(3.1415926535898) -- 180
</pre>
</td>
</tr>
<tr>
<td>n DIV m</td>
<td>整除，n 为被除数，m 为除数</td>
<td>
<p>计算 10 除于 5：</p>
<pre>
SELECT 10 DIV 5;  -- 2
</pre>
</td>
</tr>
<tr>
<td>EXP(x)</td>
<td>返回 e 的 x 次方　　</td>
<td>
<p>计算 e 的三次方：</p>
<pre>
SELECT EXP(3) -- 20.085536923188
</pre>
</td>
</tr>
<tr>
<td>FLOOR(x)</td>
<td>返回小于或等于 x 的最大整数　　</td>
<td>
<p>小于或等于 1.5 的整数：</p>
<pre>
SELECT FLOOR(1.5) -- 返回1
</pre>
</td>
</tr>
<tr>
<td>GREATEST(expr1, expr2, expr3, ...)</td>
<td>返回列表中的最大值</td>
<td>
<p>返回以下数字列表中的最大值：</p>
<pre>
SELECT GREATEST(3, 12, 34, 8, 25); -- 34
</pre>
<p>返回以下字符串列表中的最大值：</p><pre>
SELECT GREATEST("Google", "Runoob", "Apple");   -- Runoob
</pre>

</td>
</tr>
<tr>
<td>LEAST(expr1, expr2, expr3, ...)</td>
<td>返回列表中的最小值</td>
<td>
<p>返回以下数字列表中的最小值：</p>
<pre>
SELECT LEAST(3, 12, 34, 8, 25); -- 3
</pre>
<p>返回以下字符串列表中的最小值：</p><pre>
SELECT LEAST("Google", "Runoob", "Apple");   -- Apple
</pre>

</td>
</tr>
<tr>
<td><a href="func_mysql_ln.asp">LN</a></td>
<td>返回数字的自然对数</td>
<td>
<p>返回 2 的自然对数：</p>
<pre>
SELECT LN(2);  -- 0.6931471805599453
</pre>

</td>
</tr>
<tr>
<td>LOG(x)</td>
<td>返回自然对数(以 e 为底的对数)　　</td>
<td>
<pre>
SELECT LOG(20.085536923188) -- 3
</pre>

</td>
</tr>
<tr>
<td>LOG10(x)</td>
<td>返回以 10 为底的对数　　</td>
<td>
<pre>
SELECT LOG10(100) -- 2</pre>

</td>
</tr>
<tr>
<td>LOG2(x)</td>
<td>返回以 2 为底的对数</td>
<td>
<p>返回以 2 为底 6 的对数：</p>
<pre>
SELECT LOG2(6);  -- 2.584962500721156
</pre>

</td>
</tr>
<tr>
<td>MAX(expression)</td>
<td>返回字段 expression 中的最大值</td>
<td>
<p>返回数据表 Products 中字段 Price 的最大值：</p>
<pre>
SELECT MAX(Price) AS LargestPrice FROM Products;
</pre>

</td>
</tr>
<tr>
<td>MIN(expression)</td>
<td>返回字段 expression 中的最小值</td>
<td>
<p>返回数据表 Products 中字段 Price 的最小值：</p>
<pre>
SELECT MIN(Price) AS LargestPrice FROM Products;
</pre>

</tr>
<tr>
<td>MOD(x,y)</td>
<td>返回 x 除以 y 以后的余数　</td>
<td>
<p>5 除于 2 的余数：</p>
<pre>
SELECT MOD(5,2) -- 1
</pre>

</td>
</tr>
<tr>
<td>PI()</td>
<td>返回圆周率(3.141593）　　</td>
<td>
<pre>
SELECT PI() --3.141593
</pre>

</td>
</tr>
<tr>
<td>POW(x,y)</td>
<td>返回 x 的 y 次方　</td>
<td>
<p>2 的 3 次方：</p>
<pre>
SELECT POW(2,3) -- 8
</pre>

</td>
</tr>
<tr>
<td>POWER(x,y)</td>
<td>返回 x 的 y 次方　</td>
<td>
<p>2 的 3 次方：</p>
<pre>
SELECT POWER(2,3) -- 8
</pre>
</tr>
<tr>
<td>RADIANS(x)</td>
<td>将角度转换为弧度　　</td>
<td>
<p>180 度转换为弧度：</p>
<pre>
SELECT RADIANS(180) -- 3.1415926535898
</pre>

</td>
</tr>
<tr>
<td>RAND()</td>
<td>返回 0 到 1 的随机数　　</td>
<td>
<pre>
SELECT RAND() --0.93099315644334
</pre>

</td>
</tr>
<tr>
<td>ROUND(x)</td>
<td>返回离 x 最近的整数</td>
<td>
<pre>
SELECT ROUND(1.23456) --1
</pre>

</td>
</tr>
<tr>
<td>SIGN(x)</td>
<td>返回 x 的符号，x 是负数、0、正数分别返回 -1、0 和 1　</td>
<td>
<pre>
SELECT SIGN(-10) -- (-1)
</pre>

</td>
</tr>
<tr>
<td>SIN(x)</td>
<td>求正弦值(参数是弧度)　　</td>
<td>
<pre>
SELECT SIN(RADIANS(30)) -- 0.5
</pre>

</td>
</tr>
<tr>
<td>SQRT(x)</td>
<td>返回x的平方根　　</td>

<td>
<p>25 的平方根：</p>
<pre>
SELECT SQRT(25) -- 5
</pre>

</td>
</tr>
<tr>
<td>SUM(expression)</td>
<td>返回指定字段的总和</td>
<td>
<p>计算 OrderDetails 表中字段 Quantity 的总和：</p>
<pre>
SELECT SUM(Quantity) AS TotalItemsOrdered FROM OrderDetails;
</pre>

</td>
</tr>
<tr>
<td>TAN(x)</td>
<td>求正切值(参数是弧度)</td>
<td>
<pre>
SELECT TAN(1.75);  -- -5.52037992250933
</pre>

</td>
</tr>
<tr>
<td>TRUNCATE(x,y)</td>
<td>返回数值 x 保留到小数点后 y 位的值（与 ROUND 最大的区别是不会进行四舍五入）</td>
<td>
<pre>
SELECT TRUNCATE(1.23456,3) -- 1.234
</pre>

</td>
</tr>
</tbody></table></div>') where ID=6292;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:25%">函数名</th>
<th>描述</th>
<th>实例</th>
</tr>
<tr>
<td>ADDDATE(d,n)</td>
<td>计算起始日期 d 加上 n 天的日期</td>
<td>
<pre>
SELECT ADDDATE("2017-06-15", INTERVAL 10 DAY);
-&gt;2017-06-25
</pre>

</td>
</tr>
<tr>
<td>ADDTIME(t,n)</td>
<td>时间 t 加上 n 秒的时间</td>
<td>
<pre>
SELECT ADDTIME('2011-11-11 11:11:11', 5)
-&gt;2011-11-11 11:11:16 (秒)
</pre>

</td>
</tr>
<tr>
<td>CURDATE()</td>
<td>返回当前日期</td>
<td>
<pre>
SELECT CURDATE();
-&gt; 2018-09-19
</pre>

</td>
</tr>
<tr>
<td>CURRENT_DATE()</td>
<td>返回当前日期</td>
<td>
<pre>
SELECT CURRENT_DATE();
-&gt; 2018-09-19
</pre>

</td>
</tr>
<tr>
<td>CURRENT_TIME</td>
<td>返回当前时间</td>
<td>
<pre>
SELECT CURRENT_TIME();
-&gt; 19:59:02
</pre>

</td>
</tr>
<tr>
<td>CURRENT_TIMESTAMP()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT CURRENT_TIMESTAMP()
-&gt; 2018-09-19 20:57:43
</pre>

</td>
</tr>
<tr>
<td>CURTIME()</td>
<td>返回当前时间</td>
<td>
<pre>
SELECT CURTIME();
-&gt; 19:59:02
</pre>
</tr>
<tr>
<td>DATE()</td>
<td>从日期或日期时间表达式中提取日期值</td>
<td>
<pre>
SELECT DATE("2017-06-15");    
-&gt; 2017-06-15
</pre>

</td>
</tr>
<tr>
<td>DATEDIFF(d1,d2)</td>
<td>计算日期 d1-&gt;d2 之间相隔的天数</td>
<td>
<pre>
SELECT DATEDIFF('2001-01-01','2001-02-02')
-&gt; -32
</pre>

</td>
</tr>
<tr>
<td>DATE_ADD(d，INTERVAL expr type)</td>
<td>计算起始日期 d 加上一个时间段后的日期</td>
<td>
<pre>
SELECT ADDDATE('2011-11-11 11:11:11',1)
-&gt; 2011-11-12 11:11:11&nbsp;&nbsp;&nbsp;&nbsp;(默认是天)

SELECT ADDDATE('2011-11-11 11:11:11', INTERVAL 5 MINUTE)
-&gt; 2011-11-11 11:16:11 (TYPE的取值与上面那个列出来的函数类似)
</pre>

</td>
</tr>
<tr>
<td>DATE_FORMAT(d,f)</td>
<td>按表达式  f的要求显示日期 d</td>
<td>
<pre>
SELECT DATE_FORMAT('2011-11-11 11:11:11','%Y-%m-%d %r')
-&gt; 2011-11-11 11:11:11 AM
</pre>

</td>
</tr>
<tr>
<td>DATE_SUB(date,INTERVAL expr type)</td>
<td>函数从日期减去指定的时间间隔。</td>
<td>
 <p>Orders 表中 OrderDate 字段减去 2 天：</p>
<pre>
SELECT OrderId,DATE_SUB(OrderDate,INTERVAL 2 DAY) AS OrderPayDate
FROM Orders
</pre>

</td>
</tr>
<tr>
<td>DAY(d)</td>
<td>返回日期值 d 的日期部分</td>
<td>
<pre>
SELECT DAY("2017-06-15");  
-&gt; 15
</pre>

</td>
</tr>
<tr>
<td>DAYNAME(d)</a></td>
<td>返回日期 d 是星期几，如 Monday,Tuesday</td>
<td>
<pre>
SELECT DAYNAME('2011-11-11 11:11:11')
-&gt;Friday
</pre>

</td>
</tr>
<tr>
<td>DAYOFMONTH(d)</td>
<td>计算日期 d 是本月的第几天</td>
<td>
<pre>
SELECT DAYOFMONTH('2011-11-11 11:11:11')
-&gt;11
</pre>

</td>
</tr>
<tr>
<td>DAYOFWEEK(d)</td>
<td>日期 d 今天是星期几，1 星期日，2 星期一，以此类推</td>
<td>
<pre>
SELECT DAYOFWEEK('2011-11-11 11:11:11')
-&gt;6
</pre>

</td>
</tr>
<tr>
<td>DAYOFYEAR(d)</td>
<td>计算日期 d 是本年的第几天</td>
<td>
<pre>
SELECT DAYOFYEAR('2011-11-11 11:11:11')
-&gt;315
</pre>

</td>
</tr>
<tr>
<td>EXTRACT(type FROM d)</td>
<td>从日期 d 中获取指定的值，type 指定返回的值。
<br>

type可取值为：
<br><ul><li>
MICROSECOND</li><li>
SECOND</li><li>
MINUTE</li><li>
HOUR</li><li>
DAY</li><li>
WEEK</li><li>
MONTH</li><li>
QUARTER</li><li>
YEAR</li><li>
SECOND_MICROSECOND</li><li>
MINUTE_MICROSECOND</li><li>
MINUTE_SECOND</li><li>
HOUR_MICROSECOND</li><li>
HOUR_SECOND</li><li>
HOUR_MINUTE</li><li>
DAY_MICROSECOND</li><li>
DAY_SECOND</li><li>
DAY_MINUTE</li><li>
DAY_HOUR</li><li>
YEAR_MONTH</li></ul></td>

<td>
<pre>
SELECT EXTRACT(MINUTE FROM '2011-11-11 11:11:11') 
-&gt; 11
</pre>

</td>

</tr>
<tr>
<td>FROM_DAYS(n)</a></td>
<td>计算从 0000 年 1 月 1 日开始 n 天后的日期</td>
<td>
<pre>
SELECT FROM_DAYS(1111)
-&gt; 0003-01-16
</pre>

</td>
</tr>
<tr>
<td>HOUR(t)</td>
<td>返回 t 中的小时值</td>
<td>
<pre>
SELECT HOUR('1:2:3')
-&gt; 1
</pre>

</td>
</tr>
<tr>
<td>LAST_DAY(d)</td>
<td>返回给给定日期的那一月份的最后一天</td>
<td>
<pre>
SELECT LAST_DAY("2017-06-20");
-&gt; 2017-06-30
</pre>

</td>
</tr>
<tr>
<td>LOCALTIME()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT LOCALTIME()
-&gt; 2018-09-19 20:57:43
</pre>
</td>
</tr>
<tr>
<td>LOCALTIMESTAMP()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT LOCALTIMESTAMP()
-&gt; 2018-09-19 20:57:43
</pre>
</td>
</tr>
<tr>
<td>MAKEDATE(year, day-of-year)</td>
<td>基于给定参数年份 year 和所在年中的天数序号 day-of-year 返回一个日期</td>
<td>
<pre>
SELECT MAKEDATE(2017, 3);
-&gt; 2017-01-03
</pre>

</td>
</tr>
<tr>
<td>MAKETIME(hour, minute, second)</td>
<td>组合时间，参数分别为小时、分钟、秒</td>
<td>
<pre>
SELECT MAKETIME(11, 35, 4);
-&gt; 11:35:04
</pre>

</td>
</tr>
<tr>
<td>MICROSECOND(date)</td>
<td>返回日期参数所对应的微秒数</td>
<td>
<pre>
SELECT MICROSECOND("2017-06-20 09:34:00.000023");
-&gt; 23
</pre>

</td>
</tr>
<tr>
<td>MINUTE(t)</td>
<td>返回 t 中的分钟值</td>
<td>
<pre>
SELECT MINUTE('1:2:3')
-&gt; 2
</pre>

</td>
</tr>
<tr>
<td>MONTHNAME(d)</td>
<td>返回日期当中的月份名称，如 Janyary</td>
<td>
<pre>
SELECT MONTHNAME('2011-11-11 11:11:11')
-&gt; November
</pre>

</td>
</tr>
<tr>
<td>MONTH(d)</td>
<td>返回日期d中的月份值，1 到 12</td>
<td>
<pre>
SELECT MONTH('2011-11-11 11:11:11')
-&gt;11
</pre>

</td>
</tr>
<tr>
<td>NOW()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT NOW()
-&gt; 2018-09-19 20:57:43
</pre>

</td>
</tr>
<tr>
<td>PERIOD_ADD(period, number)</td>
<td>为 年-月 组合日期添加一个时段</td>
<td>
<pre>
SELECT PERIOD_ADD(201703, 5);   
-&gt; 201708
</pre>

</td>
</tr>
<tr>
<td>PERIOD_DIFF(period1, period2)</td>
<td>返回两个时段之间的月份差值</td>
<td>
<pre>
SELECT PERIOD_DIFF(201710, 201703);
-&gt; 7
</pre>

</td>
</tr>
<tr>
<td>QUARTER(d)</td>
<td>返回日期d是第几季节，返回 1 到 4</td>
<td>
<pre>
SELECT QUARTER('2011-11-11 11:11:11')
-&gt; 4
</pre>

</td>
</tr>
<tr>
<td>SECOND(t)</td>
<td>返回 t 中的秒钟值</td>
<td>
<pre>
SELECT SECOND('1:2:3')
-&gt; 3
</pre>

</td>
</tr>
<tr>
<td>SEC_TO_TIME(s)</td>
<td>将以秒为单位的时间 s 转换为时分秒的格式</td>
<td>
<pre>
SELECT SEC_TO_TIME(4320)
-&gt; 01:12:00
</pre>

</td>
</tr>
<tr>
<td>STR_TO_DATE(string, format_mask)</td>
<td>将字符串转变为日期</td>
<td>
<pre>
SELECT STR_TO_DATE("August 10 2017", "%M %d %Y");
-&gt; 2017-08-10
</pre>

</td>
</tr>
<tr>
<td>SUBDATE(d,n)</td>
<td>日期 d 减去 n 天后的日期</td>
<td>
<pre>
SELECT SUBDATE('2011-11-11 11:11:11', 1)
-&gt;2011-11-10 11:11:11 (默认是天)
</pre>

</td>
</tr>
<tr>
<td>SUBTIME(t,n)</td>
<td>时间 t 减去 n 秒的时间</td>
<td>
<pre>
SELECT SUBTIME('2011-11-11 11:11:11', 5)
-&gt;2011-11-11 11:11:06 (秒)
</pre>

</td>
</tr>
<tr>
<td>SYSDATE()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT SYSDATE()
-&gt; 2018-09-19 20:57:43
</pre>

</td>
</tr>
<tr>
<td>TIME(expression)</td>
<td>提取传入表达式的时间部分</td>
<td>
<pre>
SELECT TIME("19:30:10");
-&gt; 19:30:10
</pre>

</td>
</tr>
<tr>
<td>TIME_FORMAT(t,f)</td>
<td>按表达式 f 的要求显示时间 t</td>
<td>
<pre>
SELECT TIME_FORMAT('11:11:11','%r')
11:11:11 AM
</pre>

</td>
</tr>
<tr>
<td>TIME_TO_SEC(t)</td>
<td>将时间 t 转换为秒</td>
<td>
<pre>
SELECT TIME_TO_SEC('1:12:00')
-&gt; 4320
</pre>

</td>
</tr>
  <tr>
<td>TIMEDIFF(time1, time2)</td>
<td>计算时间差值</td>
<td>
<pre>
SELECT TIMEDIFF("13:10:11", "13:10:10");
-&gt; 00:00:01
</pre>

</td>
  </tr>
  <tr>
<td>TIMESTAMP(expression, interval)</td>
<td>单个参数时，函数返回日期或日期时间表达式；有2个参数时，将参数加和</td>
<td>
<pre>
SELECT TIMESTAMP("2017-07-23",  "13:10:11");
-&gt; 2017-07-23 13:10:11
</pre>

</td>
  </tr>
<tr>
<td>TO_DAYS(d)</td>
<td>计算日期 d 距离 0000 年 1 月 1 日的天数</td>
<td>
<pre>
SELECT TO_DAYS('0001-01-01 01:01:01')
-&gt; 366
</pre>

</td>
</tr>
<tr>
<td>WEEK(d)</td>
<td>计算日期 d 是本年的第几个星期，范围是 0 到 53</td>
<td>
<pre>
SELECT WEEK('2011-11-11 11:11:11')
-&gt; 45
</pre>

</td>
</tr>
<tr>
<td>WEEKDAY(d)</td>
<td>日期 d 是星期几，0 表示星期一，1 表示星期二</td>
<td>
<pre>
SELECT WEEKDAY("2017-06-15");
-&gt; 3
</pre>

</td>
</tr>
<tr>
<td>WEEKOFYEAR(d)</td>
<td>计算日期 d 是本年的第几个星期，范围是 0 到 53</td>
<td>
<pre>
SELECT WEEKOFYEAR('2011-11-11 11:11:11')
-&gt; 45
</pre>

</td>
</tr>
<tr>
<td>YEAR(d)</td>
<td>返回年份</td>
<td>
<pre>
SELECT YEAR("2017-06-15");
-&gt; 2017
</pre>

</td>
</tr>
<tr>
<td>YEARWEEK(date, mode)</td>
<td>返回年份及第几周（0到53），mode 中 0 表示周天，1表示周一，以此类推</td>
<td>
<pre>
SELECT YEARWEEK("2017-06-15");
-&gt; 201724
</pre>

</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:25%">函数名</th>
<th>描述</th>
<th>实例</th>
</tr>
<tr>
<td>ADDDATE(d,n)</td>
<td>计算起始日期 d 加上 n 天的日期</td>
<td>
<pre>
SELECT ADDDATE("2017-06-15", INTERVAL 10 DAY);
-&gt;2017-06-25
</pre>

</td>
</tr>
<tr>
<td>ADDTIME(t,n)</td>
<td>时间 t 加上 n 秒的时间</td>
<td>
<pre>
SELECT ADDTIME('2011-11-11 11:11:11', 5)
-&gt;2011-11-11 11:11:16 (秒)
</pre>

</td>
</tr>
<tr>
<td>CURDATE()</td>
<td>返回当前日期</td>
<td>
<pre>
SELECT CURDATE();
-&gt; 2018-09-19
</pre>

</td>
</tr>
<tr>
<td>CURRENT_DATE()</td>
<td>返回当前日期</td>
<td>
<pre>
SELECT CURRENT_DATE();
-&gt; 2018-09-19
</pre>

</td>
</tr>
<tr>
<td>CURRENT_TIME</td>
<td>返回当前时间</td>
<td>
<pre>
SELECT CURRENT_TIME();
-&gt; 19:59:02
</pre>

</td>
</tr>
<tr>
<td>CURRENT_TIMESTAMP()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT CURRENT_TIMESTAMP()
-&gt; 2018-09-19 20:57:43
</pre>

</td>
</tr>
<tr>
<td>CURTIME()</td>
<td>返回当前时间</td>
<td>
<pre>
SELECT CURTIME();
-&gt; 19:59:02
</pre>
</tr>
<tr>
<td>DATE()</td>
<td>从日期或日期时间表达式中提取日期值</td>
<td>
<pre>
SELECT DATE("2017-06-15");    
-&gt; 2017-06-15
</pre>

</td>
</tr>
<tr>
<td>DATEDIFF(d1,d2)</td>
<td>计算日期 d1-&gt;d2 之间相隔的天数</td>
<td>
<pre>
SELECT DATEDIFF('2001-01-01','2001-02-02')
-&gt; -32
</pre>

</td>
</tr>
<tr>
<td>DATE_ADD(d，INTERVAL expr type)</td>
<td>计算起始日期 d 加上一个时间段后的日期</td>
<td>
<pre>
SELECT ADDDATE('2011-11-11 11:11:11',1)
-&gt; 2011-11-12 11:11:11&nbsp;&nbsp;&nbsp;&nbsp;(默认是天)

SELECT ADDDATE('2011-11-11 11:11:11', INTERVAL 5 MINUTE)
-&gt; 2011-11-11 11:16:11 (TYPE的取值与上面那个列出来的函数类似)
</pre>

</td>
</tr>
<tr>
<td>DATE_FORMAT(d,f)</td>
<td>按表达式  f的要求显示日期 d</td>
<td>
<pre>
SELECT DATE_FORMAT('2011-11-11 11:11:11','%Y-%m-%d %r')
-&gt; 2011-11-11 11:11:11 AM
</pre>

</td>
</tr>
<tr>
<td>DATE_SUB(date,INTERVAL expr type)</td>
<td>函数从日期减去指定的时间间隔。</td>
<td>
 <p>Orders 表中 OrderDate 字段减去 2 天：</p>
<pre>
SELECT OrderId,DATE_SUB(OrderDate,INTERVAL 2 DAY) AS OrderPayDate
FROM Orders
</pre>

</td>
</tr>
<tr>
<td>DAY(d)</td>
<td>返回日期值 d 的日期部分</td>
<td>
<pre>
SELECT DAY("2017-06-15");  
-&gt; 15
</pre>

</td>
</tr>
<tr>
<td>DAYNAME(d)</a></td>
<td>返回日期 d 是星期几，如 Monday,Tuesday</td>
<td>
<pre>
SELECT DAYNAME('2011-11-11 11:11:11')
-&gt;Friday
</pre>

</td>
</tr>
<tr>
<td>DAYOFMONTH(d)</td>
<td>计算日期 d 是本月的第几天</td>
<td>
<pre>
SELECT DAYOFMONTH('2011-11-11 11:11:11')
-&gt;11
</pre>

</td>
</tr>
<tr>
<td>DAYOFWEEK(d)</td>
<td>日期 d 今天是星期几，1 星期日，2 星期一，以此类推</td>
<td>
<pre>
SELECT DAYOFWEEK('2011-11-11 11:11:11')
-&gt;6
</pre>

</td>
</tr>
<tr>
<td>DAYOFYEAR(d)</td>
<td>计算日期 d 是本年的第几天</td>
<td>
<pre>
SELECT DAYOFYEAR('2011-11-11 11:11:11')
-&gt;315
</pre>

</td>
</tr>
<tr>
<td>EXTRACT(type FROM d)</td>
<td>从日期 d 中获取指定的值，type 指定返回的值。
<br>

type可取值为：
<br><ul><li>
MICROSECOND</li><li>
SECOND</li><li>
MINUTE</li><li>
HOUR</li><li>
DAY</li><li>
WEEK</li><li>
MONTH</li><li>
QUARTER</li><li>
YEAR</li><li>
SECOND_MICROSECOND</li><li>
MINUTE_MICROSECOND</li><li>
MINUTE_SECOND</li><li>
HOUR_MICROSECOND</li><li>
HOUR_SECOND</li><li>
HOUR_MINUTE</li><li>
DAY_MICROSECOND</li><li>
DAY_SECOND</li><li>
DAY_MINUTE</li><li>
DAY_HOUR</li><li>
YEAR_MONTH</li></ul></td>

<td>
<pre>
SELECT EXTRACT(MINUTE FROM '2011-11-11 11:11:11') 
-&gt; 11
</pre>

</td>

</tr>
<tr>
<td>FROM_DAYS(n)</a></td>
<td>计算从 0000 年 1 月 1 日开始 n 天后的日期</td>
<td>
<pre>
SELECT FROM_DAYS(1111)
-&gt; 0003-01-16
</pre>

</td>
</tr>
<tr>
<td>HOUR(t)</td>
<td>返回 t 中的小时值</td>
<td>
<pre>
SELECT HOUR('1:2:3')
-&gt; 1
</pre>

</td>
</tr>
<tr>
<td>LAST_DAY(d)</td>
<td>返回给给定日期的那一月份的最后一天</td>
<td>
<pre>
SELECT LAST_DAY("2017-06-20");
-&gt; 2017-06-30
</pre>

</td>
</tr>
<tr>
<td>LOCALTIME()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT LOCALTIME()
-&gt; 2018-09-19 20:57:43
</pre>
</td>
</tr>
<tr>
<td>LOCALTIMESTAMP()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT LOCALTIMESTAMP()
-&gt; 2018-09-19 20:57:43
</pre>
</td>
</tr>
<tr>
<td>MAKEDATE(year, day-of-year)</td>
<td>基于给定参数年份 year 和所在年中的天数序号 day-of-year 返回一个日期</td>
<td>
<pre>
SELECT MAKEDATE(2017, 3);
-&gt; 2017-01-03
</pre>

</td>
</tr>
<tr>
<td>MAKETIME(hour, minute, second)</td>
<td>组合时间，参数分别为小时、分钟、秒</td>
<td>
<pre>
SELECT MAKETIME(11, 35, 4);
-&gt; 11:35:04
</pre>

</td>
</tr>
<tr>
<td>MICROSECOND(date)</td>
<td>返回日期参数所对应的微秒数</td>
<td>
<pre>
SELECT MICROSECOND("2017-06-20 09:34:00.000023");
-&gt; 23
</pre>

</td>
</tr>
<tr>
<td>MINUTE(t)</td>
<td>返回 t 中的分钟值</td>
<td>
<pre>
SELECT MINUTE('1:2:3')
-&gt; 2
</pre>

</td>
</tr>
<tr>
<td>MONTHNAME(d)</td>
<td>返回日期当中的月份名称，如 Janyary</td>
<td>
<pre>
SELECT MONTHNAME('2011-11-11 11:11:11')
-&gt; November
</pre>

</td>
</tr>
<tr>
<td>MONTH(d)</td>
<td>返回日期d中的月份值，1 到 12</td>
<td>
<pre>
SELECT MONTH('2011-11-11 11:11:11')
-&gt;11
</pre>

</td>
</tr>
<tr>
<td>NOW()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT NOW()
-&gt; 2018-09-19 20:57:43
</pre>

</td>
</tr>
<tr>
<td>PERIOD_ADD(period, number)</td>
<td>为 年-月 组合日期添加一个时段</td>
<td>
<pre>
SELECT PERIOD_ADD(201703, 5);   
-&gt; 201708
</pre>

</td>
</tr>
<tr>
<td>PERIOD_DIFF(period1, period2)</td>
<td>返回两个时段之间的月份差值</td>
<td>
<pre>
SELECT PERIOD_DIFF(201710, 201703);
-&gt; 7
</pre>

</td>
</tr>
<tr>
<td>QUARTER(d)</td>
<td>返回日期d是第几季节，返回 1 到 4</td>
<td>
<pre>
SELECT QUARTER('2011-11-11 11:11:11')
-&gt; 4
</pre>

</td>
</tr>
<tr>
<td>SECOND(t)</td>
<td>返回 t 中的秒钟值</td>
<td>
<pre>
SELECT SECOND('1:2:3')
-&gt; 3
</pre>

</td>
</tr>
<tr>
<td>SEC_TO_TIME(s)</td>
<td>将以秒为单位的时间 s 转换为时分秒的格式</td>
<td>
<pre>
SELECT SEC_TO_TIME(4320)
-&gt; 01:12:00
</pre>

</td>
</tr>
<tr>
<td>STR_TO_DATE(string, format_mask)</td>
<td>将字符串转变为日期</td>
<td>
<pre>
SELECT STR_TO_DATE("August 10 2017", "%M %d %Y");
-&gt; 2017-08-10
</pre>

</td>
</tr>
<tr>
<td>SUBDATE(d,n)</td>
<td>日期 d 减去 n 天后的日期</td>
<td>
<pre>
SELECT SUBDATE('2011-11-11 11:11:11', 1)
-&gt;2011-11-10 11:11:11 (默认是天)
</pre>

</td>
</tr>
<tr>
<td>SUBTIME(t,n)</td>
<td>时间 t 减去 n 秒的时间</td>
<td>
<pre>
SELECT SUBTIME('2011-11-11 11:11:11', 5)
-&gt;2011-11-11 11:11:06 (秒)
</pre>

</td>
</tr>
<tr>
<td>SYSDATE()</td>
<td>返回当前日期和时间</td>
<td>
<pre>
SELECT SYSDATE()
-&gt; 2018-09-19 20:57:43
</pre>

</td>
</tr>
<tr>
<td>TIME(expression)</td>
<td>提取传入表达式的时间部分</td>
<td>
<pre>
SELECT TIME("19:30:10");
-&gt; 19:30:10
</pre>

</td>
</tr>
<tr>
<td>TIME_FORMAT(t,f)</td>
<td>按表达式 f 的要求显示时间 t</td>
<td>
<pre>
SELECT TIME_FORMAT('11:11:11','%r')
11:11:11 AM
</pre>

</td>
</tr>
<tr>
<td>TIME_TO_SEC(t)</td>
<td>将时间 t 转换为秒</td>
<td>
<pre>
SELECT TIME_TO_SEC('1:12:00')
-&gt; 4320
</pre>

</td>
</tr>
  <tr>
<td>TIMEDIFF(time1, time2)</td>
<td>计算时间差值</td>
<td>
<pre>
SELECT TIMEDIFF("13:10:11", "13:10:10");
-&gt; 00:00:01
</pre>

</td>
  </tr>
  <tr>
<td>TIMESTAMP(expression, interval)</td>
<td>单个参数时，函数返回日期或日期时间表达式；有2个参数时，将参数加和</td>
<td>
<pre>
SELECT TIMESTAMP("2017-07-23",  "13:10:11");
-&gt; 2017-07-23 13:10:11
</pre>

</td>
  </tr>
<tr>
<td>TO_DAYS(d)</td>
<td>计算日期 d 距离 0000 年 1 月 1 日的天数</td>
<td>
<pre>
SELECT TO_DAYS('0001-01-01 01:01:01')
-&gt; 366
</pre>

</td>
</tr>
<tr>
<td>WEEK(d)</td>
<td>计算日期 d 是本年的第几个星期，范围是 0 到 53</td>
<td>
<pre>
SELECT WEEK('2011-11-11 11:11:11')
-&gt; 45
</pre>

</td>
</tr>
<tr>
<td>WEEKDAY(d)</td>
<td>日期 d 是星期几，0 表示星期一，1 表示星期二</td>
<td>
<pre>
SELECT WEEKDAY("2017-06-15");
-&gt; 3
</pre>

</td>
</tr>
<tr>
<td>WEEKOFYEAR(d)</td>
<td>计算日期 d 是本年的第几个星期，范围是 0 到 53</td>
<td>
<pre>
SELECT WEEKOFYEAR('2011-11-11 11:11:11')
-&gt; 45
</pre>

</td>
</tr>
<tr>
<td>YEAR(d)</td>
<td>返回年份</td>
<td>
<pre>
SELECT YEAR("2017-06-15");
-&gt; 2017
</pre>

</td>
</tr>
<tr>
<td>YEARWEEK(date, mode)</td>
<td>返回年份及第几周（0到53），mode 中 0 表示周天，1表示周一，以此类推</td>
<td>
<pre>
SELECT YEARWEEK("2017-06-15");
-&gt; 201724
</pre>

</td>
</tr>
</tbody></table></div>') where ID=6292;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:25%">函数名</th>
<th>描述</th>
<th>实例</th>
</tr>
<tr>
<td>BIN(x)</td>
<td> 返回 x 的二进制编码</td>
<td>
<p> 15 的 2 进制编码:</p>
<pre>
SELECT BIN(15); -- 1111
</pre>

</td>
</tr>
<tr>
<td>BINARY(s)</td>
<td>将字符串 s 转换为二进制字符串</td>
<td>
<pre>
SELECT BINARY "RUNOOB";
-&gt; RUNOOB
</pre>

</td>
</tr>
<tr>
<td><pre>
CASE expression
    WHEN condition1 THEN result1
    WHEN condition2 THEN result2
   ...
    WHEN conditionN THEN resultN
    ELSE result
END</pre>
</td>
<td>CASE 表示函数开始，END 表示函数结束。如果 condition1 成立，则返回 result1, 如果 condition2 成立，则返回 result2，当全部不成立则返回 result，而当有一个成立之后，后面的就不执行了。</td>
<td>
<pre>
SELECT CASE 
　　WHEN 1 &gt; 0
　　THEN '1 &gt; 0'
　　WHEN 2 &gt; 0
　　THEN '2 &gt; 0'
　　ELSE '3 &gt; 0'
　　END
-&gt;1 &gt; 0
</pre>

</td>
</tr>
<tr>
<td>CAST(x AS type)</td>
<td>转换数据类型</td>
<td>
<p>字符串日期转换为日期：</p>
<pre>
SELECT CAST("2017-08-29" AS DATE);
-&gt; 2017-08-29
</pre>

</td>
</tr>
<tr>
<td>COALESCE(expr1, expr2, ...., expr_n)</td>
<td>返回参数中的第一个非空表达式（从左向右）</td>
<td>
<pre>
SELECT COALESCE(NULL, NULL, NULL, 'panku.in', NULL, 'google.com');
-&gt; panku.in
</pre>

</td>
</tr>
<tr>
<td>CONNECTION_ID()</td>
<td>返回服务器的连接数</td>
<td>
<pre>
SELECT CONNECTION_ID();
-&gt; 4292835
</pre>

</td>
</tr>
<tr>
<td>CONV(x,f1,f2)</td>
<td>返回 f1 进制数变成 f2 进制数</td>
<td>
<pre>
SELECT CONV(15, 10, 2);
-&gt; 1111
</pre>

</td>
</tr>
<tr>
<td>CONVERT(s USING cs)</td>
<td>函数将字符串 s 的字符集变成 cs</td>
<td>
<pre>
SELECT CHARSET('ABC')
-&gt;utf-8    

SELECT CHARSET(CONVERT('ABC' USING gbk))
-&gt;gbk
</pre>

</td>
</tr>
<tr>
<td>CURRENT_USER()</td>
<td>返回当前用户</td>
<td>
<pre>
SELECT CURRENT_USER();
-&gt; guest@%
</pre>

</td>
</tr>
<tr>
<td>DATABASE()</td>
<td>返回当前数据库名</td>
<td>
<pre>
SELECT DATABASE();   
-&gt; runoob
</pre>

</td>
</tr>
<tr>
<td>IF(expr,v1,v2)</td>
<td>如果表达式 expr 成立，返回结果 v1；否则，返回结果 v2。</td>
<td>
<pre>
SELECT IF(1 &gt; 0,'正确','错误')    
-&gt;正确
</pre>

</td>
</tr>
<tr>
<td><a href="/mysql/mysql-func-ifnull.html" rel="noopener noreferrer" target="_blank">IFNULL(v1,v2)</a></td>
<td>如果 v1 的值不为 NULL，则返回 v1，否则返回 v2。</td>
<td>
<pre>
SELECT IFNULL(null,'Hello Word')
-&gt;Hello Word
</pre>

</td>
</tr>
<tr>
<td>ISNULL(expression)</td>
<td>判断表达式是否为 NULL</td>
<td>
<pre>
SELECT ISNULL(NULL);
-&gt;1
</pre>

</td>
</tr>
<tr>
<td>LAST_INSERT_ID()</td>
<td>返回最近生成的 AUTO_INCREMENT 值</td>
<td>
<pre>
SELECT LAST_INSERT_ID();
-&gt;6
</pre>

</td>
</tr>
<tr>
<td>NULLIF(expr1, expr2)</td>
<td>比较两个字符串，如果字符串 expr1 与 expr2 相等 返回 NULL，否则返回 expr1 </td>
<td>
<pre>
SELECT NULLIF(25, 25);
-&gt;
</pre>

</td>
</tr>
<tr>
<td>SESSION_USER()</td>
<td>返回当前用户</td>
<td>
<pre>
SELECT SESSION_USER();
-&gt; guest@%
</pre>
</tr>
<tr>
<td>SYSTEM_USER()</td>
<td>返回当前用户</td>
<td>
<pre>
SELECT SYSTEM_USER();
-&gt; guest@%
</pre>
</tr>
<tr>
<td>USER()</td>
<td>返回当前用户</td>
<td>
<pre>
SELECT USER();
-&gt; guest@%
</pre>
</tr>
<tr>
<td>VERSION()</td>
<td>返回数据库的版本号</td>
<td>
<pre>
SELECT VERSION()
-&gt; 5.6.34
</pre>

</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:25%">函数名</th>
<th>描述</th>
<th>实例</th>
</tr>
<tr>
<td>BIN(x)</td>
<td> 返回 x 的二进制编码</td>
<td>
<p> 15 的 2 进制编码:</p>
<pre>
SELECT BIN(15); -- 1111
</pre>

</td>
</tr>
<tr>
<td>BINARY(s)</td>
<td>将字符串 s 转换为二进制字符串</td>
<td>
<pre>
SELECT BINARY "RUNOOB";
-&gt; RUNOOB
</pre>

</td>
</tr>
<tr>
<td><pre>
CASE expression
    WHEN condition1 THEN result1
    WHEN condition2 THEN result2
   ...
    WHEN conditionN THEN resultN
    ELSE result
END</pre>
</td>
<td>CASE 表示函数开始，END 表示函数结束。如果 condition1 成立，则返回 result1, 如果 condition2 成立，则返回 result2，当全部不成立则返回 result，而当有一个成立之后，后面的就不执行了。</td>
<td>
<pre>
SELECT CASE 
　　WHEN 1 &gt; 0
　　THEN '1 &gt; 0'
　　WHEN 2 &gt; 0
　　THEN '2 &gt; 0'
　　ELSE '3 &gt; 0'
　　END
-&gt;1 &gt; 0
</pre>

</td>
</tr>
<tr>
<td>CAST(x AS type)</td>
<td>转换数据类型</td>
<td>
<p>字符串日期转换为日期：</p>
<pre>
SELECT CAST("2017-08-29" AS DATE);
-&gt; 2017-08-29
</pre>

</td>
</tr>
<tr>
<td>COALESCE(expr1, expr2, ...., expr_n)</td>
<td>返回参数中的第一个非空表达式（从左向右）</td>
<td>
<pre>
SELECT COALESCE(NULL, NULL, NULL, 'panku.in', NULL, 'google.com');
-&gt; panku.in
</pre>

</td>
</tr>
<tr>
<td>CONNECTION_ID()</td>
<td>返回服务器的连接数</td>
<td>
<pre>
SELECT CONNECTION_ID();
-&gt; 4292835
</pre>

</td>
</tr>
<tr>
<td>CONV(x,f1,f2)</td>
<td>返回 f1 进制数变成 f2 进制数</td>
<td>
<pre>
SELECT CONV(15, 10, 2);
-&gt; 1111
</pre>

</td>
</tr>
<tr>
<td>CONVERT(s USING cs)</td>
<td>函数将字符串 s 的字符集变成 cs</td>
<td>
<pre>
SELECT CHARSET('ABC')
-&gt;utf-8    

SELECT CHARSET(CONVERT('ABC' USING gbk))
-&gt;gbk
</pre>

</td>
</tr>
<tr>
<td>CURRENT_USER()</td>
<td>返回当前用户</td>
<td>
<pre>
SELECT CURRENT_USER();
-&gt; guest@%
</pre>

</td>
</tr>
<tr>
<td>DATABASE()</td>
<td>返回当前数据库名</td>
<td>
<pre>
SELECT DATABASE();   
-&gt; runoob
</pre>

</td>
</tr>
<tr>
<td>IF(expr,v1,v2)</td>
<td>如果表达式 expr 成立，返回结果 v1；否则，返回结果 v2。</td>
<td>
<pre>
SELECT IF(1 &gt; 0,'正确','错误')    
-&gt;正确
</pre>

</td>
</tr>
<tr>
<td><a href="/mysql/mysql-func-ifnull.html" rel="noopener noreferrer" target="_blank">IFNULL(v1,v2)</a></td>
<td>如果 v1 的值不为 NULL，则返回 v1，否则返回 v2。</td>
<td>
<pre>
SELECT IFNULL(null,'Hello Word')
-&gt;Hello Word
</pre>

</td>
</tr>
<tr>
<td>ISNULL(expression)</td>
<td>判断表达式是否为 NULL</td>
<td>
<pre>
SELECT ISNULL(NULL);
-&gt;1
</pre>

</td>
</tr>
<tr>
<td>LAST_INSERT_ID()</td>
<td>返回最近生成的 AUTO_INCREMENT 值</td>
<td>
<pre>
SELECT LAST_INSERT_ID();
-&gt;6
</pre>

</td>
</tr>
<tr>
<td>NULLIF(expr1, expr2)</td>
<td>比较两个字符串，如果字符串 expr1 与 expr2 相等 返回 NULL，否则返回 expr1 </td>
<td>
<pre>
SELECT NULLIF(25, 25);
-&gt;
</pre>

</td>
</tr>
<tr>
<td>SESSION_USER()</td>
<td>返回当前用户</td>
<td>
<pre>
SELECT SESSION_USER();
-&gt; guest@%
</pre>
</tr>
<tr>
<td>SYSTEM_USER()</td>
<td>返回当前用户</td>
<td>
<pre>
SELECT SYSTEM_USER();
-&gt; guest@%
</pre>
</tr>
<tr>
<td>USER()</td>
<td>返回当前用户</td>
<td>
<pre>
SELECT USER();
-&gt; guest@%
</pre>
</tr>
<tr>
<td>VERSION()</td>
<td>返回数据库的版本号</td>
<td>
<pre>
SELECT VERSION()
-&gt; 5.6.34
</pre>

</td>
</tr>
</tbody></table></div>') where ID=6292;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr><th>序号</th><th>方法 &amp; 描述</th></tr>
<tr><td>1</td><td><b>addListener(event, listener)</b><br>
为指定事件添加一个监听器到监听器数组的尾部。</td></tr>
<tr><td>2</td><td><b>on(event, listener)</b><br>为指定事件注册一个监听器，接受一个字符串 event 和一个回调函数。
<pre>
server.on('connection', function (stream) {
  console.log('someone connected!');
});
</pre>

</td></tr>
<tr><td>3</td><td><b>once(event, listener)</b><br>为指定事件注册一个单次监听器，即 监听器最多只会触发一次，触发后立刻解除该监听器。
<pre>
server.once('connection', function (stream) {
  console.log('Ah, we have our first user!');
});
</pre>

</td></tr>
<tr><td>4</td><td><b>removeListener(event, listener)</b><br><p>移除指定事件的某个监听器，监听器必须是该事件已经注册过的监听器。</p><p>它接受两个参数，第一个是事件名称，第二个是回调函数名称。</p>
<pre>
var callback = function(stream) {
  console.log('someone connected!');
};
server.on('connection', callback);
// ...
server.removeListener('connection', callback);
</pre>

</td></tr>
<tr><td>5</td><td><b>removeAllListeners([event])</b><br>移除所有事件的所有监听器， 如果指定事件，则移除指定事件的所有监听器。</td></tr>
<tr><td>6</td><td><b>setMaxListeners(n)</b><br>默认情况下， EventEmitters 如果你添加的监听器超过 10 个就会输出警告信息。
setMaxListeners 函数用于提高监听器的默认限制的数量。</td></tr>
<tr><td>7</td><td><b>listeners(event)</b><br>返回指定事件的监听器数组。</td></tr>
<tr><td>8</td><td><b>emit(event, [arg1], [arg2], [...])</b><br>按监听器的顺序执行执行每个监听器，如果事件有注册监听返回 true，否则返回 false。</td></tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr><th>序号</th><th>方法 &amp; 描述</th></tr>
<tr><td>1</td><td><b>addListener(event, listener)</b><br>
为指定事件添加一个监听器到监听器数组的尾部。</td></tr>
<tr><td>2</td><td><b>on(event, listener)</b><br>为指定事件注册一个监听器，接受一个字符串 event 和一个回调函数。
<pre>
server.on('connection', function (stream) {
  console.log('someone connected!');
});
</pre>

</td></tr>
<tr><td>3</td><td><b>once(event, listener)</b><br>为指定事件注册一个单次监听器，即 监听器最多只会触发一次，触发后立刻解除该监听器。
<pre>
server.once('connection', function (stream) {
  console.log('Ah, we have our first user!');
});
</pre>

</td></tr>
<tr><td>4</td><td><b>removeListener(event, listener)</b><br><p>移除指定事件的某个监听器，监听器必须是该事件已经注册过的监听器。</p><p>它接受两个参数，第一个是事件名称，第二个是回调函数名称。</p>
<pre>
var callback = function(stream) {
  console.log('someone connected!');
};
server.on('connection', callback);
// ...
server.removeListener('connection', callback);
</pre>

</td></tr>
<tr><td>5</td><td><b>removeAllListeners([event])</b><br>移除所有事件的所有监听器， 如果指定事件，则移除指定事件的所有监听器。</td></tr>
<tr><td>6</td><td><b>setMaxListeners(n)</b><br>默认情况下， EventEmitters 如果你添加的监听器超过 10 个就会输出警告信息。
setMaxListeners 函数用于提高监听器的默认限制的数量。</td></tr>
<tr><td>7</td><td><b>listeners(event)</b><br>返回指定事件的监听器数组。</td></tr>
<tr><td>8</td><td><b>emit(event, [arg1], [arg2], [...])</b><br>按监听器的顺序执行执行每个监听器，如果事件有注册监听返回 true，否则返回 false。</td></tr>
</tbody></table></div>') where ID=6306;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr><th>序号</th><th>方法 &amp; 描述</th></tr>
<tr><td>1</td><td><b>new Buffer(size)</b> <br>分配一个新的 size 大小单位为8位字节的 buffer。  注意, size 必须小于 kMaxLength，否则，将会抛出异常 RangeError。<span class="deprecated">废弃的: 使用 Buffer.alloc() 代替（或 Buffer.allocUnsafe()）。</span></td></tr>
<tr><td>2</td><td><b>new Buffer(buffer)</b> <br>拷贝参数 buffer 的数据到 Buffer 实例。<span class="deprecated">废弃的: 使用 Buffer.from(buffer) 代替。</span></td></tr>
<tr><td>3</td><td><b>new Buffer(str[, encoding])</b><br> 分配一个新的 buffer ，其中包含着传入的 str 字符串。 encoding 编码方式默认为 'utf8'。
<span class="deprecated">废弃的: 使用 Buffer.from(string[, encoding]) 代替。</span>
</td></tr>
<tr><td>4</td><td><b>buf.length</b><br> 返回这个 buffer 的 bytes 数。注意这未必是 buffer 里面内容的大小。length 是 buffer 对象所分配的内存数，它不会随着这个 buffer 对象内容的改变而改变。</td></tr>
<tr><td>5</td><td><b>buf.write(string[, offset[, length]][, encoding])</b><br>根据参数 offset 偏移量和指定的 encoding 编码方式，将参数 string 数据写入buffer。 offset 偏移量默认值是 0, encoding 编码方式默认是 utf8。 length 长度是将要写入的字符串的 bytes 大小。 返回 number 类型，表示写入了多少 8 位字节流。如果 buffer 没有足够的空间来放整个 string，它将只会只写入部分字符串。 length 默认是 buffer.length - offset。 这个方法不会出现写入部分字符。</td></tr>
<tr><td>6</td><td><b>buf.writeUIntLE(value, offset, byteLength[, noAssert])</b><br>将 value 写入到 buffer 里， 它由 offset 和 byteLength 决定，最高支持 48 位无符号整数，小端对齐，例如：<br>
<pre>
const buf = Buffer.allocUnsafe(6);

buf.writeUIntLE(0x1234567890ab, 0, 6);

// 输出: &lt;Buffer ab 90 78 56 34 12&gt;
console.log(buf);
</pre>
noAssert 值为 true 时，不再验证 value 和 offset 的有效性。 默认是 false。</td></tr>
<tr><td>7</td><td><b>buf.writeUIntBE(value, offset, byteLength[, noAssert])</b><br>将 value 写入到 buffer 里， 它由 offset 和 byteLength 决定，最高支持 48 位无符号整数，大端对齐。noAssert 值为 true 时，不再验证 value 和 offset 的有效性。 默认是 false。
<pre>
const buf = Buffer.allocUnsafe(6);

buf.writeUIntBE(0x1234567890ab, 0, 6);

// 输出: &lt;Buffer 12 34 56 78 90 ab&gt;
console.log(buf);
</pre>
</td></tr>
<tr><td>8</td><td><b>buf.writeIntLE(value, offset, byteLength[, noAssert])</b><br>将value 写入到 buffer 里， 它由offset 和 byteLength 决定，最高支持48位有符号整数，小端对齐。noAssert 值为 true 时，不再验证 value 和 offset 的有效性。 默认是 false。</td></tr>
<tr><td>9</td><td><b>buf.writeIntBE(value, offset, byteLength[, noAssert])</b><br>将value 写入到 buffer 里， 它由offset 和 byteLength 决定，最高支持48位有符号整数，大端对齐。noAssert 值为 true 时，不再验证 value 和 offset 的有效性。 默认是 false。</td></tr>
<tr><td>10</td><td><b>buf.readUIntLE(offset, byteLength[, noAssert])</b><br>支持读取 48 位以下的无符号数字，小端对齐。noAssert 值为 true 时， offset 不再验证是否超过 buffer 的长度，默认为 false。</td></tr>
<tr><td>11</td><td><b>buf.readUIntBE(offset, byteLength[, noAssert])</b><br>支持读取 48 位以下的无符号数字，大端对齐。noAssert 值为 true 时， offset 不再验证是否超过 buffer 的长度，默认为 false。</td></tr>
<tr><td>12</td><td><b>buf.readIntLE(offset, byteLength[, noAssert])</b><br>支持读取 48 位以下的有符号数字，小端对齐。noAssert 值为 true 时， offset 不再验证是否超过 buffer 的长度，默认为 false。</td></tr>
<tr><td>13</td><td><b>buf.readIntBE(offset, byteLength[, noAssert])</b><br>支持读取 48 位以下的有符号数字，大端对齐。noAssert 值为 true 时， offset 不再验证是否超过 buffer 的长度，默认为 false。</td></tr>
<tr><td>14</td><td><b>buf.toString([encoding[, start[, end]]])</b><br>根据 encoding 参数（默认是 'utf8'）返回一个解码过的 string 类型。还会根据传入的参数 start (默认是 0) 和 end (默认是 buffer.length)作为取值范围。</td></tr>
<tr><td>15</td><td><b>buf.toJSON()</b><br>将 Buffer 实例转换为 JSON 对象。</td></tr>
<tr><td>16</td><td><b>buf[index]</b><br>获取或设置指定的字节。返回值代表一个字节，所以返回值的合法范围是十六进制0x00到0xFF 或者十进制0至 255。</td></tr>
<tr><td>17</td><td><b>buf.equals(otherBuffer)</b><br>比较两个缓冲区是否相等，如果是返回 true，否则返回 false。</td></tr>
<tr><td>18</td><td><b>buf.compare(otherBuffer)</b><br>比较两个 Buffer 对象，返回一个数字，表示 buf 在 otherBuffer 之前，之后或相同。</td></tr>
<tr><td>19</td><td><b>buf.copy(targetBuffer[, targetStart[, sourceStart[, sourceEnd]]])</b><br>buffer 拷贝，源和目标可以相同。 targetStart 目标开始偏移和 sourceStart 源开始偏移默认都是 0。 sourceEnd 源结束位置偏移默认是源的长度 buffer.length 。</td></tr>
<tr><td>20</td><td><b>buf.slice([start[, end]])</b><br>剪切 Buffer 对象，根据 start(默认是 0 ) 和 end (默认是 buffer.length ) 偏移和裁剪了索引。 负的索引是从 buffer 尾部开始计算的。</td></tr>
<tr><td>21</td><td><b>buf.readUInt8(offset[, noAssert])</b><br>根据指定的偏移量，读取一个无符号 8 位整数。若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 如果这样 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>22</td><td><b>buf.readUInt16LE(offset[, noAssert])</b><br>根据指定的偏移量，使用特殊的 endian 字节序格式读取一个无符号 16 位整数。若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>23</td><td><b>buf.readUInt16BE(offset[, noAssert])</b><br>根据指定的偏移量，使用特殊的 endian 字节序格式读取一个无符号 16 位整数，大端对齐。若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>24</td><td><b>buf.readUInt32LE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个无符号 32 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>25</td><td><b>buf.readUInt32BE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个无符号 32 位整数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>26</td><td><b>buf.readInt8(offset[, noAssert])</b><br>根据指定的偏移量，读取一个有符号 8 位整数。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>27</td><td><b>buf.readInt16LE(offset[, noAssert])</b><br>根据指定的偏移量，使用特殊的 endian 格式读取一个 有符号 16 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>28</td><td><b>buf.readInt16BE(offset[, noAssert])</b><br>根据指定的偏移量，使用特殊的 endian 格式读取一个 有符号 16 位整数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>29</td><td><b>buf.readInt32LE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个有符号 32 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>30</td><td><b>buf.readInt32BE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个有符号 32 位整数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>31</td><td><b>buf.readFloatLE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个 32 位双浮点数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer的末尾。默认是 false。</td></tr>
<tr><td>32</td><td><b>buf.readFloatBE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个 32 位双浮点数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer的末尾。默认是 false。</td></tr>
<tr><td>33</td><td><b>buf.readDoubleLE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian字节序格式读取一个  64 位双精度数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>34</td><td><b>buf.readDoubleBE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian字节序格式读取一个  64 位双精度数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>35</td><td><b>buf.writeUInt8(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量将 value 写入 buffer。注意：value 必须是一个合法的无符号 8 位整数。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则不要使用。默认是 false。</td></tr>
<tr><td>36</td><td><b>buf.writeUInt16LE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的无符号 16 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出buffer的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>37</td><td><b>buf.writeUInt16BE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的无符号 16 位整数，大端对齐。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出buffer的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>38</td><td><b>buf.writeUInt32LE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式(LITTLE-ENDIAN:小字节序)将 value 写入buffer。注意：value 必须是一个合法的无符号 32 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着value 可能过大，或者offset可能会超出buffer的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>39</td><td><b>buf.writeUInt32BE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式(Big-Endian:大字节序)将 value 写入buffer。注意：value 必须是一个合法的有符号 32 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者offset可能会超出buffer的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>40</td><td><b>buf.writeInt8(value, offset[, noAssert])</b><br根据传入的 offset 偏移量将 value 写入 buffer 。注意：value 必须是一个合法的 signed 8 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>41</td><td><b>buf.writeInt16LE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的 signed 16 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false 。</td></tr>
<tr><td>42</td><td><b>buf.writeInt16BE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的 signed 16 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false 。</td></tr>
<tr><td>43</td><td><b>buf.writeInt32LE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的 signed 32 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>44</td><td><b>buf.writeInt32BE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的 signed 32 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>45</td><td><b>buf.writeFloatLE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer 。注意：当 value 不是一个 32 位浮点数类型的值时，结果将是不确定的。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>46</td><td><b>buf.writeFloatBE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer 。注意：当 value 不是一个 32 位浮点数类型的值时，结果将是不确定的。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>47</td><td><b>buf.writeDoubleLE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个有效的 64 位double 类型的值。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成value被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>48</td><td><b>buf.writeDoubleBE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个有效的 64 位double 类型的值。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成value被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>49</td><td><b>buf.fill(value[, offset][, end])</b><br>使用指定的 value 来填充这个 buffer。如果没有指定 offset (默认是 0) 并且 end (默认是 buffer.length) ，将会填充整个buffer。</td></tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr><th>序号</th><th>方法 &amp; 描述</th></tr>
<tr><td>1</td><td><b>new Buffer(size)</b> <br>分配一个新的 size 大小单位为8位字节的 buffer。  注意, size 必须小于 kMaxLength，否则，将会抛出异常 RangeError。<span class="deprecated">废弃的: 使用 Buffer.alloc() 代替（或 Buffer.allocUnsafe()）。</span></td></tr>
<tr><td>2</td><td><b>new Buffer(buffer)</b> <br>拷贝参数 buffer 的数据到 Buffer 实例。<span class="deprecated">废弃的: 使用 Buffer.from(buffer) 代替。</span></td></tr>
<tr><td>3</td><td><b>new Buffer(str[, encoding])</b><br> 分配一个新的 buffer ，其中包含着传入的 str 字符串。 encoding 编码方式默认为 'utf8'。
<span class="deprecated">废弃的: 使用 Buffer.from(string[, encoding]) 代替。</span>
</td></tr>
<tr><td>4</td><td><b>buf.length</b><br> 返回这个 buffer 的 bytes 数。注意这未必是 buffer 里面内容的大小。length 是 buffer 对象所分配的内存数，它不会随着这个 buffer 对象内容的改变而改变。</td></tr>
<tr><td>5</td><td><b>buf.write(string[, offset[, length]][, encoding])</b><br>根据参数 offset 偏移量和指定的 encoding 编码方式，将参数 string 数据写入buffer。 offset 偏移量默认值是 0, encoding 编码方式默认是 utf8。 length 长度是将要写入的字符串的 bytes 大小。 返回 number 类型，表示写入了多少 8 位字节流。如果 buffer 没有足够的空间来放整个 string，它将只会只写入部分字符串。 length 默认是 buffer.length - offset。 这个方法不会出现写入部分字符。</td></tr>
<tr><td>6</td><td><b>buf.writeUIntLE(value, offset, byteLength[, noAssert])</b><br>将 value 写入到 buffer 里， 它由 offset 和 byteLength 决定，最高支持 48 位无符号整数，小端对齐，例如：<br>
<pre>
const buf = Buffer.allocUnsafe(6);

buf.writeUIntLE(0x1234567890ab, 0, 6);

// 输出: &lt;Buffer ab 90 78 56 34 12&gt;
console.log(buf);
</pre>
noAssert 值为 true 时，不再验证 value 和 offset 的有效性。 默认是 false。</td></tr>
<tr><td>7</td><td><b>buf.writeUIntBE(value, offset, byteLength[, noAssert])</b><br>将 value 写入到 buffer 里， 它由 offset 和 byteLength 决定，最高支持 48 位无符号整数，大端对齐。noAssert 值为 true 时，不再验证 value 和 offset 的有效性。 默认是 false。
<pre>
const buf = Buffer.allocUnsafe(6);

buf.writeUIntBE(0x1234567890ab, 0, 6);

// 输出: &lt;Buffer 12 34 56 78 90 ab&gt;
console.log(buf);
</pre>
</td></tr>
<tr><td>8</td><td><b>buf.writeIntLE(value, offset, byteLength[, noAssert])</b><br>将value 写入到 buffer 里， 它由offset 和 byteLength 决定，最高支持48位有符号整数，小端对齐。noAssert 值为 true 时，不再验证 value 和 offset 的有效性。 默认是 false。</td></tr>
<tr><td>9</td><td><b>buf.writeIntBE(value, offset, byteLength[, noAssert])</b><br>将value 写入到 buffer 里， 它由offset 和 byteLength 决定，最高支持48位有符号整数，大端对齐。noAssert 值为 true 时，不再验证 value 和 offset 的有效性。 默认是 false。</td></tr>
<tr><td>10</td><td><b>buf.readUIntLE(offset, byteLength[, noAssert])</b><br>支持读取 48 位以下的无符号数字，小端对齐。noAssert 值为 true 时， offset 不再验证是否超过 buffer 的长度，默认为 false。</td></tr>
<tr><td>11</td><td><b>buf.readUIntBE(offset, byteLength[, noAssert])</b><br>支持读取 48 位以下的无符号数字，大端对齐。noAssert 值为 true 时， offset 不再验证是否超过 buffer 的长度，默认为 false。</td></tr>
<tr><td>12</td><td><b>buf.readIntLE(offset, byteLength[, noAssert])</b><br>支持读取 48 位以下的有符号数字，小端对齐。noAssert 值为 true 时， offset 不再验证是否超过 buffer 的长度，默认为 false。</td></tr>
<tr><td>13</td><td><b>buf.readIntBE(offset, byteLength[, noAssert])</b><br>支持读取 48 位以下的有符号数字，大端对齐。noAssert 值为 true 时， offset 不再验证是否超过 buffer 的长度，默认为 false。</td></tr>
<tr><td>14</td><td><b>buf.toString([encoding[, start[, end]]])</b><br>根据 encoding 参数（默认是 'utf8'）返回一个解码过的 string 类型。还会根据传入的参数 start (默认是 0) 和 end (默认是 buffer.length)作为取值范围。</td></tr>
<tr><td>15</td><td><b>buf.toJSON()</b><br>将 Buffer 实例转换为 JSON 对象。</td></tr>
<tr><td>16</td><td><b>buf[index]</b><br>获取或设置指定的字节。返回值代表一个字节，所以返回值的合法范围是十六进制0x00到0xFF 或者十进制0至 255。</td></tr>
<tr><td>17</td><td><b>buf.equals(otherBuffer)</b><br>比较两个缓冲区是否相等，如果是返回 true，否则返回 false。</td></tr>
<tr><td>18</td><td><b>buf.compare(otherBuffer)</b><br>比较两个 Buffer 对象，返回一个数字，表示 buf 在 otherBuffer 之前，之后或相同。</td></tr>
<tr><td>19</td><td><b>buf.copy(targetBuffer[, targetStart[, sourceStart[, sourceEnd]]])</b><br>buffer 拷贝，源和目标可以相同。 targetStart 目标开始偏移和 sourceStart 源开始偏移默认都是 0。 sourceEnd 源结束位置偏移默认是源的长度 buffer.length 。</td></tr>
<tr><td>20</td><td><b>buf.slice([start[, end]])</b><br>剪切 Buffer 对象，根据 start(默认是 0 ) 和 end (默认是 buffer.length ) 偏移和裁剪了索引。 负的索引是从 buffer 尾部开始计算的。</td></tr>
<tr><td>21</td><td><b>buf.readUInt8(offset[, noAssert])</b><br>根据指定的偏移量，读取一个无符号 8 位整数。若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 如果这样 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>22</td><td><b>buf.readUInt16LE(offset[, noAssert])</b><br>根据指定的偏移量，使用特殊的 endian 字节序格式读取一个无符号 16 位整数。若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>23</td><td><b>buf.readUInt16BE(offset[, noAssert])</b><br>根据指定的偏移量，使用特殊的 endian 字节序格式读取一个无符号 16 位整数，大端对齐。若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>24</td><td><b>buf.readUInt32LE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个无符号 32 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>25</td><td><b>buf.readUInt32BE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个无符号 32 位整数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>26</td><td><b>buf.readInt8(offset[, noAssert])</b><br>根据指定的偏移量，读取一个有符号 8 位整数。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>27</td><td><b>buf.readInt16LE(offset[, noAssert])</b><br>根据指定的偏移量，使用特殊的 endian 格式读取一个 有符号 16 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>28</td><td><b>buf.readInt16BE(offset[, noAssert])</b><br>根据指定的偏移量，使用特殊的 endian 格式读取一个 有符号 16 位整数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出 buffer 的末尾。默认是 false。</td></tr>
<tr><td>29</td><td><b>buf.readInt32LE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个有符号 32 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>30</td><td><b>buf.readInt32BE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个有符号 32 位整数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>31</td><td><b>buf.readFloatLE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个 32 位双浮点数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer的末尾。默认是 false。</td></tr>
<tr><td>32</td><td><b>buf.readFloatBE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian 字节序格式读取一个 32 位双浮点数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer的末尾。默认是 false。</td></tr>
<tr><td>33</td><td><b>buf.readDoubleLE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian字节序格式读取一个  64 位双精度数，小端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>34</td><td><b>buf.readDoubleBE(offset[, noAssert])</b><br>根据指定的偏移量，使用指定的 endian字节序格式读取一个  64 位双精度数，大端对齐。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 offset 可能会超出buffer 的末尾。默认是 false。</td></tr>
<tr><td>35</td><td><b>buf.writeUInt8(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量将 value 写入 buffer。注意：value 必须是一个合法的无符号 8 位整数。

若参数 noAssert 为 true 将不会验证 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则不要使用。默认是 false。</td></tr>
<tr><td>36</td><td><b>buf.writeUInt16LE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的无符号 16 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出buffer的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>37</td><td><b>buf.writeUInt16BE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的无符号 16 位整数，大端对齐。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出buffer的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>38</td><td><b>buf.writeUInt32LE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式(LITTLE-ENDIAN:小字节序)将 value 写入buffer。注意：value 必须是一个合法的无符号 32 位整数，小端对齐。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着value 可能过大，或者offset可能会超出buffer的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>39</td><td><b>buf.writeUInt32BE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式(Big-Endian:大字节序)将 value 写入buffer。注意：value 必须是一个合法的有符号 32 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者offset可能会超出buffer的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>40</td><td><b>buf.writeInt8(value, offset[, noAssert])</b><br根据传入的 offset 偏移量将 value 写入 buffer 。注意：value 必须是一个合法的 signed 8 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>41</td><td><b>buf.writeInt16LE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的 signed 16 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false 。</td></tr>
<tr><td>42</td><td><b>buf.writeInt16BE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的 signed 16 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false 。</td></tr>
<tr><td>43</td><td><b>buf.writeInt32LE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的 signed 32 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>44</td><td><b>buf.writeInt32BE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个合法的 signed 32 位整数。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>45</td><td><b>buf.writeFloatLE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer 。注意：当 value 不是一个 32 位浮点数类型的值时，结果将是不确定的。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>46</td><td><b>buf.writeFloatBE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer 。注意：当 value 不是一个 32 位浮点数类型的值时，结果将是不确定的。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value可能过大，或者 offset 可能会超出 buffer 的末尾从而造成 value 被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>47</td><td><b>buf.writeDoubleLE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个有效的 64 位double 类型的值。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成value被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>48</td><td><b>buf.writeDoubleBE(value, offset[, noAssert])</b><br>根据传入的 offset 偏移量和指定的 endian 格式将 value 写入 buffer。注意：value 必须是一个有效的 64 位double 类型的值。

若参数 noAssert 为 true 将不会验证 value 和 offset 偏移量参数。 这意味着 value 可能过大，或者 offset 可能会超出 buffer 的末尾从而造成value被丢弃。 除非你对这个参数非常有把握，否则尽量不要使用。默认是 false。</td></tr>
<tr><td>49</td><td><b>buf.fill(value[, offset][, end])</b><br>使用指定的 value 来填充这个 buffer。如果没有指定 offset (默认是 0) 并且 end (默认是 buffer.length) ，将会填充整个buffer。</td></tr>
</tbody></table></div>') where ID=6317;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr><th>序号</th><th>方法 &amp; 描述</th></tr>
<tr><td>1</td><td><b>path.normalize(p)</b><br>规范化路径，注意'..' 和 '.'。</td></tr>
<tr><td>2</td><td><b>path.join([path1][, path2][, ...])</b><br>用于连接路径。该方法的主要用途在于，会正确使用当前系统的路径分隔符，Unix系统是"/"，Windows系统是"\"。</td></tr>
<tr><td>3</td><td><b>path.resolve([from ...], to)</b><br>将 <b>to</b> 参数解析为绝对路径，给定的路径的序列是从右往左被处理的，后面每个 path 被依次解析，直到构造完成一个绝对路径。 例如，给定的路径片段的序列为：/foo、/bar、baz，则调用 path.resolve('/foo', '/bar', 'baz') 会返回 /bar/baz。

<pre>path.resolve('/foo/bar', './baz');
// 返回: '/foo/bar/baz'

path.resolve('/foo/bar', '/tmp/file/');
// 返回: '/tmp/file'

path.resolve('wwwroot', 'static_files/png/', '../gif/image.gif');
// 如果当前工作目录为 /home/myself/node，
// 则返回 '/home/myself/node/wwwroot/static_files/gif/image.gif'</pre>
</td></tr>
<tr><td>4</td><td><b>path.isAbsolute(path)</b><br>判断参数 <b>path</b> 是否是绝对路径。</td></tr>
<tr><td>5</td><td><b>path.relative(from, to)</b><br><p>用于将绝对路径转为相对路径，返回从 from 到 to 的相对路径（基于当前工作目录）。</p>

<p>在 Linux 上：</p>

<pre>path.relative('/data/orandea/test/aaa', '/data/orandea/impl/bbb');
// 返回: '../../impl/bbb'</pre>
<p>在 Windows 上：</p>

<pre>path.relative('C:\\orandea\\test\\aaa', 'C:\\orandea\\impl\\bbb');
// 返回: '..\\..\\impl\\bbb'</pre>
</td></tr>
<tr><td>6</td><td><b>path.dirname(p)</b><br>返回路径中代表文件夹的部分，同 Unix 的dirname 命令类似。</td></tr>
<tr><td>7</td><td><b>path.basename(p[, ext])</b><br>返回路径中的最后一部分。同 Unix 命令 bashname 类似。</td></tr>
<tr><td>8</td><td><b>path.extname(p)</b><br>返回路径中文件的后缀名，即路径中最后一个'.'之后的部分。如果一个路径中并不包含'.'或该路径只包含一个'.' 且这个'.'为路径的第一个字符，则此命令返回空字符串。</td></tr>
<tr><td>9</td><td><b>path.parse(pathString)</b><br>返回路径字符串的对象。</td></tr>
<tr><td>10</td><td><b>path.format(pathObject)</b><br>从对象中返回路径字符串，和 path.parse 相反。</td></tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr><th>序号</th><th>方法 &amp; 描述</th></tr>
<tr><td>1</td><td><b>path.normalize(p)</b><br>规范化路径，注意'..' 和 '.'。</td></tr>
<tr><td>2</td><td><b>path.join([path1][, path2][, ...])</b><br>用于连接路径。该方法的主要用途在于，会正确使用当前系统的路径分隔符，Unix系统是"/"，Windows系统是"\"。</td></tr>
<tr><td>3</td><td><b>path.resolve([from ...], to)</b><br>将 <b>to</b> 参数解析为绝对路径，给定的路径的序列是从右往左被处理的，后面每个 path 被依次解析，直到构造完成一个绝对路径。 例如，给定的路径片段的序列为：/foo、/bar、baz，则调用 path.resolve('/foo', '/bar', 'baz') 会返回 /bar/baz。

<pre>path.resolve('/foo/bar', './baz');
// 返回: '/foo/bar/baz'

path.resolve('/foo/bar', '/tmp/file/');
// 返回: '/tmp/file'

path.resolve('wwwroot', 'static_files/png/', '../gif/image.gif');
// 如果当前工作目录为 /home/myself/node，
// 则返回 '/home/myself/node/wwwroot/static_files/gif/image.gif'</pre>
</td></tr>
<tr><td>4</td><td><b>path.isAbsolute(path)</b><br>判断参数 <b>path</b> 是否是绝对路径。</td></tr>
<tr><td>5</td><td><b>path.relative(from, to)</b><br><p>用于将绝对路径转为相对路径，返回从 from 到 to 的相对路径（基于当前工作目录）。</p>

<p>在 Linux 上：</p>

<pre>path.relative('/data/orandea/test/aaa', '/data/orandea/impl/bbb');
// 返回: '../../impl/bbb'</pre>
<p>在 Windows 上：</p>

<pre>path.relative('C:\\orandea\\test\\aaa', 'C:\\orandea\\impl\\bbb');
// 返回: '..\\..\\impl\\bbb'</pre>
</td></tr>
<tr><td>6</td><td><b>path.dirname(p)</b><br>返回路径中代表文件夹的部分，同 Unix 的dirname 命令类似。</td></tr>
<tr><td>7</td><td><b>path.basename(p[, ext])</b><br>返回路径中的最后一部分。同 Unix 命令 bashname 类似。</td></tr>
<tr><td>8</td><td><b>path.extname(p)</b><br>返回路径中文件的后缀名，即路径中最后一个'.'之后的部分。如果一个路径中并不包含'.'或该路径只包含一个'.' 且这个'.'为路径的第一个字符，则此命令返回空字符串。</td></tr>
<tr><td>9</td><td><b>path.parse(pathString)</b><br>返回路径字符串的对象。</td></tr>
<tr><td>10</td><td><b>path.format(pathObject)</b><br>从对象中返回路径字符串，和 path.parse 相反。</td></tr>
</tbody></table></div>') where ID=6321;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th width="10%">操作符</th><th>描述</th><th width="20%">实例</th>
</tr>
<tr>
<td>+</td><td>字符串连接</td><td> a + b 输出结果： HelloPython</td>
</tr>
<tr>
<td>*</td><td>重复输出字符串</td><td> a*2 输出结果：HelloHello</td>
</tr>
<tr>
<td>[]</td><td>通过索引获取字符串中字符</td><td> a[1] 输出结果 <b>e</b></td>
</tr>
<tr>
<td>[ : ]</td><td>截取字符串中的一部分，遵循<strong>左闭右开</strong>原则，str[0,2] 是不包含第 3 个字符的。</td><td> a[1:4] 输出结果 <b>ell</b></td>
</tr>
<tr>
<td>in</td><td>成员运算符 - 如果字符串中包含给定的字符返回 True </td><td> <b>'H' in a</b> 输出结果 True</td>
</tr>
<tr>
<td>not in </td><td>成员运算符 - 如果字符串中不包含给定的字符返回 True </td><td> <b>'M' not in a</b> 输出结果 True</td>
</tr>
<tr>
<td>r/R</td><td>原始字符串 - 原始字符串：所有的字符串都是直接按照字面的意思来使用，没有转义特殊或不能打印的字符。

原始字符串除在字符串的第一个引号前加上字母 <span class="marked">r</span>（可以大小写）以外，与普通字符串有着几乎完全相同的语法。</td><td><pre>
print( r'\n' )
print( R'\n' )
</pre></td>
</tr>
<tr>
<td>%</td><td>格式字符串</td><td>请看下一节内容。</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th width="10%">操作符</th><th>描述</th><th width="20%">实例</th>
</tr>
<tr>
<td>+</td><td>字符串连接</td><td> a + b 输出结果： HelloPython</td>
</tr>
<tr>
<td>*</td><td>重复输出字符串</td><td> a*2 输出结果：HelloHello</td>
</tr>
<tr>
<td>[]</td><td>通过索引获取字符串中字符</td><td> a[1] 输出结果 <b>e</b></td>
</tr>
<tr>
<td>[ : ]</td><td>截取字符串中的一部分，遵循<strong>左闭右开</strong>原则，str[0,2] 是不包含第 3 个字符的。</td><td> a[1:4] 输出结果 <b>ell</b></td>
</tr>
<tr>
<td>in</td><td>成员运算符 - 如果字符串中包含给定的字符返回 True </td><td> <b>'H' in a</b> 输出结果 True</td>
</tr>
<tr>
<td>not in </td><td>成员运算符 - 如果字符串中不包含给定的字符返回 True </td><td> <b>'M' not in a</b> 输出结果 True</td>
</tr>
<tr>
<td>r/R</td><td>原始字符串 - 原始字符串：所有的字符串都是直接按照字面的意思来使用，没有转义特殊或不能打印的字符。

原始字符串除在字符串的第一个引号前加上字母 <span class="marked">r</span>（可以大小写）以外，与普通字符串有着几乎完全相同的语法。</td><td><pre>
print( r'\n' )
print( R'\n' )
</pre></td>
</tr>
<tr>
<td>%</td><td>格式字符串</td><td>请看下一节内容。</td>
</tr>
</tbody></table></div>') where ID=6447;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th>运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>+</td><td>加 - 两个对象相加</td><td> a + b 输出结果 31</td>
</tr>
<tr>
<td>-</td><td>减 - 得到负数或是一个数减去另一个数</td><td> a - b 输出结果 -11</td>
</tr>
<tr>
<td>*</td><td>乘 - 两个数相乘或是返回一个被重复若干次的字符串</td><td> a * b 输出结果 210</td>
</tr>
<tr>
<td>/</td><td>除 - x 除以 y</td><td> b / a 输出结果 2.1</td>
</tr>
<tr>
<td>%</td><td>取模 - 返回除法的余数</td><td> b % a 输出结果 1</td>
</tr>
<tr>
<td>**</td><td>幂 - 返回x的y次幂</td><td> a**b 为10的21次方</td>
</tr>
<tr>
<td>//</td><td>取整除 - 向下取接近除数的整数</td><td> 
<pre>&gt;&gt;&gt; 9//2
4
&gt;&gt;&gt; -9//2
-5</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th>运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>+</td><td>加 - 两个对象相加</td><td> a + b 输出结果 31</td>
</tr>
<tr>
<td>-</td><td>减 - 得到负数或是一个数减去另一个数</td><td> a - b 输出结果 -11</td>
</tr>
<tr>
<td>*</td><td>乘 - 两个数相乘或是返回一个被重复若干次的字符串</td><td> a * b 输出结果 210</td>
</tr>
<tr>
<td>/</td><td>除 - x 除以 y</td><td> b / a 输出结果 2.1</td>
</tr>
<tr>
<td>%</td><td>取模 - 返回除法的余数</td><td> b % a 输出结果 1</td>
</tr>
<tr>
<td>**</td><td>幂 - 返回x的y次幂</td><td> a**b 为10的21次方</td>
</tr>
<tr>
<td>//</td><td>取整除 - 向下取接近除数的整数</td><td> 
<pre>&gt;&gt;&gt; 9//2
4
&gt;&gt;&gt; -9//2
-5</pre>
</td>
</tr>
</tbody></table></div>') where ID=6494;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:25%">方法及描述</th><th style="width:45%">实例</th></tr>

<tr><td>1</td><td>len(tuple)<br>计算元组元素个数。</td>
<td>
<pre>
&gt;&gt;&gt; tuple1 = ('Google', 'Runoob', 'Taobao')
&gt;&gt;&gt; len(tuple1)
3
&gt;&gt;&gt; 
</pre>
</td>
</tr>
<tr><td>2</td><td>max(tuple)<br>返回元组中元素最大值。</td>
<td>
<pre>
>>> tuple2 = ('5', '4', '8')
>>> max(tuple2)
'8'
>>> 
</td>

</tr>
<tr><td>3</td><td>min(tuple)<br>返回元组中元素最小值。</td>
<td>
<pre>
>>> tuple2 = ('5', '4', '8')
>>> min(tuple2)
'4'
>>> 
</td>
</tr>
<tr><td>4</td><td>tuple(seq)<br>将列表转换为元组。</td>
<td>
<pre>
>>> list1= ['Google', 'Taobao', 'Runoob', 'Baidu']
>>> tuple1=tuple(list1)
>>> tuple1
('Google', 'Taobao', 'Runoob', 'Baidu')
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:25%">方法及描述</th><th style="width:45%">实例</th></tr>

<tr><td>1</td><td>len(tuple)<br>计算元组元素个数。</td>
<td>
<pre>
&gt;&gt;&gt; tuple1 = ('Google', 'Runoob', 'Taobao')
&gt;&gt;&gt; len(tuple1)
3
&gt;&gt;&gt; 
</pre>
</td>
</tr>
<tr><td>2</td><td>max(tuple)<br>返回元组中元素最大值。</td>
<td>
<pre>
>>> tuple2 = ('5', '4', '8')
>>> max(tuple2)
'8'
>>> 
</td>

</tr>
<tr><td>3</td><td>min(tuple)<br>返回元组中元素最小值。</td>
<td>
<pre>
>>> tuple2 = ('5', '4', '8')
>>> min(tuple2)
'4'
>>> 
</td>
</tr>
<tr><td>4</td><td>tuple(seq)<br>将列表转换为元组。</td>
<td>
<pre>
>>> list1= ['Google', 'Taobao', 'Runoob', 'Baidu']
>>> tuple1=tuple(list1)
>>> tuple1
('Google', 'Taobao', 'Runoob', 'Baidu')
</td>
</tr>
</tbody></table></div>') where ID=6580;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th width="5%">序号</th><th width="25%">函数及描述</th><th>实例</th></tr>

<tr><td>1</td><td>len(dict)<br>计算字典元素个数，即键的总数。</td>
<td>
<pre>
&gt;&gt;&gt; dict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}
&gt;&gt;&gt; len(dict)
3
</pre>
</td>
</tr>
<tr><td>2</td><td>str(dict)<br>输出字典，以可打印的字符串表示。</td>
<td>
<pre>
&gt;&gt;&gt; dict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}
&gt;&gt;&gt; str(dict)
"{'Name': 'Runoob', 'Class': 'First', 'Age': 7}"
</pre>
</td>
</tr>
<tr><td>3</td><td>type(variable)<br>返回输入的变量类型，如果变量是字典就返回字典类型。</td>
<td>
<pre>
&gt;&gt;&gt; dict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}
&gt;&gt;&gt; type(dict)
&lt;class 'dict'&gt;
</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th width="5%">序号</th><th width="25%">函数及描述</th><th>实例</th></tr>

<tr><td>1</td><td>len(dict)<br>计算字典元素个数，即键的总数。</td>
<td>
<pre>
&gt;&gt;&gt; dict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}
&gt;&gt;&gt; len(dict)
3
</pre>
</td>
</tr>
<tr><td>2</td><td>str(dict)<br>输出字典，以可打印的字符串表示。</td>
<td>
<pre>
&gt;&gt;&gt; dict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}
&gt;&gt;&gt; str(dict)
"{'Name': 'Runoob', 'Class': 'First', 'Age': 7}"
</pre>
</td>
</tr>
<tr><td>3</td><td>type(variable)<br>返回输入的变量类型，如果变量是字典就返回字典类型。</td>
<td>
<pre>
&gt;&gt;&gt; dict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}
&gt;&gt;&gt; type(dict)
&lt;class 'dict'&gt;
</pre>
</td>
</tr>
</tbody></table></div>') where ID=6581;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:35%">函数及描述</th><th>实例</th></tr>
<tr><td>1</td><td>time.altzone<br>返回格林威治西部的夏令时地区的偏移秒数。如果该地区在格林威治东部会返回负值（如西欧，包括英国）。对夏令时启用地区才能使用。</td>
<td>
<p>以下实例展示了 altzone()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print ("time.altzone %d " % time.altzone)
time.altzone -28800 
</pre>
</td>
</tr>
<tr><td>2</td><td>time.asctime([tupletime])<br>接受时间元组并返回一个可读的形式为"Tue Dec 11 18:07:14 2008"（2008年12月11日 周二18时07分14秒）的24个字符的字符串。</td>
<td>
<p>以下实例展示了 asctime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; t = time.localtime()
&gt;&gt;&gt; print ("time.asctime(t): %s " % time.asctime(t))
time.asctime(t): Thu Apr  7 10:36:20 2016 
</pre>
</td>

</tr>
<tr><td>3</td><td><a href="python3-att-time-clock.html" target="_blank" rel="noopener noreferrer">time.clock()</a><br>用以浮点数计算的秒数返回当前的CPU时间。用来衡量不同程序的耗时，比time.time()更有用。</td><td><p><a href="python3-att-time-clock.html" target="_blank" rel="noopener noreferrer">实例</a></p>
<p>由于该方法依赖操作系统，在 Python 3.3 以后不被推荐，而在 3.8 版本中被移除，需使用下列两个函数替代。</p>
<pre>time.perf_counter()  # 返回系统运行时间
time.process_time()  # 返回进程运行时间</pre>

</td></tr>
<tr><td>4</td><td>time.ctime([secs])<br>作用相当于asctime(localtime(secs))，未给参数相当于asctime()</td>
<td>
<p>以下实例展示了 ctime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print ("time.ctime() : %s" % time.ctime())
time.ctime() : Thu Apr  7 10:51:58 2016
</pre>
</td>


</tr>
<tr><td>5</td><td>time.gmtime([secs])<br>接收时间戳（1970纪元后经过的浮点秒数）并返回格林威治天文时间下的时间元组t。注：t.tm_isdst始终为0</td>
<td>
<p>以下实例展示了 gmtime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print ("gmtime :", time.gmtime(1455508609.34375))
gmtime : time.struct_time(tm_year=2016, tm_mon=2, tm_mday=15, tm_hour=3, tm_min=56, tm_sec=49, tm_wday=0, tm_yday=46, tm_isdst=0)
</pre>
</td>
</tr>
<tr><td>6</td><td>time.localtime([secs]<br>接收时间戳（1970纪元后经过的浮点秒数）并返回当地时间下的时间元组t（t.tm_isdst可取0或1，取决于当地当时是不是夏令时）。</td>
<td>
<p>以下实例展示了 localtime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print ("localtime(): ", time.localtime(1455508609.34375))
localtime():  time.struct_time(tm_year=2016, tm_mon=2, tm_mday=15, tm_hour=11, tm_min=56, tm_sec=49, tm_wday=0, tm_yday=46, tm_isdst=0)
</pre>
</td>
</tr>
<tr><td>7</td><td><a href="python3-att-time-mktime.html" target="_blank" rel="noopener noreferrer">time.mktime(tupletime)</a><br>接受时间元组并返回时间戳（1970纪元后经过的浮点秒数）。</td><td><a href="python3-att-time-mktime.html" target="_blank" rel="noopener noreferrer">实例</a></td></tr>
<tr><td>8</td><td>time.sleep(secs)<br>推迟调用线程的运行，secs指秒数。</td>
<td>
<p>以下实例展示了 sleep()函数的使用方法：</p>
<pre>
#!/usr/bin/python3
import time

print ("Start : %s" % time.ctime())
time.sleep( 5 )
print ("End : %s" % time.ctime())
</pre>
</td>

</tr>
<tr><td>9</td><td>time.strftime(fmt[,tupletime])<br>接收以时间元组，并返回以可读字符串表示的当地时间，格式由fmt决定。</td>
<td>
<p>以下实例展示了 strftime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
2016-04-07 11:18:05
</pre>
</td>
</tr>
<tr><td>10</td><td>time.strptime(str,fmt='%a %b %d %H:%M:%S %Y')<br>根据fmt的格式把一个时间字符串解析为时间元组。</td>
<td>
<p>以下实例展示了 strptime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; struct_time = time.strptime("30 Nov 00", "%d %b %y")
&gt;&gt;&gt; print ("返回元组: ", struct_time)
返回元组:  time.struct_time(tm_year=2000, tm_mon=11, tm_mday=30, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=3, tm_yday=335, tm_isdst=-1)
</pre>
</td>
</tr>
<tr><td>11</td><td>time.time( )<br>返回当前时间的时间戳（1970纪元后经过的浮点秒数）。</td>

<td>
<p>以下实例展示了 time()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print(time.time())
1459999336.1963577
</pre>
</td>
</tr>
<tr><td>12</td><td><a href="python3-att-time-tzset.html" target="_blank" rel="noopener noreferrer">time.tzset()</a><br>根据环境变量TZ重新初始化时间相关设置。 </td><td><a href="python3-att-time-tzset.html" target="_blank" rel="noopener noreferrer">实例</a></td></tr>

<tr><td>13</td><td><b>
time.perf_counter()</b><br>

返回计时器的精准时间（系统的运行时间），包含整个系统的睡眠时间。由于返回值的基准点是未定义的，所以，只有连续调用的结果之间的差才是有效的。
</td><td>
<a href="#comment-35499">实例</a>
</td></tr>
<tr><td>14</td><td><b>
time.process_time() </b><br>

返回当前进程执行 CPU 的时间总和，不包含睡眠时间。由于返回值的基准点是未定义的，所以，只有连续调用的结果之间的差才是有效的。
</td><td>
&nbsp;
</td></tr>

</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:35%">函数及描述</th><th>实例</th></tr>
<tr><td>1</td><td>time.altzone<br>返回格林威治西部的夏令时地区的偏移秒数。如果该地区在格林威治东部会返回负值（如西欧，包括英国）。对夏令时启用地区才能使用。</td>
<td>
<p>以下实例展示了 altzone()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print ("time.altzone %d " % time.altzone)
time.altzone -28800 
</pre>
</td>
</tr>
<tr><td>2</td><td>time.asctime([tupletime])<br>接受时间元组并返回一个可读的形式为"Tue Dec 11 18:07:14 2008"（2008年12月11日 周二18时07分14秒）的24个字符的字符串。</td>
<td>
<p>以下实例展示了 asctime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; t = time.localtime()
&gt;&gt;&gt; print ("time.asctime(t): %s " % time.asctime(t))
time.asctime(t): Thu Apr  7 10:36:20 2016 
</pre>
</td>

</tr>
<tr><td>3</td><td><a href="python3-att-time-clock.html" target="_blank" rel="noopener noreferrer">time.clock()</a><br>用以浮点数计算的秒数返回当前的CPU时间。用来衡量不同程序的耗时，比time.time()更有用。</td><td><p><a href="python3-att-time-clock.html" target="_blank" rel="noopener noreferrer">实例</a></p>
<p>由于该方法依赖操作系统，在 Python 3.3 以后不被推荐，而在 3.8 版本中被移除，需使用下列两个函数替代。</p>
<pre>time.perf_counter()  # 返回系统运行时间
time.process_time()  # 返回进程运行时间</pre>

</td></tr>
<tr><td>4</td><td>time.ctime([secs])<br>作用相当于asctime(localtime(secs))，未给参数相当于asctime()</td>
<td>
<p>以下实例展示了 ctime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print ("time.ctime() : %s" % time.ctime())
time.ctime() : Thu Apr  7 10:51:58 2016
</pre>
</td>


</tr>
<tr><td>5</td><td>time.gmtime([secs])<br>接收时间戳（1970纪元后经过的浮点秒数）并返回格林威治天文时间下的时间元组t。注：t.tm_isdst始终为0</td>
<td>
<p>以下实例展示了 gmtime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print ("gmtime :", time.gmtime(1455508609.34375))
gmtime : time.struct_time(tm_year=2016, tm_mon=2, tm_mday=15, tm_hour=3, tm_min=56, tm_sec=49, tm_wday=0, tm_yday=46, tm_isdst=0)
</pre>
</td>
</tr>
<tr><td>6</td><td>time.localtime([secs]<br>接收时间戳（1970纪元后经过的浮点秒数）并返回当地时间下的时间元组t（t.tm_isdst可取0或1，取决于当地当时是不是夏令时）。</td>
<td>
<p>以下实例展示了 localtime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print ("localtime(): ", time.localtime(1455508609.34375))
localtime():  time.struct_time(tm_year=2016, tm_mon=2, tm_mday=15, tm_hour=11, tm_min=56, tm_sec=49, tm_wday=0, tm_yday=46, tm_isdst=0)
</pre>
</td>
</tr>
<tr><td>7</td><td><a href="python3-att-time-mktime.html" target="_blank" rel="noopener noreferrer">time.mktime(tupletime)</a><br>接受时间元组并返回时间戳（1970纪元后经过的浮点秒数）。</td><td><a href="python3-att-time-mktime.html" target="_blank" rel="noopener noreferrer">实例</a></td></tr>
<tr><td>8</td><td>time.sleep(secs)<br>推迟调用线程的运行，secs指秒数。</td>
<td>
<p>以下实例展示了 sleep()函数的使用方法：</p>
<pre>
#!/usr/bin/python3
import time

print ("Start : %s" % time.ctime())
time.sleep( 5 )
print ("End : %s" % time.ctime())
</pre>
</td>

</tr>
<tr><td>9</td><td>time.strftime(fmt[,tupletime])<br>接收以时间元组，并返回以可读字符串表示的当地时间，格式由fmt决定。</td>
<td>
<p>以下实例展示了 strftime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
2016-04-07 11:18:05
</pre>
</td>
</tr>
<tr><td>10</td><td>time.strptime(str,fmt='%a %b %d %H:%M:%S %Y')<br>根据fmt的格式把一个时间字符串解析为时间元组。</td>
<td>
<p>以下实例展示了 strptime()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; struct_time = time.strptime("30 Nov 00", "%d %b %y")
&gt;&gt;&gt; print ("返回元组: ", struct_time)
返回元组:  time.struct_time(tm_year=2000, tm_mon=11, tm_mday=30, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=3, tm_yday=335, tm_isdst=-1)
</pre>
</td>
</tr>
<tr><td>11</td><td>time.time( )<br>返回当前时间的时间戳（1970纪元后经过的浮点秒数）。</td>

<td>
<p>以下实例展示了 time()函数的使用方法：</p>
<pre>
&gt;&gt;&gt; import time
&gt;&gt;&gt; print(time.time())
1459999336.1963577
</pre>
</td>
</tr>
<tr><td>12</td><td><a href="python3-att-time-tzset.html" target="_blank" rel="noopener noreferrer">time.tzset()</a><br>根据环境变量TZ重新初始化时间相关设置。 </td><td><a href="python3-att-time-tzset.html" target="_blank" rel="noopener noreferrer">实例</a></td></tr>

<tr><td>13</td><td><b>
time.perf_counter()</b><br>

返回计时器的精准时间（系统的运行时间），包含整个系统的睡眠时间。由于返回值的基准点是未定义的，所以，只有连续调用的结果之间的差才是有效的。
</td><td>
<a href="#comment-35499">实例</a>
</td></tr>
<tr><td>14</td><td><b>
time.process_time() </b><br>

返回当前进程执行 CPU 的时间总和，不包含睡眠时间。由于返回值的基准点是未定义的，所以，只有连续调用的结果之间的差才是有效的。
</td><td>
&nbsp;
</td></tr>

</tbody></table></div>') where ID=6677;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">函数及描述</th></tr>
<tr><td>1</td><td><b>calendar.calendar(year,w=2,l=1,c=6)</b><br>返回一个多行字符串格式的year年年历，3个月一行，间隔距离为c。 每日宽度间隔为w字符。每行长度为21* W+18+2* C。l是每星期行数。</td></tr>
<tr><td>2</td><td><b>calendar.firstweekday( )</b><br>返回当前每周起始日期的设置。默认情况下，首次载入caendar模块时返回0，即星期一。</td></tr>
<tr><td>3</td><td><b>calendar.isleap(year)</b><br><p> 是闰年返回 True，否则为 false。</p>
<pre>&gt;&gt;&gt; import calendar
&gt;&gt;&gt; print(calendar.isleap(2000))
True
&gt;&gt;&gt; print(calendar.isleap(1900))
False</pre>
 


</td></tr>
<tr><td>4</td><td><b>calendar.leapdays(y1,y2)</b><br>返回在Y1，Y2两年之间的闰年总数。</td></tr>
<tr><td>5</td><td><b>calendar.month(year,month,w=2,l=1)</b><br>返回一个多行字符串格式的year年month月日历，两行标题，一周一行。每日宽度间隔为w字符。每行的长度为7* w+6。l是每星期的行数。</td></tr>
<tr><td>6</td><td><b>calendar.monthcalendar(year,month)</b><br>返回一个整数的单层嵌套列表。每个子列表装载代表一个星期的整数。Year年month月外的日期都设为0;范围内的日子都由该月第几日表示，从1开始。</td></tr>
<tr><td>7</td><td><b>calendar.monthrange(year,month)</b><br><p>返回两个整数。第一个是该月的星期几，第二个是该月有几天。星期几是从0（星期一）到 6（星期日）。</p>
<pre>&gt;&gt;&gt; import calendar
&gt;&gt;&gt; calendar.monthrange(2014, 11)
(5, 30)</pre>
<p>(5, 30)解释：5 表示 2014 年 11 月份的第一天是周六，30 表示 2014 年 11 月份总共有 30 天。</p>
</td></tr>
<tr><td>8</td><td><b>calendar.prcal(year,w=2,l=1,c=6)</b><br>相当于 print calendar.calendar(year,w,l,c).</td></tr>
<tr><td>9</td><td><b>calendar.prmonth(year,month,w=2,l=1)</b><br>相当于 print calendar.calendar（year，w，l，c）。</td></tr>
<tr><td>10</td><td><b>calendar.setfirstweekday(weekday)</b><br>设置每周的起始日期码。0（星期一）到6（星期日）。</td></tr>
<tr><td>11</td><td><b>calendar.timegm(tupletime)</b><br>和time.gmtime相反：接受一个时间元组形式，返回该时刻的时间戳（1970纪元后经过的浮点秒数）。</td></tr>
<tr><td>12</td><td><b>calendar.weekday(year,month,day)</b><br>返回给定日期的日期码。0（星期一）到6（星期日）。月份为 1（一月） 到 12（12月）。</td></tr>



</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">函数及描述</th></tr>
<tr><td>1</td><td><b>calendar.calendar(year,w=2,l=1,c=6)</b><br>返回一个多行字符串格式的year年年历，3个月一行，间隔距离为c。 每日宽度间隔为w字符。每行长度为21* W+18+2* C。l是每星期行数。</td></tr>
<tr><td>2</td><td><b>calendar.firstweekday( )</b><br>返回当前每周起始日期的设置。默认情况下，首次载入caendar模块时返回0，即星期一。</td></tr>
<tr><td>3</td><td><b>calendar.isleap(year)</b><br><p> 是闰年返回 True，否则为 false。</p>
<pre>&gt;&gt;&gt; import calendar
&gt;&gt;&gt; print(calendar.isleap(2000))
True
&gt;&gt;&gt; print(calendar.isleap(1900))
False</pre>
 


</td></tr>
<tr><td>4</td><td><b>calendar.leapdays(y1,y2)</b><br>返回在Y1，Y2两年之间的闰年总数。</td></tr>
<tr><td>5</td><td><b>calendar.month(year,month,w=2,l=1)</b><br>返回一个多行字符串格式的year年month月日历，两行标题，一周一行。每日宽度间隔为w字符。每行的长度为7* w+6。l是每星期的行数。</td></tr>
<tr><td>6</td><td><b>calendar.monthcalendar(year,month)</b><br>返回一个整数的单层嵌套列表。每个子列表装载代表一个星期的整数。Year年month月外的日期都设为0;范围内的日子都由该月第几日表示，从1开始。</td></tr>
<tr><td>7</td><td><b>calendar.monthrange(year,month)</b><br><p>返回两个整数。第一个是该月的星期几，第二个是该月有几天。星期几是从0（星期一）到 6（星期日）。</p>
<pre>&gt;&gt;&gt; import calendar
&gt;&gt;&gt; calendar.monthrange(2014, 11)
(5, 30)</pre>
<p>(5, 30)解释：5 表示 2014 年 11 月份的第一天是周六，30 表示 2014 年 11 月份总共有 30 天。</p>
</td></tr>
<tr><td>8</td><td><b>calendar.prcal(year,w=2,l=1,c=6)</b><br>相当于 print calendar.calendar(year,w,l,c).</td></tr>
<tr><td>9</td><td><b>calendar.prmonth(year,month,w=2,l=1)</b><br>相当于 print calendar.calendar（year，w，l，c）。</td></tr>
<tr><td>10</td><td><b>calendar.setfirstweekday(weekday)</b><br>设置每周的起始日期码。0（星期一）到6（星期日）。</td></tr>
<tr><td>11</td><td><b>calendar.timegm(tupletime)</b><br>和time.gmtime相反：接受一个时间元组形式，返回该时刻的时间戳（1970纪元后经过的浮点秒数）。</td></tr>
<tr><td>12</td><td><b>calendar.weekday(year,month,day)</b><br>返回给定日期的日期码。0（星期一）到6（星期日）。月份为 1（一月） 到 12（12月）。</td></tr>



</tbody></table></div>') where ID=6677;
update wp_posts set post_content=replace(post_content,'<table class="reference notranslate">
<tr><th style="width:10%">运算符</th><th style="width:55%;">描述</th><th>实例</th></tr>
<tr><td>&amp;</td><td><p>按位与操作，按二进制位进行"与"运算。运算规则：</p>
<pre>0&amp;0=0;   
0&amp;1=0;    
1&amp;0=0;     
1&amp;1=1;</pre></td><td> (A &amp; B) 将得到 12，即为 0000 1100</td></tr>
<tr><td>|</td><td><p>按位或运算符，按二进制位进行"或"运算。运算规则：</p>
<pre>0|0=0;   
0|1=1;   
1|0=1;    
1|1=1;</pre>
</td><td> (A | B) 将得到 61，即为 0011 1101</td></tr>
<tr><td>^</td><td><p>异或运算符，按二进制位进行"异或"运算。运算规则：</p>
<pre>0^0=0;   
0^1=1;   
1^0=1;  
1^1=0;</pre>

</td><td> (A ^ B) 将得到 49，即为 0011 0001</td></tr>
<tr><td>~</td><td><p>取反运算符，按二进制位进行"取反"运算。运算规则：</p>
<pre>~1=0;   
~0=1;</pre></td><td> (~A ) 将得到 -61，即为 1100 0011，一个有符号二进制数的补码形式。</td></tr>
<tr><td>&lt;&lt;</td><td>二进制左移运算符。将一个运算对象的各二进制位全部左移若干位（左边的二进制位丢弃，右边补0）。</td><td> A &lt;&lt; 2 将得到 240，即为 1111 0000</td></tr>
<tr><td>&gt;&gt;</td><td>二进制右移运算符。将一个数的各二进制位全部右移若干位，正数左补0，负数左补1，右边丢弃。</td><td> A &gt;&gt; 2 将得到 15，即为 0000 1111</td></tr>
</table>','<div class="w3-responsive"><table class="reference notranslate">
<tr><th style="width:10%">运算符</th><th style="width:55%;">描述</th><th>实例</th></tr>
<tr><td>&amp;</td><td><p>按位与操作，按二进制位进行"与"运算。运算规则：</p>
<pre>0&amp;0=0;   
0&amp;1=0;    
1&amp;0=0;     
1&amp;1=1;</pre></td><td> (A &amp; B) 将得到 12，即为 0000 1100</td></tr>
<tr><td>|</td><td><p>按位或运算符，按二进制位进行"或"运算。运算规则：</p>
<pre>0|0=0;   
0|1=1;   
1|0=1;    
1|1=1;</pre>
</td><td> (A | B) 将得到 61，即为 0011 1101</td></tr>
<tr><td>^</td><td><p>异或运算符，按二进制位进行"异或"运算。运算规则：</p>
<pre>0^0=0;   
0^1=1;   
1^0=1;  
1^1=0;</pre>

</td><td> (A ^ B) 将得到 49，即为 0011 0001</td></tr>
<tr><td>~</td><td><p>取反运算符，按二进制位进行"取反"运算。运算规则：</p>
<pre>~1=0;   
~0=1;</pre></td><td> (~A ) 将得到 -61，即为 1100 0011，一个有符号二进制数的补码形式。</td></tr>
<tr><td>&lt;&lt;</td><td>二进制左移运算符。将一个运算对象的各二进制位全部左移若干位（左边的二进制位丢弃，右边补0）。</td><td> A &lt;&lt; 2 将得到 240，即为 1111 0000</td></tr>
<tr><td>&gt;&gt;</td><td>二进制右移运算符。将一个数的各二进制位全部右移若干位，正数左补0，负数左补1，右边丢弃。</td><td> A &gt;&gt; 2 将得到 15，即为 0000 1111</td></tr>
</table></div>') where ID=7335;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tr><th>序号</th><th>配置项</th><th>说明</th></tr>
<tr><td>
1</td>
<td><pre>daemonize no</pre></td>
<td>Redis 默认不是以守护进程的方式运行，可以通过该配置项修改，使用 yes 启用守护进程（Windows 不支持守护线程的配置为 no ）
</td></tr>
    
<tr><td>
2</td> 
<td><pre>pidfile /var/run/redis.pid</pre></td> 
<td>
当 Redis 以守护进程方式运行时，Redis 默认会把 pid 写入 /var/run/redis.pid 文件，可以通过 pidfile 指定

</td></tr>
    
<tr><td>

3</td> 
<td>
<pre>port 6379</pre>
</td> 
<td>
指定 Redis 监听端口，默认端口为 6379，作者在自己的一篇博文中解释了为什么选用 6379 作为默认端口，因为 6379 在手机按键上 MERZ 对应的号码，而 MERZ 取自意大利歌女 Alessia Merz 的名字

 </td></tr>
    
<tr><td>

4</td> 
<td><pre>bind 127.0.0.1</pre>
</td> 
<td>绑定的主机地址
 </td></tr>
    
<tr><td>
    

5</td> 
<td> <pre>timeout 300</pre></td> 
<td>
当客户端闲置多长时间后关闭连接，如果指定为 0，表示关闭该功能
 </td></tr>
    
<tr><td>
    
   

6</td> 
<td> <pre>loglevel notice</pre></td> 
<td>
指定日志记录级别，Redis 总共支持四个级别：debug、verbose、notice、warning，默认为 notice
 </td></tr>
    
<tr><td>
    

7</td> 
<td><pre>logfile stdout</pre></td> 
<td>日志记录方式，默认为标准输出，如果配置 Redis 为守护进程方式运行，而这里又配置为日志记录方式为标准输出，则日志将会发送给 /dev/null
 </td></tr>
    
<tr><td>
    

8</td> 
<td> <pre>databases 16</pre></td> 
<td>设置数据库的数量，默认数据库为0，可以使用SELECT <dbid>命令在连接上指定数据库id
 </td></tr>
    
<tr><td>
    
    

9</td> 
<td>
<pre>save &lt;seconds&gt; &lt;changes&gt;</pre>

<p>Redis 默认配置文件中提供了三个条件：</p>

<p><strong>save 900 1</strong></p>
<p><strong>save 300 10</strong></p>
<p><strong>save 60 10000</strong></p>

<p>分别表示 900 秒（15 分钟）内有 1 个更改，300 秒（5 分钟）内有 10 个更改以及 60 秒内有 10000 个更改。</p>
</td> 
<td>

指定在多长时间内，有多少次更新操作，就将数据同步到数据文件，可以多个条件配合
 </td></tr>
    
<tr><td>
    
 

10</td> 
<td><pre>rdbcompression yes</pre></td> 
<td>
指定存储至本地数据库时是否压缩数据，默认为 yes，Redis 采用 LZF 压缩，如果为了节省 CPU 时间，可以关闭该选项，但会导致数据库文件变的巨大
 </td></tr>
    
<tr><td>
    
    

11</td> 
<td> <pre>dbfilename dump.rdb</pre></td> 
<td>指定本地数据库文件名，默认值为 dump.rdb
 </td></tr>
    
<tr><td>
    

12</td> 
<td> <pre>dir ./</pre></td> 
<td>指定本地数据库存放目录
 </td></tr>
    
<tr><td>
    

13</td> 
<td> <pre>slaveof &lt;masterip&gt; &lt;masterport&gt;</pre> </td> 
<td>设置当本机为 slav 服务时，设置 master 服务的 IP 地址及端口，在 Redis 启动时，它会自动从 master 进行数据同步
</td></tr>
    
<tr><td>
    
    

14</td> 
<td>  <pre>masterauth &lt;master-password&gt;</pre></td> 
<td>当 master 服务设置了密码保护时，slav 服务连接 master 的密码

    
</td></tr>
    
<tr><td>
    
15</td> 
<td> <pre>requirepass foobared</pre></td> 
<td>设置 Redis 连接密码，如果配置了连接密码，客户端在连接 Redis 时需要通过 AUTH &lt;password&gt; 命令提供密码，默认关闭

 </td></tr>
    
<tr><td>
    

16</td> 
<td> <pre> maxclients 128</pre></td> 
<td>设置同一时间最大客户端连接数，默认无限制，Redis 可以同时打开的客户端连接数为 Redis 进程可以打开的最大文件描述符数，如果设置 maxclients 0，表示不作限制。当客户端连接数到达限制时，Redis 会关闭新的连接并向客户端返回 max number of clients reached 错误信息
</td></tr>
    
<tr><td>
    
   

17</td> 
<td> <pre>maxmemory &lt;bytes&gt;</pre></td> 
<td>指定 Redis 最大内存限制，Redis 在启动时会把数据加载到内存中，达到最大内存后，Redis 会先尝试清除已到期或即将到期的 Key，当此方法处理 后，仍然到达最大内存设置，将无法再进行写入操作，但仍然可以进行读取操作。Redis 新的 vm 机制，会把 Key 存放内存，Value 会存放在 swap 区
</td></tr>
    
<tr><td>
    

18</td> 
<td><pre>appendonly no</pre></td> 
<td>
指定是否在每次更新操作后进行日志记录，Redis 在默认情况下是异步的把数据写入磁盘，如果不开启，可能会在断电时导致一段时间内的数据丢失。因为  redis 本身同步数据文件是按上面 save 条件来同步的，所以有的数据会在一段时间内只存在于内存中。默认为 no
</td></tr>
    
<tr><td>
    
19</td> 
<td><pre>appendfilename appendonly.aof</pre></td> 
<td>指定更新日志文件名，默认为 appendonly.aof
</td></tr>
    
<tr><td>
     

20</td> 
<td><pre>appendfsync everysec</pre></td> 
<td>
<p>指定更新日志条件，共有 3 个可选值： </p>
<ul><li>
<strong>no</strong>：表示等操作系统进行数据缓存同步到磁盘（快） </li><li>
<strong>always</strong>：表示每次更新操作后手动调用 fsync() 将数据写到磁盘（慢，安全） </li><li>
<strong>everysec</strong>：表示每秒同步一次（折中，默认值）</li></ul>
</td></tr>
    
<tr><td>
    

 

21</td> 
<td><pre>vm-enabled no</pre></td> 
<td>
指定是否启用虚拟内存机制，默认值为 no，简单的介绍一下，VM 机制将数据分页存放，由 Redis 将访问量较少的页即冷数据 swap 到磁盘上，访问多的页面由磁盘自动换出到内存中（在后面的文章我会仔细分析 Redis 的 VM 机制）
</td></tr>
    
<tr><td>
     
22</td> 
<td><pre>vm-swap-file /tmp/redis.swap</pre></td> 
<td>
虚拟内存文件路径，默认值为 /tmp/redis.swap，不可多个 Redis 实例共享
</td></tr>
    
<tr><td>
     
23</td> 
<td> <pre>vm-max-memory 0</pre></td> 
<td>将所有大于 vm-max-memory 的数据存入虚拟内存，无论 vm-max-memory 设置多小，所有索引数据都是内存存储的(Redis 的索引数据 就是 keys)，也就是说，当 vm-max-memory 设置为 0 的时候，其实是所有 value 都存在于磁盘。默认值为 0
</td></tr>
    
<tr><td>

24</td> 
<td><pre>vm-page-size 32</pre> </td> 
<td>Redis swap 文件分成了很多的 page，一个对象可以保存在多个 page 上面，但一个 page 上不能被多个对象共享，vm-page-size 是要根据存储的 数据大小来设定的，作者建议如果存储很多小对象，page 大小最好设置为 32 或者 64bytes；如果存储很大大对象，则可以使用更大的 page，如果不确定，就使用默认值
</td></tr>
    
<tr><td>
     

25</td> 
<td><pre>vm-pages 134217728</pre></td> 
<td>设置 swap 文件中的 page 数量，由于页表（一种表示页面空闲或使用的 bitmap）是在放在内存中的，，在磁盘上每 8 个 pages 将消耗 1byte 的内存。
</td></tr>
    
<tr><td>
     
     

26</td> 
<td><pre>vm-max-threads 4</pre>
</td> 
<td>
设置访问swap文件的线程数,最好不要超过机器的核数,如果设置为0,那么所有对swap文件的操作都是串行的，可能会造成比较长时间的延迟。默认值为4
</td></tr>
    
<tr><td>
     

27</td> 
<td><pre>glueoutputbuf yes</pre>
</td> 
<td>

 设置在向客户端应答时，是否把较小的包合并为一个包发送，默认为开启
</td></tr>
    
<tr><td>
    

28</td> 
<td>
<pre>hash-max-zipmap-entries 64
hash-max-zipmap-value 512</pre>
</td> 
<td>指定在超过一定的数量或者最大的元素超过某一临界值时，采用一种特殊的哈希算法
</td></tr>
    
<tr><td>
    

29</td> 
<td><pre>activerehashing yes</pre>
</td> 
<td>
指定是否激活重置哈希，默认为开启（后面在介绍 Redis 的哈希算法时具体介绍）
</td></tr>
    
<tr><td>
    

30</td> 
<td>
<pre>include /path/to/local.conf</pre>
</td> 
<td>
指定包含其它的配置文件，可以在同一主机上多个Redis实例之间使用同一份配置文件，而同时各个实例又拥有自己的特定配置文件</td></tr></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tr><th>序号</th><th>配置项</th><th>说明</th></tr>
<tr><td>
1</td>
<td><pre>daemonize no</pre></td>
<td>Redis 默认不是以守护进程的方式运行，可以通过该配置项修改，使用 yes 启用守护进程（Windows 不支持守护线程的配置为 no ）
</td></tr>
    
<tr><td>
2</td> 
<td><pre>pidfile /var/run/redis.pid</pre></td> 
<td>
当 Redis 以守护进程方式运行时，Redis 默认会把 pid 写入 /var/run/redis.pid 文件，可以通过 pidfile 指定

</td></tr>
    
<tr><td>

3</td> 
<td>
<pre>port 6379</pre>
</td> 
<td>
指定 Redis 监听端口，默认端口为 6379，作者在自己的一篇博文中解释了为什么选用 6379 作为默认端口，因为 6379 在手机按键上 MERZ 对应的号码，而 MERZ 取自意大利歌女 Alessia Merz 的名字

 </td></tr>
    
<tr><td>

4</td> 
<td><pre>bind 127.0.0.1</pre>
</td> 
<td>绑定的主机地址
 </td></tr>
    
<tr><td>
    

5</td> 
<td> <pre>timeout 300</pre></td> 
<td>
当客户端闲置多长时间后关闭连接，如果指定为 0，表示关闭该功能
 </td></tr>
    
<tr><td>
    
   

6</td> 
<td> <pre>loglevel notice</pre></td> 
<td>
指定日志记录级别，Redis 总共支持四个级别：debug、verbose、notice、warning，默认为 notice
 </td></tr>
    
<tr><td>
    

7</td> 
<td><pre>logfile stdout</pre></td> 
<td>日志记录方式，默认为标准输出，如果配置 Redis 为守护进程方式运行，而这里又配置为日志记录方式为标准输出，则日志将会发送给 /dev/null
 </td></tr>
    
<tr><td>
    

8</td> 
<td> <pre>databases 16</pre></td> 
<td>设置数据库的数量，默认数据库为0，可以使用SELECT <dbid>命令在连接上指定数据库id
 </td></tr>
    
<tr><td>
    
    

9</td> 
<td>
<pre>save &lt;seconds&gt; &lt;changes&gt;</pre>

<p>Redis 默认配置文件中提供了三个条件：</p>

<p><strong>save 900 1</strong></p>
<p><strong>save 300 10</strong></p>
<p><strong>save 60 10000</strong></p>

<p>分别表示 900 秒（15 分钟）内有 1 个更改，300 秒（5 分钟）内有 10 个更改以及 60 秒内有 10000 个更改。</p>
</td> 
<td>

指定在多长时间内，有多少次更新操作，就将数据同步到数据文件，可以多个条件配合
 </td></tr>
    
<tr><td>
    
 

10</td> 
<td><pre>rdbcompression yes</pre></td> 
<td>
指定存储至本地数据库时是否压缩数据，默认为 yes，Redis 采用 LZF 压缩，如果为了节省 CPU 时间，可以关闭该选项，但会导致数据库文件变的巨大
 </td></tr>
    
<tr><td>
    
    

11</td> 
<td> <pre>dbfilename dump.rdb</pre></td> 
<td>指定本地数据库文件名，默认值为 dump.rdb
 </td></tr>
    
<tr><td>
    

12</td> 
<td> <pre>dir ./</pre></td> 
<td>指定本地数据库存放目录
 </td></tr>
    
<tr><td>
    

13</td> 
<td> <pre>slaveof &lt;masterip&gt; &lt;masterport&gt;</pre> </td> 
<td>设置当本机为 slav 服务时，设置 master 服务的 IP 地址及端口，在 Redis 启动时，它会自动从 master 进行数据同步
</td></tr>
    
<tr><td>
    
    

14</td> 
<td>  <pre>masterauth &lt;master-password&gt;</pre></td> 
<td>当 master 服务设置了密码保护时，slav 服务连接 master 的密码

    
</td></tr>
    
<tr><td>
    
15</td> 
<td> <pre>requirepass foobared</pre></td> 
<td>设置 Redis 连接密码，如果配置了连接密码，客户端在连接 Redis 时需要通过 AUTH &lt;password&gt; 命令提供密码，默认关闭

 </td></tr>
    
<tr><td>
    

16</td> 
<td> <pre> maxclients 128</pre></td> 
<td>设置同一时间最大客户端连接数，默认无限制，Redis 可以同时打开的客户端连接数为 Redis 进程可以打开的最大文件描述符数，如果设置 maxclients 0，表示不作限制。当客户端连接数到达限制时，Redis 会关闭新的连接并向客户端返回 max number of clients reached 错误信息
</td></tr>
    
<tr><td>
    
   

17</td> 
<td> <pre>maxmemory &lt;bytes&gt;</pre></td> 
<td>指定 Redis 最大内存限制，Redis 在启动时会把数据加载到内存中，达到最大内存后，Redis 会先尝试清除已到期或即将到期的 Key，当此方法处理 后，仍然到达最大内存设置，将无法再进行写入操作，但仍然可以进行读取操作。Redis 新的 vm 机制，会把 Key 存放内存，Value 会存放在 swap 区
</td></tr>
    
<tr><td>
    

18</td> 
<td><pre>appendonly no</pre></td> 
<td>
指定是否在每次更新操作后进行日志记录，Redis 在默认情况下是异步的把数据写入磁盘，如果不开启，可能会在断电时导致一段时间内的数据丢失。因为  redis 本身同步数据文件是按上面 save 条件来同步的，所以有的数据会在一段时间内只存在于内存中。默认为 no
</td></tr>
    
<tr><td>
    
19</td> 
<td><pre>appendfilename appendonly.aof</pre></td> 
<td>指定更新日志文件名，默认为 appendonly.aof
</td></tr>
    
<tr><td>
     

20</td> 
<td><pre>appendfsync everysec</pre></td> 
<td>
<p>指定更新日志条件，共有 3 个可选值： </p>
<ul><li>
<strong>no</strong>：表示等操作系统进行数据缓存同步到磁盘（快） </li><li>
<strong>always</strong>：表示每次更新操作后手动调用 fsync() 将数据写到磁盘（慢，安全） </li><li>
<strong>everysec</strong>：表示每秒同步一次（折中，默认值）</li></ul>
</td></tr>
    
<tr><td>
    

 

21</td> 
<td><pre>vm-enabled no</pre></td> 
<td>
指定是否启用虚拟内存机制，默认值为 no，简单的介绍一下，VM 机制将数据分页存放，由 Redis 将访问量较少的页即冷数据 swap 到磁盘上，访问多的页面由磁盘自动换出到内存中（在后面的文章我会仔细分析 Redis 的 VM 机制）
</td></tr>
    
<tr><td>
     
22</td> 
<td><pre>vm-swap-file /tmp/redis.swap</pre></td> 
<td>
虚拟内存文件路径，默认值为 /tmp/redis.swap，不可多个 Redis 实例共享
</td></tr>
    
<tr><td>
     
23</td> 
<td> <pre>vm-max-memory 0</pre></td> 
<td>将所有大于 vm-max-memory 的数据存入虚拟内存，无论 vm-max-memory 设置多小，所有索引数据都是内存存储的(Redis 的索引数据 就是 keys)，也就是说，当 vm-max-memory 设置为 0 的时候，其实是所有 value 都存在于磁盘。默认值为 0
</td></tr>
    
<tr><td>

24</td> 
<td><pre>vm-page-size 32</pre> </td> 
<td>Redis swap 文件分成了很多的 page，一个对象可以保存在多个 page 上面，但一个 page 上不能被多个对象共享，vm-page-size 是要根据存储的 数据大小来设定的，作者建议如果存储很多小对象，page 大小最好设置为 32 或者 64bytes；如果存储很大大对象，则可以使用更大的 page，如果不确定，就使用默认值
</td></tr>
    
<tr><td>
     

25</td> 
<td><pre>vm-pages 134217728</pre></td> 
<td>设置 swap 文件中的 page 数量，由于页表（一种表示页面空闲或使用的 bitmap）是在放在内存中的，，在磁盘上每 8 个 pages 将消耗 1byte 的内存。
</td></tr>
    
<tr><td>
     
     

26</td> 
<td><pre>vm-max-threads 4</pre>
</td> 
<td>
设置访问swap文件的线程数,最好不要超过机器的核数,如果设置为0,那么所有对swap文件的操作都是串行的，可能会造成比较长时间的延迟。默认值为4
</td></tr>
    
<tr><td>
     

27</td> 
<td><pre>glueoutputbuf yes</pre>
</td> 
<td>

 设置在向客户端应答时，是否把较小的包合并为一个包发送，默认为开启
</td></tr>
    
<tr><td>
    

28</td> 
<td>
<pre>hash-max-zipmap-entries 64
hash-max-zipmap-value 512</pre>
</td> 
<td>指定在超过一定的数量或者最大的元素超过某一临界值时，采用一种特殊的哈希算法
</td></tr>
    
<tr><td>
    

29</td> 
<td><pre>activerehashing yes</pre>
</td> 
<td>
指定是否激活重置哈希，默认为开启（后面在介绍 Redis 的哈希算法时具体介绍）
</td></tr>
    
<tr><td>
    

30</td> 
<td>
<pre>include /path/to/local.conf</pre>
</td> 
<td>
指定包含其它的配置文件，可以在同一主机上多个Redis实例之间使用同一份配置文件，而同时各个实例又拥有自己的特定配置文件</td></tr></table></div>') where ID=7723;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<td>
<p><strong>属性</strong></p>
</td>
<td>
<p><strong>描述</strong></p>
</td>
</tr>

<tr>
<td>
<p>path</p>
</td>
<td>
<p>请求页面的全路径,不包括域名—例如, "/hello/"。</p>
</td>
</tr>
<tr>
<td>
<p>method</p>
</td>
<td>
<p>请求中使用的HTTP方法的字符串表示。全大写表示。例如:</p>
<p>if&nbsp;request.method&nbsp;==&nbsp;'GET':<br>&nbsp;&nbsp;&nbsp;&nbsp;do_something()<br>elif&nbsp;request.method&nbsp;==&nbsp;'POST':<br>&nbsp;&nbsp;&nbsp;&nbsp;do_something_else()</p>
</td>
</tr>
<tr>
<td>
<p>GET</p>
</td>
<td>
<p>包含所有HTTP GET参数的类字典对象。参见QueryDict 文档。</p>
</td>
</tr>
<tr>
<td>
<p>POST</p>
</td>
<td>
<p>包含所有HTTP POST参数的类字典对象。参见QueryDict 文档。</p>
<p>服务器收到空的POST请求的情况也是有可能发生的。也就是说，表单form通过HTTP POST方法提交请求，但是表单中可以没有数据。因此，不能使用语句if request.POST来判断是否使用HTTP POST方法；应该使用if request.method == "POST" (参见本表的method属性)。</p>
<p>注意: POST不包括file-upload信息。参见FILES属性。</p>
</td>
</tr>
<tr>
<td>
<p>REQUEST</p>
</td>
<td>
<p>为了方便，该属性是POST和GET属性的集合体，但是有特殊性，先查找POST属性，然后再查找GET属性。借鉴PHP's $_REQUEST。</p>
<p>例如，如果GET = {"name": "john"} 和POST = {"age": '34'},则 REQUEST["name"] 的值是"john", REQUEST["age"]的值是"34".</p>
<p>强烈建议使用GET and POST,因为这两个属性更加显式化，写出的代码也更易理解。</p>
</td>
</tr>
<tr>
<td>
<p>COOKIES</p>
</td>
<td>
<p>包含所有cookies的标准Python字典对象。Keys和values都是字符串。</p>
</td>
</tr>
<tr>
<td>
<p>FILES</p>
</td>
<td>
<p>包含所有上传文件的类字典对象。FILES中的每个Key都是&lt;input type="file" name="" /&gt;标签中name属性的值. FILES中的每个value 同时也是一个标准Python字典对象，包含下面三个Keys:</p>
<ul>
<li>filename: 上传文件名,用Python字符串表示</li>
<li>content-type: 上传文件的Content type</li>
<li>content: 上传文件的原始内容</li>
</ul>
<p>注意：只有在请求方法是POST，并且请求页面中&lt;form&gt;有enctype="multipart/form-data"属性时FILES才拥有数据。否则，FILES 是一个空字典。</p>
</td>
</tr>
<tr>
<td>
<p>META</p>
</td>
<td>
<p>包含所有可用HTTP头部信息的字典。 例如:</p>
<ul>
<li>CONTENT_LENGTH</li>
<li>CONTENT_TYPE</li>
<li>QUERY_STRING: 未解析的原始查询字符串</li>
<li>REMOTE_ADDR: 客户端IP地址</li>
<li>REMOTE_HOST: 客户端主机名</li>
<li>SERVER_NAME: 服务器主机名</li>
<li>SERVER_PORT: 服务器端口</li>
</ul>
<p>META 中这些头加上前缀HTTP_最为Key, 例如:</p>
<ul>
<li>HTTP_ACCEPT_ENCODING</li>
<li>HTTP_ACCEPT_LANGUAGE</li>
<li>HTTP_HOST: 客户发送的HTTP主机头信息</li>
<li>HTTP_REFERER: referring页</li>
<li>HTTP_USER_AGENT: 客户端的user-agent字符串</li>
<li>HTTP_X_BENDER: X-Bender头信息</li>
</ul>
</td>
</tr>
<tr>
<td>
<p>user</p>
</td>
<td>
<p>
是一个django.contrib.auth.models.User 对象，代表当前登录的用户。</p><p>
如果访问用户当前没有登录，user将被初始化为django.contrib.auth.models.AnonymousUser的实例。</p><p>
你可以通过user的is_authenticated()方法来辨别用户是否登录：</p>
</p>
<pre>
if request.user.is_authenticated():
    # Do something for logged-in users.
else:
    # Do something for anonymous users.
</pre>
<p>
只有激活Django中的AuthenticationMiddleware时该属性才可用
</p>
</td>
</tr>
<tr>
<td>
<p>session</p>
</td>
<td>
<p>唯一可读写的属性，代表当前会话的字典对象。只有激活Django中的session支持时该属性才可用。 </p>
</td>
</tr>
<tr>
<td>
<p>raw_post_data</p>
</td>
<td>
<p>原始HTTP POST数据，未解析过。 高级处理时会有用处。</p>
</td>
</tr>

</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<td>
<p><strong>属性</strong></p>
</td>
<td>
<p><strong>描述</strong></p>
</td>
</tr>

<tr>
<td>
<p>path</p>
</td>
<td>
<p>请求页面的全路径,不包括域名—例如, "/hello/"。</p>
</td>
</tr>
<tr>
<td>
<p>method</p>
</td>
<td>
<p>请求中使用的HTTP方法的字符串表示。全大写表示。例如:</p>
<p>if&nbsp;request.method&nbsp;==&nbsp;'GET':<br>&nbsp;&nbsp;&nbsp;&nbsp;do_something()<br>elif&nbsp;request.method&nbsp;==&nbsp;'POST':<br>&nbsp;&nbsp;&nbsp;&nbsp;do_something_else()</p>
</td>
</tr>
<tr>
<td>
<p>GET</p>
</td>
<td>
<p>包含所有HTTP GET参数的类字典对象。参见QueryDict 文档。</p>
</td>
</tr>
<tr>
<td>
<p>POST</p>
</td>
<td>
<p>包含所有HTTP POST参数的类字典对象。参见QueryDict 文档。</p>
<p>服务器收到空的POST请求的情况也是有可能发生的。也就是说，表单form通过HTTP POST方法提交请求，但是表单中可以没有数据。因此，不能使用语句if request.POST来判断是否使用HTTP POST方法；应该使用if request.method == "POST" (参见本表的method属性)。</p>
<p>注意: POST不包括file-upload信息。参见FILES属性。</p>
</td>
</tr>
<tr>
<td>
<p>REQUEST</p>
</td>
<td>
<p>为了方便，该属性是POST和GET属性的集合体，但是有特殊性，先查找POST属性，然后再查找GET属性。借鉴PHP's $_REQUEST。</p>
<p>例如，如果GET = {"name": "john"} 和POST = {"age": '34'},则 REQUEST["name"] 的值是"john", REQUEST["age"]的值是"34".</p>
<p>强烈建议使用GET and POST,因为这两个属性更加显式化，写出的代码也更易理解。</p>
</td>
</tr>
<tr>
<td>
<p>COOKIES</p>
</td>
<td>
<p>包含所有cookies的标准Python字典对象。Keys和values都是字符串。</p>
</td>
</tr>
<tr>
<td>
<p>FILES</p>
</td>
<td>
<p>包含所有上传文件的类字典对象。FILES中的每个Key都是&lt;input type="file" name="" /&gt;标签中name属性的值. FILES中的每个value 同时也是一个标准Python字典对象，包含下面三个Keys:</p>
<ul>
<li>filename: 上传文件名,用Python字符串表示</li>
<li>content-type: 上传文件的Content type</li>
<li>content: 上传文件的原始内容</li>
</ul>
<p>注意：只有在请求方法是POST，并且请求页面中&lt;form&gt;有enctype="multipart/form-data"属性时FILES才拥有数据。否则，FILES 是一个空字典。</p>
</td>
</tr>
<tr>
<td>
<p>META</p>
</td>
<td>
<p>包含所有可用HTTP头部信息的字典。 例如:</p>
<ul>
<li>CONTENT_LENGTH</li>
<li>CONTENT_TYPE</li>
<li>QUERY_STRING: 未解析的原始查询字符串</li>
<li>REMOTE_ADDR: 客户端IP地址</li>
<li>REMOTE_HOST: 客户端主机名</li>
<li>SERVER_NAME: 服务器主机名</li>
<li>SERVER_PORT: 服务器端口</li>
</ul>
<p>META 中这些头加上前缀HTTP_最为Key, 例如:</p>
<ul>
<li>HTTP_ACCEPT_ENCODING</li>
<li>HTTP_ACCEPT_LANGUAGE</li>
<li>HTTP_HOST: 客户发送的HTTP主机头信息</li>
<li>HTTP_REFERER: referring页</li>
<li>HTTP_USER_AGENT: 客户端的user-agent字符串</li>
<li>HTTP_X_BENDER: X-Bender头信息</li>
</ul>
</td>
</tr>
<tr>
<td>
<p>user</p>
</td>
<td>
<p>
是一个django.contrib.auth.models.User 对象，代表当前登录的用户。</p><p>
如果访问用户当前没有登录，user将被初始化为django.contrib.auth.models.AnonymousUser的实例。</p><p>
你可以通过user的is_authenticated()方法来辨别用户是否登录：</p>
</p>
<pre>
if request.user.is_authenticated():
    # Do something for logged-in users.
else:
    # Do something for anonymous users.
</pre>
<p>
只有激活Django中的AuthenticationMiddleware时该属性才可用
</p>
</td>
</tr>
<tr>
<td>
<p>session</p>
</td>
<td>
<p>唯一可读写的属性，代表当前会话的字典对象。只有激活Django中的session支持时该属性才可用。 </p>
</td>
</tr>
<tr>
<td>
<p>raw_post_data</p>
</td>
<td>
<p>原始HTTP POST数据，未解析过。 高级处理时会有用处。</p>
</td>
</tr>

</tbody></table></div>') where ID=7943;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">

<tbody>
<tr>
<th>
<strong>方法</strong>
</th>
<th>
<strong>描述</strong>
</th>
</tr>


<tr>
<td>
<p>__getitem__</p>
</td>
<td>
<p>和标准字典的处理有一点不同，就是，如果Key对应多个Value，__getitem__()返回最后一个value。</p>
</td>
</tr>
<tr>
<td>
<p>__setitem__</p>
</td>
<td>
<p>设置参数指定key的value列表(一个Python list)。注意：它只能在一个mutable QueryDict 对象上被调用(就是通过copy()产生的一个QueryDict对象的拷贝).</p>
</td>
</tr>
<tr>
<td>
<p>get()</p>
</td>
<td>
<p>如果key对应多个value，get()返回最后一个value。</p>
</td>
</tr>
<tr>
<td>
<p>update()</p>
</td>
<td>
<p>参数可以是QueryDict，也可以是标准字典。和标准字典的update方法不同，该方法添加字典 items，而不是替换它们:
</p>
<pre>
&gt;&gt;&gt; q = QueryDict('a=1')

&gt;&gt;&gt; q = q.copy() # to make it mutable

&gt;&gt;&gt; q.update({'a': '2'})

&gt;&gt;&gt; q.getlist('a')

 ['1', '2']

&gt;&gt;&gt; q['a'] # returns the last

['2']
</pre>
</td>
</tr>
<tr>
<td>
<p>items()</p>
</td>
<td>
<p>
和标准字典的items()方法有一点不同,该方法使用单值逻辑的__getitem__():
</p>
<pre>
&gt;&gt;&gt; q = QueryDict('a=1&amp;a=2&amp;a=3')

&gt;&gt;&gt; q.items()

[('a', '3')]
</pre>
</td>
</tr>
<tr>
<td>
<p>values()</p>
</td>
<td>
<p>和标准字典的values()方法有一点不同,该方法使用单值逻辑的__getitem__():</p>
</td>
</tr>

</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">

<tbody>
<tr>
<th>
<strong>方法</strong>
</th>
<th>
<strong>描述</strong>
</th>
</tr>


<tr>
<td>
<p>__getitem__</p>
</td>
<td>
<p>和标准字典的处理有一点不同，就是，如果Key对应多个Value，__getitem__()返回最后一个value。</p>
</td>
</tr>
<tr>
<td>
<p>__setitem__</p>
</td>
<td>
<p>设置参数指定key的value列表(一个Python list)。注意：它只能在一个mutable QueryDict 对象上被调用(就是通过copy()产生的一个QueryDict对象的拷贝).</p>
</td>
</tr>
<tr>
<td>
<p>get()</p>
</td>
<td>
<p>如果key对应多个value，get()返回最后一个value。</p>
</td>
</tr>
<tr>
<td>
<p>update()</p>
</td>
<td>
<p>参数可以是QueryDict，也可以是标准字典。和标准字典的update方法不同，该方法添加字典 items，而不是替换它们:
</p>
<pre>
&gt;&gt;&gt; q = QueryDict('a=1')

&gt;&gt;&gt; q = q.copy() # to make it mutable

&gt;&gt;&gt; q.update({'a': '2'})

&gt;&gt;&gt; q.getlist('a')

 ['1', '2']

&gt;&gt;&gt; q['a'] # returns the last

['2']
</pre>
</td>
</tr>
<tr>
<td>
<p>items()</p>
</td>
<td>
<p>
和标准字典的items()方法有一点不同,该方法使用单值逻辑的__getitem__():
</p>
<pre>
&gt;&gt;&gt; q = QueryDict('a=1&amp;a=2&amp;a=3')

&gt;&gt;&gt; q.items()

[('a', '3')]
</pre>
</td>
</tr>
<tr>
<td>
<p>values()</p>
</td>
<td>
<p>和标准字典的values()方法有一点不同,该方法使用单值逻辑的__getitem__():</p>
</td>
</tr>

</tbody></table></div>') where ID=7943;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">

<tbody>
<tr>
<th>
<strong>方法</strong>
</th>
<th>
<strong>描述</strong>
</th>
</tr>


<tr>
<td>
<p>copy()</p>
</td>
<td>
<p>返回对象的拷贝，内部实现是用Python标准库的copy.deepcopy()。该拷贝是mutable(可更改的) — 就是说，可以更改该拷贝的值。</p>
</td>
</tr>
<tr>
<td>
<p>getlist(key)</p>
</td>
<td>
<p>返回和参数key对应的所有值，作为一个Python list返回。如果key不存在，则返回空list。 It's guaranteed to return a list of some sort..</p>
</td>
</tr>
<tr>
<td>
<p>setlist(key,list_)</p>
</td>
<td>
<p>设置key的值为list_ (unlike __setitem__()).</p>
</td>
</tr>
<tr>
<td>
<p>appendlist(key,item)</p>
</td>
<td>
<p>添加item到和key关联的内部list.</p>
</td>
</tr>
<tr>
<td>
<p>setlistdefault(key,list)</p>
</td>
<td>
<p>和setdefault有一点不同，它接受list而不是单个value作为参数。</p>
</td>
</tr>
<tr>
<td>
<p>lists()</p>
</td>
<td>
<p>和items()有一点不同, 它会返回key的所有值，作为一个list, 例如:
</p>
<pre>
>>> q = QueryDict('a=1&a=2&a=3')

>>> q.lists()

[('a', ['1', '2', '3'])]
</p>
</td>
</tr>
<tr>
<td>
<p>urlencode()</p>
</td>
<td>
<p>返回一个以查询字符串格式进行格式化后的字符串(e.g., "a=2&amp;b=3&amp;b=5").</p>
</td>
</tr>

</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">

<tbody>
<tr>
<th>
<strong>方法</strong>
</th>
<th>
<strong>描述</strong>
</th>
</tr>


<tr>
<td>
<p>copy()</p>
</td>
<td>
<p>返回对象的拷贝，内部实现是用Python标准库的copy.deepcopy()。该拷贝是mutable(可更改的) — 就是说，可以更改该拷贝的值。</p>
</td>
</tr>
<tr>
<td>
<p>getlist(key)</p>
</td>
<td>
<p>返回和参数key对应的所有值，作为一个Python list返回。如果key不存在，则返回空list。 It's guaranteed to return a list of some sort..</p>
</td>
</tr>
<tr>
<td>
<p>setlist(key,list_)</p>
</td>
<td>
<p>设置key的值为list_ (unlike __setitem__()).</p>
</td>
</tr>
<tr>
<td>
<p>appendlist(key,item)</p>
</td>
<td>
<p>添加item到和key关联的内部list.</p>
</td>
</tr>
<tr>
<td>
<p>setlistdefault(key,list)</p>
</td>
<td>
<p>和setdefault有一点不同，它接受list而不是单个value作为参数。</p>
</td>
</tr>
<tr>
<td>
<p>lists()</p>
</td>
<td>
<p>和items()有一点不同, 它会返回key的所有值，作为一个list, 例如:
</p>
<pre>
>>> q = QueryDict('a=1&a=2&a=3')

>>> q.lists()

[('a', ['1', '2', '3'])]
</p>
</td>
</tr>
<tr>
<td>
<p>urlencode()</p>
</td>
<td>
<p>返回一个以查询字符串格式进行格式化后的字符串(e.g., "a=2&amp;b=3&amp;b=5").</p>
</td>
</tr>

</tbody></table></div>') where ID=7943;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr><th style="width:5%">序号</th><th>方法 &amp; 用途</th></tr>
<tr><td>1</td><td><b>string.upper(argument):</b><br>字符串全部转为大写字母。</td></tr>
<tr><td>2</td><td><b>string.lower(argument):</b><br>字符串全部转为小写字母。</td></tr>
<tr><td>3</td><td><b>string.gsub(mainString,findString,replaceString,num)</b><p>在字符串中替换。</p>
mainString 为要操作的字符串， findString 为被替换的字符，replaceString 要替换的字符，num 替换次数（可以忽略，则全部替换），如：	
<pre>
&gt; string.gsub("aaaa","a","z",3);
zzza&nbsp;&nbsp;&nbsp;&nbsp;3
</pre>
</td></tr>
<tr><td>4</td><td><b>string.find (str, substr, [init, [end]])</b><br>
在一个指定的目标字符串中搜索指定的内容(第三个参数为索引),返回其具体位置。不存在则返回 nil。
<pre>
&gt; string.find("Hello Lua user", "Lua", 1) 
7&nbsp;&nbsp;&nbsp;&nbsp;9
</pre>
</td></tr>
<tr><td>5</td><td><b>string.reverse(arg)</b><br>字符串反转<pre>
&gt; string.reverse("Lua")
auL</pre></td></tr>
<tr><td>6</td><td><b>string.format(...)</b><br>返回一个类似printf的格式化字符串
<pre>
&gt; string.format("the value is:%d",4)
the value is:4
</pre></td></tr>
<tr><td>7</td><td><b>string.char(arg) 和 string.byte(arg[,int])</b><br>char 将整型数字转成字符并连接， byte 转换字符为整数值(可以指定某个字符，默认第一个字符)。
<pre>
&gt; string.char(97,98,99,100)
abcd
&gt; string.byte("ABCD",4)
68
&gt; string.byte("ABCD")
65
&gt;
</pre></td></tr>
<tr><td>8</td><td><b>string.len(arg)</b><br>计算字符串长度。
<pre>
string.len("abc")
3
</pre></td></tr>
<tr><td>9</td><td><b>string.rep(string, n)</b><br>返回字符串string的n个拷贝
<pre>
&gt; string.rep("abcd",2)
abcdabcd
</pre>

</td></tr>
<tr><td>10</td><td><b>..</b><br>链接两个字符串
<pre>
&gt; print("www.runoob.".."com")
www.panku.in
</pre>
</td></tr>

<tr><td>11</td><td><b>string.gmatch(str, pattern)</b><br>回一个迭代器函数，每一次调用这个函数，返回一个在字符串 str 找到的下一个符合 pattern 描述的子串。如果参数 pattern 描述的字符串没有找到，迭代函数返回nil。
<pre>
&gt; for word in string.gmatch("Hello Lua user", "%a+") do print(word) end
Hello
Lua
user
</pre>
</td></tr>
<tr><td>12</td><td><b>string.match(str, pattern, init)</b><br>string.match()只寻找源字串str中的第一个配对. 参数init可选, 指定搜寻过程的起点, 默认为1。
<br>
在成功配对时, 函数将返回配对表达式中的所有捕获结果; 如果没有设置捕获标记, 则返回整个配对字符串. 当没有成功的配对时, 返回nil。
<pre>
&gt; = string.match("I have 2 questions for you.", "%d+ %a+")
2 questions

&gt; = string.format("%d, %q", string.match("I have 2 questions for you.", "(%d+) (%a+)"))
2, "questions"
</pre>
</td></tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr><th style="width:5%">序号</th><th>方法 &amp; 用途</th></tr>
<tr><td>1</td><td><b>string.upper(argument):</b><br>字符串全部转为大写字母。</td></tr>
<tr><td>2</td><td><b>string.lower(argument):</b><br>字符串全部转为小写字母。</td></tr>
<tr><td>3</td><td><b>string.gsub(mainString,findString,replaceString,num)</b><p>在字符串中替换。</p>
mainString 为要操作的字符串， findString 为被替换的字符，replaceString 要替换的字符，num 替换次数（可以忽略，则全部替换），如：	
<pre>
&gt; string.gsub("aaaa","a","z",3);
zzza&nbsp;&nbsp;&nbsp;&nbsp;3
</pre>
</td></tr>
<tr><td>4</td><td><b>string.find (str, substr, [init, [end]])</b><br>
在一个指定的目标字符串中搜索指定的内容(第三个参数为索引),返回其具体位置。不存在则返回 nil。
<pre>
&gt; string.find("Hello Lua user", "Lua", 1) 
7&nbsp;&nbsp;&nbsp;&nbsp;9
</pre>
</td></tr>
<tr><td>5</td><td><b>string.reverse(arg)</b><br>字符串反转<pre>
&gt; string.reverse("Lua")
auL</pre></td></tr>
<tr><td>6</td><td><b>string.format(...)</b><br>返回一个类似printf的格式化字符串
<pre>
&gt; string.format("the value is:%d",4)
the value is:4
</pre></td></tr>
<tr><td>7</td><td><b>string.char(arg) 和 string.byte(arg[,int])</b><br>char 将整型数字转成字符并连接， byte 转换字符为整数值(可以指定某个字符，默认第一个字符)。
<pre>
&gt; string.char(97,98,99,100)
abcd
&gt; string.byte("ABCD",4)
68
&gt; string.byte("ABCD")
65
&gt;
</pre></td></tr>
<tr><td>8</td><td><b>string.len(arg)</b><br>计算字符串长度。
<pre>
string.len("abc")
3
</pre></td></tr>
<tr><td>9</td><td><b>string.rep(string, n)</b><br>返回字符串string的n个拷贝
<pre>
&gt; string.rep("abcd",2)
abcdabcd
</pre>

</td></tr>
<tr><td>10</td><td><b>..</b><br>链接两个字符串
<pre>
&gt; print("www.runoob.".."com")
www.panku.in
</pre>
</td></tr>

<tr><td>11</td><td><b>string.gmatch(str, pattern)</b><br>回一个迭代器函数，每一次调用这个函数，返回一个在字符串 str 找到的下一个符合 pattern 描述的子串。如果参数 pattern 描述的字符串没有找到，迭代函数返回nil。
<pre>
&gt; for word in string.gmatch("Hello Lua user", "%a+") do print(word) end
Hello
Lua
user
</pre>
</td></tr>
<tr><td>12</td><td><b>string.match(str, pattern, init)</b><br>string.match()只寻找源字串str中的第一个配对. 参数init可选, 指定搜寻过程的起点, 默认为1。
<br>
在成功配对时, 函数将返回配对表达式中的所有捕获结果; 如果没有设置捕获标记, 则返回整个配对字符串. 当没有成功的配对时, 返回nil。
<pre>
&gt; = string.match("I have 2 questions for you.", "%d+ %a+")
2 questions

&gt; = string.format("%d, %q", string.match("I have 2 questions for you.", "(%d+) (%a+)"))
2, "questions"
</pre>
</td></tr>
</tbody></table></div>') where ID=8089;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<thead>
<tr>
<th width="10%">事件 </th>
<th width="20%">描述 </th>
<th width="50%"> 用法 </th>
<th> 实例</th>
</tr>
</thead>
<tbody>
<tr>
<td>on-hold </td>
<td> 长按的时间是500毫秒。  </td>
<td><pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-hold="onHold()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/button&gt;
</pre></td>
<td><a target="_blank" href="/try/tryit.php?filename=ionic_on-hold" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-tap </td>
<td> 这个是手势轻击事件，如果长按时间超过250毫秒，那就不是轻击了。。  </td>
<td><pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-tap="onTap()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-tap" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-double-tap </td>
<td> 手双击屏幕事件  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-double-tap="onDoubleTap()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-double-tap" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-touch </td>
<td> 这个和 on-tap 还是有区别的，这个是立即执行，而且是用户点击立马执行。不用等待 touchend/mouseup 。  </td>
<td> <pre>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;button on-touch="onTouch()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-touch" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-release </td>
<td> 当用户结束触摸事件时触发。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-release="onRelease()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-release" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-drag </td>
<td> 这个有点类似于PC端的拖拽。当你一直点击某个物体，并且手开始移动，都会触发 on-drag。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag="onDrag()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-drag-up</td>
<td> 向上拖拽。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag-up="onDragUp()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;
</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag-up" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
<tr>
<td>on-drag-right</td>
<td> 向右拖拽。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag-right="onDragRight()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag-up" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
<tr>
<td>on-drag-down</td>
<td> 向下拖拽。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag-down="onDragDown()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag-up" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
</tbody>
<tr>
<td>on-drag-left</td>
<td> 向左边拖拽。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag-left="onDragLeft()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag-up" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
<tr>
<td>on-swipe</td>
<td> 指手指滑动效果，可以是任何方向上的。而且也和 on-drag 类似，都有四个方向上单独的事件。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe="onSwipe()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-swipe-up</td>
<td> 向上的手指滑动效果。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe-up="onSwipeUp()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-swipe-right</td>
<td> 向右的手指滑动效果。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe-right="onSwipeRight()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-swipe-down</td>
<td> 向下的手指滑动效果。  </td>
<td> <pre>&lt;button
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe-down="onSwipeDown()"
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
<tr>
<td>on-swipe-left</td>
<td> 向左的手指滑动效果。  </td>
<td> <pre>&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe-left="onSwipeLeft()"
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
</table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<thead>
<tr>
<th width="10%">事件 </th>
<th width="20%">描述 </th>
<th width="50%"> 用法 </th>
<th> 实例</th>
</tr>
</thead>
<tbody>
<tr>
<td>on-hold </td>
<td> 长按的时间是500毫秒。  </td>
<td><pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-hold="onHold()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/button&gt;
</pre></td>
<td><a target="_blank" href="/try/tryit.php?filename=ionic_on-hold" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-tap </td>
<td> 这个是手势轻击事件，如果长按时间超过250毫秒，那就不是轻击了。。  </td>
<td><pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-tap="onTap()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-tap" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-double-tap </td>
<td> 手双击屏幕事件  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-double-tap="onDoubleTap()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-double-tap" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-touch </td>
<td> 这个和 on-tap 还是有区别的，这个是立即执行，而且是用户点击立马执行。不用等待 touchend/mouseup 。  </td>
<td> <pre>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;button on-touch="onTouch()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-touch" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-release </td>
<td> 当用户结束触摸事件时触发。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-release="onRelease()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-release" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-drag </td>
<td> 这个有点类似于PC端的拖拽。当你一直点击某个物体，并且手开始移动，都会触发 on-drag。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag="onDrag()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-drag-up</td>
<td> 向上拖拽。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag-up="onDragUp()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;
</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag-up" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
<tr>
<td>on-drag-right</td>
<td> 向右拖拽。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag-right="onDragRight()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag-up" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
<tr>
<td>on-drag-down</td>
<td> 向下拖拽。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag-down="onDragDown()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag-up" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
</tbody>
<tr>
<td>on-drag-left</td>
<td> 向左边拖拽。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-drag-left="onDragLeft()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-drag-up" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
<tr>
<td>on-swipe</td>
<td> 指手指滑动效果，可以是任何方向上的。而且也和 on-drag 类似，都有四个方向上单独的事件。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe="onSwipe()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-swipe-up</td>
<td> 向上的手指滑动效果。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe-up="onSwipeUp()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-swipe-right</td>
<td> 向右的手指滑动效果。  </td>
<td> <pre>
&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe-right="onSwipeRight()" 
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>

<tr>
<td>on-swipe-down</td>
<td> 向下的手指滑动效果。  </td>
<td> <pre>&lt;button
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe-down="onSwipeDown()"
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
<tr>
<td>on-swipe-left</td>
<td> 向左的手指滑动效果。  </td>
<td> <pre>&lt;button 
&nbsp;&nbsp;&nbsp;&nbsp;on-swipe-left="onSwipeLeft()"
&nbsp;&nbsp;&nbsp;&nbsp;class="button"&gt;
&nbsp;&nbsp;&nbsp;&nbsp;Test
&lt;/button&gt;</pre></td>
<td> <a target="_blank" href="/try/tryit.php?filename=ionic_on-swipe" class="tryitbtn" rel="noopener noreferrer">尝试一下 »</a></td>
</tr>
</table></div>') where ID=8277;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th>序号</th>
<th>函数/运算符 &amp; 描述</th>
</tr>
<tr>
<td>1</td>
<td><p><b>isEmpty</b></p>
<p>判断字符串是否为空，返回布尔值</p></td>
</tr>
<tr>
<td>2</td>
<td><p><b>hasPrefix(prefix: String)</b></p>
<p>检查字符串是否拥有特定前缀</p></td>
</tr>
<tr>
<td>3</td>
<td><p><b>hasSuffix(suffix: String)</b></p>
<p>检查字符串是否拥有特定后缀。</p></td>
</tr>
<tr>
<td>4</td>
<td><p><b>Int(String)</b></p>
<p>转换字符串数字为整型。
实例:</br>
<pre>
let myString: String = "256"
let myInt: Int? = Int(myString)
</pre>
</p></td>
</tr>
<tr>
<td>5</td>
<td><p><b>String.count</b></p>
<p class="important"><b>Swift 3 版本使用的是 String.characters.count</b></p>
<p>计算字符串的长度</p></td>
</tr>
<tr>
<td>6</td>
<td><p><b>utf8</b></p>
<p>您可以通过遍历 String 的 utf8 属性来访问它的 UTF-8 编码</p></td>
</tr>
<tr>
<td>7</td>
<td><p><b>utf16</b></p>
<p>您可以通过遍历 String 的 utf8 属性来访问它的 utf16 编码</p></td>
</tr>
<tr>
<td>8</td>
<td><p><b>unicodeScalars</b></p>
<p>您可以通过遍历String值的unicodeScalars属性来访问它的 Unicode 标量编码.</p></td>
</tr>
<tr>
<td>9</td>
<td><p><b>+</b></p>
<p>连接两个字符串，并返回一个新的字符串</p></td>
</tr>
<tr>
<td>10</td>
<td><p><b>+=</b></p>
<p>连接操作符两边的字符串并将新字符串赋值给左边的操作符变量</p></td>
</tr>
<tr>
<td>11</td>
<td><p><b>==</b></p>
<p>判断两个字符串是否相等</p></td>
</tr>
<tr>
<td>12</td>
<td><p><b>&lt;</b></p>
<p>比较两个字符串，对两个字符串的字母逐一比较。</p></td>
</tr>
<tr>
<td>13</td>
<td><p><b>!=</b></p>
<p>比较两个字符串是否不相等。</p></td>
</tr>

</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th>序号</th>
<th>函数/运算符 &amp; 描述</th>
</tr>
<tr>
<td>1</td>
<td><p><b>isEmpty</b></p>
<p>判断字符串是否为空，返回布尔值</p></td>
</tr>
<tr>
<td>2</td>
<td><p><b>hasPrefix(prefix: String)</b></p>
<p>检查字符串是否拥有特定前缀</p></td>
</tr>
<tr>
<td>3</td>
<td><p><b>hasSuffix(suffix: String)</b></p>
<p>检查字符串是否拥有特定后缀。</p></td>
</tr>
<tr>
<td>4</td>
<td><p><b>Int(String)</b></p>
<p>转换字符串数字为整型。
实例:</br>
<pre>
let myString: String = "256"
let myInt: Int? = Int(myString)
</pre>
</p></td>
</tr>
<tr>
<td>5</td>
<td><p><b>String.count</b></p>
<p class="important"><b>Swift 3 版本使用的是 String.characters.count</b></p>
<p>计算字符串的长度</p></td>
</tr>
<tr>
<td>6</td>
<td><p><b>utf8</b></p>
<p>您可以通过遍历 String 的 utf8 属性来访问它的 UTF-8 编码</p></td>
</tr>
<tr>
<td>7</td>
<td><p><b>utf16</b></p>
<p>您可以通过遍历 String 的 utf8 属性来访问它的 utf16 编码</p></td>
</tr>
<tr>
<td>8</td>
<td><p><b>unicodeScalars</b></p>
<p>您可以通过遍历String值的unicodeScalars属性来访问它的 Unicode 标量编码.</p></td>
</tr>
<tr>
<td>9</td>
<td><p><b>+</b></p>
<p>连接两个字符串，并返回一个新的字符串</p></td>
</tr>
<tr>
<td>10</td>
<td><p><b>+=</b></p>
<p>连接操作符两边的字符串并将新字符串赋值给左边的操作符变量</p></td>
</tr>
<tr>
<td>11</td>
<td><p><b>==</b></p>
<p>判断两个字符串是否相等</p></td>
</tr>
<tr>
<td>12</td>
<td><p><b>&lt;</b></p>
<p>比较两个字符串，对两个字符串的字母逐一比较。</p></td>
</tr>
<tr>
<td>13</td>
<td><p><b>!=</b></p>
<p>比较两个字符串是否不相等。</p></td>
</tr>

</tbody></table></div>') where ID=8314;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<td>指定构造器</td>
<td>便利构造器</td>
</tr>
<tr>
<td>类中最主要的构造器</td>
<td>类中比较次要的、辅助型的构造器</td>
</tr>
<tr>
<td>初始化类中提供的所有属性，并根据父类链往上调用父类的构造器来实现父类的初始化。</td>
<td>可以定义便利构造器来调用同一个类中的指定构造器，并为其参数提供默认值。你也可以定义便利构造器来创建一个特殊用途或特定输入的实例。</td>
</tr>
<tr>
<td>每一个类都必须拥有至少一个指定构造器</td>
<td>只在必要的时候为类提供便利构造器</td>
</tr>
<tr>
<td>
<pre>
Init(parameters) {
    statements
}
</pre>
</td>
<td>
<pre>
convenience init(parameters) {
      statements
}
</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<td>指定构造器</td>
<td>便利构造器</td>
</tr>
<tr>
<td>类中最主要的构造器</td>
<td>类中比较次要的、辅助型的构造器</td>
</tr>
<tr>
<td>初始化类中提供的所有属性，并根据父类链往上调用父类的构造器来实现父类的初始化。</td>
<td>可以定义便利构造器来调用同一个类中的指定构造器，并为其参数提供默认值。你也可以定义便利构造器来创建一个特殊用途或特定输入的实例。</td>
</tr>
<tr>
<td>每一个类都必须拥有至少一个指定构造器</td>
<td>只在必要的时候为类提供便利构造器</td>
</tr>
<tr>
<td>
<pre>
Init(parameters) {
    statements
}
</pre>
</td>
<td>
<pre>
convenience init(parameters) {
      statements
}
</pre>
</td>
</tr>
</tbody></table></div>') where ID=8327;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th>
<th style="width:95%">方法和描述</th>
</tr>
<tr>
<td>1</td>
<td>
<p><b>def apply( x: T, xs: T* ): Array[T]</b></p>
<p>创建指定对象 T 的数组,  T 的值可以是 Unit, Double, Float, Long, Int, Char, Short, Byte, Boolean。</p>
</td>
</tr>
<tr>
<td>2</td>
<td>
<p><b>def concat[T]( xss: Array[T]* ): Array[T]</b></p>
<p>合并数组</p>
</td>
</tr>
<tr>
<td>3</td>
<td>
<p><b>def copy( src: AnyRef, srcPos: Int, dest: AnyRef, destPos: Int, length: Int ): Unit</b></p>
<p>复制一个数组到另一个数组上。相等于 Java's System.arraycopy(src, srcPos, dest, destPos, length)。</p>
</td>
</tr>
<tr>
<td>4</td>
<td>
<p><b>def empty[T]: Array[T]</b></p>
<p>返回长度为 0 的数组</p>
</td>
</tr>
<tr>
<td>5</td>
<td>
<p><b>def iterate[T]( start: T, len: Int )( f: (T) =&gt; T ): Array[T]</b></p>
<p>返回指定长度数组，每个数组元素为指定函数的返回值。</p>
<p>以上实例数组初始值为 0，长度为 3，计算函数为<b>a=>a+1</b>：</p>
<pre>
scala&gt; Array.iterate(0,3)(a=&gt;a+1)
res1: Array[Int] = Array(0, 1, 2)
</pre>
</td>
</tr>
<tr>
<td>6</td>
<td>
<p><b>def fill[T]( n: Int )(elem: =&gt;  T): Array[T]</b></p>
<p>返回数组，长度为第一个参数指定，同时每个元素使用第二个参数进行填充。</p>
</td>
</tr>
<tr>
<td>7</td>
<td>
<p><b>def fill[T]( n1: Int, n2: Int )( elem: =&gt; T ): Array[Array[T]]</b></p>
<p>返回二数组，长度为第一个参数指定，同时每个元素使用第二个参数进行填充。</p>
</td>
</tr>

<tr>
<td>8</td>
<td>
<p><b>def ofDim[T]( n1: Int ): Array[T]</b></p>
<p>创建指定长度的数组</p>
</td>
</tr>
<tr>
<td>9</td>
<td>
<p><b>def ofDim[T]( n1: Int, n2: Int ): Array[Array[T]]</b></p>
<p>创建二维数组</p>
</td>
</tr>
<tr>
<td>10</td>
<td>
<p><b>def ofDim[T]( n1: Int, n2: Int, n3: Int ): Array[Array[Array[T]]]</b></p>
<p>创建三维数组</p>
</td>
</tr>
<tr>
<td>11</td>
<td>
<p><b>def range( start: Int, end: Int, step: Int ): Array[Int]</b></p>
<p>创建指定区间内的数组，step 为每个元素间的步长</p>
</td>
</tr>
<tr>
<td>12</td>
<td>
<p><b>def range( start: Int, end: Int ): Array[Int]</b></p>
<p>创建指定区间内的数组</p>
</td>
</tr>
<tr>
<td>13</td>
<td>
<p><b>def tabulate[T]( n: Int )(f: (Int)=&gt; T): Array[T]</b></p>
<p>返回指定长度数组，每个数组元素为指定函数的返回值，默认从 0 开始。</p>
<p>以上实例返回 3 个元素：</p>
<pre>
scala&gt; Array.tabulate(3)(a =&gt; a + 5)
res0: Array[Int] = Array(5, 6, 7)
</pre>
</td>
</tr>
<tr>
<td>14</td>
<td>
<p><b>def tabulate[T]( n1: Int, n2: Int )( f: (Int, Int ) =&gt; T): Array[Array[T]]</b></p>
<p>返回指定长度的二维数组，每个数组元素为指定函数的返回值，默认从 0 开始。</p>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th>
<th style="width:95%">方法和描述</th>
</tr>
<tr>
<td>1</td>
<td>
<p><b>def apply( x: T, xs: T* ): Array[T]</b></p>
<p>创建指定对象 T 的数组,  T 的值可以是 Unit, Double, Float, Long, Int, Char, Short, Byte, Boolean。</p>
</td>
</tr>
<tr>
<td>2</td>
<td>
<p><b>def concat[T]( xss: Array[T]* ): Array[T]</b></p>
<p>合并数组</p>
</td>
</tr>
<tr>
<td>3</td>
<td>
<p><b>def copy( src: AnyRef, srcPos: Int, dest: AnyRef, destPos: Int, length: Int ): Unit</b></p>
<p>复制一个数组到另一个数组上。相等于 Java's System.arraycopy(src, srcPos, dest, destPos, length)。</p>
</td>
</tr>
<tr>
<td>4</td>
<td>
<p><b>def empty[T]: Array[T]</b></p>
<p>返回长度为 0 的数组</p>
</td>
</tr>
<tr>
<td>5</td>
<td>
<p><b>def iterate[T]( start: T, len: Int )( f: (T) =&gt; T ): Array[T]</b></p>
<p>返回指定长度数组，每个数组元素为指定函数的返回值。</p>
<p>以上实例数组初始值为 0，长度为 3，计算函数为<b>a=>a+1</b>：</p>
<pre>
scala&gt; Array.iterate(0,3)(a=&gt;a+1)
res1: Array[Int] = Array(0, 1, 2)
</pre>
</td>
</tr>
<tr>
<td>6</td>
<td>
<p><b>def fill[T]( n: Int )(elem: =&gt;  T): Array[T]</b></p>
<p>返回数组，长度为第一个参数指定，同时每个元素使用第二个参数进行填充。</p>
</td>
</tr>
<tr>
<td>7</td>
<td>
<p><b>def fill[T]( n1: Int, n2: Int )( elem: =&gt; T ): Array[Array[T]]</b></p>
<p>返回二数组，长度为第一个参数指定，同时每个元素使用第二个参数进行填充。</p>
</td>
</tr>

<tr>
<td>8</td>
<td>
<p><b>def ofDim[T]( n1: Int ): Array[T]</b></p>
<p>创建指定长度的数组</p>
</td>
</tr>
<tr>
<td>9</td>
<td>
<p><b>def ofDim[T]( n1: Int, n2: Int ): Array[Array[T]]</b></p>
<p>创建二维数组</p>
</td>
</tr>
<tr>
<td>10</td>
<td>
<p><b>def ofDim[T]( n1: Int, n2: Int, n3: Int ): Array[Array[Array[T]]]</b></p>
<p>创建三维数组</p>
</td>
</tr>
<tr>
<td>11</td>
<td>
<p><b>def range( start: Int, end: Int, step: Int ): Array[Int]</b></p>
<p>创建指定区间内的数组，step 为每个元素间的步长</p>
</td>
</tr>
<tr>
<td>12</td>
<td>
<p><b>def range( start: Int, end: Int ): Array[Int]</b></p>
<p>创建指定区间内的数组</p>
</td>
</tr>
<tr>
<td>13</td>
<td>
<p><b>def tabulate[T]( n: Int )(f: (Int)=&gt; T): Array[T]</b></p>
<p>返回指定长度数组，每个数组元素为指定函数的返回值，默认从 0 开始。</p>
<p>以上实例返回 3 个元素：</p>
<pre>
scala&gt; Array.tabulate(3)(a =&gt; a + 5)
res0: Array[Int] = Array(5, 6, 7)
</pre>
</td>
</tr>
<tr>
<td>14</td>
<td>
<p><b>def tabulate[T]( n1: Int, n2: Int )( f: (Int, Int ) =&gt; T): Array[Array[T]]</b></p>
<p>返回指定长度的二维数组，每个数组元素为指定函数的返回值，默认从 0 开始。</p>
</td>
</tr>
</tbody></table></div>') where ID=8403;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th>
<th style="width:95%">方法及描述</th>
</tr>
<tr>
<td>1</td>
<td>
<p><b>def +:(elem: A): List[A]</b></p>

<p>为列表预添加元素</p>
<pre>
scala&gt; val x = List(1)
x: List[Int] = List(1)

scala&gt; val y = 2 +: x
y: List[Int] = List(2, 1)

scala&gt; println(x)
List(1)
</pre>
</td>
</tr>
<tr>
<td>2</td>
<td>
<p><b>def ::(x: A): List[A]</b></p>
<p>在列表开头添加元素</p>
</td>
</tr>
<tr>
<td>3</td>
<td>
<p><b>def :::(prefix: List[A]): List[A]</b></p>
<p>在列表开头添加指定列表的元素</p>
</td>
</tr>
<tr>
<td>4</td>
<td>
<p><b>def :+(elem: A): List[A]</b></p>
<p>复制添加元素后列表。 </p>
<pre>
scala&gt; val a = List(1)
a: List[Int] = List(1)

scala&gt; val b = a :+ 2
b: List[Int] = List(1, 2)

scala&gt; println(a)
List(1)
</pre>
</td>
</tr>
<tr>
<td>5</td>
<td>
<p><b>def addString(b: StringBuilder): StringBuilder</b></p>
<p>将列表的所有元素添加到 StringBuilder</p>
</td>
</tr>
<tr>
<td>6</td>
<td>
<p><b>def addString(b: StringBuilder, sep: String): StringBuilder</b></p>
<p>将列表的所有元素添加到 StringBuilder，并指定分隔符</p>
</td>
</tr>
<tr>
<td>7</td>
<td>
<p><b>def apply(n: Int): A</b></p>
<p>通过列表索引获取元素</p>
</td>
</tr>
<tr>
<td>8</td>
<td>
<p><b>def contains(elem: Any): Boolean</b></p>
<p>检测列表中是否包含指定的元素</p>
</td>
</tr>
<tr>
<td>9</td>
<td>
<p><b>def copyToArray(xs: Array[A], start: Int, len: Int): Unit</b></p>
<p>将列表的元素复制到数组中。</p>
</td>
</tr>
<tr>
<td>10</td>
<td>
<p><b>def distinct: List[A]</b></p>
<p>去除列表的重复元素，并返回新列表</p>
</td>
</tr>
<tr>
<td>11</td>
<td>
<p><b>def drop(n: Int): List[A]</b></p>
<p>丢弃前n个元素，并返回新列表</p>
</td>
</tr>
<tr>
<td>12</td>
<td>
<p><b>def dropRight(n: Int): List[A]</b></p>
<p>丢弃最后n个元素，并返回新列表</p>
</td>
</tr>
<tr>
<td>13</td>
<td>
<p><b>def dropWhile(p: (A) =&gt; Boolean): List[A]</b></p>
<p>从左向右丢弃元素，直到条件p不成立</p>
</td>
</tr>
<tr>
<td>14</td>
<td>
<p><b>def endsWith[B](that: Seq[B]): Boolean</b></p>
<p>检测列表是否以指定序列结尾</p>
</td>
</tr>
<tr>
<td>15</td>
<td>
<p><b>def equals(that: Any): Boolean</b></p>
<p>判断是否相等</p>
</td>
</tr>
<tr>
<td>16</td>
<td>
<p><b>def exists(p: (A) =&gt; Boolean): Boolean</b></p>
<p>判断列表中指定条件的元素是否存在。</p>
<p>判断l是否存在某个元素:
</p><pre>
scala&gt; l.exists(s =&gt; s == "Hah")
res7: Boolean = true</pre>
</td>
</tr>
<tr>
<td>17</td>
<td>
<p><b>def filter(p: (A) =&gt; Boolean): List[A]</b></p>
<p>输出符号指定条件的所有元素。</p>
<p>过滤出长度为3的元素:</p>
<pre>
scala&gt; l.filter(s =&gt; s.length == 3)
res8: List[String] = List(Hah, WOW)
</pre>
</td>
</tr>
<tr>
<td>18</td>
<td>
<p><b>def forall(p: (A) =&gt; Boolean): Boolean</b></p>
<p>检测所有元素。</p>
<p>
例如：判断所有元素是否以"H"开头：</p>
</pre>
scala> l.forall(s => s.startsWith("H"))
res10: Boolean = false
</pre>
</td>
</tr>
<tr>
<td>19</td>
<td>
<p><b>def foreach(f: (A) =&gt; Unit): Unit</b></p>
<p>将函数应用到列表的所有元素</p>
</td>
</tr>
<tr>
<td>20</td>
<td>
<p><b>def head: A</b></p>
<p>获取列表的第一个元素</p>
</td>
</tr>
<tr>
<td>21</td>
<td>
<p><b>def indexOf(elem: A, from: Int): Int</b></p>
<p>从指定位置 from
开始查找元素第一次出现的位置</p>
</td>
</tr>
<tr>
<td>22</td>
<td>
<p><b>def init: List[A]</b></p>
<p>返回所有元素，除了最后一个</p>
</td>
</tr>
<tr>
<td>23</td>
<td>
<p><b>def intersect(that: Seq[A]): List[A]</b></p>
<p>计算多个集合的交集</p>
</td>
</tr>
<tr>
<td>24</td>
<td>
<p><b>def isEmpty: Boolean</b></p>
<p>检测列表是否为空</p>
</td>
</tr>
<tr>
<td>25</td>
<td>
<p><b>def iterator: Iterator[A]</b></p>
<p>创建一个新的迭代器来迭代元素</p>
</td>
</tr>
<tr>
<td>26</td>
<td>
<p><b>def last: A</b></p>
<p>返回最后一个元素</p>
</td>
</tr>
<tr>
<td>27</td>
<td>
<p><b>def lastIndexOf(elem: A, end: Int): Int</b></p>
<p>在指定的位置 end 开始查找元素最后出现的位置</p>
</td>
</tr>
<tr>
<td>28</td>
<td>
<p><b>def length: Int</b></p>
<p>返回列表长度</p>
</td>
</tr>
<tr>
<td>29</td>
<td>
<p><b>def map[B](f: (A) =&gt; B): List[B]</b></p>
<p>通过给定的方法将所有元素重新计算</p>
</td>
</tr>
<tr>
<td>30</td>
<td>
<p><b>def max: A</b></p>
<p>查找最大元素</p>
</td>
</tr>
<tr>
<td>31</td>
<td>
<p><b>def min: A</b></p>
<p>查找最小元素</p>
</td>
</tr>
<tr>
<td>32</td>
<td>
<p><b>def mkString: String</b></p>
<p>列表所有元素作为字符串显示</p>
</td>
</tr>
<tr>
<td>33</td>
<td>
<p><b>def mkString(sep: String): String</b></p>
<p>使用分隔符将列表所有元素作为字符串显示</p>
</td>
</tr>
<tr>
<td>34</td>
<td>
<p><b>def reverse: List[A]</b></p>
<p>列表反转</p>
</td>
</tr>
<tr>
<td>35</td>
<td>
<p><b>def sorted[B &gt;: A]: List[A]</b></p>
<p>列表排序</p>
</td>
</tr>
<tr>
<td>36</td>
<td>
<p><b>def startsWith[B](that: Seq[B], offset: Int): Boolean</b></p>
<p>检测列表在指定位置是否包含指定序列</p>
</td>
</tr>
<tr>
<td>37</td>
<td>
<p><b>def sum: A</b></p>
<p>计算集合元素之和</p>
</td>
</tr>
<tr>
<td>38</td>
<td>
<p><b>def tail: List[A]</b></p>
<p>返回所有元素，除了第一个</p>
</td>
</tr>
<tr>
<td>39</td>
<td>
<p><b>def take(n: Int): List[A]</b></p>
<p>提取列表的前n个元素</p>
</td>
</tr>
<tr>
<td>40</td>
<td>
<p><b>def takeRight(n: Int): List[A]</b></p>
<p>提取列表的后n个元素</p>
</td>
</tr>
<tr>
<td>41</td>
<td>
<p><b>def toArray: Array[A]</b></p>
<p>列表转换为数组</p>
</td>
</tr>
<tr>
<td>42</td>
<td>
<p><b>def toBuffer[B &gt;: A]: Buffer[B]</b></p>
<p>返回缓冲区，包含了列表的所有元素</p>
</td>
</tr>
<tr>
<td>43</td>
<td>
<p><b>def toMap[T, U]: Map[T, U]</b></p>
<p>List 转换为 Map</p>
</td>
</tr>
<tr>
<td>44</td>
<td>
<p><b>def toSeq: Seq[A]</b></p>
<p>List 转换为 Seq</p>
</td>
</tr>
<tr>
<td>45</td>
<td>
<p><b>def toSet[B &gt;: A]: Set[B]</b></p>
<p>List 转换为 Set</p>
</td>
</tr>
<tr>
<td>46</td>
<td>
<p><b>def toString(): String</b></p>
<p>列表转换为字符串</p>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:5%">序号</th>
<th style="width:95%">方法及描述</th>
</tr>
<tr>
<td>1</td>
<td>
<p><b>def +:(elem: A): List[A]</b></p>

<p>为列表预添加元素</p>
<pre>
scala&gt; val x = List(1)
x: List[Int] = List(1)

scala&gt; val y = 2 +: x
y: List[Int] = List(2, 1)

scala&gt; println(x)
List(1)
</pre>
</td>
</tr>
<tr>
<td>2</td>
<td>
<p><b>def ::(x: A): List[A]</b></p>
<p>在列表开头添加元素</p>
</td>
</tr>
<tr>
<td>3</td>
<td>
<p><b>def :::(prefix: List[A]): List[A]</b></p>
<p>在列表开头添加指定列表的元素</p>
</td>
</tr>
<tr>
<td>4</td>
<td>
<p><b>def :+(elem: A): List[A]</b></p>
<p>复制添加元素后列表。 </p>
<pre>
scala&gt; val a = List(1)
a: List[Int] = List(1)

scala&gt; val b = a :+ 2
b: List[Int] = List(1, 2)

scala&gt; println(a)
List(1)
</pre>
</td>
</tr>
<tr>
<td>5</td>
<td>
<p><b>def addString(b: StringBuilder): StringBuilder</b></p>
<p>将列表的所有元素添加到 StringBuilder</p>
</td>
</tr>
<tr>
<td>6</td>
<td>
<p><b>def addString(b: StringBuilder, sep: String): StringBuilder</b></p>
<p>将列表的所有元素添加到 StringBuilder，并指定分隔符</p>
</td>
</tr>
<tr>
<td>7</td>
<td>
<p><b>def apply(n: Int): A</b></p>
<p>通过列表索引获取元素</p>
</td>
</tr>
<tr>
<td>8</td>
<td>
<p><b>def contains(elem: Any): Boolean</b></p>
<p>检测列表中是否包含指定的元素</p>
</td>
</tr>
<tr>
<td>9</td>
<td>
<p><b>def copyToArray(xs: Array[A], start: Int, len: Int): Unit</b></p>
<p>将列表的元素复制到数组中。</p>
</td>
</tr>
<tr>
<td>10</td>
<td>
<p><b>def distinct: List[A]</b></p>
<p>去除列表的重复元素，并返回新列表</p>
</td>
</tr>
<tr>
<td>11</td>
<td>
<p><b>def drop(n: Int): List[A]</b></p>
<p>丢弃前n个元素，并返回新列表</p>
</td>
</tr>
<tr>
<td>12</td>
<td>
<p><b>def dropRight(n: Int): List[A]</b></p>
<p>丢弃最后n个元素，并返回新列表</p>
</td>
</tr>
<tr>
<td>13</td>
<td>
<p><b>def dropWhile(p: (A) =&gt; Boolean): List[A]</b></p>
<p>从左向右丢弃元素，直到条件p不成立</p>
</td>
</tr>
<tr>
<td>14</td>
<td>
<p><b>def endsWith[B](that: Seq[B]): Boolean</b></p>
<p>检测列表是否以指定序列结尾</p>
</td>
</tr>
<tr>
<td>15</td>
<td>
<p><b>def equals(that: Any): Boolean</b></p>
<p>判断是否相等</p>
</td>
</tr>
<tr>
<td>16</td>
<td>
<p><b>def exists(p: (A) =&gt; Boolean): Boolean</b></p>
<p>判断列表中指定条件的元素是否存在。</p>
<p>判断l是否存在某个元素:
</p><pre>
scala&gt; l.exists(s =&gt; s == "Hah")
res7: Boolean = true</pre>
</td>
</tr>
<tr>
<td>17</td>
<td>
<p><b>def filter(p: (A) =&gt; Boolean): List[A]</b></p>
<p>输出符号指定条件的所有元素。</p>
<p>过滤出长度为3的元素:</p>
<pre>
scala&gt; l.filter(s =&gt; s.length == 3)
res8: List[String] = List(Hah, WOW)
</pre>
</td>
</tr>
<tr>
<td>18</td>
<td>
<p><b>def forall(p: (A) =&gt; Boolean): Boolean</b></p>
<p>检测所有元素。</p>
<p>
例如：判断所有元素是否以"H"开头：</p>
</pre>
scala> l.forall(s => s.startsWith("H"))
res10: Boolean = false
</pre>
</td>
</tr>
<tr>
<td>19</td>
<td>
<p><b>def foreach(f: (A) =&gt; Unit): Unit</b></p>
<p>将函数应用到列表的所有元素</p>
</td>
</tr>
<tr>
<td>20</td>
<td>
<p><b>def head: A</b></p>
<p>获取列表的第一个元素</p>
</td>
</tr>
<tr>
<td>21</td>
<td>
<p><b>def indexOf(elem: A, from: Int): Int</b></p>
<p>从指定位置 from
开始查找元素第一次出现的位置</p>
</td>
</tr>
<tr>
<td>22</td>
<td>
<p><b>def init: List[A]</b></p>
<p>返回所有元素，除了最后一个</p>
</td>
</tr>
<tr>
<td>23</td>
<td>
<p><b>def intersect(that: Seq[A]): List[A]</b></p>
<p>计算多个集合的交集</p>
</td>
</tr>
<tr>
<td>24</td>
<td>
<p><b>def isEmpty: Boolean</b></p>
<p>检测列表是否为空</p>
</td>
</tr>
<tr>
<td>25</td>
<td>
<p><b>def iterator: Iterator[A]</b></p>
<p>创建一个新的迭代器来迭代元素</p>
</td>
</tr>
<tr>
<td>26</td>
<td>
<p><b>def last: A</b></p>
<p>返回最后一个元素</p>
</td>
</tr>
<tr>
<td>27</td>
<td>
<p><b>def lastIndexOf(elem: A, end: Int): Int</b></p>
<p>在指定的位置 end 开始查找元素最后出现的位置</p>
</td>
</tr>
<tr>
<td>28</td>
<td>
<p><b>def length: Int</b></p>
<p>返回列表长度</p>
</td>
</tr>
<tr>
<td>29</td>
<td>
<p><b>def map[B](f: (A) =&gt; B): List[B]</b></p>
<p>通过给定的方法将所有元素重新计算</p>
</td>
</tr>
<tr>
<td>30</td>
<td>
<p><b>def max: A</b></p>
<p>查找最大元素</p>
</td>
</tr>
<tr>
<td>31</td>
<td>
<p><b>def min: A</b></p>
<p>查找最小元素</p>
</td>
</tr>
<tr>
<td>32</td>
<td>
<p><b>def mkString: String</b></p>
<p>列表所有元素作为字符串显示</p>
</td>
</tr>
<tr>
<td>33</td>
<td>
<p><b>def mkString(sep: String): String</b></p>
<p>使用分隔符将列表所有元素作为字符串显示</p>
</td>
</tr>
<tr>
<td>34</td>
<td>
<p><b>def reverse: List[A]</b></p>
<p>列表反转</p>
</td>
</tr>
<tr>
<td>35</td>
<td>
<p><b>def sorted[B &gt;: A]: List[A]</b></p>
<p>列表排序</p>
</td>
</tr>
<tr>
<td>36</td>
<td>
<p><b>def startsWith[B](that: Seq[B], offset: Int): Boolean</b></p>
<p>检测列表在指定位置是否包含指定序列</p>
</td>
</tr>
<tr>
<td>37</td>
<td>
<p><b>def sum: A</b></p>
<p>计算集合元素之和</p>
</td>
</tr>
<tr>
<td>38</td>
<td>
<p><b>def tail: List[A]</b></p>
<p>返回所有元素，除了第一个</p>
</td>
</tr>
<tr>
<td>39</td>
<td>
<p><b>def take(n: Int): List[A]</b></p>
<p>提取列表的前n个元素</p>
</td>
</tr>
<tr>
<td>40</td>
<td>
<p><b>def takeRight(n: Int): List[A]</b></p>
<p>提取列表的后n个元素</p>
</td>
</tr>
<tr>
<td>41</td>
<td>
<p><b>def toArray: Array[A]</b></p>
<p>列表转换为数组</p>
</td>
</tr>
<tr>
<td>42</td>
<td>
<p><b>def toBuffer[B &gt;: A]: Buffer[B]</b></p>
<p>返回缓冲区，包含了列表的所有元素</p>
</td>
</tr>
<tr>
<td>43</td>
<td>
<p><b>def toMap[T, U]: Map[T, U]</b></p>
<p>List 转换为 Map</p>
</td>
</tr>
<tr>
<td>44</td>
<td>
<p><b>def toSeq: Seq[A]</b></p>
<p>List 转换为 Seq</p>
</td>
</tr>
<tr>
<td>45</td>
<td>
<p><b>def toSet[B &gt;: A]: Set[B]</b></p>
<p>List 转换为 Set</p>
</td>
</tr>
<tr>
<td>46</td>
<td>
<p><b>def toString(): String</b></p>
<p>列表转换为字符串</p>
</td>
</tr>
</tbody></table></div>') where ID=8405;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th>序号</th>
<th>类型和描述</th>
</tr>
<tr>
<td>1</td>
<td><b>标量</b>
<p>标量是 Perl 语言中最简单的一种数据类型。这种数据类型的变量可以是数字，字符串，浮点数，不作严格的区分。在使用时在变量的名字前面加上一个 <span class="marked">$</span>，表示是标量。例如：</p>
<pre>
$myfirst=123;　    #数字123　

$mysecond="123";   #字符串123　
</pre>

</td>
</tr>
<tr>
<td>2</td>
<td><b>数组</b>
<p>数组变量以字符 <span class="marked">@</span> 开头，索引从 0 开始，如：<span class="marked">@arr=(1,2,3)</span></p>
<pre>
@arr=(1,2,3)
</pre>

</td>
</tr>
<tr>
<td>3</td>
<td><b>哈希</b>
<p>哈希是一个无序的 <strong>key/value</strong> 对集合。可以使用键作为下标获取值。哈希变量以字符 <span class="marked">%</span> 开头。</p>
<pre>
%h=('a'=&gt;1,'b'=&gt;2); 
</pre>

</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th>序号</th>
<th>类型和描述</th>
</tr>
<tr>
<td>1</td>
<td><b>标量</b>
<p>标量是 Perl 语言中最简单的一种数据类型。这种数据类型的变量可以是数字，字符串，浮点数，不作严格的区分。在使用时在变量的名字前面加上一个 <span class="marked">$</span>，表示是标量。例如：</p>
<pre>
$myfirst=123;　    #数字123　

$mysecond="123";   #字符串123　
</pre>

</td>
</tr>
<tr>
<td>2</td>
<td><b>数组</b>
<p>数组变量以字符 <span class="marked">@</span> 开头，索引从 0 开始，如：<span class="marked">@arr=(1,2,3)</span></p>
<pre>
@arr=(1,2,3)
</pre>

</td>
</tr>
<tr>
<td>3</td>
<td><b>哈希</b>
<p>哈希是一个无序的 <strong>key/value</strong> 对集合。可以使用键作为下标获取值。哈希变量以字符 <span class="marked">%</span> 开头。</p>
<pre>
%h=('a'=&gt;1,'b'=&gt;2); 
</pre>

</td>
</tr>
</tbody></table></div>') where ID=8490;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tr>
<td width="30%"><p align="center">$n</td>
<td width="70%"  align="left">包含上次模式匹配的第n个子串</td>
</tr>
<tr>
<td width="30%"><p align="center">$&amp;</td>
<td width="70%" rowspan="2" align="left">前一次成功模式匹配的字符串</td>
</tr>
<tr>
<td width="30%"><p align="center">$MATCH</td>
</tr>
<tr>
<td width="30%"><p align="center">$`</td>
<td width="70%" rowspan="2" align="left">前次匹配成功的子串之前的内容</td>
</tr>
<tr>
<td width="30%"><p align="center">$PREMATCH</td>
</tr>
<tr>
<td width="30%"><p align="center">$'</td>
<td width="70%" rowspan="2" align="left">前次匹配成功的子串之后的内容</td>
</tr>
<tr>
<td width="30%">
    <p align="center">$POSTMATCH</td>
  </tr>
  <tr>
    <td width="30%">
    <p align="center">$+</td>
    <td width="70%" rowspan="2" align="left">
<p>与上个正则表达式搜索格式匹配的最后一个括号。例如：</p>
<pre>/Version: (.*)|Revision: (.*)/ &amp;&amp; ($rev = $+);</pre>
    </td>
  </tr>
  <tr>
    <td width="30%">
    <p align="center">$LAST_PAREN_MATCH</td>
  </tr>
</table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tr>
<td width="30%"><p align="center">$n</td>
<td width="70%"  align="left">包含上次模式匹配的第n个子串</td>
</tr>
<tr>
<td width="30%"><p align="center">$&amp;</td>
<td width="70%" rowspan="2" align="left">前一次成功模式匹配的字符串</td>
</tr>
<tr>
<td width="30%"><p align="center">$MATCH</td>
</tr>
<tr>
<td width="30%"><p align="center">$`</td>
<td width="70%" rowspan="2" align="left">前次匹配成功的子串之前的内容</td>
</tr>
<tr>
<td width="30%"><p align="center">$PREMATCH</td>
</tr>
<tr>
<td width="30%"><p align="center">$'</td>
<td width="70%" rowspan="2" align="left">前次匹配成功的子串之后的内容</td>
</tr>
<tr>
<td width="30%">
    <p align="center">$POSTMATCH</td>
  </tr>
  <tr>
    <td width="30%">
    <p align="center">$+</td>
    <td width="70%" rowspan="2" align="left">
<p>与上个正则表达式搜索格式匹配的最后一个括号。例如：</p>
<pre>/Version: (.*)|Revision: (.*)/ &amp;&amp; ($rev = $+);</pre>
    </td>
  </tr>
  <tr>
    <td width="30%">
    <p align="center">$LAST_PAREN_MATCH</td>
  </tr>
</table></div>') where ID=8523;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<thead>
<tr>
<th>操作系统</th>
<th>任务</th>
<th>命令</th>
</tr>
</thead>
<tbody>
<tr>
<td>Windows</td>
<td>打开命令控制台</td>
<td><pre>c:\&gt; java -version</pre></td>
</tr>
<tr>
<td>Linux</td>
<td>打开命令终端</td>
<td><pre># java -version</pre></td>
</tr>
<tr>
<td>Mac</td>
<td>打开终端</td>
<td><pre>$ java -version</pre></td>
</tr>
</tbody>
</table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<thead>
<tr>
<th>操作系统</th>
<th>任务</th>
<th>命令</th>
</tr>
</thead>
<tbody>
<tr>
<td>Windows</td>
<td>打开命令控制台</td>
<td><pre>c:\&gt; java -version</pre></td>
</tr>
<tr>
<td>Linux</td>
<td>打开命令终端</td>
<td><pre># java -version</pre></td>
</tr>
<tr>
<td>Mac</td>
<td>打开终端</td>
<td><pre>$ java -version</pre></td>
</tr>
</tbody>
</table></div>') where ID=8646;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:20%;">系统</th>
<th>配置</th>
</tr>
<tr>
<td>Windows</td>
<td><p>右键 "计算机"，选择 "属性"，之后点击 "高级系统设置"，点击"环境变量"，来设置环境变量，有以下系统变量需要配置：</p>

<p>新建系统变量 <strong>MAVEN_HOME</strong>，变量值：<span class="marked">E:\Maven\apache-maven-3.3.9</span></p>
<p><img src="/images/maven/1536057115-1481-20151218175411912-170761788.png"></p>
<p>编辑系统变量 <strong>Path</strong>，添加变量值：<span class="marked">;%MAVEN_HOME%\bin</span></p>

<p><img src="/images/maven/1536057115-7470-20151218175417006-1644078150.png"></p>

<p><strong>注意：</strong>注意多个值之间需要有分号隔开，然后点击确定。</p>
</td>
</tr>
<tr>
<td>Linux</td>
<td><p>下载解压：</p>
<pre># wget http://mirrors.hust.edu.cn/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
# tar -xvf  apache-maven-3.3.9-bin.tar.gz
# sudo mv -f apache-maven-3.3.9 /usr/local/</pre>
<p>编辑 <strong>/etc/profile</strong> 文件 <span class="marked">sudo vim /etc/profile</span>，在文件末尾添加如下代码：</p>
<pre>export MAVEN_HOME=/usr/local/apache-maven-3.3.9
export PATH=${PATH}:${MAVEN_HOME}/bin</pre>
<p>保存文件，并运行如下命令使环境变量生效：</p>
<p># source /etc/profile</p>
<p>在控制台输入如下命令，如果能看到 Maven 相关版本信息，则说明 Maven 已经安装成功：</p>
<pre># mvn -v</pre>
</td>
</tr>
<tr>
<td>Mac</td>
<td><p>下载解压：</p>
<pre>$ curl -O http://mirrors.hust.edu.cn/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
$ tar -xvf  apache-maven-3.3.9-bin.tar.gz
$ sudo mv -f apache-maven-3.3.9 /usr/local/</pre>
<p>编辑 <strong>/etc/profile</strong> 文件 <span class="marked">sudo vim /etc/profile</span>，在文件末尾添加如下代码：</p>

<pre>export MAVEN_HOME=/usr/local/apache-maven-3.3.9
export PATH=${PATH}:${MAVEN_HOME}/bin</pre>
<p>保存文件，并运行如下命令使环境变量生效：</p>
<pre>$ source /etc/profile</pre>
<p>在控制台输入如下命令，如果能看到 Maven 相关版本信息，则说明 Maven 已经安装成功：</p>
<pre>$ mvn -v
Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-11T00:41:47+08:00)
Maven home: /usr/local/apache-maven-3.3.9
Java version: 1.8.0_31, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_31.jdk/Contents/Home/jre
Default locale: zh_CN, platform encoding: ISO8859-1
OS name: "mac os x", version: "10.13.4", arch: "x86_64", family: "mac"
</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="width:20%;">系统</th>
<th>配置</th>
</tr>
<tr>
<td>Windows</td>
<td><p>右键 "计算机"，选择 "属性"，之后点击 "高级系统设置"，点击"环境变量"，来设置环境变量，有以下系统变量需要配置：</p>

<p>新建系统变量 <strong>MAVEN_HOME</strong>，变量值：<span class="marked">E:\Maven\apache-maven-3.3.9</span></p>
<p><img src="/images/maven/1536057115-1481-20151218175411912-170761788.png"></p>
<p>编辑系统变量 <strong>Path</strong>，添加变量值：<span class="marked">;%MAVEN_HOME%\bin</span></p>

<p><img src="/images/maven/1536057115-7470-20151218175417006-1644078150.png"></p>

<p><strong>注意：</strong>注意多个值之间需要有分号隔开，然后点击确定。</p>
</td>
</tr>
<tr>
<td>Linux</td>
<td><p>下载解压：</p>
<pre># wget http://mirrors.hust.edu.cn/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
# tar -xvf  apache-maven-3.3.9-bin.tar.gz
# sudo mv -f apache-maven-3.3.9 /usr/local/</pre>
<p>编辑 <strong>/etc/profile</strong> 文件 <span class="marked">sudo vim /etc/profile</span>，在文件末尾添加如下代码：</p>
<pre>export MAVEN_HOME=/usr/local/apache-maven-3.3.9
export PATH=${PATH}:${MAVEN_HOME}/bin</pre>
<p>保存文件，并运行如下命令使环境变量生效：</p>
<p># source /etc/profile</p>
<p>在控制台输入如下命令，如果能看到 Maven 相关版本信息，则说明 Maven 已经安装成功：</p>
<pre># mvn -v</pre>
</td>
</tr>
<tr>
<td>Mac</td>
<td><p>下载解压：</p>
<pre>$ curl -O http://mirrors.hust.edu.cn/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
$ tar -xvf  apache-maven-3.3.9-bin.tar.gz
$ sudo mv -f apache-maven-3.3.9 /usr/local/</pre>
<p>编辑 <strong>/etc/profile</strong> 文件 <span class="marked">sudo vim /etc/profile</span>，在文件末尾添加如下代码：</p>

<pre>export MAVEN_HOME=/usr/local/apache-maven-3.3.9
export PATH=${PATH}:${MAVEN_HOME}/bin</pre>
<p>保存文件，并运行如下命令使环境变量生效：</p>
<pre>$ source /etc/profile</pre>
<p>在控制台输入如下命令，如果能看到 Maven 相关版本信息，则说明 Maven 已经安装成功：</p>
<pre>$ mvn -v
Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-11T00:41:47+08:00)
Maven home: /usr/local/apache-maven-3.3.9
Java version: 1.8.0_31, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_31.jdk/Contents/Home/jre
Default locale: zh_CN, platform encoding: ISO8859-1
OS name: "mac os x", version: "10.13.4", arch: "x86_64", family: "mac"
</pre>
</td>
</tr>
</tbody></table></div>') where ID=8646;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<thead>
<tr>
  <th>节点</th>
  <th>描述</th>
</tr>
</thead>
<tbody>

<tr>
  <td>project</td>
  <td>工程的根标签。</td>
</tr>
<tr>
  <td>modelVersion</td>
  <td>模型版本需要设置为 4.0。</td>
</tr>

<tr>
  <td>groupId</td>
  <td>这是工程组的标识。它在一个组织或者项目中通常是唯一的。例如，一个银行组织 com.companyname.project-group 拥有所有的和银行相关的项目。</td>
</tr>
<tr>
  <td>artifactId</td>
  <td>这是工程的标识。它通常是工程的名称。例如，消费者银行。groupId 和 artifactId 一起定义了 artifact 在仓库中的位置。</td>
</tr>
<tr>
  <td>version</td>
  <td>
  <p>这是工程的版本号。在 artifact 的仓库中，它用来区分不同的版本。例如：</p>
<pre>com.company.bank:consumer-banking:1.0
com.company.bank:consumer-banking:1.1</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<thead>
<tr>
  <th>节点</th>
  <th>描述</th>
</tr>
</thead>
<tbody>

<tr>
  <td>project</td>
  <td>工程的根标签。</td>
</tr>
<tr>
  <td>modelVersion</td>
  <td>模型版本需要设置为 4.0。</td>
</tr>

<tr>
  <td>groupId</td>
  <td>这是工程组的标识。它在一个组织或者项目中通常是唯一的。例如，一个银行组织 com.companyname.project-group 拥有所有的和银行相关的项目。</td>
</tr>
<tr>
  <td>artifactId</td>
  <td>这是工程的标识。它通常是工程的名称。例如，消费者银行。groupId 和 artifactId 一起定义了 artifact 在仓库中的位置。</td>
</tr>
<tr>
  <td>version</td>
  <td>
  <p>这是工程的版本号。在 artifact 的仓库中，它用来区分不同的版本。例如：</p>
<pre>com.company.bank:consumer-banking:1.0
com.company.bank:consumer-banking:1.1</pre>
</td>
</tr>
</tbody></table></div>') where ID=8647;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center; width:10%;">序号</th>
<th style="text-align:center;">编译参数说明</th>
</tr>
<tr>
<td class="ts">1.</td>
<td><p><b>--help</b></p>
<p>显示帮助信息</p>
</td>
</tr>
<tr>
<td class="ts">2.</td>
<td><p><b>--module</b></p>
<p>载入扩展模块</p>
</td>
</tr>
<tr>
<td class="ts">3.</td>
<td><p><b>--target</b></p>
<p>设置 ECMA 版本</p>
</td>
</tr>
<tr>
<td class="ts">4.</td>
<td><p><b>--declaration</b></p>
<p>额外生成一个 .d.ts 扩展名的文件。</p>
<pre>
tsc ts-hw.ts --declaration
</pre><p>
以上命令会生成 ts-hw.d.ts、ts-hw.js 两个文件。</p>
</td>
</tr>
<tr>
<td class="ts">5.</td>
<td><p><b>--removeComments</b></p>
<p>删除文件的注释</p>
</td>
</tr>
<tr>
<td class="ts">6.</td>
<td><p><b>--out</b></p>
<p>编译多个文件并合并到一个输出的文件</p>
</td>
</tr>
<tr>
<td class="ts">7.</td>
<td><p><b>--sourcemap</b></p>
<p>生成一个 sourcemap (.map) 文件。</p>
<p>sourcemap 是一个存储源代码与编译代码对应位置映射的信息文件。</p>
</td>
</tr>
<tr>
<td class="ts">8.</td>
<td><p><b>--module noImplicitAny</b></p>
<p>在表达式和声明上有隐含的 any 类型时报错</p>
</td>
</tr>
<tr>
<td class="ts">9.</td>
<td><p><b>--watch</b></p>
<p>在监视模式下运行编译器。会监视输出文件，在它们改变时重新编译。</p>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center; width:10%;">序号</th>
<th style="text-align:center;">编译参数说明</th>
</tr>
<tr>
<td class="ts">1.</td>
<td><p><b>--help</b></p>
<p>显示帮助信息</p>
</td>
</tr>
<tr>
<td class="ts">2.</td>
<td><p><b>--module</b></p>
<p>载入扩展模块</p>
</td>
</tr>
<tr>
<td class="ts">3.</td>
<td><p><b>--target</b></p>
<p>设置 ECMA 版本</p>
</td>
</tr>
<tr>
<td class="ts">4.</td>
<td><p><b>--declaration</b></p>
<p>额外生成一个 .d.ts 扩展名的文件。</p>
<pre>
tsc ts-hw.ts --declaration
</pre><p>
以上命令会生成 ts-hw.d.ts、ts-hw.js 两个文件。</p>
</td>
</tr>
<tr>
<td class="ts">5.</td>
<td><p><b>--removeComments</b></p>
<p>删除文件的注释</p>
</td>
</tr>
<tr>
<td class="ts">6.</td>
<td><p><b>--out</b></p>
<p>编译多个文件并合并到一个输出的文件</p>
</td>
</tr>
<tr>
<td class="ts">7.</td>
<td><p><b>--sourcemap</b></p>
<p>生成一个 sourcemap (.map) 文件。</p>
<p>sourcemap 是一个存储源代码与编译代码对应位置映射的信息文件。</p>
</td>
</tr>
<tr>
<td class="ts">8.</td>
<td><p><b>--module noImplicitAny</b></p>
<p>在表达式和声明上有隐含的 any 类型时报错</p>
</td>
</tr>
<tr>
<td class="ts">9.</td>
<td><p><b>--watch</b></p>
<p>在监视模式下运行编译器。会监视输出文件，在它们改变时重新编译。</p>
</td>
</tr>
</tbody></table></div>') where ID=8692;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th>数据类型</th>
<th>关键字</th>
<th>描述</th>
</tr>
<tr>
<td>任意类型</td>
<td>any</td>
<td>声明为 any 的变量可以赋予任意类型的值。</td>
</tr>
<tr>
<td class="ts">数字类型</td>
<td class="ts">number</td>
<td><p>双精度 64 位浮点值。它可以用来表示整数和分数。</p>

<pre>let binaryLiteral: number = 0b1010; // 二进制
let octalLiteral: number = 0o744;    // 八进制
let decLiteral: number = 6;    // 十进制
let hexLiteral: number = 0xf00d;    // 十六进制</pre>
</td>
</tr>
<tr>
<td>字符串类型</td>
<td>string</td>
<td><p>一个字符系列，使用单引号（<span class="marked">'</span>）或双引号（<span class="marked">"</span>）来表示字符串类型。反引号（<span class="marked">`</span>）来定义多行文本和内嵌表达式。</p>

<pre>let name: string = "Runoob";
let years: number = 5;
let words: string = `您好，今年是 ${ name } 发布 ${ years + 1} 周年`;</pre>
</td>
</tr>
<tr>
<td>布尔类型</td>
<td>boolean</td>
<td><p>表示逻辑值：true 和 false。</p>


<pre>let flag: boolean = true;</pre>
</td>
</tr>

<tr>
<td>数组类型</td>
<td>无</td>
<td><p>声明变量为数组。</p>
<pre>// 在元素类型后面加上[]
let arr: number[] = [1, 2];

// 或者使用数组泛型
let arr: Array&lt;number&gt; = [1, 2];</pre>

</td>
</tr>

<tr>
<td>元组</td>
<td>无</td>
<td><p>元组类型用来表示已知元素数量和类型的数组，各元素的类型不必相同，对应位置的类型需要相同。</p>
<pre>let x: [string, number];
x = ['Runoob', 1];    // 运行正常
x = [1, 'Runoob'];    // 报错
console.log(x[0]);    // 输出 Runoob</pre>

</td>
</tr>

<tr>
<td>枚举</td>
<td>enum</td>
<td><p>枚举类型用于定义数值集合。</p>
<pre>enum Color {Red, Green, Blue};
let c: Color = Color.Blue;
console.log(c);    // 输出 2</pre>



</td>
</tr>

<tr>
<td class="ts">void</td>
<td class="ts">void</td>
<td><p>用于标识方法返回值的类型，表示该方法没有返回值。</p>
<pre>function hello(): void {
    alert("Hello Runoob");
}</pre>

</td>
</tr>
<tr>
<td>null</td>
<td>null</td>
<td><p>表示对象值缺失。</p></td>
</tr>
<tr>
<td>undefined</td>
<td>undefined</td>
<td><p>用于初始化变量为一个未定义的值</p></td>
</tr>

<tr>
<td>never</td>
<td>never</td>
<td><p>never 是其它类型（包括 null 和 undefined）的子类型，代表从不会出现的值。</p></td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th>数据类型</th>
<th>关键字</th>
<th>描述</th>
</tr>
<tr>
<td>任意类型</td>
<td>any</td>
<td>声明为 any 的变量可以赋予任意类型的值。</td>
</tr>
<tr>
<td class="ts">数字类型</td>
<td class="ts">number</td>
<td><p>双精度 64 位浮点值。它可以用来表示整数和分数。</p>

<pre>let binaryLiteral: number = 0b1010; // 二进制
let octalLiteral: number = 0o744;    // 八进制
let decLiteral: number = 6;    // 十进制
let hexLiteral: number = 0xf00d;    // 十六进制</pre>
</td>
</tr>
<tr>
<td>字符串类型</td>
<td>string</td>
<td><p>一个字符系列，使用单引号（<span class="marked">'</span>）或双引号（<span class="marked">"</span>）来表示字符串类型。反引号（<span class="marked">`</span>）来定义多行文本和内嵌表达式。</p>

<pre>let name: string = "Runoob";
let years: number = 5;
let words: string = `您好，今年是 ${ name } 发布 ${ years + 1} 周年`;</pre>
</td>
</tr>
<tr>
<td>布尔类型</td>
<td>boolean</td>
<td><p>表示逻辑值：true 和 false。</p>


<pre>let flag: boolean = true;</pre>
</td>
</tr>

<tr>
<td>数组类型</td>
<td>无</td>
<td><p>声明变量为数组。</p>
<pre>// 在元素类型后面加上[]
let arr: number[] = [1, 2];

// 或者使用数组泛型
let arr: Array&lt;number&gt; = [1, 2];</pre>

</td>
</tr>

<tr>
<td>元组</td>
<td>无</td>
<td><p>元组类型用来表示已知元素数量和类型的数组，各元素的类型不必相同，对应位置的类型需要相同。</p>
<pre>let x: [string, number];
x = ['Runoob', 1];    // 运行正常
x = [1, 'Runoob'];    // 报错
console.log(x[0]);    // 输出 Runoob</pre>

</td>
</tr>

<tr>
<td>枚举</td>
<td>enum</td>
<td><p>枚举类型用于定义数值集合。</p>
<pre>enum Color {Red, Green, Blue};
let c: Color = Color.Blue;
console.log(c);    // 输出 2</pre>



</td>
</tr>

<tr>
<td class="ts">void</td>
<td class="ts">void</td>
<td><p>用于标识方法返回值的类型，表示该方法没有返回值。</p>
<pre>function hello(): void {
    alert("Hello Runoob");
}</pre>

</td>
</tr>
<tr>
<td>null</td>
<td>null</td>
<td><p>表示对象值缺失。</p></td>
</tr>
<tr>
<td>undefined</td>
<td>undefined</td>
<td><p>用于初始化变量为一个未定义的值</p></td>
</tr>

<tr>
<td>never</td>
<td>never</td>
<td><p>never 是其它类型（包括 null 和 undefined）的子类型，代表从不会出现的值。</p></td>
</tr>
</tbody></table></div>') where ID=8693;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
    <tbody><tr>
    <th style="text-align:center;">序号</th>
    <th style="text-align:center;">方法 &amp; 描述</th>
    <th style="text-align:center;">实例</th>
    </tr>
    <tr>
    <td>1.</td>
    <td>toExponential()
    <p>把对象的值转换为指数计数法。</p></td>
    <td>
<pre>
//toExponential() 
var num1 = 1225.30 
var val = num1.toExponential(); 
console.log(val) // 输出： 1.2253e+3
</pre>
    </td>
    </tr>
    <tr>
    <td>2.</td>
    <td>toFixed()
    <p>把数字转换为字符串，并对小数点指定位数。</p></td>
    <td>
<pre>
var num3 = 177.234 
console.log("num3.toFixed() 为 "+num3.toFixed())    // 输出：177
console.log("num3.toFixed(2) 为 "+num3.toFixed(2))  // 输出：177.23
console.log("num3.toFixed(6) 为 "+num3.toFixed(6))  // 输出：177.234000
</pre>
    </td>
    </tr>
    <tr>
    <td>3.</td>
    <td>toLocaleString()
    <p>把数字转换为字符串，使用本地数字格式顺序。</p></td>
    <td>

<pre>
var num = new Number(177.1234); 
console.log( num.toLocaleString());  // 输出：177.1234
</pre>
    </td>
    </tr>
    <tr>
    <td>4.</td>
    <td>toPrecision()
    <p>把数字格式化为指定的长度。</p></td>
    <td>
<pre>
var num = new Number(7.123456); 
console.log(num.toPrecision());  // 输出：7.123456 
console.log(num.toPrecision(1)); // 输出：7
console.log(num.toPrecision(2)); // 输出：7.1
</pre>
    </td>
    </tr>
    <tr>
    <td>5.</td>
    <td>toString()
    <p>把数字转换为字符串，使用指定的基数。数字的基数是 2 ~ 36 之间的整数。若省略该参数，则使用基数 10。</p></td>
    <td>
<pre>
var num = new Number(10); 
console.log(num.toString());  // 输出10进制：10
console.log(num.toString(2)); // 输出2进制：1010
console.log(num.toString(8)); // 输出8进制：12
</pre>

    </td>
    </tr>
    <tr>
    <td>6.</td>
    <td>valueOf()
    <p>返回一个 Number 对象的原始数字值。</p></td>
<td><pre>
var num = new Number(10); 
console.log(num.valueOf()); // 输出：10
</pre>
</td>
    </tr>
    </tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
    <tbody><tr>
    <th style="text-align:center;">序号</th>
    <th style="text-align:center;">方法 &amp; 描述</th>
    <th style="text-align:center;">实例</th>
    </tr>
    <tr>
    <td>1.</td>
    <td>toExponential()
    <p>把对象的值转换为指数计数法。</p></td>
    <td>
<pre>
//toExponential() 
var num1 = 1225.30 
var val = num1.toExponential(); 
console.log(val) // 输出： 1.2253e+3
</pre>
    </td>
    </tr>
    <tr>
    <td>2.</td>
    <td>toFixed()
    <p>把数字转换为字符串，并对小数点指定位数。</p></td>
    <td>
<pre>
var num3 = 177.234 
console.log("num3.toFixed() 为 "+num3.toFixed())    // 输出：177
console.log("num3.toFixed(2) 为 "+num3.toFixed(2))  // 输出：177.23
console.log("num3.toFixed(6) 为 "+num3.toFixed(6))  // 输出：177.234000
</pre>
    </td>
    </tr>
    <tr>
    <td>3.</td>
    <td>toLocaleString()
    <p>把数字转换为字符串，使用本地数字格式顺序。</p></td>
    <td>

<pre>
var num = new Number(177.1234); 
console.log( num.toLocaleString());  // 输出：177.1234
</pre>
    </td>
    </tr>
    <tr>
    <td>4.</td>
    <td>toPrecision()
    <p>把数字格式化为指定的长度。</p></td>
    <td>
<pre>
var num = new Number(7.123456); 
console.log(num.toPrecision());  // 输出：7.123456 
console.log(num.toPrecision(1)); // 输出：7
console.log(num.toPrecision(2)); // 输出：7.1
</pre>
    </td>
    </tr>
    <tr>
    <td>5.</td>
    <td>toString()
    <p>把数字转换为字符串，使用指定的基数。数字的基数是 2 ~ 36 之间的整数。若省略该参数，则使用基数 10。</p></td>
    <td>
<pre>
var num = new Number(10); 
console.log(num.toString());  // 输出10进制：10
console.log(num.toString(2)); // 输出2进制：1010
console.log(num.toString(8)); // 输出8进制：12
</pre>

    </td>
    </tr>
    <tr>
    <td>6.</td>
    <td>valueOf()
    <p>返回一个 Number 对象的原始数字值。</p></td>
<td><pre>
var num = new Number(10); 
console.log(num.valueOf()); // 输出：10
</pre>
</td>
    </tr>
    </tbody></table></div>') where ID=8699;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center;">序号</th>
<th style="text-align:center;">属性 &amp; 描述</th>
<th style="text-align:center;">实例</th>
</tr>
<tr>
<td>1.</td>
<td>constructor
<p>对创建该对象的函数的引用。</p></td>
<td>
<pre>var str = new String( "This is string" ); 
console.log("str.constructor is:" + str.constructor)</pre>
<p>输出结果：</p>
<pre>str.constructor is:function String() { [native code] }</pre>
</td>
</tr>
<tr>
<td>2.</td>
<td>length
<p>返回字符串的长度。</p></td>
<td>
<pre>var uname = new String("Hello World") 
console.log("Length "+uname.length)  // 输出 11</pre></td>
</tr>
<tr>
<td>3.</td>
<td>prototype
<p>允许您向对象添加属性和方法。</p></td>
<td>
<pre>function employee(id:number,name:string) { 
    this.id = id 
    this.name = name 
 } 
 var emp = new employee(123,"admin") 
 employee.prototype.email="admin@panku.in" // 添加属性 email
 console.log("员工号: "+emp.id) 
 console.log("员工姓名: "+emp.name) 
 console.log("员工邮箱: "+emp.email)</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center;">序号</th>
<th style="text-align:center;">属性 &amp; 描述</th>
<th style="text-align:center;">实例</th>
</tr>
<tr>
<td>1.</td>
<td>constructor
<p>对创建该对象的函数的引用。</p></td>
<td>
<pre>var str = new String( "This is string" ); 
console.log("str.constructor is:" + str.constructor)</pre>
<p>输出结果：</p>
<pre>str.constructor is:function String() { [native code] }</pre>
</td>
</tr>
<tr>
<td>2.</td>
<td>length
<p>返回字符串的长度。</p></td>
<td>
<pre>var uname = new String("Hello World") 
console.log("Length "+uname.length)  // 输出 11</pre></td>
</tr>
<tr>
<td>3.</td>
<td>prototype
<p>允许您向对象添加属性和方法。</p></td>
<td>
<pre>function employee(id:number,name:string) { 
    this.id = id 
    this.name = name 
 } 
 var emp = new employee(123,"admin") 
 employee.prototype.email="admin@panku.in" // 添加属性 email
 console.log("员工号: "+emp.id) 
 console.log("员工姓名: "+emp.name) 
 console.log("员工邮箱: "+emp.email)</pre>
</td>
</tr>
</tbody></table></div>') where ID=8700;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center;">序号</th>
<th style="text-align:center;">方法 &amp; 描述</th>
<th style="text-align:center;">实例</th>
</tr>
<tr>
<td class="ts">1.</td>
<td>charAt()
<p>返回在指定位置的字符。</p></td>
<td><pre>
var str = new String("RUNOOB"); 
console.log("str.charAt(0) 为:" + str.charAt(0)); // R
console.log("str.charAt(1) 为:" + str.charAt(1)); // U 
console.log("str.charAt(2) 为:" + str.charAt(2)); // N 
console.log("str.charAt(3) 为:" + str.charAt(3)); // O 
console.log("str.charAt(4) 为:" + str.charAt(4)); // O 
console.log("str.charAt(5) 为:" + str.charAt(5)); // B
</pre>
</td>
</tr>
<tr>
<td class="ts">2.</td>
<td>charCodeAt()
<p>返回在指定的位置的字符的 Unicode 编码。</p></td>
<td><pre>
var str = new String("RUNOOB"); 
console.log("str.charCodeAt(0) 为:" + str.charCodeAt(0)); // 82
console.log("str.charCodeAt(1) 为:" + str.charCodeAt(1)); // 85 
console.log("str.charCodeAt(2) 为:" + str.charCodeAt(2)); // 78 
console.log("str.charCodeAt(3) 为:" + str.charCodeAt(3)); // 79 
console.log("str.charCodeAt(4) 为:" + str.charCodeAt(4)); // 79
console.log("str.charCodeAt(5) 为:" + str.charCodeAt(5)); // 66
</pre>
</td>
</tr>
<tr>
<td class="ts">3.</td>
<td>concat()
<p>连接两个或更多字符串，并返回新的字符串。</p></td>
<td><pre>
var str1 = new String( "RUNOOB" ); 
var str2 = new String( "GOOGLE" ); 
var str3 = str1.concat( str2 ); 
console.log("str1 + str2 : "+str3) // RUNOOBGOOGLE
</pre>
</td>
</tr>
<tr>
<td class="ts">4.</td>
<td>indexOf()
<p>返回某个指定的字符串值在字符串中首次出现的位置。</p></td>
<td><pre>
var str1 = new String( "RUNOOB" ); 

var index = str1.indexOf( "OO" ); 
console.log("查找的字符串位置 :" + index );  // 3
</pre>
</td>
</tr>
<tr>
<td class="ts">5.</td>
<td>lastIndexOf()
<p>从后向前搜索字符串，并从起始位置（0）开始计算返回字符串最后出现的位置。</p></td>
<td><pre>
var str1 = new String( "This is string one and again string" ); 
var index = str1.lastIndexOf( "string" );
console.log("lastIndexOf 查找到的最后字符串位置 :" + index ); // 29
    
index = str1.lastIndexOf( "one" ); 
console.log("lastIndexOf 查找到的最后字符串位置 :" + index ); // 15
</pre>
</td>
</tr>
<tr>
<td class="ts">6.</td>
<td>localeCompare()
<p>用本地特定的顺序来比较两个字符串。</p></td>
<td><pre>
var str1 = new String( "This is beautiful string" );
  
var index = str1.localeCompare( "This is beautiful string");  

console.log("localeCompare first :" + index );  // 0
</pre>
</td>
</tr>
<tr>
<td class="ts">7.</td>
<td><p><b>match()</b></p>
<p>查找找到一个或多个正则表达式的匹配。</p></td>
<td><pre>
var str="The rain in SPAIN stays mainly in the plain"; 
var n=str.match(/ain/g);  // ain,ain,ain
</pre>
</td>
</tr>
<tr>
<td class="ts">8.</td>
<td>replace()
<p>替换与正则表达式匹配的子串</p></td>
<td><pre>
var re = /(\w+)\s(\w+)/; 
var str = "zara ali"; 
var newstr = str.replace(re, "$2, $1"); 
console.log(newstr); // ali, zara
</pre>
</td>
</tr>
<tr>
<td class="ts">9.</td>
<td>search()
<p>检索与正则表达式相匹配的值</p></td>
<td><pre>
var re = /apples/gi; 
var str = "Apples are round, and apples are juicy.";
if (str.search(re) == -1 ) { 
   console.log("Does not contain Apples" ); 
} else { 
   console.log("Contains Apples" ); 
} 
</pre>
</td>
</tr>
<tr>
<td class="ts">10.</td>
<td>slice()
<p>提取字符串的片断，并在新的字符串中返回被提取的部分。</p></td>
</tr>
<tr>
<td class="ts">11.</td>
<td>split()
<p>把字符串分割为子字符串数组。</p></td>
<td><pre>
var str = "Apples are round, and apples are juicy."; 
var splitted = str.split(" ", 3); 
console.log(splitted)  // [ 'Apples', 'are', 'round,' ]
</pre>
</td>
</tr>
<tr>
<td class="ts">12.</td>
<td>substr()<p></p>
<p>从起始索引号提取字符串中指定数目的字符。</p></td>
</tr>
<tr>
<td class="ts">13.</td>
<td>substring()
<p>提取字符串中两个指定的索引号之间的字符。</p></td>
<td><pre>
var str = "RUNOOB GOOGLE TAOBAO FACEBOOK"; 
console.log("(1,2): "    + str.substring(1,2));   // U
console.log("(0,10): "   + str.substring(0, 10)); // RUNOOB GOO
console.log("(5): "      + str.substring(5));     // B GOOGLE TAOBAO FACEBOOK
</pre>
</td>
</tr>
<tr>
<td class="ts">14.</td>
<td>toLocaleLowerCase()
<p>根据主机的语言环境把字符串转换为小写，只有几种语言（如土耳其语）具有地方特有的大小写映射。</p></td>
<td><pre>
var str = "Runoob Google"; 
console.log(str.toLocaleLowerCase( ));  // runoob google
</pre>
</td>
</tr>
<tr>
<td class="ts">15.</td>
<td>toLocaleUpperCase()
<p>据主机的语言环境把字符串转换为大写，只有几种语言（如土耳其语）具有地方特有的大小写映射。</p></td>
<td><pre>
var str = "Runoob Google"; 
console.log(str.toLocaleUpperCase( ));  // RUNOOB GOOGLE
</pre>
</td>
</tr>
</tr>
<tr>
<td class="ts">16.</td>
<td>toLowerCase()
<p>把字符串转换为小写。</p></td>
<td><pre>
var str = "Runoob Google"; 
console.log(str.toLowerCase( ));  // runoob google
</pre>
</td>
</tr>
<tr>
<td class="ts">17.</td>
<td>toString()
<p>返回字符串。</p></td>
<td><pre>
var str = "Runoob"; 
console.log(str.toString( )); // Runoob
</pre>
</td>
</tr>
<tr>
<td class="ts">18.</td>
<td>toUpperCase()
<p>把字符串转换为大写。</p></td>
<td><pre>
var str = "Runoob Google"; 
console.log(str.toUpperCase( ));  // RUNOOB GOOGLE
</pre>
</td>
</tr>
<tr>
<td class="ts">19.</td>
<td>valueOf()
<p>返回指定字符串对象的原始值。</p></td>
<td><pre>
var str = new String("Runoob"); 
console.log(str.valueOf( ));  // Runoob
</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center;">序号</th>
<th style="text-align:center;">方法 &amp; 描述</th>
<th style="text-align:center;">实例</th>
</tr>
<tr>
<td class="ts">1.</td>
<td>charAt()
<p>返回在指定位置的字符。</p></td>
<td><pre>
var str = new String("RUNOOB"); 
console.log("str.charAt(0) 为:" + str.charAt(0)); // R
console.log("str.charAt(1) 为:" + str.charAt(1)); // U 
console.log("str.charAt(2) 为:" + str.charAt(2)); // N 
console.log("str.charAt(3) 为:" + str.charAt(3)); // O 
console.log("str.charAt(4) 为:" + str.charAt(4)); // O 
console.log("str.charAt(5) 为:" + str.charAt(5)); // B
</pre>
</td>
</tr>
<tr>
<td class="ts">2.</td>
<td>charCodeAt()
<p>返回在指定的位置的字符的 Unicode 编码。</p></td>
<td><pre>
var str = new String("RUNOOB"); 
console.log("str.charCodeAt(0) 为:" + str.charCodeAt(0)); // 82
console.log("str.charCodeAt(1) 为:" + str.charCodeAt(1)); // 85 
console.log("str.charCodeAt(2) 为:" + str.charCodeAt(2)); // 78 
console.log("str.charCodeAt(3) 为:" + str.charCodeAt(3)); // 79 
console.log("str.charCodeAt(4) 为:" + str.charCodeAt(4)); // 79
console.log("str.charCodeAt(5) 为:" + str.charCodeAt(5)); // 66
</pre>
</td>
</tr>
<tr>
<td class="ts">3.</td>
<td>concat()
<p>连接两个或更多字符串，并返回新的字符串。</p></td>
<td><pre>
var str1 = new String( "RUNOOB" ); 
var str2 = new String( "GOOGLE" ); 
var str3 = str1.concat( str2 ); 
console.log("str1 + str2 : "+str3) // RUNOOBGOOGLE
</pre>
</td>
</tr>
<tr>
<td class="ts">4.</td>
<td>indexOf()
<p>返回某个指定的字符串值在字符串中首次出现的位置。</p></td>
<td><pre>
var str1 = new String( "RUNOOB" ); 

var index = str1.indexOf( "OO" ); 
console.log("查找的字符串位置 :" + index );  // 3
</pre>
</td>
</tr>
<tr>
<td class="ts">5.</td>
<td>lastIndexOf()
<p>从后向前搜索字符串，并从起始位置（0）开始计算返回字符串最后出现的位置。</p></td>
<td><pre>
var str1 = new String( "This is string one and again string" ); 
var index = str1.lastIndexOf( "string" );
console.log("lastIndexOf 查找到的最后字符串位置 :" + index ); // 29
    
index = str1.lastIndexOf( "one" ); 
console.log("lastIndexOf 查找到的最后字符串位置 :" + index ); // 15
</pre>
</td>
</tr>
<tr>
<td class="ts">6.</td>
<td>localeCompare()
<p>用本地特定的顺序来比较两个字符串。</p></td>
<td><pre>
var str1 = new String( "This is beautiful string" );
  
var index = str1.localeCompare( "This is beautiful string");  

console.log("localeCompare first :" + index );  // 0
</pre>
</td>
</tr>
<tr>
<td class="ts">7.</td>
<td><p><b>match()</b></p>
<p>查找找到一个或多个正则表达式的匹配。</p></td>
<td><pre>
var str="The rain in SPAIN stays mainly in the plain"; 
var n=str.match(/ain/g);  // ain,ain,ain
</pre>
</td>
</tr>
<tr>
<td class="ts">8.</td>
<td>replace()
<p>替换与正则表达式匹配的子串</p></td>
<td><pre>
var re = /(\w+)\s(\w+)/; 
var str = "zara ali"; 
var newstr = str.replace(re, "$2, $1"); 
console.log(newstr); // ali, zara
</pre>
</td>
</tr>
<tr>
<td class="ts">9.</td>
<td>search()
<p>检索与正则表达式相匹配的值</p></td>
<td><pre>
var re = /apples/gi; 
var str = "Apples are round, and apples are juicy.";
if (str.search(re) == -1 ) { 
   console.log("Does not contain Apples" ); 
} else { 
   console.log("Contains Apples" ); 
} 
</pre>
</td>
</tr>
<tr>
<td class="ts">10.</td>
<td>slice()
<p>提取字符串的片断，并在新的字符串中返回被提取的部分。</p></td>
</tr>
<tr>
<td class="ts">11.</td>
<td>split()
<p>把字符串分割为子字符串数组。</p></td>
<td><pre>
var str = "Apples are round, and apples are juicy."; 
var splitted = str.split(" ", 3); 
console.log(splitted)  // [ 'Apples', 'are', 'round,' ]
</pre>
</td>
</tr>
<tr>
<td class="ts">12.</td>
<td>substr()<p></p>
<p>从起始索引号提取字符串中指定数目的字符。</p></td>
</tr>
<tr>
<td class="ts">13.</td>
<td>substring()
<p>提取字符串中两个指定的索引号之间的字符。</p></td>
<td><pre>
var str = "RUNOOB GOOGLE TAOBAO FACEBOOK"; 
console.log("(1,2): "    + str.substring(1,2));   // U
console.log("(0,10): "   + str.substring(0, 10)); // RUNOOB GOO
console.log("(5): "      + str.substring(5));     // B GOOGLE TAOBAO FACEBOOK
</pre>
</td>
</tr>
<tr>
<td class="ts">14.</td>
<td>toLocaleLowerCase()
<p>根据主机的语言环境把字符串转换为小写，只有几种语言（如土耳其语）具有地方特有的大小写映射。</p></td>
<td><pre>
var str = "Runoob Google"; 
console.log(str.toLocaleLowerCase( ));  // runoob google
</pre>
</td>
</tr>
<tr>
<td class="ts">15.</td>
<td>toLocaleUpperCase()
<p>据主机的语言环境把字符串转换为大写，只有几种语言（如土耳其语）具有地方特有的大小写映射。</p></td>
<td><pre>
var str = "Runoob Google"; 
console.log(str.toLocaleUpperCase( ));  // RUNOOB GOOGLE
</pre>
</td>
</tr>
</tr>
<tr>
<td class="ts">16.</td>
<td>toLowerCase()
<p>把字符串转换为小写。</p></td>
<td><pre>
var str = "Runoob Google"; 
console.log(str.toLowerCase( ));  // runoob google
</pre>
</td>
</tr>
<tr>
<td class="ts">17.</td>
<td>toString()
<p>返回字符串。</p></td>
<td><pre>
var str = "Runoob"; 
console.log(str.toString( )); // Runoob
</pre>
</td>
</tr>
<tr>
<td class="ts">18.</td>
<td>toUpperCase()
<p>把字符串转换为大写。</p></td>
<td><pre>
var str = "Runoob Google"; 
console.log(str.toUpperCase( ));  // RUNOOB GOOGLE
</pre>
</td>
</tr>
<tr>
<td class="ts">19.</td>
<td>valueOf()
<p>返回指定字符串对象的原始值。</p></td>
<td><pre>
var str = new String("Runoob"); 
console.log(str.valueOf( ));  // Runoob
</pre>
</td>
</tr>
</tbody></table></div>') where ID=8700;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center">序号</th>
<th style="text-align:center">方法 &amp; 描述</th>
<th style="text-align:center">实例</th>
</tr>
<tr>
<td>1.</td>
<td>concat()
<p>连接两个或更多的数组，并返回结果。</p></td>
<td>
<pre>
var alpha = ["a", "b", "c"]; 
var numeric = [1, 2, 3];

var alphaNumeric = alpha.concat(numeric); 
console.log("alphaNumeric : " + alphaNumeric );    // a,b,c,1,2,3   
</pre>
</td>
</tr>
<tr>
<td>2.</td>
<td>every()
<p>检测数值元素的每个元素是否都符合条件。</p></td>
<td>
<pre>
function isBigEnough(element, index, array) { 
        return (element &gt;= 10); 
} 
        
var passed = [12, 5, 8, 130, 44].every(isBigEnough); 
console.log("Test Value : " + passed ); // false
</pre>
</td>
</tr>
<tr>
<td>3.</td>
<td>filter()
<p>检测数值元素，并返回符合条件所有元素的数组。</p></td>
<td>
<pre>
function isBigEnough(element, index, array) { 
   return (element &gt;= 10); 
} 
          
var passed = [12, 5, 8, 130, 44].filter(isBigEnough); 
console.log("Test Value : " + passed ); // 12,130,44
</pre>
</td>
</tr>
<tr>
<td>4.</td>
<td>forEach()
<p>数组每个元素都执行一次回调函数。</p></td>
<td>
<pre>
let num = [7, 8, 9];
num.forEach(function (value) {
    console.log(value);
}); 
</pre><p>
编译成 JavaScript 代码：</p><pre>
var num = [7, 8, 9];
num.forEach(function (value) {
    console.log(value);  // 7   8   9
});</pre>
</td>
</tr>
<tr>
<td>5.</td>
<td>indexOf()
<p>搜索数组中的元素，并返回它所在的位置。</p></td>
<td>
<pre>
var index = [12, 5, 8, 130, 44].indexOf(8); 
console.log("index is : " + index );  // 2
</pre>
</td>
</tr>
<tr>
<td>6.</td>
<td>join()
<p>把数组的所有元素放入一个字符串。</p></td>
<td>
<pre>
var arr = new Array("First","Second","Third"); 
          
var str = arr.join(); 
console.log("str : " + str );  // First,Second,Third 
          
var str = arr.join(", "); 
console.log("str : " + str );  // First, Second, Third
          
var str = arr.join(" + "); 
console.log("str : " + str );  // First + Second + Third
</pre>
</td>
</tr>
<tr>
<td>7.</td>
<td>lastIndexOf()
<p>返回一个指定的字符串值最后出现的位置，在一个字符串中的指定位置从后向前搜索。</p></td>
<td>
<pre>
var index = [12, 5, 8, 130, 44].lastIndexOf(8); 
console.log("index is : " + index );  // 2
</pre>
</td>
</tr>
<tr>
<td>8.</td>
<td>map()
<p>通过指定函数处理数组的每个元素，并返回处理后的数组。</p></td>
<td>
<pre>
var numbers = [1, 4, 9]; 
var roots = numbers.map(Math.sqrt); 
console.log("roots is : " + roots );  // 1,2,3
</pre>
</td>
</tr>
<tr>
<td>9.</td>
<td>pop()
<p>删除数组的最后一个元素并返回删除的元素。</p></td>
<td>
<pre>
var numbers = [1, 4, 9]; 
          
var element = numbers.pop(); 
console.log("element is : " + element );  // 9
          
var element = numbers.pop(); 
console.log("element is : " + element );  // 4
</pre>
</td>
</tr>
<tr>
<td>10.</td>
<td>push()
<p>向数组的末尾添加一个或更多元素，并返回新的长度。</p></td>
<td>
<pre>
var numbers = new Array(1, 4, 9); 
var length = numbers.push(10); 
console.log("new numbers is : " + numbers );  // 1,4,9,10 
length = numbers.push(20); 
console.log("new numbers is : " + numbers );  // 1,4,9,10,20
</pre>
</td>
</tr>
<tr>
<td>11.</td>
<td>reduce()
<p>将数组元素计算为一个值（从左到右）。</p></td>
<td>
<pre>
var total = [0, 1, 2, 3].reduce(function(a, b){ return a + b; }); 
console.log("total is : " + total );  // 6
</pre>
</td>
</tr>
<tr>
<td>12.</td>
<td>reduceRight()
<p>将数组元素计算为一个值（从右到左）。</p></td>
<td>
<pre>
var total = [0, 1, 2, 3].reduceRight(function(a, b){ return a + b; }); 
console.log("total is : " + total );  // 6
</pre>
</td>
</tr>
<tr>
<td>13.</td>
<td>reverse()
<p>反转数组的元素顺序。</p></td>
<td>
<pre>
var arr = [0, 1, 2, 3].reverse(); 
console.log("Reversed array is : " + arr );  // 3,2,1,0
</pre>
</td>
</tr>
<tr>
<td>14.</td>
<td>shift()
<p>删除并返回数组的第一个元素。</p></td>
<td>
<pre>
var arr = [10, 1, 2, 3].shift(); 
console.log("Shifted value is : " + arr );  // 10
</pre>
</td>
</tr>
<tr>
<td>15.</td>
<td>slice()
<p>选取数组的的一部分，并返回一个新数组。</p></td>
<td>
<pre>
var arr = ["orange", "mango", "banana", "sugar", "tea"]; 
console.log("arr.slice( 1, 2) : " + arr.slice( 1, 2) );  // mango
console.log("arr.slice( 1, 3) : " + arr.slice( 1, 3) );  // mango,banana
</pre>
</td>
</tr>
<tr>
<td>16.</td>
<td>some()
<p>检测数组元素中是否有元素符合指定条件。</p></td>
<td>
<pre>
function isBigEnough(element, index, array) { 
   return (element &gt;= 10); 
          
} 
          
var retval = [2, 5, 8, 1, 4].some(isBigEnough);
console.log("Returned value is : " + retval );  // false
          
var retval = [12, 5, 8, 1, 4].some(isBigEnough); 
console.log("Returned value is : " + retval );  // true
</pre>
</td>
</tr>

<tr>
<td>17.</td>
<td>sort()
<p>对数组的元素进行排序。</p></td>
<td>
<pre>
var arr = new Array("orange", "mango", "banana", "sugar"); 
var sorted = arr.sort(); 
console.log("Returned string is : " + sorted );  // banana,mango,orange,sugar
</pre>
</td>
</tr>
<tr>
<td>18.</td>
<td>splice()
<p>从数组中添加或删除元素。</p></td>
<td>
<pre>
var arr = ["orange", "mango", "banana", "sugar", "tea"];  
var removed = arr.splice(2, 0, "water");  
console.log("After adding 1: " + arr );    // orange,mango,water,banana,sugar,tea 
console.log("removed is: " + removed); 
          
removed = arr.splice(3, 1);  
console.log("After removing 1: " + arr );  // orange,mango,water,sugar,tea 
console.log("removed is: " + removed);  // banana
</pre>
</td>
</tr>
<tr>
<td>19.</td>
<td>toString()
<p>把数组转换为字符串，并返回结果。</p></td>
<td>
<pre>
var arr = new Array("orange", "mango", "banana", "sugar");         
var str = arr.toString(); 
console.log("Returned string is : " + str );  // orange,mango,banana,sugar
</pre>
</td>
</tr>
<tr>
<td>20.</td>
<td>unshift()<p></p>
<p>向数组的开头添加一个或更多元素，并返回新的长度。</p></td>
<td>
<pre>
var arr = new Array("orange", "mango", "banana", "sugar"); 
var length = arr.unshift("water"); 
console.log("Returned array is : " + arr );  // water,orange,mango,banana,sugar 
console.log("Length of the array is : " + length ); // 5
</pre>
</td>
</tr>
</tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
<tbody><tr>
<th style="text-align:center">序号</th>
<th style="text-align:center">方法 &amp; 描述</th>
<th style="text-align:center">实例</th>
</tr>
<tr>
<td>1.</td>
<td>concat()
<p>连接两个或更多的数组，并返回结果。</p></td>
<td>
<pre>
var alpha = ["a", "b", "c"]; 
var numeric = [1, 2, 3];

var alphaNumeric = alpha.concat(numeric); 
console.log("alphaNumeric : " + alphaNumeric );    // a,b,c,1,2,3   
</pre>
</td>
</tr>
<tr>
<td>2.</td>
<td>every()
<p>检测数值元素的每个元素是否都符合条件。</p></td>
<td>
<pre>
function isBigEnough(element, index, array) { 
        return (element &gt;= 10); 
} 
        
var passed = [12, 5, 8, 130, 44].every(isBigEnough); 
console.log("Test Value : " + passed ); // false
</pre>
</td>
</tr>
<tr>
<td>3.</td>
<td>filter()
<p>检测数值元素，并返回符合条件所有元素的数组。</p></td>
<td>
<pre>
function isBigEnough(element, index, array) { 
   return (element &gt;= 10); 
} 
          
var passed = [12, 5, 8, 130, 44].filter(isBigEnough); 
console.log("Test Value : " + passed ); // 12,130,44
</pre>
</td>
</tr>
<tr>
<td>4.</td>
<td>forEach()
<p>数组每个元素都执行一次回调函数。</p></td>
<td>
<pre>
let num = [7, 8, 9];
num.forEach(function (value) {
    console.log(value);
}); 
</pre><p>
编译成 JavaScript 代码：</p><pre>
var num = [7, 8, 9];
num.forEach(function (value) {
    console.log(value);  // 7   8   9
});</pre>
</td>
</tr>
<tr>
<td>5.</td>
<td>indexOf()
<p>搜索数组中的元素，并返回它所在的位置。</p></td>
<td>
<pre>
var index = [12, 5, 8, 130, 44].indexOf(8); 
console.log("index is : " + index );  // 2
</pre>
</td>
</tr>
<tr>
<td>6.</td>
<td>join()
<p>把数组的所有元素放入一个字符串。</p></td>
<td>
<pre>
var arr = new Array("First","Second","Third"); 
          
var str = arr.join(); 
console.log("str : " + str );  // First,Second,Third 
          
var str = arr.join(", "); 
console.log("str : " + str );  // First, Second, Third
          
var str = arr.join(" + "); 
console.log("str : " + str );  // First + Second + Third
</pre>
</td>
</tr>
<tr>
<td>7.</td>
<td>lastIndexOf()
<p>返回一个指定的字符串值最后出现的位置，在一个字符串中的指定位置从后向前搜索。</p></td>
<td>
<pre>
var index = [12, 5, 8, 130, 44].lastIndexOf(8); 
console.log("index is : " + index );  // 2
</pre>
</td>
</tr>
<tr>
<td>8.</td>
<td>map()
<p>通过指定函数处理数组的每个元素，并返回处理后的数组。</p></td>
<td>
<pre>
var numbers = [1, 4, 9]; 
var roots = numbers.map(Math.sqrt); 
console.log("roots is : " + roots );  // 1,2,3
</pre>
</td>
</tr>
<tr>
<td>9.</td>
<td>pop()
<p>删除数组的最后一个元素并返回删除的元素。</p></td>
<td>
<pre>
var numbers = [1, 4, 9]; 
          
var element = numbers.pop(); 
console.log("element is : " + element );  // 9
          
var element = numbers.pop(); 
console.log("element is : " + element );  // 4
</pre>
</td>
</tr>
<tr>
<td>10.</td>
<td>push()
<p>向数组的末尾添加一个或更多元素，并返回新的长度。</p></td>
<td>
<pre>
var numbers = new Array(1, 4, 9); 
var length = numbers.push(10); 
console.log("new numbers is : " + numbers );  // 1,4,9,10 
length = numbers.push(20); 
console.log("new numbers is : " + numbers );  // 1,4,9,10,20
</pre>
</td>
</tr>
<tr>
<td>11.</td>
<td>reduce()
<p>将数组元素计算为一个值（从左到右）。</p></td>
<td>
<pre>
var total = [0, 1, 2, 3].reduce(function(a, b){ return a + b; }); 
console.log("total is : " + total );  // 6
</pre>
</td>
</tr>
<tr>
<td>12.</td>
<td>reduceRight()
<p>将数组元素计算为一个值（从右到左）。</p></td>
<td>
<pre>
var total = [0, 1, 2, 3].reduceRight(function(a, b){ return a + b; }); 
console.log("total is : " + total );  // 6
</pre>
</td>
</tr>
<tr>
<td>13.</td>
<td>reverse()
<p>反转数组的元素顺序。</p></td>
<td>
<pre>
var arr = [0, 1, 2, 3].reverse(); 
console.log("Reversed array is : " + arr );  // 3,2,1,0
</pre>
</td>
</tr>
<tr>
<td>14.</td>
<td>shift()
<p>删除并返回数组的第一个元素。</p></td>
<td>
<pre>
var arr = [10, 1, 2, 3].shift(); 
console.log("Shifted value is : " + arr );  // 10
</pre>
</td>
</tr>
<tr>
<td>15.</td>
<td>slice()
<p>选取数组的的一部分，并返回一个新数组。</p></td>
<td>
<pre>
var arr = ["orange", "mango", "banana", "sugar", "tea"]; 
console.log("arr.slice( 1, 2) : " + arr.slice( 1, 2) );  // mango
console.log("arr.slice( 1, 3) : " + arr.slice( 1, 3) );  // mango,banana
</pre>
</td>
</tr>
<tr>
<td>16.</td>
<td>some()
<p>检测数组元素中是否有元素符合指定条件。</p></td>
<td>
<pre>
function isBigEnough(element, index, array) { 
   return (element &gt;= 10); 
          
} 
          
var retval = [2, 5, 8, 1, 4].some(isBigEnough);
console.log("Returned value is : " + retval );  // false
          
var retval = [12, 5, 8, 1, 4].some(isBigEnough); 
console.log("Returned value is : " + retval );  // true
</pre>
</td>
</tr>

<tr>
<td>17.</td>
<td>sort()
<p>对数组的元素进行排序。</p></td>
<td>
<pre>
var arr = new Array("orange", "mango", "banana", "sugar"); 
var sorted = arr.sort(); 
console.log("Returned string is : " + sorted );  // banana,mango,orange,sugar
</pre>
</td>
</tr>
<tr>
<td>18.</td>
<td>splice()
<p>从数组中添加或删除元素。</p></td>
<td>
<pre>
var arr = ["orange", "mango", "banana", "sugar", "tea"];  
var removed = arr.splice(2, 0, "water");  
console.log("After adding 1: " + arr );    // orange,mango,water,banana,sugar,tea 
console.log("removed is: " + removed); 
          
removed = arr.splice(3, 1);  
console.log("After removing 1: " + arr );  // orange,mango,water,sugar,tea 
console.log("removed is: " + removed);  // banana
</pre>
</td>
</tr>
<tr>
<td>19.</td>
<td>toString()
<p>把数组转换为字符串，并返回结果。</p></td>
<td>
<pre>
var arr = new Array("orange", "mango", "banana", "sugar");         
var str = arr.toString(); 
console.log("Returned string is : " + str );  // orange,mango,banana,sugar
</pre>
</td>
</tr>
<tr>
<td>20.</td>
<td>unshift()<p></p>
<p>向数组的开头添加一个或更多元素，并返回新的长度。</p></td>
<td>
<pre>
var arr = new Array("orange", "mango", "banana", "sugar"); 
var length = arr.unshift("water"); 
console.log("Returned array is : " + arr );  // water,orange,mango,banana,sugar 
console.log("Length of the array is : " + length ); // 5
</pre>
</td>
</tr>
</tbody></table></div>') where ID=8701;
update wp_posts set post_content=replace(post_content,'<table class="reference ">
<tr><th style="width:10%">运算符</th><th style="width:55%;">描述</th><th>实例</th></tr>
<tr><td>&amp;</td><td><p>按位与操作，按二进制位进行"与"运算。运算规则：</p>
<pre>0&amp;0=0;   
0&amp;1=0;    
1&amp;0=0;     
1&amp;1=1;</pre></td><td> (A &amp; B) 将得到 12，即为 0000 1100</td></tr>
<tr><td>|</td><td><p>按位或运算符，按二进制位进行"或"运算。运算规则：</p>
<pre>0|0=0;   
0|1=1;   
1|0=1;    
1|1=1;</pre>
</td><td> (A | B) 将得到 61，即为 0011 1101</td></tr>
<tr><td>#</td><td><p>异或运算符，按二进制位进行"异或"运算。运算规则：</p>
<pre>0#0=0;   
0#1=1;   
1#0=1;  
1#1=0;</pre>

</td><td> (A # B) 将得到 49，即为 0011 0001</td></tr>
<tr><td>~</td><td><p>取反运算符，按二进制位进行"取反"运算。运算规则：</p>
<pre>~1=0;   
~0=1;</pre></td><td> (~A ) 将得到 -61，即为 1100 0011，一个有符号二进制数的补码形式。</td></tr>
<tr><td>&lt;&lt;</td><td>二进制左移运算符。将一个运算对象的各二进制位全部左移若干位（左边的二进制位丢弃，右边补0）。</td><td> A &lt;&lt; 2 将得到 240，即为 1111 0000</td></tr>
<tr><td>&gt;&gt;</td><td>二进制右移运算符。将一个数的各二进制位全部右移若干位，正数左补0，负数左补1，右边丢弃。</td><td> A &gt;&gt; 2 将得到 15，即为 0000 1111</td></tr>
</table>','<div class="w3-responsive"><table class="reference ">
<tr><th style="width:10%">运算符</th><th style="width:55%;">描述</th><th>实例</th></tr>
<tr><td>&amp;</td><td><p>按位与操作，按二进制位进行"与"运算。运算规则：</p>
<pre>0&amp;0=0;   
0&amp;1=0;    
1&amp;0=0;     
1&amp;1=1;</pre></td><td> (A &amp; B) 将得到 12，即为 0000 1100</td></tr>
<tr><td>|</td><td><p>按位或运算符，按二进制位进行"或"运算。运算规则：</p>
<pre>0|0=0;   
0|1=1;   
1|0=1;    
1|1=1;</pre>
</td><td> (A | B) 将得到 61，即为 0011 1101</td></tr>
<tr><td>#</td><td><p>异或运算符，按二进制位进行"异或"运算。运算规则：</p>
<pre>0#0=0;   
0#1=1;   
1#0=1;  
1#1=0;</pre>

</td><td> (A # B) 将得到 49，即为 0011 0001</td></tr>
<tr><td>~</td><td><p>取反运算符，按二进制位进行"取反"运算。运算规则：</p>
<pre>~1=0;   
~0=1;</pre></td><td> (~A ) 将得到 -61，即为 1100 0011，一个有符号二进制数的补码形式。</td></tr>
<tr><td>&lt;&lt;</td><td>二进制左移运算符。将一个运算对象的各二进制位全部左移若干位（左边的二进制位丢弃，右边补0）。</td><td> A &lt;&lt; 2 将得到 240，即为 1111 0000</td></tr>
<tr><td>&gt;&gt;</td><td>二进制右移运算符。将一个数的各二进制位全部右移若干位，正数左补0，负数左补1，右边丢弃。</td><td> A &gt;&gt; 2 将得到 15，即为 0000 1111</td></tr>
</table></div>') where ID=8734;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
  <tbody><tr>
    <th>函数</th>
    <th>描述 &amp; 实例</th>
  </tr>
  <tr>
    <td>quote(<em>string</em>)</td>
    <td>给字符串添加引号。<br><br>
    <strong>实例:</strong><br>quote(runoob)<br>结果: "runoob"</td>
  </tr>
  <tr>
    <td>str-index(<em>string</em>,<em> substring</em>)</td>
    <td>返回 substring 子字符串第一次在 string 中出现的位置。如果没有匹配到子字符串，则返回 null。<br><br>
<pre>
str-index(abcd, a)  =&gt; 1
str-index(abcd, ab) =&gt; 1
str-index(abcd, X)  =&gt; null
str-index(abcd, c)  =&gt; 3
</pre>
  </tr>
  <tr>
    <td>str-insert(<em>string</em>, <em>insert</em>, <em>index</em>)</td>
    <td>在字符串 string 中  index 位置插入 insert。<br><br>
    <strong>实例:</strong><br>str-insert("Hello world!", " runoob", 6)<br>结果: "Hello 
    runoob world!"</td>
  </tr>
  <tr>
    <td>str-length(<em>string</em>)</td>
    <td>返回字符串的长度。<br><br>
    <strong>实例:</strong><br>str-length("runoob")<br>结果: 6</td>
  </tr>
  <tr>
    <td>str-slice(<em>string</em>, <em>start</em>, <em>end</em>)</td>
    <td>从 string 中截取子字符串，通过  start-at 和 end-at 设置始末位置，未指定结束索引值则默认截取到字符串末尾。<br><br>
<pre>
str-slice("abcd", 2, 3)   =&gt; "bc"
str-slice("abcd", 2)      =&gt; "bcd"
str-slice("abcd", -3, -2) =&gt; "bc"
str-slice("abcd", 2, -2)  =&gt; "bc"
</pre>
</td>
  </tr>
  <tr>
    <td>to-lower-case(<em>string</em>)</td>
    <td>将字符串转成小写<br><br>
    <strong>实例:</strong><br>to-lower-case("RUNOOB")<br>结果: "runoob"</td>
  </tr>
  <tr>
    <td>to-upper-case(<em>string</em>)</td>
    <td>将字符串转成大写<br><br>
    <strong>实例:</strong><br>to-upper-case("runoob")<br>结果: "RUNOOB"</td>
  </tr>
  <tr>
    <td>unique-id()</td>
    <td>返回一个无引号的随机字符串作为 id。不过也只能保证在单次的 Sass 编译中确保这个 id 的唯一性。<br><br>
    <strong>实例:</strong><br>unique-id()<br>Result: 
    uad053b1c</td>
  </tr>
  <tr>
    <td>unquote(<em>string</em>)</td>
    <td>移除字符串的引号<br>
    <br>
    <strong>实例:</strong><br>unquote("runoob")<br>结果: runoob</td>
  </tr>
  </tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
  <tbody><tr>
    <th>函数</th>
    <th>描述 &amp; 实例</th>
  </tr>
  <tr>
    <td>quote(<em>string</em>)</td>
    <td>给字符串添加引号。<br><br>
    <strong>实例:</strong><br>quote(runoob)<br>结果: "runoob"</td>
  </tr>
  <tr>
    <td>str-index(<em>string</em>,<em> substring</em>)</td>
    <td>返回 substring 子字符串第一次在 string 中出现的位置。如果没有匹配到子字符串，则返回 null。<br><br>
<pre>
str-index(abcd, a)  =&gt; 1
str-index(abcd, ab) =&gt; 1
str-index(abcd, X)  =&gt; null
str-index(abcd, c)  =&gt; 3
</pre>
  </tr>
  <tr>
    <td>str-insert(<em>string</em>, <em>insert</em>, <em>index</em>)</td>
    <td>在字符串 string 中  index 位置插入 insert。<br><br>
    <strong>实例:</strong><br>str-insert("Hello world!", " runoob", 6)<br>结果: "Hello 
    runoob world!"</td>
  </tr>
  <tr>
    <td>str-length(<em>string</em>)</td>
    <td>返回字符串的长度。<br><br>
    <strong>实例:</strong><br>str-length("runoob")<br>结果: 6</td>
  </tr>
  <tr>
    <td>str-slice(<em>string</em>, <em>start</em>, <em>end</em>)</td>
    <td>从 string 中截取子字符串，通过  start-at 和 end-at 设置始末位置，未指定结束索引值则默认截取到字符串末尾。<br><br>
<pre>
str-slice("abcd", 2, 3)   =&gt; "bc"
str-slice("abcd", 2)      =&gt; "bcd"
str-slice("abcd", -3, -2) =&gt; "bc"
str-slice("abcd", 2, -2)  =&gt; "bc"
</pre>
</td>
  </tr>
  <tr>
    <td>to-lower-case(<em>string</em>)</td>
    <td>将字符串转成小写<br><br>
    <strong>实例:</strong><br>to-lower-case("RUNOOB")<br>结果: "runoob"</td>
  </tr>
  <tr>
    <td>to-upper-case(<em>string</em>)</td>
    <td>将字符串转成大写<br><br>
    <strong>实例:</strong><br>to-upper-case("runoob")<br>结果: "RUNOOB"</td>
  </tr>
  <tr>
    <td>unique-id()</td>
    <td>返回一个无引号的随机字符串作为 id。不过也只能保证在单次的 Sass 编译中确保这个 id 的唯一性。<br><br>
    <strong>实例:</strong><br>unique-id()<br>Result: 
    uad053b1c</td>
  </tr>
  <tr>
    <td>unquote(<em>string</em>)</td>
    <td>移除字符串的引号<br>
    <br>
    <strong>实例:</strong><br>unquote("runoob")<br>结果: runoob</td>
  </tr>
  </tbody></table></div>') where ID=8772;
update wp_posts set post_content=replace(post_content,'<table class="w3-table-all bsReference">
  <tbody><tr>
    <th>函数</th>
    <th>描述 &amp; 实例</th>
  </tr>
  <tr>
    <td>mix(<em>color1</em>, <em>color2</em>, <em>weight</em>)</td>
    <td>把两种颜色混合起来。
    <em>weight</em> 参数必须是 0% 到 100%。默认 weight 为 50%，表明新颜色各取 50% color1 和 color2 的色值相加。如果 weight 为 25%，那表明新颜色为 25% color1 和 75% color2 的色值相加。</td>
  </tr>
  <tr>
    <td>adjust-hue(<em>color</em>, <em>degrees</em>)</td>
    <td>通过改变一个颜色的色相值（-360deg - 360deg），创建一个新的颜色。<br><br>
    <strong>实例:</strong><br>adjust-hue(#7fffd4, 80deg);<br>结果: #8080ff</td>
  </tr>
  <tr>
    <td>adjust-color(<em>color</em>, <em>red</em>, <em>green</em>, <em>blue</em>,
    <em>hue</em>, <em>saturation</em>, <em>lightness</em>, <em>alpha</em>)</td>
    <td>这个函数能够调整给定色彩的一个或多个属性值，包括 RGB 和 HSL 色彩的各项色值参数，另外还有 alpha 通道的取值。这些属性值的调整依赖传入的关键值参数，通过这些参数再与给定颜色相应的色彩值做加减运算。<br>
    <br>
    <strong>实例:</strong><br>adjust-color(#7fffd4, blue: 25);<br>结果:</td>
  </tr>
  <tr>
    <td>change-color(<em>color</em>, <em>red</em>, <em>green</em>, <em>blue</em>,
    <em>hue</em>, <em>saturation</em>, <em>lightness</em>, <em>alpha</em>)</td>
    <td>跟上面 adjust-color 类似，只是在该函数中传入的参数将直接替换原来的值，而不做任何的运算。<br><br>
    <strong>实例:</strong><br>change-color(#7fffd4, red: 255);<br>结果: 
    #ffffd4</td>
  </tr>
  <tr>
    <td>scale-color(<em>color</em>, <em>red</em>, <em>green</em>, <em>blue</em>,&nbsp;
    <em>saturation</em>, <em>lightness</em>, <em>alpha</em>)</td>
    <td>另一种实用的颜色调节函数。adjust-color 通过传入的参数简单的与本身的色值参数做加减，有时候可能会导致累加值溢出，当然，函数会把结果控制在有效的阈值内。而 scale-color 函数则避免了这种情况，可以不必担心溢出，让参数在阈值范围内进行有效的调节。<br><br>

举个例子，一个颜色的亮度 lightness 取值在 0% ~ 100% 之间，假如执行 scale-color($color, $lightness: 40%)，表明该颜色的亮度将有 (100 - 原始值) × 40% 的增幅。<br><br>

另一个例子，执行 scale-color($color, $lightness: -40%)，表明这个颜色的亮度将减少 (原始值 - 0) × 40% 这么多的值。<br><br>

所有传参的取值范围都在 0% ~ 100% 之间，并且 RGB 同 HSL 的传参不能冲突。<br><br>

<pre>
scale-color(hsl(120, 70%, 80%), $lightness: 50%) =&gt; hsl(120, 70%, 90%)
scale-color(rgb(200, 150, 170), $green: -40%, $blue: 70%) =&gt; rgb(200, 90, 229)
scale-color(hsl(200, 70%, 80%), $saturation: -90%, $alpha: -30%) =&gt; hsla(200, 7%, 80%, 0.7)
</pre>
</td>
  </tr>
  <tr>
    <td>rgba(<em>color</em>, <em>alpha</em>)</td>
    <td>根据红、绿、蓝和透明度值创建一个颜色。<br><br><strong>实例:</strong><br>rgba(#7fffd4, 30%);<br>结果: 
    rgba(127, 255, 212, 0.3)</td>
  </tr>
  <tr>
    <td>lighten(<em>color</em>, <em>amount</em>)</td>
    <td>通过改变颜色的亮度值（0% - 100%），让颜色变亮，创建一个新的颜色。</td>
  </tr>
  <tr>
    <td>darken(<em>color</em>, <em>amount</em>)</td>
    <td>通过改变颜色的亮度值（0% - 100%），让颜色变暗，创建一个新的颜色。</td>
  </tr>
  <tr>
    <td>saturate(<em>color</em>, <em>amount</em>)</td>
    <td>提高传入颜色的色彩饱和度。等同于 adjust-color( color, saturation: amount)</td>
  </tr>
  <tr>
    <td>desaturate(<em>color</em>, <em>amount</em>)</td>
    <td>调低一个颜色的饱和度后产生一个新的色值。同样，饱和度的取值区间在 0% ~ 100%。等同于 adjust-color(color, saturation: -amount)</td>
  </tr>
  <tr>
    <td>opacify(<em>color</em>, <em>amount</em>)</td>
    <td>降低颜色的透明度，取值在 0-1 之。等价于 adjust-color(color, alpha: amount)</td>
  </tr>
  <tr>
    <td>fade-in(<em>color</em>, <em>amount</em>)</td>
    <td>降低颜色的透明度，取值在 0-1 之。等价于 adjust-color(color, alpha: amount)</td>
  </tr>
  <tr>
    <td>transparentize(<em>color</em>, <em>amount</em>)</td>
    <td>提升颜色的透明度，取值在 0-1 之间。等价于 adjust-color(color, alpha: -amount)</td>
  </tr>
  <tr>
    <td>fade-out(<em>color</em>, <em>amount</em>)</td>
    <td>提升颜色的透明度，取值在 0-1 之间。等价于 adjust-color(color, alpha: -amount)</td>
  </tr>
  </tbody></table>','<div class="w3-responsive"><table class="w3-table-all bsReference">
  <tbody><tr>
    <th>函数</th>
    <th>描述 &amp; 实例</th>
  </tr>
  <tr>
    <td>mix(<em>color1</em>, <em>color2</em>, <em>weight</em>)</td>
    <td>把两种颜色混合起来。
    <em>weight</em> 参数必须是 0% 到 100%。默认 weight 为 50%，表明新颜色各取 50% color1 和 color2 的色值相加。如果 weight 为 25%，那表明新颜色为 25% color1 和 75% color2 的色值相加。</td>
  </tr>
  <tr>
    <td>adjust-hue(<em>color</em>, <em>degrees</em>)</td>
    <td>通过改变一个颜色的色相值（-360deg - 360deg），创建一个新的颜色。<br><br>
    <strong>实例:</strong><br>adjust-hue(#7fffd4, 80deg);<br>结果: #8080ff</td>
  </tr>
  <tr>
    <td>adjust-color(<em>color</em>, <em>red</em>, <em>green</em>, <em>blue</em>,
    <em>hue</em>, <em>saturation</em>, <em>lightness</em>, <em>alpha</em>)</td>
    <td>这个函数能够调整给定色彩的一个或多个属性值，包括 RGB 和 HSL 色彩的各项色值参数，另外还有 alpha 通道的取值。这些属性值的调整依赖传入的关键值参数，通过这些参数再与给定颜色相应的色彩值做加减运算。<br>
    <br>
    <strong>实例:</strong><br>adjust-color(#7fffd4, blue: 25);<br>结果:</td>
  </tr>
  <tr>
    <td>change-color(<em>color</em>, <em>red</em>, <em>green</em>, <em>blue</em>,
    <em>hue</em>, <em>saturation</em>, <em>lightness</em>, <em>alpha</em>)</td>
    <td>跟上面 adjust-color 类似，只是在该函数中传入的参数将直接替换原来的值，而不做任何的运算。<br><br>
    <strong>实例:</strong><br>change-color(#7fffd4, red: 255);<br>结果: 
    #ffffd4</td>
  </tr>
  <tr>
    <td>scale-color(<em>color</em>, <em>red</em>, <em>green</em>, <em>blue</em>,&nbsp;
    <em>saturation</em>, <em>lightness</em>, <em>alpha</em>)</td>
    <td>另一种实用的颜色调节函数。adjust-color 通过传入的参数简单的与本身的色值参数做加减，有时候可能会导致累加值溢出，当然，函数会把结果控制在有效的阈值内。而 scale-color 函数则避免了这种情况，可以不必担心溢出，让参数在阈值范围内进行有效的调节。<br><br>

举个例子，一个颜色的亮度 lightness 取值在 0% ~ 100% 之间，假如执行 scale-color($color, $lightness: 40%)，表明该颜色的亮度将有 (100 - 原始值) × 40% 的增幅。<br><br>

另一个例子，执行 scale-color($color, $lightness: -40%)，表明这个颜色的亮度将减少 (原始值 - 0) × 40% 这么多的值。<br><br>

所有传参的取值范围都在 0% ~ 100% 之间，并且 RGB 同 HSL 的传参不能冲突。<br><br>

<pre>
scale-color(hsl(120, 70%, 80%), $lightness: 50%) =&gt; hsl(120, 70%, 90%)
scale-color(rgb(200, 150, 170), $green: -40%, $blue: 70%) =&gt; rgb(200, 90, 229)
scale-color(hsl(200, 70%, 80%), $saturation: -90%, $alpha: -30%) =&gt; hsla(200, 7%, 80%, 0.7)
</pre>
</td>
  </tr>
  <tr>
    <td>rgba(<em>color</em>, <em>alpha</em>)</td>
    <td>根据红、绿、蓝和透明度值创建一个颜色。<br><br><strong>实例:</strong><br>rgba(#7fffd4, 30%);<br>结果: 
    rgba(127, 255, 212, 0.3)</td>
  </tr>
  <tr>
    <td>lighten(<em>color</em>, <em>amount</em>)</td>
    <td>通过改变颜色的亮度值（0% - 100%），让颜色变亮，创建一个新的颜色。</td>
  </tr>
  <tr>
    <td>darken(<em>color</em>, <em>amount</em>)</td>
    <td>通过改变颜色的亮度值（0% - 100%），让颜色变暗，创建一个新的颜色。</td>
  </tr>
  <tr>
    <td>saturate(<em>color</em>, <em>amount</em>)</td>
    <td>提高传入颜色的色彩饱和度。等同于 adjust-color( color, saturation: amount)</td>
  </tr>
  <tr>
    <td>desaturate(<em>color</em>, <em>amount</em>)</td>
    <td>调低一个颜色的饱和度后产生一个新的色值。同样，饱和度的取值区间在 0% ~ 100%。等同于 adjust-color(color, saturation: -amount)</td>
  </tr>
  <tr>
    <td>opacify(<em>color</em>, <em>amount</em>)</td>
    <td>降低颜色的透明度，取值在 0-1 之。等价于 adjust-color(color, alpha: amount)</td>
  </tr>
  <tr>
    <td>fade-in(<em>color</em>, <em>amount</em>)</td>
    <td>降低颜色的透明度，取值在 0-1 之。等价于 adjust-color(color, alpha: amount)</td>
  </tr>
  <tr>
    <td>transparentize(<em>color</em>, <em>amount</em>)</td>
    <td>提升颜色的透明度，取值在 0-1 之间。等价于 adjust-color(color, alpha: -amount)</td>
  </tr>
  <tr>
    <td>fade-out(<em>color</em>, <em>amount</em>)</td>
    <td>提升颜色的透明度，取值在 0-1 之间。等价于 adjust-color(color, alpha: -amount)</td>
  </tr>
  </tbody></table></div>') where ID=8778;
