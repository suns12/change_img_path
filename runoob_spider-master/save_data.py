#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import pymysql.cursors
import json
from pathlib import Path

class Save_data(object):
    def __init__(self):
        my_file = Path("db_info.json")
        if my_file.is_file():
            with open('db_info.json', 'r') as f:
                db_info = json.load(f)
                f.close()
        else:
            # Reading data back
            with open('db_info.json', 'w') as f:
                # Writing JSON data
                db_info = {
                    'host': 'host',
                    'user': 'user',
                    'passwd': 'password',
                    'db': 'db',
                    'charset': 'utf8',
                }
                json.dump(db_info, f)
                f.flush()
                f.close()
        params = dict(
            host=db_info['host'],
            db=db_info['db'],
            user=db_info['user'],
            passwd=db_info['passwd'],
            charset=db_info['charset'],
            cursorclass=pymysql.cursors.DictCursor,
        )
        self.connection = pymysql.connect(**params)

    def save_categories(self, data):
        with self.connection.cursor() as cursor:
            sql = "REPLACE INTO `tbl_categories` (`id`,`data`,`create_time`) VALUES (%s, %s, %s)"
            cursor.executemany(sql, data)
            self.connection.commit()

    def save_posts(self, data):
        with self.connection.cursor() as cursor:
            sql = "REPLACE INTO `tbl_posts` (`id`,`categories`,`data`,`create_time`) VALUES (%s, %s, %s, %s)"
            cursor.executemany(sql, data)
            self.connection.commit()

    def set_categories(self):
        with self.connection.cursor() as cursor:
            sql = "select * from tbl_posts;"
            cursor.execute(sql)
            items = cursor.fetchall()
            data = []
            for item in items:
                a = json.loads(item['data'])
                b = (a['categories'][0], a['id'])
                data.append(b)
            sql = "update `tbl_posts` set `categories` = %s where id = %s"
            cursor.executemany(sql, data)
            self.connection.commit()

    def save_pages(self, data):
        with self.connection.cursor() as cursor:
            sql = "REPLACE INTO `tbl_pages` (`id`,`data`,`create_time`) VALUES (%s, %s, %s)"
            cursor.executemany(sql, data)
            self.connection.commit()

    def save_media(self, data):
        with self.connection.cursor() as cursor:
            sql = "REPLACE INTO `tbl_media` (`id`,`data`,`create_time`) VALUES (%s, %s, %s)"
            cursor.executemany(sql, data)
            self.connection.commit()

    def save_tags(self, data):
        with self.connection.cursor() as cursor:
            sql = "REPLACE INTO `tbl_tags` (`id`,`data`,`create_time`) VALUES (%s, %s, %s)"
            cursor.executemany(sql, data)
            self.connection.commit()

    def get_datas(self, page):
        with self.connection.cursor() as cursor:
            page = (page - 1) * 100
            sql = "SELECT * FROM `tbl_posts` WHERE data like '%img%' LIMIT "+str(page)+",100;"
            cursor.execute(sql)
            return cursor.fetchall()

    def set_status(self, ids):
        with self.connection.cursor() as cursor:
            sql = "update `tbl_posts` set status=1 where id in ("+ids+");"
            cursor.execute(sql)
            self.connection.commit()

    def __del__(self):
        self.connection.close()







