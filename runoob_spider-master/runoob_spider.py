#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from __future__ import unicode_literals
from save_data import Save_data
import requests
import json
import time
import urllib
from lxml import etree
from lxml.etree import tostring
import os

class Runoob_spider:
    'runoob spider'

    def get_categories(self):
        database = Save_data()
        i = 1
        while i > 0:
            r = requests.get('https://www.runoob.com/wp-json/wp/v2/categories?per_page=100&page=' + str(i))
            items = json.loads(r.text)
            lis = []
            for item in items:
                create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                post = (item['id'], json.dumps(item, ensure_ascii=False), create_time)
                lis.append(post)
            database.save_categories(lis)
            i += 1
            if len(items) < 100:
                break

    def get_posts(self):
        database = Save_data()
        i = 87
        while i > 0:
            r = requests.get('https://www.runoob.com/wp-json/wp/v2/posts?per_page=100&page=' + str(i))
            items = r.json()
            # print(r.json())
            lis = []
            for item in items:
                create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                post = (item['id'], item['categories'][0], json.dumps(item, ensure_ascii=False), create_time)
                lis.append(post)
            database.save_posts(lis)
            print(i)
            i += 1
            if len(items) < 100:
                break

    def get_pages(self):
        database = Save_data()
        i = 1
        while i > 0:
            r = requests.get('https://www.runoob.com/wp-json/wp/v2/pages?per_page=100&page=' + str(i))
            items = json.loads(r.text)
            lis = []
            for item in items:
                create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                post = (item['id'], json.dumps(item, ensure_ascii=False), create_time)
                lis.append(post)
            database.save_pages(lis)
            i += 1
            if len(items) < 100:
                break

    def get_media(self):
        database = Save_data()
        i = 1
        while i > 0:
            r = requests.get('https://www.runoob.com/wp-json/wp/v2/media?per_page=100&page=' + str(i))
            items = json.loads(r.text)
            lis = []
            for item in items:
                create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                post = (item['id'], json.dumps(item, ensure_ascii=False), create_time)
                lis.append(post)
            database.save_media(lis)
            i += 1
            if len(items) < 100:
                break

    def get_tags(self):
        database = Save_data()
        i = 1
        while i > 0:
            r = requests.get('https://www.runoob.com/wp-json/wp/v2/tags?per_page=100&page=' + str(i))
            items = json.loads(r.text)
            lis = []
            for item in items:
                create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                post = (item['id'], json.dumps(item, ensure_ascii=False), create_time)
                lis.append(post)
            database.save_tags(lis)
            i += 1
            if len(items) < 100:
                break

    def set_categories(self):
        database = Save_data()
        database.set_categories()

    def download_images(self):
        database = Save_data()
        page = 1
        data_list = database.get_datas(page)
        counter = 0
        while len(data_list) > 0:
            id_str = ''
            for item in data_list:
                res_dic = json.loads(item['data'])
                html_str = res_dic['content']['rendered']
                images_links = etree.HTML(html_str).xpath('//img/@src')
                for images_link in images_links:
                    # print images_link
                    if images_link.find('www.runoob.com') == -1 and images_link.find('//', 0, 2) == -1 and images_link.find('http', 0, 4) == -1:
                        images_link = 'https://www.runoob.com' + images_link
                    if images_link.find('//') == 0:
                        images_link = 'http:' + images_link
                    file_path = "./file" + urllib.parse.urlparse(images_link).path
                    file_dir = os.path.dirname(file_path)
                    if os.path.isfile(file_path) is False:
                        print(images_link)
                        os.system('wget -P ' + file_dir + ' ' + images_link)
                        counter += 1
                id_str = str(item['id']) + ','
            id_str = id_str[:-1]
            print(id_str)
            page = page + 1
            data_list = database.get_datas(page)



x = Runoob_spider()
# x.get_categories()
x.get_posts()
# x.get_pages()
# x.get_media()
# x.get_tags()
# x.download_images()

# x.set_categories()













